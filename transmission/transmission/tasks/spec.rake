# frozen_string_literal: true

namespace :spec do
  require 'rspec/core/rake_task'

  desc 'Run unit test specs'
  RSpec::Core::RakeTask.new(:all) do |t|
    t.rspec_opts = '-c --format documentation '\
      '--format html '\
      '--out reports/spec/index.html'
  end
end

desc 'Default task for namespace spec'
task spec: 'spec:all'
