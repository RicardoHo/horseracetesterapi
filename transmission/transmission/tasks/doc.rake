# frozen_string_literal: true

namespace :doc do
  require 'yard'

  desc 'Generate RDoc by Yard (include private method)'
  YARD::Rake::YardocTask.new(:yard) do |task|
    task.options = ['--private', '--output-dir', 'reports/doc']
  end
end

desc 'Default task for namespace doc'
task doc: 'doc:yard'
