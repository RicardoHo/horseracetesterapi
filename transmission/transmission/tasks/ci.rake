# frozen_string_literal: true

namespace :ci do
  require 'rspec/core/rake_task'

  desc "Run all spec and doc task for CI"
  task :all do
    Rake::Task['doc:yard'].invoke
    Rake::Task['spec:all'].invoke
  end
end

desc 'Default task for namespace ci'
task ci: 'ci:all'
