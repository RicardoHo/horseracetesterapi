# frozen_string_literal: true

ENV['TRANSMISSION_ENV'] ||= 'development'

# gem_root = File.join(File.expand_path('../..', dir), 'test_gem', 'gems')
gem_root = File.join(File.expand_path('../..', dir), 'gems')


$LOAD_PATH.unshift(
  File.join(File.expand_path('..', dir), 'lib'),
  File.join(File.expand_path('..', dir), 'development', 'lib'),
  File.join(gem_root, 'stdlib'),
  *Dir.glob("#{gem_root}/*/lib"),
)

require "rbconfig"
require 'rubygems'
require 'representable/hash'
require 'representable/json'

require 'transmission'
require 'transmission_dev'
require 'transmission_dev/util'
require 'transmission_dev/support/game_engine_instance_struct'
require 'transmission_dev/support/game_engine_wrapper'

class Runner
  attr_reader :str

  def initialize
    @game_id = 913142505
    @driver = Transmission::Driver::GameboxDriver.new(@game_id)
    @driver.update(query_game_msg.to_json)
  end

  def play(data, seed)
    srand(seed.to_i(16))
    @driver.update(new_table.to_json) # reset table
    @str = @driver.update(data)
  end
   
  private

  def query_game_msg
    {
      action: 'query_game',
      data_type: 'query_game',
      data: {
        player_token: 'fake_token',
        game_id: @game_id,
        property_id: 10,
        login_name: 'math_validater',
      },
    }
  end

  def bet_msg
    {
      action: 'multipleBet',
      data_type: 'multipleBet',
      data: {
        denom: 1,
        play_mode: "forecast",
        bet_options: [
          {
            name: "win_0",
            betAmount: {
              credit: 100,
            },
          },
        ],
      },
    }
  end

  def new_table
    {
      action: 'new_game',
      data_type: 'new_game',
      data: {
        # player_token: 'fake_token',
        # game_id: @game_id,
        # property_id: 10,
        # login_name: 'math_validater',
      },
    }
  end
end

# p Runner.new.play(nil, 'FF')
# p Runner.new.play(nil, 'FF')
