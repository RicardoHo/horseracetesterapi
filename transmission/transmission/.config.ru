# frozen_string_literal: true

$LOAD_PATH.unshift(
  File.join(__dir__, 'lib'),
  File.join(__dir__, 'development', 'lib'),
)

require 'transmission'
require 'transmission_dev/inspection'

if ENV['TRANSMISSION_ENV'] == Transmission::Enum::Env::DEVELOPMENT
  Transmission::Inspection::Inspecter.inspect_classes.each do |klass|
    Transmission::System.const_get(klass.class_name.to_sym).prepend(klass)
  end
end

Transmission::Service.run!(
  port: Transmission.config[:port],
  host: Transmission.config[:host]
)
