# frozen_string_literal: true

module Transmission
  module Helper
    #
    # @author Alpha Huang
    # @since 0.4.0
    #
    # Provide poker hand determination helper method
    #
    module PokerHandHelper
      extend self

      #
      # get all high card face of all group
      #
      # @return [Array<Integer>]
      #
      def high_card_faces
        grouped_card_faces.keys
      end

      #
      # group the card faces, face as key and amount of face as value
      #
      # @return [Hash<Integer, Integer>]
      #
      def grouped_card_faces
        @card_faces
          .group_by(&:itself)
          .map { |k, v| [k, v.size] }
          .sort_by { |v| [-v[1], -v[0]] }
          .to_h
      end

      #
      # determine is fulfill constraint for a rank
      #
      # @param [rank] rank
      #
      # @return [Boolean]
      #
      def fulfill_constraint?(rank)
        constraint = @paytable.dig(rank, :constraint)
        return true unless constraint

        constraint.all? do |type, condition|
          case type
          when :highest_card
            high_card_faces[0] >= condition.dig(:face)
          end
        end
      end

      #
      # determine is the cards has numbers of any kind of card
      #
      # @param [Integer] n the desire number
      #
      # @return [Boolean]
      #
      def n_of_a_kind?(n)
        @card_faces
          .group_by(&:itself)
          .map { |_k, v| v.size }
          .include?(n)
      end

      #
      # determine is the cards has numbers of a given face
      #
      # @param [Integer] n the desire number
      # @param [Integer] face the desire face value of poker card
      #
      # @return [Boolean]
      #
      def n_of_a_face?(n, face)
        @card_faces.count(face) == n
      end

      #
      # determine is the cards has numbers of pair(s)
      #
      # @param [Integer] n the desire number
      #
      # @return [Boolean]
      #
      def n_pair?(n)
        @card_faces
          .group_by { |face| face }
          .count { |_k, v| v.size == 2 } == n
      end

      #
      # determine is cards has specificed card in face
      #
      # @param [Integer] face the desire face value of poker card
      #
      # @return [Boolean]
      #
      def has_face?(face)
        @card_faces.include?(face)
      end

      #
      # determine is the cards can rank as royal straight flush
      #
      # @return [Boolean]
      #
      def royal_straight_flush?
        (Util::PokerHandUtil::ROYAL_FACES - @card_faces).empty? && flush?
      end

      #
      # determine is the cards can rank as straight flush
      #
      # @return [Boolean]
      #
      def straight_flush?
        straight? && flush?
      end

      #
      # determine is the cards can rank as straight
      #
      # @return [Boolean]
      #
      def straight?
        if @card_faces.include?(Util::PokerHandUtil::HIGH_ACE_VALUE)
          @card_faces + [Util::PokerHandUtil::LOW_ACE_VALUE]
        else
          @card_faces
        end.each_cons(@card_faces.size).any? do |cards|
          cards.each_cons(2).all? { |x, y| x - 1 == y }
        end
      end

      #
      # determine is the cards can rank as flush
      #
      # @return [Boolean]
      #
      def flush?
        @card_suits.uniq.size == 1
      end

      #
      # determine is the cards can rank as full house
      #
      # @return [Boolean]
      #
      def full_house?
        n_of_a_kind?(3) && n_pair?(1)
      end

      #
      # determine is the cards can rank as quadruple 8
      #
      # @return [Boolean]
      #
      def quadruple_8?
        n_of_a_face?(4, 8)
      end

      #
      # determine is the cards can rank as quadruple 6
      #
      # @return [Boolean]
      #
      def quadruple_6?
        n_of_a_face?(4, 6)
      end

      #
      # determine is the cards can rank as four of a kind
      #
      # @return [Boolean]
      #
      def four_of_a_kind?
        n_of_a_kind?(4)
      end

      #
      # determine is the cards can rank as 6688
      #
      # @return [Boolean]
      #
      def pair_of_6_with_pair_of_8?
        n_of_a_face?(2, 6) && n_of_a_face?(2, 8)
      end

      #
      # determine is the cards can rank as triple 8
      #
      # @return [Boolean]
      #
      def triple_8?
        n_of_a_face?(3, 8)
      end

      #
      # determine is the cards can rank as triple 6
      #
      # @return [Boolean]
      #
      def triple_6?
        n_of_a_face?(3, 6)
      end

      #
      # determine is the cards can rank as three of a kind
      #
      # @return [Boolean]
      #
      def three_of_a_kind?
        n_of_a_kind?(3)
      end

      #
      # determine is the cards can rank as two pairs with pair of 8
      #
      # @return [Boolean]
      #
      def two_pair_with_pair_of_8?
        n_pair?(2) && n_of_a_face?(2, 8)
      end

      #
      # determine is the cards can rank as two pairs with pair of 6
      #
      # @return [Boolean]
      #
      def two_pair_with_pair_of_6?
        n_pair?(2) && n_of_a_face?(2, 6)
      end

      #
      # determine is the cards can rank as pair of 8
      #
      # @return [Boolean]
      #
      def pair_of_8?
        n_of_a_face?(2, 8)
      end

      #
      # determine is the cards can rank as pair of 6
      #
      # @return [Boolean]
      #
      def pair_of_6?
        n_of_a_face?(2, 6)
      end

      #
      # determine is the cards can rank as two pairs
      #
      # @return [Boolean]
      #
      def two_pair?
        n_pair?(2)
      end

      #
      # determine is the cards can rank as one pair
      #
      # @return [Boolean]
      #
      def one_pair?
        n_pair?(1)
      end
    end
  end
end
