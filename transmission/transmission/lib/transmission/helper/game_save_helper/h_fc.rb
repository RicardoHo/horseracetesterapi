# frozen_string_literal: true

module Transmission
  module GameSaveHelper
    #
    # @author Alpha Huang
    # @since 0.7.0
    #
    # game save helper for code name H-FC
    #
    module HFc
      extend self

      def instance_data
        base_instance_data
      end

      def action_data
        Marshal.load(Marshal.dump(@result))
      end

      def game_context_data
        nil
      end
    end
  end
end
