# frozen_string_literal: true

module Transmission
  module GameSaveHelper
    #
    # @author ben.b.wu
    # @since 0.1.0
    #
    # game save helper for code name H-HR
    #
    module HHr
      extend self

      def instance_data
        base_instance_data
      end

      def action_data
        # Marshal.load(Marshal.dump(@result))

        Marshal.load(Marshal.dump(@result)).tap do |table|
          table.delete(:game_context)
          table.update(@game_engine.rng_capture)
        end
      end

      def game_context_data
        @result.dig(:game_context)
      end

      def add_playtable
        @paytable ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
          .dig(:paytable)

        @result.dig(:table).merge!(paytable: @paytable)
      end
    end
  end
end
