# frozen_string_literal: true

module Transmission
  module GameSaveHelper
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # game save helper for code name H-EB
    #
    module HEb
      extend self

      def instance_data
        base_instance_data.merge!(
          @result.dig(:table).slice(:shoe, :shoe_score_board, :skip_hand_indicator)
        )
      end

      def action_data
        action_data = Marshal.load(Marshal.dump(@result))
        action_data[:table].tap do |table|
          table.delete(:skip_hand_indicator)
          table[:shoe].keep_if { |k, _| k == :id }
          table[:shoe_score_board].keep_if { |k, _| k == :shoe_id }
        end

        action_data
      end

      def game_context_data
        nil
      end
    end
  end
end
