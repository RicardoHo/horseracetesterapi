# frozen_string_literal: true

module Transmission
  module GameSaveHelper
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # game save helper for code name H-SG
    #
    module HSg
      def instance_data
        base_instance_data
      end

      def action_data
        Marshal.load(Marshal.dump(@result))
      end

      def game_context_data
        nil
      end

      def add_playtable
        @paytable ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
          .fetch(:paytable)

        @result.dig(:table).merge!(paytable: @paytable)
      end
    end
  end
end
