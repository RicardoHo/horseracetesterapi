# frozen_string_literal: true

module Transmission
  module GameSaveHelper
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # game save helper for code name H-CK
    #
    module HCk
      def instance_data
        base_instance_data.merge!(free_game_meter: @result.dig(:table, :free_game_meter))
      end

      def action_data
        return @save if @event.command.casecmp?('draw_finish')

        Marshal.load(Marshal.dump(@result))
      end

      def game_context_data
        nil
      end

      def take_win?
        @result.dig(:table, :stage, :next).snake_case
      end

      def add_paytable
        paytable ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
          .dig(:paytable)

        makeup_paytable = {
          value: paytable
            .map do |key, value|
              [key.to_s, value.collect { |_k, v| v[:odds] }]
            end.to_h,
        }

        @result.dig(:table).merge!(paytable: makeup_paytable)
      end
    end
  end
end
