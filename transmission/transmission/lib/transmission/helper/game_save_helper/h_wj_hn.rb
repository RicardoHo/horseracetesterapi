# frozen_string_literal: true

module Transmission
  module GameSaveHelper
    #
    # @author Alpha Huang
    # @since 0.6.0
    #
    # game save helper for code name H-WJ
    #
    module HWjHn
      extend self

      def instance_data
        base_instance_data
      end

      def action_data
        action_data = Marshal.load(Marshal.dump(@result))
        action_data.tap do |table|
          table.delete(:game_context)
          table[:table].delete(:card_pool)
          table.update(table[:table].delete(:RNG))
        end

        action_data
      end

      def game_context_data
        @result.dig(:game_context)
      end
    end
  end
end
