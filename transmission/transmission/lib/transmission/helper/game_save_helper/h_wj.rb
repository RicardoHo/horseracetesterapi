# frozen_string_literal: true

module Transmission
  module GameSaveHelper
    #
    # @author Alpha Huang
    # @since 0.5.0
    #
    # game save helper for code name H-WJ
    #
    module HWj
      extend self

      def instance_data
        base_instance_data
      end

      def action_data
        action_data = Marshal.load(Marshal.dump(@result))
        action_data.tap do |table|
          table.delete(:game_context)
        end

        action_data
      end

      def game_context_data
        nil
      end
    end
  end
end
