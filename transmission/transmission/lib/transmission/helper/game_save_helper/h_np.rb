# frozen_string_literal: true

module Transmission
  module GameSaveHelper
    #
    # @author Richard Fong
    # @since 0.8.0
    #
    # game save helper for code name H-NP
    #
    module HNp
      def instance_data
        @game_engine.instance_data[:round_data].shift if @event.command.casecmp?('drop_finish')

        return @game_engine.instance_data.merge!(base_instance_data) unless @event.command.casecmp?('drop')

        @game_engine.instance_data[:round_data] ||= []
        round_data = @game_engine.instance_data[:round_data].append(instance_data_filter(@result))
        base_instance_data.merge!(round_data: round_data)
      end

      def action_data
        return @save if @event.command.casecmp?('drop_finish')

        Marshal.load(Marshal.dump(@result))
      end

      def game_context_data
        nil
      end

      def ball_result_handler
        records = []
        @result.dig(:table, :score_board, :history)&.each do |score_card|
          record = {
            risk_level: score_card.dig(:bet_option, :selected_risk_level),
            bet_amount_in_credit: score_card.dig(:bet_option, :selected_bet_options, :amount, :credit),
            bet_amount_in_cash: score_card.dig(:bet_option, :selected_bet_options, :amount, :cash),
            odds: score_card.dig(:bet_option, :odds).to_s,
            win_level: score_card.dig(:win_level),
            payout_in_credit: score_card.dig(:bet_option, :payout, :amount, :credit),
            payout_in_cash: score_card.dig(:bet_option, :payout, :amount, :cash),
          }
          records.append(record)
        end
        new_message_standard = {
          game: {
            round_info: @result.dig(:table, :round_info),
            stage: @result.dig(:table, :stage),
            denom_info: @result.dig(:table, :denom_info),
          },
          bet_options: [@result.dig(:bet_options)],
          line_options: [@result.dig(:table, :lines_info)],
          risk_level_options: [@result.dig(:table, :risk_level)],
          result: {
            pocket: @result.dig(:table, :pocket_info),
            win_level: @result.dig(:table, :score_card, :win_level),
            path: @result.dig(:table, :path_info, :path_left_right),
          },
          selected_bet_options: [@result.dig(:table, :score_card, :bet_option, :selected_bet_options)],
          selected_line: @result.dig(:table, :score_card, :bet_option, :selected_line),
          selected_risk_level: @result.dig(:table, :score_card, :bet_option, :selected_risk_level),
          payouts: [@result.dig(:table, :score_card, :bet_option, :payout)],
          history: {
            records: records,
            max_record: @result.dig(:table, :score_board, :max_amount),
          },
          player: @result.dig(:player),
        }

        @result = new_message_standard
        @result[:instance_data] = @game_engine.instance_data if @game_engine.instance_data
        result_append_game_engine_info
      end

      def instance_data_filter(instance_data)
        records = []
        instance_data.dig(:table, :score_board, :history)&.each do |score_card|
          record = {
            risk_level: score_card.dig(:bet_option, :selected_risk_level),
            bet_amount_in_credit: score_card.dig(:bet_option, :selected_bet_options, :amount, :credit),
            bet_amount_in_cash: score_card.dig(:bet_option, :selected_bet_options, :amount, :cash),
            odds: score_card.dig(:bet_option, :odds).to_s,
            win_level: score_card.dig(:win_level),
            payout_in_credit: score_card.dig(:bet_option, :payout, :amount, :credit),
            payout_in_cash: score_card.dig(:bet_option, :payout, :amount, :cash),
          }
          records.append(record)
        end

        {
          game: {
            round_info: instance_data.dig(:table, :round_info),
            stage: instance_data.dig(:table, :stage),
          },
          result: {
            pocket: instance_data.dig(:table, :pocket_info),
            win_level: instance_data.dig(:table, :score_card, :win_level),
            path: instance_data.dig(:table, :path_info, :path_left_right),
          },
          selected_bet_options: [instance_data.dig(:table, :score_card, :bet_option, :selected_bet_options)],
          selected_line: instance_data.dig(:table, :score_card, :bet_option, :selected_line),
          selected_risk_level: instance_data.dig(:table, :score_card, :bet_option, :selected_risk_level),
          payouts: [instance_data.dig(:table, :score_card, :bet_option, :payout)],
          history: {
            records: records,
            max_record: instance_data.dig(:table, :score_board, :max_amount),
          },
          player: instance_data.dig(:player),
        }
      end
    end
  end
end
