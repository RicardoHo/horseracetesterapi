# frozen_string_literal: true

module Transmission
  module SystemCompleteStrategy
    module HCh
      extend self

      def after_deal
        OpenStruct.new(
          command: 'fold',
          data_type: 'fold',
          input: Message::Request::HCh::FoldRequest.new(
            balance: {
              mode: Enum::BalanceMode::CASH,
            }
          )
        )
      end
    end
  end
end
