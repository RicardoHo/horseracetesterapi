# frozen_string_literal: true

module Transmission
  module SystemCompleteStrategy
    module HNp
      def after_drop
        OpenStruct.new(
          command: 'drop_finish',
          data_type: 'drop_finish',
          input: Message::Request::HNp::DropFinishRequest.new(
            round_info_with_finish: [
              {
                round_info: { id: @game_engine.current_round.ref_id },
                finish: TRUE,
              },
            ]
          )
        )
      end
    end
  end
end
