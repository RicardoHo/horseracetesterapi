# frozen_string_literal: true

module Transmission
  module SystemCompleteStrategy
    module HCk
      def draw_free_game
        OpenStruct.new(
          command: 'draw_free_game',
          data_type: 'draw_free_game',
          input: Message::Request::HCk::DrawFreeGameRequest.new
        )
      end

      def draw_finish
        OpenStruct.new(
          command: 'draw_finish',
          data_type: 'draw_finish',
          input: Message::Request::HCk::DrawFinishRequest.new(
            round_info_with_finish: [
              {
                round_info: { id: @game_engine.current_round.ref_id },
                finish: TRUE,
              },
            ]
          )
        )
      end

      def after_draw
        if @game_engine.current_round.actions.last.dig(:table, :stage, :next) == "freeGame"
          draw_free_game
        else
          draw_finish
        end
      end
      alias_method :after_draw_free_game, :after_draw
    end
  end
end
