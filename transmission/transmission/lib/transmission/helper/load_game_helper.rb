# frozen_string_literal: true

module Transmission
  module Helper
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # provide load game related helper methods
    #
    module LoadGameHelper
      extend self

      #
      # is resume game which means has a round not closed
      #
      # @return [Boolean]
      #
      def resume?
        @game_engine.resume_round?
      end

      #
      # is the game can be re-load which means either has
      # old round or can be resume
      #
      # @return [Boolean]
      #
      def loadable?
        @game_engine.new_round? || resume?
      end

      #
      # is the instance save exist for reload
      #
      # @return [Boolean]
      #
      def instance_loadable?
        !@game_engine.instance_data.empty? && !@game_engine.new_instance?
      end

      #
      # load the save data
      #
      # @return [Hash]
      #
      def load_save
        @save = if @game_engine.new_round?
          last_round_save
        elsif @game_engine.resume_round?
          current_round_save
        end
      end

      #
      # load the instance save data
      #
      # @return [Hash]
      #
      def load_instance_save
        @instance_save = @game_engine.instance_data
      end

      private

      #
      # get the save data from last round
      #
      # @return [Hash]
      #
      def last_round_save
        @game_engine.last_round.last_action
      end

      #
      # get the save data from current round
      #
      # @return [Hash]
      #
      def current_round_save
        @game_engine.current_round.last_action
      end
    end
  end
end
