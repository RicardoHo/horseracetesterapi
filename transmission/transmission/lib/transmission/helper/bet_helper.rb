# frozen_string_literal: true

module Transmission
  module Helper
    #
    # @author Alpha Huang
    # @since 0.2.2
    #
    # provide bet related helper methods
    #
    module BetHelper
      extend self

      #
      # bet handle entry point, if the given bet option entity
      # is the desire bet option, then make the bet
      #
      # @param [String] bet_option_name the bet option want to bet
      # @param [Clutch::Entity] entity the bet option entity
      # @param [Bollean] must_present is the bet input must present
      #
      # @raise [MissingBetParamsError] if missing bet input
      #
      def bet(bet_option_name, entity, must_present: false)
        bet_name = entity.find(Component::BetAreaLabel).name
        return unless bet_name.casecmp?(bet_option_name.to_s)

        bet_input = @input.bet_options.find do |opt|
          opt.name.casecmp?(bet_name)
        end

        unless bet_input
          return unless must_present

          raise MissingBetParamsError, "Missing #{bet_name} bet input data"
        end

        on_bet(bet_input.bet_amount.credit, entity)
      end

      private

      #
      # validate is bet amount in between min
      # and max bet of the bet option
      #
      # @param [Integer] bet_amount
      # @param [Clutch::Entity] entity the bet option entity
      #
      # @raise [BetAmountNotInRangeError] if bet amount not in
      #   the range of min and max bet
      #
      def validate_bet_amount(bet_credit, entity)
        max_bet = entity.find(Component::MaxBet)
        min_bet = entity.find(Component::MinBet)
        raise BetAmountNotInRangeError,
          "Bet amount #{bet_credit} not in range [#{min_bet.credit}, #{max_bet.credit}]" \
          unless bet_credit.between?(min_bet.credit, max_bet.credit)
      end

      #
      # bet the bet amount on given bet option
      #
      # @param [Integer] bet_credit bet amount in credit
      # @param [Clutch::Entity] entity the bet option entity
      #
      def on_bet(bet_credit, entity)
        validate_bet_amount(bet_credit, entity)

        bet        = entity.find(Component::BetAmount)
        bet.cash   = bet_credit * @denom_info.denom
        bet.credit = bet_credit
      end
    end
  end
end
