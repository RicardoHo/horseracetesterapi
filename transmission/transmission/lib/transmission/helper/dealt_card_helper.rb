# frozen_string_literal: true

module Transmission
  module Helper
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # provide dealt card related helper methods
    #
    module DealtCardHelper
      extend self

      #
      # dealt card(s), the card is a string compose by suit(cdhs) and face value
      #
      # @param [Integer] num_of_card
      #
      # @yieldparam [String] card
      #
      def dealt(num_of_card = 1, type: :from_top)
        num_of_card.times.each do
          yield send(:"dealt_#{type}")
        end
      end

      def dealt_randomly
        pos = Util::RNGUtil.random(@deck.count, rng: @game_engin.rng)
        @deck.take(pos)
      end

      def dealt_from_top
        @shoe.deal
      end
    end
  end
end
