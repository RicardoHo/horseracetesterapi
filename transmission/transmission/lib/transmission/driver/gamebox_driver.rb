# frozen_string_literal: true

module Transmission
  module Driver
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Driver for drive gamebox
    #
    class GameboxDriver
      attr_reader :gamebox
      FORMAT = Message::Format::JSON
      # TODO: Binaray is not ready
      # case Transmission.env
      # when Enum::Env::PRODUCTION
      #   Message::Format::BINARY
      # when Enum::Env::DEVELOPMENT
      #   Message::Format::JSON
      # end

      def initialize(game_id)
        @game_def = Transmission::GameDefinition.new(
          Util::GameDefinitionUtil.load(game_id)
        )
        @before_execute = @game_def.game_config.before_execute
        @after_execute  = @game_def.game_config.after_execute
        @game_engine    = GameEngineWrapper.new(game_id)

        @req_namespace  = Message::Request.const_get(@game_def.code_name.camel_case)
        @resp_namespace = Message::Filter.const_get(@game_def.code_name.camel_case)
        @gamebox = Clutch::Gamebox.new(@game_def, @game_engine)
        extend(GameSaveHelper.const_get(@game_def.code_name.constanize))
      end

      def update(msg)
        @msg = msg
        before_execute
        process
        after_execute
      rescue => e
        on_error(e)
      ensure
        @event  = nil
        @result = nil
      end

      def close
        @game_engine.close
      rescue => e
        on_error(e)
      end

      private

      def allow_test_mode?
        Transmission.env == Transmission::Enum::Env::PRODUCTION && @game_engine.test_mode
      end

      def setup_inspection_for_test_mode
        Inspection::Inspecter.inspect_classes.each do |klass|
          system_class = System.const_get(klass.class_name.to_sym)
          gamebox.system_admin.find(system_class)&.extend(klass)
        end
      end

      def create_game_engine
        @game_engine.create(
          @event.input.player_token,
          @event.input.instance_info&.id,
          @event.input.property_id,
          @event.input.login_name,
          @event.input.machine_token
        )
      end

      def before_execute
        @event = compose_event(@msg)
        create_game_engine if
          @before_execute.dig(:create_game_engine) == @event.command.to_sym
        @before_execute.dig(:api_call, @event.command.to_sym).tap do |api_list|
          break unless api_list

          @game_engine.process(api_list)
        end
        @data_type = compose_data_type
        setup_inspection_for_test_mode if
          allow_test_mode? && @event.command == 'query_game'
        @event.data_type = @data_type
      end

      def process
        gamebox.execute_event(@event)
        @result = gamebox.inspect[:entities]
      end

      def makeup_last_command_api_call
        return if @game_engine.current_round.init?
        return unless @event.input.instance_info&.id

        return if
          @game_engine.current_round.hit_jps? &&
          @game_engine.current_round.att_paid_jps_locked?

        last_command = @game_engine.last_command&.to_sym
        api_list     = @after_execute.dig(:api_call, last_command)
        return unless api_list

        save_state = @game_engine.save_state
        begin_idx  = api_list.index(save_state) + 1
        return if begin_idx >= api_list.size

        @game_engine.process(
          api_list[begin_idx..-1]
        )
      end

      def current_command_api_call
        @after_execute.dig(:api_call, @event.command.to_sym).tap do |api_list|
          break unless api_list

          if api_list.instance_of?(Hash)
            api_list_key = __send__(api_list[:condition])
            api_list = api_list[api_list_key.to_s.to_sym]
          end

          @game_engine.process(
            api_list,
            instance_data: instance_data,
            action_data: action_data,
            ctx_data: game_context_data
          )
        end
      end

      def result_append_game_engine_info
        @result.deep_merge!(@game_engine.business_data)
      end

      def result_handle
        handler = @after_execute.dig(
          :result_handle,
          @event.command.snake_case.to_sym
        )
        send(handler) if handler
      end

      def after_execute
        makeup_last_command_api_call
        current_command_api_call
        result_append_game_engine_info
        result_handle

        compose_response
      end

      def compose_data_type
        if @event.command == 'query_game' &&
           (@game_engine.resume_round? || @game_engine.new_round?)
          @game_engine.instance_data[:last_command].snake_case
        else
          @event.command
        end
      end

      def compose_event(msg)
        case FORMAT
        when Message::Format::BINARY
          event = Message::Entry.decode(msg.pack('C*'))
          input = @req_namespace
            .const_get(event.action.camel_case + 'Request')
            .decode(event.data)

          msg = OpenStruct.new
          msg[:command] = event.action.snake_case
          msg[:input] = input
          msg
        when Message::Format::JSON
          msg = MultiJson.load(msg).deep_snakize_keys!(symbolize: true)
          Transmission.log_info(caller_id: __id__, request: msg)

          msg[:command] = msg.delete(:action).snake_case
          msg[:input]   = @req_namespace
            .const_get(msg.dig(:command).camel_case + 'Request')
            .new(msg.delete(:data))
          OpenStruct.new(msg)
        end
      end

      def compose_response
        resp = {
          action: @event.command.camel_case(lower: true),
          data_type: @data_type.camel_case(lower: true),
        }
        if @data_type != @event.command
          resp[:act_with] = @data_type.camel_case(lower: true)
          filter_name = "#{@event.command}_act_with_#{@data_type}".camel_case
        else
          filter_name = @event.command.camel_case
        end
        resp = resp.deep_camelize_keys
        filter_data = @resp_namespace
          .const_get("#{filter_name}ResponseFilter")
          .new(@result).to_hash

        data = filter_data.deep_camelize_keys
        case FORMAT
        when Message::Format::JSON
          resp[:data] = data
          resp.to_json
        when Message::Format::BINARY
          data_msg_module = Message::Response
            .const_get("#{@data_type.camel_case}Response")
          resp[:data] = data_msg_module.encode(data_msg_module.new(data))
          response_data = Message::Entry.new(resp)
          Message::Entry.encode(response_data).unpack('C*')
        end
      ensure
        Transmission.log_info(caller_id: __id__, response: data)
      end

      def on_error(e)
        Transmission.log_error(e)
        e = UnexpectedError.new(e.message) unless e.respond_to?(:to_hash)

        {
          action: @event&.command&.camel_case(lower: true),
          data_type: 'error',
          data: e.to_hash,
        }.deep_camelize_keys.to_json
      end

      def update_bet_opts
        return unless @game_engine.resume_round?
        return if @result.key?(:bet_opts)

        @game_engine.update_bet_amount_hash
      end

      def base_instance_data
        {
          last_command: @event.command,
          round_id: @game_engine.current_round.ref_id,
        }
      end
    end
  end
end
