# frozen_string_literal: true

module Transmission
  module Driver
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Driver for drive system complete
    #
    class SystemCompleteDriver
      attr_reader :game_def
      attr_reader :gamebox

      def self.complete(info)
        instance    = new(info)
        module_name = Transmission::SystemCompleteStrategy
        code_name   = instance.game_def.code_name.camel_case
        instance.extend(
          module_name.const_get(code_name)
        ) if module_name.const_defined?(code_name)
        instance.complete
      end

      def initialize(info)
        Transmission.log_info(
          system_complete_start: {
            game_id: info[:game_id],
            instance_id: info.dig(:instance_id),
            player_token: info.dig(:player_token),
            system_completed_code: info.dig(:system_completed_code),
          },
        )
        @info = OpenStruct.new(info)
        @game_def = Transmission::GameDefinition.new(
          Util::GameDefinitionUtil.load(info[:game_id])
        )
        @before_execute = game_def.game_config.before_execute
        @after_execute  = game_def.game_config.after_execute
        @game_engine    = GameEngineWrapper.new(
          info[:game_id],
          info[:system_completed_code],
        )

        @req_namespace  = Message::Request.const_get(@game_def.code_name.camel_case)
        @resp_namespace = Message::Filter.const_get(@game_def.code_name.camel_case)
        @gamebox = Clutch::Gamebox.new(@game_def, @game_engine)
        extend(GameSaveHelper.const_get(@game_def.code_name.constanize))
      end

      def complete
        @event = query_game_event
        loop do
          before_execute
          process
          after_execute
          @event = compose_next_event
          break unless @event
        end
      rescue => e
        on_error(e)
      ensure
        close
        @event  = nil
        @result = nil
        Transmission.log_info(
          system_complete_end: {
            game_id: @info.game_id,
            instance_id: @info.instance_id,
            player_token: @info.player_token,
            system_completed_code: @info.system_completed_code,
          },
        )
      end

      def close
        @game_engine.close
      rescue => e
        on_error(e)
      end

      private

      def before_execute
        if @before_execute.dig(:create_game_engine) == @event.command.to_sym
          @game_engine.create(
            @event.input.player_token,
            @event.input.instance_info&.id,
            @event.input.property_id,
            @event.input.login_name,
            @event.input.machine_token
          )
        end
        @data_type = compose_data_type
        @event.data_type = @data_type
      end

      def process
        Transmission.log_info(command: @event.command)
        gamebox.execute_event(@event)
        @result = gamebox.inspect[:entities]
      end

      def makeup_last_command_api_call
        return unless @event.input.instance_info&.id

        last_command = @game_engine.last_command&.to_sym
        api_list     = @after_execute.dig(:api_call, last_command)
        return unless api_list

        save_state = @game_engine.save_state
        return unless api_list.index(save_state)

        begin_idx = api_list.index(save_state) + 1
        return if begin_idx >= api_list.size

        @game_engine.process(
          api_list[begin_idx..-1]
        )
      end

      def current_command_api_call
        @after_execute.dig(:api_call, @event.command.to_sym).tap do |api_list|
          break unless api_list

          @game_engine.process(
            api_list,
            instance_data: instance_data,
            action_data: action_data,
            ctx_data: game_context_data
          )
        end
      end

      def result_append_game_engine_info
        @result.deep_merge!(@game_engine.business_data)
      end

      def denom
        @result.dig(:table, :denom_info, :denom)
      end

      def response_info_from_game_engine
        before_result_balance_hash = {
          cash: @game_engine.before_result_balance,
          credit: @game_engine.before_result_balance / denom,
        } if @game_engine.before_result_balance
        balance_hash = {
          # TODO: wait for game engine provide
          mode: Enum::BalanceMode::CASH,
        }
        if @game_engine.balance
          balance_hash[:cash]   = @game_engine.balance
          balance_hash[:credit] = @game_engine.balance / denom
        end

        {
          before_result_balance: before_result_balance_hash,
          balance: balance_hash,
          session_id: @game_engine.session_id,
          instance_id: @game_engine.instance_id,
        }.delete_if { |_, v| v.nil? }
      end

      def result_handle
        handler = @after_execute.dig(
          :result_handle,
          @event.command.snake_case.to_sym
        )
        send(handler) if handler
      end

      def after_execute
        makeup_last_command_api_call
        current_command_api_call
        result_append_game_engine_info
        result_handle
      end

      def compose_data_type
        if @event.command == 'query_game' &&
           (@game_engine.resume_round? || @game_engine.new_round?)
          @game_engine.instance_data[:last_command].snake_case
        else
          @event.command
        end
      end

      def query_game_event
        OpenStruct.new(
          command: 'query_game',
          data_type: 'query_game',
          input:  @req_namespace.const_get(:QueryGameRequest).new(
            player_token: @info.player_token,
            game_id: @info.game_id,
            instance_info: { id: @info.instance_id },
            machine_token: @info.machine_token,
            property_id: @info.property_id,
            login_name: @info.login_name,
          )
        )
      end

      def compose_next_event
        method_name = "after_#{@data_type}"
        return unless respond_to?(method_name)

        send(method_name)
      end

      def on_error(e)
        Transmission.log_error(e)
        e = UnexpectedError.new(e.message) unless e.respond_to?(:to_hash)

        {
          data_type: 'error',
          data: e.to_hash,
        }.to_json
      end

      def update_bet_opts
        return unless @game_engine.resume_round?
        return if @result.key?(:bet_opts)

        @game_engine.update_bet_amount_hash
      end

      def base_instance_data
        {
          last_command: @event.command,
          round_id: @game_engine.current_round.ref_id,
        }
      end
    end
  end
end
