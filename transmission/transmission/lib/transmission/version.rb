# frozen_string_literal: true

module Transmission
  VERSION = '1.2.0.rc4'
end
