# frozen_string_literal: true

# require all component's file here
failed_files = []
Dir[File.join(__dir__, 'component', '**.rb')].each do |file|
  require file
rescue
  failed_files.push(file)
end
failed_files.each { |file| require file }
