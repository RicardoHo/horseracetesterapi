# frozen_string_literal: true

# require all util's file here
Dir[File.join(__dir__, 'util', '**.rb')].each { |file| require file }
