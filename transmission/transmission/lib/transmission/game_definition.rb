# frozen_string_literal: true

module Transmission
  module Struct
    class MathModel < Dry::Struct
      attribute :models, Dry.Types::Hash

      def model(rtp)
        models.fetch(rtp)
      rescue KeyError
        raise MathModelNotFoundError, "Math Model #{rtp} not found."
      end
    end

    class GameConfig < Clutch::Struct::GameConfig
      attribute :before_execute,
        Dry.Types::Hash.meta(of: Dry.Types.Array(Dry.Types::String))
      attribute :after_execute,
        Dry.Types::Hash.meta(of: Dry.Types.Array(Dry.Types::String))
    end
  end

  class GameDefinition < Clutch::GameDefinition
    attribute :code_name, Dry.Types::String
    attribute :math_model, Struct::MathModel
    attribute :game_config, Struct::GameConfig
  end
end
