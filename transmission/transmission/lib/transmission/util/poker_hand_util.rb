# frozen_string_literal: true
# @todo
# change to own logic
require 'ruby-poker'

module Transmission
  module Util
    module PokerHandUtil
      extend self

      ROYAL_FACES    = [14, 13, 12, 11, 10]
      CARD_VALUE_MAP = {
        a: 14,
        t: 10,
        j: 11,
        q: 12,
        k: 13,
      }
      CARD_POINT_MAP = {
        a: 1,
        t: 0,
        j: 0,
        q: 0,
        k: 0,
      }
      LOW_ACE_VALUE = 1
      HIGH_ACE_VALUE = 14
      SUIT_RANK = ['d', 'c', 'h', 's']
      PICTURE_CARD_VALUE = ['J', 'Q', 'K']

      def best_hand(cards)
        poker_hand = PokerHand.new(cards)
        [
          poker_hand.sort_using_rank.split(' ').first(5),
          poker_hand.rank.snake_case,
        ]
      end

      def card_face_value(card, low_ace: false, seed: CARD_VALUE_MAP)
        case card
        when String
          value = card[0]
          if low_ace && value.casecmp?('a')
            LOW_ACE_VALUE
          else
            seed.fetch(:"#{value.to_s.downcase}", value.to_i)
          end
        when Integer
          card
        end
      end

      def card_baccarat_point(card)
        card_face_value(card, seed: CARD_POINT_MAP)
      end
      alias_method :san_gong_card_point, :card_baccarat_point

      def card_compare(left_card, right_card, ignore_suit: true, low_ace: false, suit_rank: SUIT_RANK)
        if ignore_suit
          card_face_value(left_card, low_ace: low_ace) <=> card_face_value(right_card, low_ace: low_ace)
        else
          left = card_face_value(left_card, low_ace: low_ace) * 4 + suit_rank.index(left_card[1])
          right = card_face_value(right_card, low_ace: low_ace) * 4 + suit_rank.index(right_card[1])
          left <=> right
        end
      end

      def picture_card?(card)
        PICTURE_CARD_VALUE.include?(card[0])
      end
    end
  end
end
