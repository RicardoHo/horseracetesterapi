# frozen_string_literal: true

module Transmission
  module Util
    #
    # @author Alpha Huang
    # @since 0.0.1
    #
    # Handle and manage connections
    #
    module ConnectionUtil
      extend self

      def new_connection(request, params)
        # create connection and config it's ping time to 25ms
        ws     = Faye::WebSocket.new(request.env, nil, ping: 25)
        driver = Driver::GameboxDriver.new(params[:game_id].to_i)
        record(ws)

        # web socket on new connection open callback
        ws.on(:open) do
          ws.send(driver.class::FORMAT)
          Transmission.logger.info('Gamebox driver created')
        rescue => e
          Transmission.log_error(e)
          ws.close
        end

        # web socket on message received callback
        ws.on(:message) do |event|
          if @closing
            ws.send(Constant::CLOSING_RSP)
            next
          end

          result = driver.update(event.data)
          ws.send(result)
        end

        # web socket on close callback
        ws.on(:close) do |event|
          close_connection(ws)
          if event.code == 1000 && !@closing
            driver.close
          end
          ws = nil
        end

        ws.rack_response
      end

      def close_connection(connection)
        @connections&.delete(connection)
      end

      def record(connection)
        @connections ||= []
        @connections.push(connection)
      end

      def broadcast_closing
        @closing = true
        @connections&.each do |ws|
          ws.send(Constant::CLOSING_RSP)
        end
      end

      def close_all_connection
        @connections&.each(&:close)
        @connections&.clear
      end
    end
  end
end
