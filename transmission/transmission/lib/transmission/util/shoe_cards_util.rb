# frozen_string_literal: true

module Transmission
  module Util
    #
    # @author Alpha HUang
    # @since 0.1.0
    #
    module ShoeCardsUtil
      extend self
      SUITS = %w(h d s c).freeze
      FACES = %w(A K Q J T 9 8 7 6 5 4 3 2).freeze
      DECK_CARDS = FACES.product(SUITS).map!(&:join)

      JOKER_CARDS           = ['JokerB', 'JokerR']
      DECK_CARDS_WITH_JOKER = DECK_CARDS + JOKER_CARDS

      SUIT_JOKER_MAP = {
        h: 'r',
        d: 'r',
        s: 'b',
        c: 'b',
      }
      JOKER_SUIT_MAP = {
        r: ['h', 'd'],
        b: ['s', 'c'],
      }
      JOKER_CARDS_MAP = JOKER_SUIT_MAP
        .map { |color, suits| [color, FACES.product(suits).map!(&:join)] }
        .to_h

      #
      # generate n deck of cards where n is parameter num_of_deck
      #
      # @param [Integer] num_of_deck
      #
      # @return [Array<Integer>]
      #
      def cards(num_of_deck)
        DECK_CARDS * num_of_deck
      end

      #
      # @since 0.3.0
      #
      # genereate a card pool by given parameter num_of_deck
      # a card pool is a hash which key is card and value is remaining amount:
      # @example:
      #   { 'As': 8, 'Ad': 8, 'Ac': 8, 'Ah': 8, ... }
      #
      # @param [Integer] num_of_deck
      # @param [Boolean] with_joker is the deck with joker cards or not
      #
      # @return [Hash<Symbol, Integer>]
      #
      def card_pool(num_of_deck, with_joker: false)
        if with_joker
          DECK_CARDS_WITH_JOKER
        else
          DECK_CARDS
        end.each_with_object({}) { |card, hash| hash[card.to_sym] = num_of_deck }
      end

      #
      # @since 0.4.0
      #
      # genereate a card face pool by given parameter num_of_deck
      # a card face pool is a hash which key is card face and value is remaining amount:
      # @example:
      #   { 'A': 8, 'K': 8, 'Q': 8, 'J': 8, ... }
      #
      # @param [Integer] num_of_deck
      #
      # @return [Hash<Symbol, Integer>]
      #
      def card_face_pool(num_of_deck)
        FACES.each_with_object({}) { |face, hash| hash[face.to_sym] = num_of_deck }
      end

      def joker?(card)
        JOKER_CARDS.include?(card)
      end

      def contain_joker?(cards)
        cards.any? do |card|
          joker?(card)
        end
      end
    end
  end
end
