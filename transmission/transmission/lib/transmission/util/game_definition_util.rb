# frozen_string_literal: true

module Transmission
  module Util
    module GameDefinitionUtil
      extend self

      GAME_DEFINITION_MAP_PATH = File.join(
        Transmission.game_definition_path,
        'game_definition_map.yaml'
      )

      def load(game_id)
        return lookup(game_id) if game_def_data_lut.key?(game_id)

        game_def_data = (game_def_data_lut[game_id] = {})
        glob_pattern  = File.join(dir_path(game_id), '**')
        real_code_name = code_name_lut[game_id]
        Dir.glob(glob_pattern).each do |file|
          config_data = load_yaml(file)
          config_type = config_data.dig(:meta, :type).to_sym
          real_code_name = config_data.dig(:meta, :code_name) if config_type == :game_config
          game_def_data[config_type] = parse_config(config_type, config_data)
        end
        game_def_data[:code_name] = real_code_name
        game_def_data
      end

      private

      def load_yaml(file_path)
        YAML.safe_load(
          File.open(file_path),
          permitted_classes: [Symbol, Range],
          aliases: true
        )
      end

      def parse_config(type, data)
        case type
        when :system_config
          parse_system_config(data)
        else
          data
        end
      end

      def parse_system_config(data)
        parsed = data.to_hash
        parsed[:system].each do |_system_type, system_names|
          system_names.map! do |system_name|
            Transmission::System.const_get(system_name.camel_case)
          end
        end

        parsed
      end

      def code_name_lut
        @code_name_lut ||= begin
          raw = load_yaml(GAME_DEFINITION_MAP_PATH)
          raw.each_with_object({}) do |(code_name, id_list), parsed|
            id_list.each do |id|
              parsed[id] = code_name
            end
          end
        end
      end

      def dir_path(game_id)
        File.join(Transmission.game_definition_path, code_name_lut[game_id])
      end

      def game_def_data_lut
        @game_def_data_lut ||= {}
      end

      def lookup(game_id)
        game_def_data_lut[game_id]
      end
    end
  end
end
