# frozen_string_literal: true

module Transmission
  module Util
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # random number generator util powered by Wheel
    #
    module RNGUtil
      extend self
      # WheelApi.base_url = Transmission.config[:rng_url]

      #
      # generate a random number in range 0 to upper limit - 1
      #
      # @param [Integer] upper_limit
      #
      # @return [Integer]
      #
      def random(upper_limit, rng:, rng_capture: nil)
        return 0 if upper_limit == 0

        rng_info = compose_rng_info(upper_limit)
        rng_ref_id = rng_capture&.fetch(:rng_ref_id)
        result = rng.draw(rng_info, ref_id: rng_ref_id).first

        rng_capture[:rng_result].push(result) if rng_capture
        result
      end

      #
      # @since 0.7.0
      #
      # generate list of random number in range 0 to upper limit - 1
      #
      # @param [Integer] num_of_random
      # @param [Integer] upper_limit
      #
      # @return [Array<Integer>]
      #
      def random_multiple(num_of_random, upper_limit, repeat: false, rng:, rng_capture: nil)
        return [0] * num_of_random if upper_limit <= 1

        rng_info = compose_rng_info(upper_limit, num_of_random, repeat: repeat)
        result = rng.multi_draw(rng_info, rng_capture&.fetch(:rng_ref_id))
        rng_capture[:rng_result].push(result) if rng_capture
        result
      end
      #
      # @since 0.3.0
      #
      # generate list of random number by a list of upper limit
      #
      # @param [Array<Integer>] upper_limit_list
      #
      # @return [Array<Integer>]
      #
      # def batch_random(upper_limit_list, rng:, rng_capture: nil)
      def batch_random(upper_limit_list, rng:)
        rng_infos = upper_limit_list.map do |upper_limit|
          compose_rng_info(upper_limit)
        end
        # p method(__method__).name, method(__method__).source_location.join(':'), caller(1,1)
        rng.multi_draw(rng_infos).flatten
      end

      #
      # @since 0.3.0
      #
      # random draw a symbol base on weight table
      # set replacement to true for avoid modify original table
      #
      # @param [Hash<Symbol, Integer>] weight_table
      # @param [Boolean] replacement
      #
      # @return [Integer]
      #
      def draw_from_weight_table(weight_table, replacement: false, rng:, rng_capture: nil)
        result_in_list, table = rng.draw(
          table: weight_table,
          replacement: replacement,
          ref_id: rng_capture&.fetch(:rng_ref_id),
        )
        rng_capture[:rng_result].push(result_in_list) if rng_capture

        [result_in_list[0], table]
      end

      #
      # @since 0.3.0
      #
      # random draw a list of symbol base on weight table
      # set replacement to true for avoid modify original table
      #
      # @param [Hash<Symbol, Integer>] weight_table
      # @param [Boolean] replacement
      #
      # @return [Integer]
      #
      def draw_multiple_from_weight_table(weight_table, num, replacement: false, rng:, rng_capture: nil)
        result = rng.draw(
          table: weight_table,
          num: num,
          replacement: replacement,
          ref_id: rng_capture&.fetch(:rng_ref_id),
        )
        rng_capture[:rng_result].push(result.first) if rng_capture
        result
      end

      private

      #
      # @since 0.3.0
      #
      # compose rng info for Wheel API client to draw random number
      #
      # @param [Integer] upper_limit
      #
      # @return [Wheel::RNG::Info]
      #
      def compose_rng_info(upper_limit, num_of_random = 1, repeat: false)
        WheelApi::RNG::Info.new(
          num_of_rng: num_of_random,
          limit:      upper_limit,
          repeat:     repeat
        )
      end
    end
  end
end
