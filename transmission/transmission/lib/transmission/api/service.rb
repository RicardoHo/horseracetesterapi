# frozen_string_literal: true

module Transmission
  module Api
    class Service < Sinatra::Base
      register Route

      Faye::WebSocket.load_adapter('puma')

      use Rack::RequestId
      set :logging, true
      set :show_exceptions, false
      set :headers,
        'X-Powered-By' => "Transmission#{Transmission::VERSION}",
        'X-Api-Version' => Transmission::Api::VERSION,
        'Access-Control-Allow-Origin' => '*',
        'Access-Control-Allow-Headers' => '*',
        'Access-Control-Allow-Methods' => ['OPTIONS', 'GET', 'POST']
      set :greeting, <<~STR
        Greeting From Transmission
        Service Version #{Transmission::VERSION}
        API Version #{Transmission::Api::VERSION}
      STR
      set :service_config, Transmission.config.to_collector_hash.to_json
      set :server_settings, signals: false
      disable :closing

      # Rewrite sinatra's signal "TERM" handler
      def self.setup_traps
        super
        trap(:TERM) do
          Thread.new do
            settings.closing = true
            Util::ConnectionUtil.broadcast_closing
          end
        end
      end

      before do
        halt 403, Constant::SERVER_CLOSING_RSP if settings.closing

        headers settings.headers
        if request.request_method == 'OPTIONS'
          halt 204
        end

        D13n::Metric::StreamState.st_get.request_info = {
          host: request.host,
          ip: request.ip,
          requestId: env['HTTP_X_REQUEST_ID'],
        }
      end

      get '/' do
        settings.greeting
      end

      get '/check' do
        halt 200, "Hello World"
      end

      get '/config' do
        settings.service_config
      end

      #
      # is the server still accept new request or not
      #
      # @return [Boolean]
      #
      def pass?
        !settings.closing
      end
    end
  end
end
