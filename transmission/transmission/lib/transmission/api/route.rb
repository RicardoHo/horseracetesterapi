# frozen_string_literal: true

module Transmission
  module Api
    module Route
      def self.registered(app)
        app.register Sinatra::Namespace
        app.set :sc_keyword, Transmission.config[:system_complete_host_keyword]
        app.set :connections, []

        app.get '/ws/:game_id' do
          if Faye::WebSocket.websocket?(request.env)
            Util::ConnectionUtil.new_connection(request, params)
          else
            halt 426, 'You should connect this via websocket.'
          end
        end

        # system complete api route
        app.post '/system_complete' do
          Driver::SystemCompleteDriver.complete(
            JSON.parse(request.body.read, symbolize_names: true)
          )
        end

        # to shutdown current service, only allow localhost call
        app.get '/shutdown', host_name: 'localhost' do
          halt 403, SERVER_CLOSING_RSP unless pass?

          settings.closing = true
          Util::ConnectionUtil.broadcast_closing

          Constant::SERVER_CLOSING_RSP
        end

        app.get '/properties/:property_id/games/:game_id/configs' do
          GameEngineWrapper.game_configs(
            property_id: params[:property_id].to_i,
            game_id: params[:game_id].to_i,
          ).deep_camelize_keys
            .to_json
        end
      end
    end
  end
end
