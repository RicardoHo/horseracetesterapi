# frozen_string_literal: true

module Transmission
  class Error < RuntimeError
    DESCP = 'Error description was not defined.'

    def self.code(code_name)
      remove_const(:CODE) if const_defined?(:CODE)
      const_set(:CODE, code_name)
    end

    def self.description(description)
      const_set(:DESCP, description)
    end

    def initialize(description = self.class::DESCP)
      @description = description
      super
    end

    def to_hash
      {
        code: self.class::CODE,
        description: @description,
      }
    end
  end

  class GameEngineError < Error
    def initialize(code, description)
      @code = code
      super(description)
    end

    def to_hash
      {
        code: @code,
        description: @description,
      }
    end
  end

  class UnexpectedError < Error
    code :T00
  end

  class BetAmountNotInRangeError < Error
    code :T01
  end

  class MissingBetParamsError < Error
    code :T02
  end

  class DenomNotInRangeError < Error
    code :T03
    description 'The given denom is not in the denom set range.'
  end

  class MathModelNotFoundError < Error
    code :T04
  end

  class InvalidSkipError < Error
    code :T05
    description 'Skip is invalid since the shoe was used.'
  end

  class ReachSkipLimitError < Error
    code :T06
    description 'Skip is not allow since already skip 3 times.'
  end

  class TableBetsNotInRangeError < Error
    code :T07
    description 'All bets in table not in table limit.'
  end

  class EnquiryBetNotEnableError < Error
    code :T08
    description 'Enquiry Bet (Bet on both player and banker) is not allow.'
  end

  class EnquiryBetAmountInvalidError < Error
    code :T09
    description 'Enquiry Bet amount is not matched to the permitted value.'
  end

  class EnquiryBetWithOtherBetError < Error
    code :T10
    description 'Enquiry Bet with other bet is not allow.'
  end

  class CutCardRangeParameterError < Error
    code :T11
    description 'Wrong parameter for cut card range.'
  end

  class BetItemNotInRangeError < Error
    code :T12
    description 'The given bet item not in range.'
  end

  class SelectedRiskLevelNotInOptionsError < Error
    code :T13
  end

  class SelectedLineNotInRangeError < Error
    code :T14
  end

  class SelectNumbersSizeNotInRangeError < Error
    code :T15
  end

  class MultiplierNotInRangeError < Error
    code :T16
    description 'The given multiplier is not in the multiplier set range.'
  end

  class BetAmountNotMatchForMultiplierValueError < Error
    code :T17
  end

  class MultiplierSetNotFoundError < Error
    code :T18
    description 'Game config multiplier set not found from game engine'
  end

  class MultiplierDefaultIndexNotFoundError < Error
    code :T19
    description 'Game config multiplier default index not found from game engine'
  end

  class UnitBetNotFoundError < Error
    code :T20
    description 'Game config unit bet not found from game engine'
  end

  class GameSequenceWrongError < Error
    code :T21
  end

  class MultiplierSetNotAllIntegerError < Error
    code :T22
    description 'All number in multiplier set MUST BE Integer!!!'
  end

  class MultiplierSetNotAllPositiveError < Error
    code :T23
    description 'All number in multiplier set MUST BE Positive!!!'
  end

  class MultiplierDefaultIndexNotIntegerError < Error
    code :T24
    description 'Game config multiplier default index MUST BE Integer!!!'
  end

  class MultiplierDefaultIndexNotInRangeError < Error
    code :T25
    description 'Game config multiplier default index not in range'
  end
end
