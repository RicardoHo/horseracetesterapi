# frozen_string_literal: true

module Transmission
  class GameEngineWrapper
    extend Forwardable

    attr_reader :before_result_balance
    attr_reader :balance
    attr_reader :lottery_ctx
    attr_reader :rng, :weight_table
    attr_accessor :rng_capture

    def_delegators :@game_engine,
      :current_round,
      :last_round,
      :instance_data,
      :session_id,
      :instance_id,
      :currency,
      :game_config,
      :test_mode,
      :hit_prog_payout?

    # PinionApi.base_url = Transmission.config[:game_engine_url]

    def self.game_configs(property_id:, game_id:)
      PinionApi::Game.new(
        property_id: property_id,
        game_id: game_id,
      ).configs
    end

    def initialize(game_id, system_completed_code = nil)
      @game_id = game_id
      @system_completed_code = system_completed_code
      @bet_amount_hash = {}
      @running_data = {}
    end

    def create(player_token, instance_id, property_id, login_name, machine_token = nil)
      @game_engine ||= PinionApi.get_game_instance(
        player_token: player_token,
        machine_token: machine_token,
        game_id: @game_id,
        instance_id: instance_id,
        property_id: property_id,
        login_name: login_name,
        system_completed_code: @system_completed_code
      )
      @rng = WheelApi.get_rng(
        game_id: @game_id,
        property_id: property_id,
        login_name: login_name
      )
      @weight_table = WheelApi.get_weight_table(
        game_id: @game_id,
        property_id: property_id,
        login_name: login_name
      )
      @rng_capture = {
        rng_ref_id: '',
        rng_result: [],
      }
      on_open
    rescue PinionApi::Error => e
      raise GameEngineError.new(e.code, e.description)
    end

    def process(api_list, instance_data: nil, action_data: nil, ctx_data: nil)
      clear_balance
      @instance_data = instance_data
      @action_data   = action_data
      @running_data  = {}

      begin
        update_data(instance_data, action_data, ctx_data)
        api_list.each do |api|
          send(:"on_#{api}")
          break if current_round.hit_prog_payout?
        end
      rescue PinionApi::Error => e
        raise GameEngineError.new(e.code, e.description)
      end
    end

    def business_data
      @base_data
        .deep_merge(
          **@running_data,
          table: {
            round_info: {
              state: round_state,
            },
          }
        )
        .delete_if { |_, v| v.nil? || (v.respond_to?(:empty?) && v.empty?) }
    end

    def update_data(instance_data, action_data, ctx_data)
      @game_engine.instance_data = instance_data if instance_data
      current_round.current_action = action_data if action_data
      current_round.game_ctx = ctx_data if current_round.respond_to?(:game_ctx) && ctx_data
    end

    def clear_balance
      @before_result_balance = nil
      @balance               = nil
    end

    def update_bet_amount_hash
      @bet_amount_hash = retrieve_bet_opts
    end

    def close
      @game_engine&.leave
    end

    def last_command
      @game_engine.instance_data[:last_command]
    end

    def save_state
      @game_engine.instance_data[:save_state]&.downcase
    end

    def new_round?
      current_round.init? && last_round&.closed?
    end

    def resume_round?
      current_round.opened? || current_round.att_paid_jps_locked?
    end

    def new_instance?
      current_round.init? && last_round.nil?
    end

    def round_state
      if current_round.closed? || current_round.init?
        Enum::RoundState::CLOSED
      else
        Enum::RoundState::OPEN
      end
    end

    private

    def retrieve_bet_opts
      bet_opts = (
        current_round.current_action || current_round.last_action
      ).dig(:bet_options)
      if bet_opts.is_a?(Array)
        bet_opts.each_with_object({}) do |opt, obj|
          obj[opt[:name].to_sym] = opt.dig(:bet_amount, :cash)
        end.delete_if { |k, v| v.nil? || @bet_amount_hash.key?(k) }
      elsif bet_opts.is_a?(Hash)
        {
          "#{bet_opts[:name]}": bet_opts.dig(:bet_amount, :cash),
        }
      end
    end

    def retrieve_payout_opts
      bet_opts = (
        current_round.current_action || current_round.last_action
      ).dig(:bet_options)
      if bet_opts.is_a?(Array)
        bet_opts.each_with_object({}) do |opt, obj|
          bet_cash = opt.dig(:bet_amount, :cash)
          obj[opt[:name].to_sym] = opt.dig(:payout, :cash) if bet_cash
        end.delete_if { |_k, v| v.nil? }
      elsif bet_opts.is_a?(Hash)
        {
          "#{bet_opts[:name]}": bet_opts.dig(:payout, :cash),
        }
      end
    end

    def handle_take_result(result, denom)
      if result.key?(:jps_info)
        {
          info_panel: result,
        }
      elsif result.key?(:balance)
        {
          balance: {
            cash: result[:balance],
            credit: result[:balance] / denom,
          },
          lottery_ctx: result[:lottery_ctx],
        }
      end
    end

    def on_open
      @game_engine.open
      @base_data = {
        session_id: session_id,
        instance_id: instance_id,
        system_completed: last_round&.system_completed?,
        test_mode: test_mode,
        # TODO: wait for game engine provide
        balance: {
          mode: Enum::BalanceMode::CASH,
          currency: @game_engine.currency,
        },
      }
      @running_data[:info_panel] = {
        prog_payout_info: @game_engine.current_round.prog_payout_info,
        jps_info: @game_engine.current_round.jps_info,
      }.delete_if { |_, v| v.nil? }
      # @running_data[:info_panel] = {
      #   prog_payout_info: @game_engine.current_round.prog_payout_info,
      # } if @game_engine.current_round.prog_payout_info
    end

    def on_bet
      @game_engine.instance_data[:save_state] = Enum::SaveState::BET

      @bet_amount_hash      = retrieve_bet_opts
      current_round.denom   = current_round.current_action.dig(:table, :denom_info, :denom)
      response = @game_engine.bet(
        bet_opts: @bet_amount_hash
      )
      @running_data[:before_result_balance] = {
        cash: response[:balance],
        credit: response[:balance] / current_round.denom,
        currency: @game_engine.currency,
      }
      @running_data[:lottery_ctx] = response[:lottery_ctx]
      if @game_engine.current_round.hit_prog_payout?
        @running_data[:info_panel] = {
          prog_payout_info: @game_engine.current_round.prog_payout_info,
        }
      end
    end

    def on_result
      @game_engine.instance_data[:save_state] = Enum::SaveState::RESULT
      @game_engine.result
    end

    def on_take_win
      denom = current_round.denom
      if @game_engine.current_round.hit_jps?
        @game_engine.instance_data[:save_state] = Enum::SaveState::TAKE_WIN
        result = @game_engine.take_win
      else
        result = @game_engine.take_win(
          payout_opts: retrieve_payout_opts
        )
      end

      result = handle_take_result(result, denom)
      @running_data.merge!(result)
    ensure
      @bet_amount_hash = {}
    end

    def on_update
      @game_engine.instance_data[:save_state] = Enum::SaveState::UPDATE
      @game_engine.update
    end
  end
end
