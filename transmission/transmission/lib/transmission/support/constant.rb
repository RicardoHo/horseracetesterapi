# frozen_string_literal: true

module Transmission
  module Constant
    SERVER_CLOSING_MSG = 'Server is closing now, all connection will be ' \
        'closed when all event processed.'

    SERVER_CLOSING_RSP = {
      data_type: 'closing',
      data: {
        description: SERVER_CLOSING_MSG,
      },
    }.deep_camelize_keys.to_json

    CLOSING_RSP = {
      data_type: 'closing',
      data: {
        description: 'Message has been emited due to the server was closing',
      },
    }.deep_camelize_keys.to_json
  end
end
