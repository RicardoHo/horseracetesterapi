# frozen_string_literal: true

module Transmission
  module Enum
    module WinType
      WIN  = 'WIN'
      LOSE = 'LOSE'
      PUSH = 'PUSH'
    end

    module Winner
      BANKER = 'BANKER'
      PLAYER = 'PLAYER'
      TIE    = 'TIE'
      BOTH   = 'BOTH'
    end

    module BalanceMode
      CASH   = 'CASH'
      CREDIT = 'CREDIT'
    end

    module SaveState
      BET      = 'bet'
      RESULT   = 'result'
      TAKE_WIN = 'take_win'
      UPDATE   = 'update'
    end

    module Stage
      BASE_GAME = 'baseGame'
      FREE_GAME = 'freeGame'
    end

    module Env
      PRODUCTION  = 'production'
      DEVELOPMENT = 'development'
    end

    module RoundState
      OPEN   = 'open'
      CLOSED = 'closed'
    end
  end
end
