# frozen_string_literal: true

class Hash
  #
  # convert key to symbol form resursivly
  #
  # @return [Hash]
  #
  def deep_symbolize_keys
    deep_transform_keys(&:to_sym)
  end

  #
  # deep symbolize keys in self
  #
  # @return [Hash]
  #
  def deep_symbolize_keys!
    deep_transform_keys!(&:to_sym)
  end

  def deep_camelize_keys(symbolize: true)
    deep_transform_keys do |key|
      new_key = key.to_s.camel_case(lower: true)
      symbolize ? new_key.to_sym : new_key
    end
  end

  def deep_camelize_keys!(symbolize: true)
    deep_transform_keys! do |key|
      new_key = key.to_s.camel_case(lower: true)
      symbolize ? new_key.to_sym : new_key
    end
  end

  def deep_snakize_keys(symbolize: true)
    deep_transform_keys do |key|
      new_key = key.to_s.snake_case
      symbolize ? new_key.to_sym : new_key
    end
  end

  def deep_snakize_keys!(symbolize: true)
    deep_transform_keys! do |key|
      new_key = key.to_s.snake_case
      symbolize ? new_key.to_sym : new_key
    end
  end

  #
  # merge hash recursivly
  # all container value will be merge instead replace.
  # @example
  # {sample: {deep: 1}}.deep_merge(sample: {foo: 2})
  #   => {sample: {deep: 1, foo: 2}}
  #
  # @param [Hash] to_merge
  #
  # @return [Hash]
  #
  def deep_merge(to_merge)
    merger = proc do |_key, v1, v2|
      if v1.is_a?(Hash) && v2.is_a?(Hash)
        v1.merge(v2, &merger)
      elsif v1.is_a?(Array) && v2.is_a?(Array)
        v1 + v2
      else
        v2
      end
    end
    merge(to_merge, &merger)
  end

  #
  # bang version of deep_merge
  # merge hash recursivly
  # all container value will be merge instead replace.
  # @example
  # {sample: {deep: 1}}.deep_merge!(sample: {foo: 2})
  #   => {sample: {deep: 1, foo: 2}}
  #
  # @param [Hash] to_merge
  #
  # @return [Hash]
  #
  def deep_merge!(to_merge)
    merger = proc do |_key, v1, v2|
      if v1.is_a?(Hash) && v2.is_a?(Hash)
        v1.merge!(v2, &merger)
      elsif v1.is_a?(Array) && v2.is_a?(Array)
        v1 + v2
      else
        v2
      end
    end
    merge!(to_merge, &merger)
  end

  private

  def deep_transform_keys(&block)
    _deep_transform_keys_in_object(self, &block)
  end

  def deep_transform_keys!(&block)
    _deep_transform_keys_in_object!(self, &block)
  end

  def _deep_transform_keys_in_object(object, &block)
    case object
    when Hash
      object.each_with_object({}) do |(key, value), result|
        result[yield(key)] = _deep_transform_keys_in_object(value, &block)
      end
    when Array
      object.map { |e| _deep_transform_keys_in_object(e, &block) }
    else
      object
    end
  end

  def _deep_transform_keys_in_object!(object, &block)
    case object
    when Hash
      object.keys.each do |key|
        value = object.delete(key)
        object[yield(key)] = _deep_transform_keys_in_object!(value, &block)
      end
      object
    when Array
      object.map! { |e| _deep_transform_keys_in_object!(e, &block) }
    else
      object
    end
  end
end
