# frozen_string_literal: true

require_relative 'message/filter/filter.rb'

require_relative 'message/support/format.rb'
require_relative 'message/request/request.rb'

# Dir[File.join(__dir__, 'message', '**', '*.rb')].each { |file| require file }

# Google::Protobuf::Map.class_eval do
#   def key?(key)
#     has_key?(key.to_s)
#   end

#   alias_method :orig_dig, :[]
#   def [](key)
#     orig_dig(key.to_s)
#   end
#   alias_method :dig, :[]
# end

# Transmission::Message::Entry.class_eval do
#   def command
#     action.snake_case
#   end

#   def input
#     data
#   end
# end
