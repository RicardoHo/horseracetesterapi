# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: transmission/message/shared/denom_info.proto

require 'google/protobuf'

Google::Protobuf::DescriptorPool.generated_pool.build do
  add_file("transmission/message/shared/denom_info.proto", :syntax => :proto3) do
    add_message "transmission.message.shared.DenomInfo" do
      repeated :denoms, :int32, 1
      optional :index, :int32, 2
    end
  end
end

module Transmission
  module Message
    module Shared
      DenomInfo = ::Google::Protobuf::DescriptorPool.generated_pool.lookup("transmission.message.shared.DenomInfo").msgclass
    end
  end
end
