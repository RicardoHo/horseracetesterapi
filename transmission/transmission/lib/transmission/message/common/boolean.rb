# frozen_string_literal: true

module Transmission
  module Message
    module Filter
      module Common
        class Boolean < Base
          FALSE_VALUES = [false, 0, "0", "false", "FALSE"].to_set
          TRUTH_VALUES = [true, 1, "1", "true", "TRUE"].to_set
          def self.coerce(value)
            if FALSE_VALUES.include?(value)
              false
            elsif TRUTH_VALUES.include?(value)
              true
            else
              false
            end
          end
        end
      end
    end
  end
end
