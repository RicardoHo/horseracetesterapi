# frozen_string_literal: true

module Transmission
  module Message
    module Filter
      module HNp
        #
        # @author Richard Fong
        # @since 0.8.0
        #
        # Response schema for drop action
        #
        class DropResponseFilter < Base
          class Game < Base
            property :round_info, coerce: Hash
            property :stage, coerce: Hash
          end

          class Result < Base
            property :pocket, coerce: Hash
            property :win_level, coerce: String
            property :path, coerce: Array[Integer]
          end

          class Player < Base
            property :action_win, coerce: Hash
            property :chip_index, coerce: Integer
          end

          class Balance < Base
            property :cash, coerce: Integer
            property :credit, coerce: Integer
          end

          property :game, coerce: Game
          property :result, coerce: Result
          property :selected_bet_options, coerce: Array[Hash]
          property :selected_line, coerce: Integer
          property :selected_risk_level, coerce: String
          property :payouts, coerce: Array[Hash]
          property :player, coerce: Hash
          property :balance, coerce: Balance
          property :before_result_balance, coerce: Balance
          property :session_id, coerce: String
          property :info_panel, coerce: Hash
        end
      end
    end
  end
end
