# frozen_string_literal: true

module Transmission
  module Message
    module Filter
      module HNp
        #
        # @author Richard Fong
        # @since 0.8.0
        #
        # Response schema for query game action
        #
        class QueryGameResponseFilter < Base
          class DenomInfo < Base
            property :denoms, coerce: Array[Integer]
            property :index, coerce: Integer
          end

          class Game < Base
            property :round_info, coerce: Hash
            property :stage, coerce: Hash
            property :denom_info, coerce: DenomInfo
          end

          class BetOption < Base
            property :name, coerce: String
            property :max_bet, coerce: Hash
            property :min_bet, coerce: Hash
          end

          class LinesInfo < Base
            property :lines, coerce: Array[Integer]
            property :index, coerce: Integer
          end

          class RiskLevel < Base
            property :levels, coerce: Array[String]
            property :index, coerce: Integer
          end

          class Balance < Base
            property :mode, coerce: String
            property :currency, coerce: String
          end

          property :game, coerce: Game
          property :bet_options, coerce: Array[BetOption]
          property :line_options, coerce: Array[LinesInfo]
          property :risk_level_options, coerce: Array[RiskLevel]
          property :balance, coerce: Balance
          property :session_id, coerce: String
          property :instance_id, coerce: String
          property :test_mode, coerce: Common::Boolean, default: false
          property :system_completed, coerce: Common::Boolean
          property :info_panel, coerce: Hash
        end
      end
    end
  end
end
