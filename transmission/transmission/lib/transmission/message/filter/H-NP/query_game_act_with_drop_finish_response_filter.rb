# frozen_string_literal: true

module Transmission
  module Message
    module Filter
      module HNp
        #
        # @author Richard Fong
        # @since 0.8.0
        #
        # Response schema for query game action resumed from drop finish
        #
        class QueryGameActWithDropFinishResponseFilter < Base
          class DenomInfo < Base
            property :denoms, coerce: Array[Integer]
            property :index, coerce: Integer
          end

          class Game < Base
            property :round_info, coerce: Hash
            property :stage, coerce: Hash
            property :denom_info, coerce: DenomInfo
          end

          class BetOption < Base
            property :name, coerce: String
            property :max_bet, coerce: Hash
            property :min_bet, coerce: Hash
          end

          class LinesInfo < Base
            property :lines, coerce: Array[Integer]
            property :index, coerce: Integer
          end

          class RiskLevel < Base
            property :levels, coerce: Array[String]
            property :index, coerce: Integer
          end

          class Result < Base
            property :pocket, coerce: Hash
            property :win_level, coerce: String
            property :path, coerce: Array[Integer]
          end

          class History < Base
            property :records, coerce: Array[Hash]
            property :max_record, coerce: Integer
          end

          class Player < Base
            property :action_win, coerce: Hash
            property :chip_index, coerce: Integer
          end

          class Balance < Base
            property :mode, coerce: String
            property :currency, coerce: String
          end

          property :game, coerce: Game
          property :instance_data, coerce: Hash
          property :bet_options, coerce: Array[BetOption]
          property :line_options, coerce: Array[LinesInfo]
          property :risk_level_options, coerce: Array[RiskLevel]
          property :result, coerce: Result
          property :selected_bet_options, coerce: Array[Hash]
          property :selected_line, coerce: Integer
          property :selected_risk_level, coerce: String
          property :payouts, coerce: Array[Hash]
          property :history, coerce: History
          property :player, coerce: Player
          property :balance, coerce: Balance
          property :session_id, coerce: String
          property :instance_id, coerce: String
          property :test_mode, coerce: Common::Boolean, default: false
          property :system_completed, coerce: Common::Boolean
          property :info_panel, coerce: Hash
        end
      end
    end
  end
end
