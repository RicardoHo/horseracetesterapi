# frozen_string_literal: true

module Transmission
  module Message
    module Filter
      module HLlf
        #
        # @author Alpha Huang
        # @since 0.4.0
        #
        # Response schema for bet action
        #
        class BetResponseFilter < Base
          class Balance < Base
            property :cash, coerce: Integer
            property :credit, coerce: Integer
          end

          class Table < Base
            property :round_info, coerce: Hash
            property :stage, coerce: Hash
          end

          class BetOption < Base
            property :name, coerce: String
            property :bet_amount, coerce: Hash
            property :payout, coerce: Hash
            property :type, coerce: String
          end

          class Player < Base
            property :rank, coerce: String
            property :action_win, coerce: Hash
            property :card, coerce: Array[String]
            property :chip_index, coerce: Integer
          end

          property :table, coerce: Table
          property :bet_options, coerce: BetOption
          property :player, coerce: Player
          property :balance, coerce: Balance
          property :before_result_balance, coerce: Balance
          property :session_id, coerce: String
          property :info_panel, coerce: Hash
        end
      end
    end
  end
end
