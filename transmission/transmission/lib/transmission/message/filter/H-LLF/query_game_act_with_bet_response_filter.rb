# frozen_string_literal: true

module Transmission
  module Message
    module Filter
      module HLlf
        #
        # @author Alpha Huang
        # @since 0.4.0
        #
        # Response schema for query game action
        # act with bet (resume from bet)
        #
        class QueryGameActWithBetResponseFilter < Base
          class Balance < Base
            property :mode, coerce: String
            property :currency, coerce: String
          end

          class DenomInfo < Base
            property :denoms, coerce: Array[Integer]
            property :index, coerce: Integer
          end

          class Table < Base
            property :round_info, coerce: Hash
            property :stage, coerce: Hash
            property :denom_info, coerce: DenomInfo
          end

          class BetOption < Base
            property :name, coerce: String
            property :max_bet, coerce: Hash
            property :min_bet, coerce: Hash
            property :bet_amount, coerce: Hash
            property :payout, coerce: Hash
            property :type, coerce: String
          end

          class Player < Base
            property :rank, coerce: String
            property :action_win, coerce: Hash
            property :card, coerce: Array[String]
            property :chip_index, coerce: Integer
          end

          property :table, coerce: Table
          property :bet_options, coerce: BetOption
          property :balance, coerce: Balance
          property :player, coerce: Player
          property :session_id, coerce: String
          property :instance_id, coerce: String
          property :system_completed, coerce: Common::Boolean
          property :test_mode, coerce: Common::Boolean, default: false
          property :info_panel, coerce: Hash
        end
      end
    end
  end
end
