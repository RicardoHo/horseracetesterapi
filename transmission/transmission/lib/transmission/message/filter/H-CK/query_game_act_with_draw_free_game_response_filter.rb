# frozen_string_literal: true

module Transmission
  module Message
    module Filter
      module HCk
        #
        # @author Richard Fong
        # @since 0.9.0
        #
        # Response schema for query game action resumed from draw free game
        #
        class QueryGameActWithDrawFreeGameResponseFilter < Base
          class Balance < Base
            property :mode, coerce: String
            property :currency, coerce: String
          end

          class DenomInfo < Base
            property :denoms, coerce: Array[Integer]
            property :index, coerce: Integer
          end

          class MultiplierInfo < Base
            property :multipliers, coerce: Array[Integer]
            property :index, coerce: Integer
          end

          class IdBox < Base
            property :hit, coerce: Integer, from: :hit_counter
            property :result_numbers, coerce: Array[Integer], from: :result_id
            property :hit_free_game, coerce: Common::Boolean, default: false
          end

          class SelectedIdList < Base
            property :numbers, coerce: Array[Integer], from: :list
          end

          class ScoreCard < Base
            property :payout do
              property :name, coerce: String
              property :payout_amount, coerce: Hash
            end
            property :hit, coerce: Integer, from: :hit_counter
            property :odds, coerce: String
            property :win_level, coerce: String
            property :stage, coerce: Hash
          end

          class ScoreBoard < Base
            property :max_amount, coerce: Integer
            property :history, coerce: Array[ScoreCard]
          end

          class Table < Base
            property :round_info, coerce: Hash
            property :stage, coerce: Hash
            property :denom_info, coerce: DenomInfo
            property :multiplier_info, coerce: MultiplierInfo
            property :unit_bet, coerce: Hash
            property :multipliers, coerce: Hash
            property :paytable, coerce: Hash
            property :selected_numbers, coerce: SelectedIdList, from: :selected_id_list
            property :number_box, coerce: IdBox, from: :id_box
            property :free_game_meter, coerce: Hash
            property :score_board, coerce: ScoreBoard
          end

          class BetOption < Base
            property :name, coerce: String
            property :max_bet, coerce: Hash
            property :min_bet, coerce: Hash
          end

          class Player < Base
            property :action_win, coerce: Hash
            property :chip_index, coerce: Integer
          end

          property :table, coerce: Table
          property :bet_options, coerce: BetOption
          property :player, coerce: Player
          property :balance, coerce: Balance
          property :session_id, coerce: String
          property :instance_id, coerce: String
          property :test_mode, coerce: Common::Boolean, default: false
          property :system_completed, coerce: Common::Boolean
          property :info_panel, coerce: Hash
        end
      end
    end
  end
end
