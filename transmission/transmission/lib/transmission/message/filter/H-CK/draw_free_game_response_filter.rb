# frozen_string_literal: true

module Transmission
  module Message
    module Filter
      module HCk
        #
        # @author Richard Fong
        # @since 0.9.0
        #
        # Response schema for draw free game action
        #
        class DrawFreeGameResponseFilter < Base
          class Balance < Base
            property :cash, coerce: Integer
            property :credit, coerce: Integer
          end

          class IdBox < Base
            property :hit, coerce: Integer, from: :hit_counter
            property :result_numbers, coerce: Array[Integer], from: :result_id
            property :hit_free_game, coerce: Common::Boolean, default: false
          end

          class SelectedIdList < Base
            property :numbers, coerce: Array[Integer], from: :list
          end

          class ScoreCard < Base
            property :payout do
              property :name, coerce: String
              property :payout_amount, coerce: Hash
            end
            property :hit, coerce: Integer, from: :hit_counter
            property :odds, coerce: String
            property :win_level, coerce: String
            property :stage, coerce: Hash
          end

          class Table < Base
            property :round_info, coerce: Hash
            property :stage, coerce: Hash
            property :selected_numbers, coerce: SelectedIdList, from: :selected_id_list
            property :number_box, coerce: IdBox, from: :id_box
            property :free_game_meter, coerce: Hash
            property :score_card, coerce: ScoreCard
          end

          class BetOption < Base
            property :name, coerce: String
            property :bet_amount, coerce: Hash
            property :base_game_payout, coerce: Hash
            property :free_game_payout, coerce: Hash
            property :current_payout, coerce: Hash
            property :payout, coerce: Hash
          end

          property :table, coerce: Table
          property :bet_options, coerce: BetOption
          property :balance, coerce: Balance
          property :before_result_balance, coerce: Balance
          property :session_id, coerce: String
          property :info_panel, coerce: Hash
        end
      end
    end
  end
end
