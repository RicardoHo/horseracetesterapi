# frozen_string_literal: true

module Transmission
  module Message
    module Filter
      class Base < Hashie::Trash
        include Hashie::Extensions::IgnoreUndeclared
        include Hashie::Extensions::Dash::Coercion
      end
    end
  end
end

require_relative '../common/boolean'

Dir[File.join(__dir__, '**', '*.rb')].each { |file| require_relative file }
