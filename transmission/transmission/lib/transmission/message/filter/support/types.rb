# frozen_string_literal: true

module Transmission
  module Message
    module Filter
      module Types
        include ::Dry.Types

        Int     = Strict::Integer
        Nil     = Strict::Nil
        Hash    = Strict::Hash
        String  = Strict::String
        Boolean = Strict::Bool

        CoeInt  = Coercible::Integer
        CoeStr  = Coercible::String
        CoeHash = Coercible::Hash
        CoeBool = Params::Bool
        Float   = Strict::Float
      end
    end
  end
end
