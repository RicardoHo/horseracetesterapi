# frozen_string_literal: true

module Transmission
  module Message
    module Filter
      module HFc
        #
        # @author Alpha Huang
        # @since 0.7.0
        #
        # Response schema for roll action
        #
        class RollResponseFilter < Base
          class Balance < Base
            property :cash, coerce: Integer
            property :credit, coerce: Integer
          end

          class ScoreCard < Base
            property :point, coerce: Integer
            property :bet_option do
              property :name, coerce: String
              property :bet_item_value, coerce: Integer
              property :bet_amount, coerce: Hash
              property :payout, coerce: Hash
              property :odds, coerce: String
            end
            property :win_type, coerce: String
            property :win_level, coerce: String
          end

          class Table < Base
            property :round_info, coerce: Hash
            property :stage, coerce: Hash
            property :dices, coerce: Array[Integer]
            property :score_card, coerce: ScoreCard
          end

          class BetItem < Base
            property :value, coerce: Integer
          end

          class BetOption < Base
            property :name, coerce: String
            property :bet_amount, coerce: Hash
            property :payout, coerce: Hash
            property :type, coerce: String
            property :bet_item, coerce: BetItem
          end

          class Player < Base
            property :action_win, coerce: Hash
            property :chip_index, coerce: Integer
          end

          property :table, coerce: Table
          property :bet_options, coerce: Array[BetOption]
          property :player, coerce: Player
          property :balance, coerce: Balance
          property :before_result_balance, coerce: Balance
          property :session_id, coerce: String
          property :info_panel, coerce: Hash
        end
      end
    end
  end
end
