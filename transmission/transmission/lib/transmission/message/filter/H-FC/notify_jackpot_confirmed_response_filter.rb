# frozen_string_literal: true

module Transmission
  module Message
    module Filter
      module HFc
        #
        # @author Alpha Huang
        # @since 0.7.0
        #
        # Response schema for notify jackpot confirmed action
        #
        class NotifyJackpotConfirmedResponseFilter < Base
          class Table < Base
            property :round_info, coerce: Hash
            property :stage, coerce: Hash
          end

          class Balance < Base
            property :cash, coerce: Integer
            property :credit, coerce: Integer
          end

          property :table, coerce: Table
          property :balance, coerce: Balance
          property :session_id, coerce: String
        end
      end
    end
  end
end
