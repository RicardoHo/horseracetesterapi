# frozen_string_literal: true

module Transmission
  module Message
    module Filter
      module HFc
        #
        # @author Alpha Huang
        # @since 0.7.0
        #
        # Response schema for query game action resumed from roll
        #
        class QueryGameActWithRollResponseFilter < Base
          class Balance < Base
            property :mode, coerce: String
            property :currency, coerce: String
          end

          class DenomInfo < Base
            property :denoms, coerce: Array[Integer]
            property :index, coerce: Integer
          end

          class ScoreCard < Base
            property :point, coerce: Integer
            property :bet_option do
              property :name, coerce: String
              property :bet_item_value, coerce: Integer
              property :bet_credit, coerce: Integer
              property :odds, coerce: String
            end
            property :win_type, coerce: String
            property :win_level, coerce: String
          end

          class ScoreBoard < Base
            property :max_amount, coerce: Integer
            property :history, coerce: Array[ScoreCard]
          end

          class Table < Base
            property :round_info, coerce: Hash
            property :stage, coerce: Hash
            property :denom_info, coerce: DenomInfo
            property :dices, coerce: Array[Integer]
            property :score_board, coerce: Hash
          end

          class BetItem < Base
            property :range, coerce: String
            property :value, coerce: Integer
          end

          class BetOption < Base
            property :name, coerce: String
            property :max_bet, coerce: Hash
            property :min_bet, coerce: Hash
            property :bet_item, coerce: BetItem
          end

          class Player < Base
            property :action_win, coerce: Hash
            property :chip_index, coerce: Integer
          end

          property :table, coerce: Table
          property :bet_options, coerce: Array[BetOption]
          property :player, coerce: Player
          property :balance, coerce: Balance
          property :session_id, coerce: String
          property :instance_id, coerce: String
          property :test_mode, coerce: Common::Boolean, default: false
          property :system_completed, coerce: Common::Boolean
          property :info_panel, coerce: Hash
        end
      end
    end
  end
end
