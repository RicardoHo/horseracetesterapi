# frozen_string_literal: true

module Transmission
  module Message
    module Filter
      module HCh
        #
        # @author Alpha Huang
        # @since 0.3.0
        #
        # Response schema for deal action
        #
        class DealResponseFilter < Base
          class Table < Base
            property :round_info, coerce: Hash
            property :stage, coerce: Hash
            property :public_card, coerce: Array[String]
          end

          class BetOption < Base
            property :name, coerce: String
            property :bet_amount, coerce: Hash
            property :type, coerce: String
          end

          class Balance < Base
            property :cash, coerce: Integer
            property :credit, coerce: Integer
          end

          class Player < Base
            property :card, coerce: Array[String]
            property :rank, coerce: String
            property :aa_rank, coerce: String
            property :chip_index, coerce: Integer
          end

          property :table, coerce: Table
          property :bet_options, coerce: Array[BetOption]
          property :player, coerce: Player
          property :before_result_balance, coerce: Balance
          property :session_id, coerce: String
          property :info_panel, coerce: Hash
        end
      end
    end
  end
end
