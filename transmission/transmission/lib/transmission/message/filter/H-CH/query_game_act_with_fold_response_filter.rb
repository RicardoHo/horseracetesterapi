# frozen_string_literal: true

module Transmission
  module Message
    module Filter
      module HCh
        #
        # @author Alpha Huang
        # @since 0.3.0
        #
        # Response schema for fold action
        #
        class QueryGameActWithFoldResponseFilter < Base
          class Balance < Base
            property :mode, coerce: String
            property :currency, coerce: String
            property :cash, coerce: Integer
            property :credit, coerce: Integer
          end

          class DenomInfo < Base
            property :denoms, coerce: Array[Integer]
            property :index, coerce: Integer
          end

          class Table < Base
            property :round_info, coerce: Hash
            property :stage, coerce: Hash
            property :public_card, coerce: Array[String]
            property :denom_info, coerce: DenomInfo
          end

          class BetOption < Base
            property :name, coerce: String
            property :max_bet, coerce: Hash
            property :min_bet, coerce: Hash
            property :bet_amount, coerce: Hash
            property :payout, coerce: Hash
            property :type, coerce: String
            property :dealer_qualify, coerce: Common::Boolean
          end

          class Dealer < Base
            property :card, coerce: Array[String]
            property :hand, coerce: Array[String]
            property :rank, coerce: String
          end

          class Player < Base
            property :card, coerce: Array[String]
            property :hand, coerce: Array[String]
            property :rank, coerce: String
            property :aa_rank, coerce: String
            property :chip_index, coerce: Integer
          end

          property :table, coerce: Table
          property :bet_options, coerce: Array[BetOption]
          property :dealer, coerce: Dealer
          property :player, coerce: Player
          property :balance, coerce: Balance
          property :session_id, coerce: String
          property :system_completed, coerce: Common::Boolean
          property :instance_id, coerce: String
          property :test_mode, coerce: Common::Boolean, default: false
          property :info_panel, coerce: Hash
        end
      end
    end
  end
end
