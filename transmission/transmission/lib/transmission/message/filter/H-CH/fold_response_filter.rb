# frozen_string_literal: true

module Transmission
  module Message
    module Filter
      module HCh
        #
        # @author Alpha Huang
        # @since 0.3.0
        #
        # Response schema for fold action
        #
        class FoldResponseFilter < Base
          class Balance < Base
            property :cash, coerce: Integer
            property :credit, coerce: Integer
          end

          class Table < Base
            property :round_info, coerce: Hash
            property :stage, coerce: Hash
            property :public_card, coerce: Array[String]
          end

          class BetOption < Base
            property :name, coerce: String
            property :bet_amount, coerce: Hash
            property :payout, coerce: Hash
            property :type, coerce: String
            property :dealer_qualify, coerce: Common::Boolean
          end

          class Dealer < Base
            property :card, coerce: Array[String]
            property :hand, coerce: Array[String]
            property :rank, coerce: String
          end

          class Player < Base
            property :card, coerce: Array[String]
            property :hand, coerce: Array[String]
            property :rank, coerce: String
            property :aa_rank, coerce: String
          end

          property :table, coerce: Table
          property :bet_options, coerce: Array[BetOption]
          property :dealer, coerce: Dealer
          property :player, coerce: Player
          property :balance, coerce: Balance
          property :session_id, coerce: String
        end
      end
    end
  end
end
