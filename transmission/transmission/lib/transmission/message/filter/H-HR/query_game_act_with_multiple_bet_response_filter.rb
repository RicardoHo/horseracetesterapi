# frozen_string_literal: true

module Transmission
  module Message
    module Filter
      module HHr
        #
        # @author ben.b.wu
        # @since 0.1.0
        #
        # Response schema for query game action
        # act with multiple bet (resume from multiple bet)
        #
        class QueryGameActWithMultipleBetResponseFilter < Base
          class Balance < Base
            property :mode, coerce: String
            property :currency, coerce: String
          end

          class DenomInfo < Base
            property :denoms, coerce: Array[Integer]
            property :index, coerce: Integer
          end

          class MultiplierInfo < Base
            property :multipliers, coerce: Array[Integer]
            property :index, coerce: Integer
          end

          class Table < Base
            property :round_info, coerce: Hash
            property :stage, coerce: Hash
            property :denom_info, coerce: DenomInfo
            property :multiplier_info, coerce: MultiplierInfo
            property :unit_bet, coerce: Hash
            property :hr_paytable, coerce: Hash
            # property :paytable, coerce: Hash, from: :paytable
            property :max_bet, coerce: Hash
            property :min_bet, coerce: Hash

            property :play_mode, coerce: String
            property :win_indices, coerce: Array
            property :win_reference, coerce: Array
            property :win_options, coerce: Array
            property :video_file, coerce: String
            property :total_bet_amt, coerce: Integer
            property :total_payout_amt, coerce: Integer
            property :payout_details, coerce: Hash
            property :last_bet, coerce: Hash
            property :last_bet_options, coerce: Hash
            property :score_board, coerce: Hash
          end

          class BetOption < Base
            property :name, coerce: String
            property :max_bet, coerce: Hash
            property :min_bet, coerce: Hash
          end

          property :table, coerce: Table
          #property :bet_options, coerce: BetOption
          #property :bet_options, coerce: Array
          #property :bet_options, coerce: Array[Hash], transform_with: ->(bet_options) do
          #  bet_options.select {|bet| bet.key?(:bet_amount)}
          #end

          property :balance, coerce: Balance
          property :denom_info, coerce: DenomInfo
          property :session_id, coerce: String
          property :instance_id, coerce: String
          #property :system_completed, coerce: Common::Boolean
          property :test_mode, coerce: Common::Boolean, default: false
          #property :info_panel, coerce: Hash
        end
      end
    end
  end
end
