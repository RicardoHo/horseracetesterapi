# frozen_string_literal: true

module Transmission
  module Message
    module Filter
      module HEb
        #
        # @author Alpha Huang
        # @since 0.3.0
        #
        # Response schema for reshuffle action
        #
        class ReshuffleResponseFilter < Base
          class ShoeInfo < Base
            property :id, coerce: Integer
            property :remaining_amount, coerce: Integer
            property :burn, coerce: Hash
          end

          class Table < Base
            property :round_info, coerce: Hash
            property :stage, coerce: Hash
            property :shoe, coerce: ShoeInfo
          end

          property :table, coerce: Table
        end
      end
    end
  end
end
