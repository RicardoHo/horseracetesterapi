# frozen_string_literal: true

module Transmission
  module Message
    module Filter
      module HEb
        #
        # @author Alpha Huang
        # @since 0.3.0
        #
        # Response schema for deal action
        #
        class BaccaratDealResponseFilter < Base
          class ShoeInfo < Base
            property :id, coerce: Integer
            property :remaining_amount, coerce: Integer
            property :cut_card_pos, coerce: Integer
            property :cut_card_reached, coerce: Common::Boolean
          end

          class HandScoreCardInfo < Base
            property :winner, coerce: String
            property :pair, coerce: String
            property :nature, coerce: String
          end

          class Table < Base
            property :round_info, coerce: Hash
            property :stage, coerce: Hash
            property :shoe, coerce: ShoeInfo
            property :hand_score_card, coerce: HandScoreCardInfo
          end

          class BetOption < Base
            property :name, coerce: String
            property :bet_amount, coerce: Hash
            property :payout, coerce: Hash
            property :type, coerce: String
          end

          class Balance < Base
            property :mode, coerce: String
            property :currency, coerce: String
            property :cash, coerce: Integer
            property :credit, coerce: Integer
          end

          class Banker < Base
            property :card, coerce: Array[String]
            property :point, coerce: Integer
          end

          class Player < Banker
            property :action_win, coerce: Hash
            property :chip_index, coerce: Integer
          end

          property :table, coerce: Table
          property :bet_options, coerce: Array[BetOption]
          property :player, coerce: Player
          property :banker, coerce: Banker
          property :balance, coerce: Balance
          property :before_result_balance, coerce: Balance
          property :info_panel, coerce: Hash
        end
      end
    end
  end
end
