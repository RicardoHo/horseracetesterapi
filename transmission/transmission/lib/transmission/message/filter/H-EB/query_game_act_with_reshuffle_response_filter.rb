# frozen_string_literal: true

module Transmission
  module Message
    module Filter
      module HEb
        #
        # @author Alpha Huang
        # @since 0.3.0
        #
        # Response schema for query game action resumed from reshuffle
        #
        class QueryGameActWithReshuffleResponseFilter < Base
          class Balance < Base
            property :mode, coerce: String
            property :currency, coerce: String
          end

          class DenomInfo < Base
            property :denoms, coerce: Array[Integer]
            property :index, coerce: Integer
          end

          class ShoeInfo < Base
            property :id, coerce: Integer
            property :remaining_amount, coerce: Integer
          end

          class Table < Base
            property :round_info, coerce: Hash
            property :stage, coerce: Hash
            property :denom_info, coerce: DenomInfo
            property :shoe, coerce: ShoeInfo
            property :max_bet, coerce: Hash
            property :min_bet, coerce: Hash
            property :enquiry_bet_indicator, coerce: Hash
          end

          class BetOption < Base
            property :name, coerce: String
            property :max_bet, coerce: Hash
            property :min_bet, coerce: Hash
          end

          class Player < Base
            property :chip_index, coerce: Integer
          end

          property :table, coerce: Table
          property :bet_options, coerce: Array[BetOption]
          property :balance, coerce: Balance
          property :player, coerce: Player
          property :session_id, coerce: String
          property :instance_id, coerce: String
          property :system_completed, coerce: Common::Boolean
          property :test_mode, coerce: Common::Boolean, default: false
        end
      end
    end
  end
end
