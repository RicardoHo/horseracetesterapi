# frozen_string_literal: true

module Transmission
  module Message
    module Filter
      module HEb
        #
        # @author Alpha Huang
        # @since 0.3.0
        #
        # Response schema for skip hand action
        #
        class SkipHandResponseFilter < Base
          class ShoeInfo < Base
            property :id, coerce: Integer
            property :remaining_amount, coerce: Integer
          end

          class HandScoreCardInfo < Base
            property :winner, coerce: String
            property :pair, coerce: String
            property :nature, coerce: String
          end

          class Table < Base
            property :round_info, coerce: Hash
            property :stage, coerce: Hash
            property :shoe, coerce: ShoeInfo
            property :hand_score_card, coerce: HandScoreCardInfo
          end

          class Player < Base
            property :card, coerce: Array[String]
            property :point, coerce: Integer
          end

          property :table, coerce: Table
          property :player, coerce: Player
          property :banker, coerce: Player
        end
      end
    end
  end
end
