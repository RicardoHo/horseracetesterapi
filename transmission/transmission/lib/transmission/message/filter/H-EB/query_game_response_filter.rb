# frozen_string_literal: true

module Transmission
  module Message
    module Filter
      module HEb
        #
        # @author Alpha Huang
        # @since 0.3.0
        #
        # Response schema for query game action
        #
        class QueryGameResponseFilter < Base
          class Balance < Base
            property :mode, coerce: String
            property :currency, coerce: String
          end

          class DenomInfo < Base
            property :denoms, coerce: Array[Integer]
            property :index, coerce: Integer
          end

          class ShoeInfo < Base
            property :id, coerce: Integer
            property :remaining_amount, coerce: Integer
            property :burn, coerce: Hash
          end

          class HandScoreCardInfo < Base
            property :winner, coerce: String
            property :pair, coerce: String
            property :nature, coerce: String
          end

          class ShoeScoreBoardInfo < Base
            property :shoe_id, coerce: Integer
            property :history, coerce: Array[HandScoreCardInfo]
          end

          class Table < Base
            property :round_info, coerce: Hash
            property :stage, coerce: Hash
            property :denom_info, coerce: DenomInfo
            property :shoe, coerce: ShoeInfo
            property :shoe_score_board, coerce: ShoeScoreBoardInfo
            property :max_bet, coerce: Hash
            property :min_bet, coerce: Hash
            property :enquiry_bet_indicator, coerce: Hash
          end

          class BetOption < Base
            property :name, coerce: String
            property :max_bet, coerce: Hash
            property :min_bet, coerce: Hash
          end

          property :table, coerce: Table
          property :bet_options, coerce: Array[BetOption]
          property :balance, coerce: Balance
          property :session_id, coerce: String
          property :instance_id, coerce: String
          property :system_completed, coerce: Common::Boolean
          property :test_mode, coerce: Common::Boolean, default: false
          property :info_panel, coerce: Hash
        end
      end
    end
  end
end
