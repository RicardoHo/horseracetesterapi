# frozen_string_literal: true

module Transmission
  module Message
    module Filter
      module HEb
        #
        # @author Alpha Huang
        # @since 0.3.0
        #
        # Response schema for query game action
        #
        class QueryGameActWithBaccaratDealResponseFilter < Base
          class Balance < Base
            property :mode, coerce: String
            property :currency, coerce: String
          end

          class DenomInfo < Base
            property :denoms, coerce: Array[Integer]
            property :index, coerce: Integer
          end

          class ShoeInfo < Base
            property :id, coerce: Integer
            property :remaining_amount, coerce: Integer
            property :cut_card_pos, coerce: Integer
            property :cut_card_reached, coerce: Common::Boolean
          end

          class HandScoreCardInfo < Base
            property :winner, coerce: String
            property :pair, coerce: String
            property :nature, coerce: String
          end

          class ShoeScoreBoardInfo < Base
            property :shoe_id, coerce: Integer
            property :history, coerce: Array[HandScoreCardInfo]
          end

          class Table < Base
            property :round_info, coerce: Hash
            property :stage, coerce: Hash
            property :denom_info, coerce: DenomInfo
            property :shoe, coerce: ShoeInfo
            property :shoe_score_board, coerce: ShoeScoreBoardInfo
            property :enquiry_bet_indicator, coerce: Hash
            property :max_bet, coerce: Hash
            property :min_bet, coerce: Hash
          end

          class BetOption < Base
            property :name, coerce: String
            property :max_bet, coerce: Hash
            property :min_bet, coerce: Hash
            property :bet_amount, coerce: Hash
            property :payout, coerce: Hash
            property :type, coerce: String
          end

          class Banker < Base
            property :card, coerce: Array[String]
            property :point, coerce: Integer
          end

          class Player < Banker
            property :action_win, coerce: Hash
            property :chip_index, coerce: Integer
          end

          property :table, coerce: Table
          property :bet_options, coerce: Array[BetOption]
          property :player, coerce: Player
          property :banker, coerce: Banker
          property :balance, coerce: Balance
          property :session_id, coerce: String
          property :instance_id, coerce: String
          property :system_completed, coerce: Common::Boolean
          property :test_mode, coerce: Common::Boolean, default: false
          property :info_panel, coerce: Hash
        end
      end
    end
  end
end
