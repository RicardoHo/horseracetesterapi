# frozen_string_literal: true

module Transmission
  module Message
    module Filter
      module HSg
        #
        # @author Richard Fong
        # @since 0.9.0
        #
        # Response schema for query game action
        # act with multiple bet (resume from multiple bet)
        #
        class QueryGameActWithMultipleBetResponseFilter < Base
          class Balance < Base
            property :mode, coerce: String
            property :currency, coerce: String
          end

          class DenomInfo < Base
            property :denoms, coerce: Array[Integer]
            property :index, coerce: Integer
          end

          class MultiplierInfo < Base
            property :multipliers, coerce: Array[Integer]
            property :index, coerce: Integer
          end

          class ScoreCard < Base
            property :winner, coerce: String
            property :point, coerce: Integer
            property :win_type, coerce: String
          end

          class ScoreCardWithPayoutList < Base
            property :winner, coerce: String
            property :point, coerce: Integer
            property :payout_list, coerce: Array[String], transform_with: ->(payout_list) do
              payout_list.delete_if { |win_type| win_type.casecmp?('total_zero') }
            end
            property :win_type, coerce: String
          end

          class ScoreBoard < Base
            property :max_amount, coerce: Integer
            property :history, coerce: Array[ScoreCard]
          end

          class Table < Base
            property :round_info, coerce: Hash
            property :stage, coerce: Hash
            property :denom_info, coerce: DenomInfo
            property :multiplier_info, coerce: MultiplierInfo
            property :unit_bet, coerce: Hash
            property :paytable, coerce: Hash
            property :max_bet, coerce: Hash
            property :min_bet, coerce: Hash
            property :score_board, coerce: ScoreBoard
            property :score_card, coerce: ScoreCardWithPayoutList
            property :action_win, coerce: Hash
          end

          class BetOption < Base
            property :name, coerce: String
            property :max_bet, coerce: Hash
            property :min_bet, coerce: Hash
            property :bet_amount, coerce: Hash
            property :payout, coerce: Hash
          end

          class Player < Base
            property :card, coerce: Array[String]
            property :point, coerce: Integer
          end

          property :table, coerce: Table
          property :bet_options, coerce: Array[BetOption]
          property :dragon, coerce: Player
          property :phoenix, coerce: Player
          property :balance, coerce: Balance
          property :session_id, coerce: String
          property :system_completed, coerce: Common::Boolean
          property :instance_id, coerce: String
          property :test_mode, coerce: Common::Boolean, default: false
          property :info_panel, coerce: Hash
        end
      end
    end
  end
end
