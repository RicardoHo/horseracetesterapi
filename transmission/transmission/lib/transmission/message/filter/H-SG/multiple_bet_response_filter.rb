# frozen_string_literal: true

module Transmission
  module Message
    module Filter
      module HSg
        #
        # @author Richard Fong
        # @since 0.9.0
        #
        # Response schema for multiple bet action
        #
        class MultipleBetResponseFilter < Base
          class Balance < Base
            property :cash, coerce: Integer
            property :credit, coerce: Integer
          end

          class ScoreCard < Base
            property :winner, coerce: String
            property :point, coerce: Integer
            property :payout_list, coerce: Array[String], transform_with: ->(payout_list) do
              payout_list.delete_if { |win_type| win_type.casecmp?('total_zero') }
            end
            property :win_type, coerce: String
          end

          class Table < Base
            property :round_info, coerce: Hash
            property :stage, coerce: Hash
            property :score_card, coerce: ScoreCard
            property :action_win, coerce: Hash
          end

          class BetOption < Base
            property :name, coerce: String
            property :bet_amount, coerce: Hash
            property :payout, coerce: Hash
            property :type, coerce: String
          end

          class Player < Base
            property :card, coerce: Array[String]
            property :point, coerce: Integer
          end

          property :table, coerce: Table
          property :bet_options, coerce: Array[BetOption]
          property :dragon, coerce: Player
          property :phoenix, coerce: Player
          property :balance, coerce: Balance
          property :before_result_balance, coerce: Balance
          property :session_id, coerce: String
          property :info_panel, coerce: Hash
        end
      end
    end
  end
end
