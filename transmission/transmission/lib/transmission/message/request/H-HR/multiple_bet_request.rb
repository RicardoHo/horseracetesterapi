# frozen_string_literal: true

module Transmission
  module Message
    module Request
      module HHr
        #
        # @author Ben B Wu
        # @since 0.1.0
        #
        # Horse racing bet request
        #
        class MultipleBetRequest < Base
          class BetAmount < Base
            #property :cash, coerce: Integer
            property :credit, coerce: Integer
          end

          class BetOption < Base
            property :name, coerce: String
            property :bet_amount, coerce: BetAmount
            #property :payout, coerce: Hash
            #property :type, coerce: String
          end

          class Player < Base
            property :rank, coerce: String
            property :action_win, coerce: Hash
            property :card, coerce: Array[String]
            property :chip_index, coerce: Integer
          end

          #property :instance_info, coerce: Hash
          property :round_info, coerce: Hash
          #property :inspect_data, coerce: Hash
          property :denom, coerce: Integer
          #property :multiplier, coerce: Integer
          property :bet_options, coerce: Array[BetOption]
          property :play_mode, coerce: String
          #property :player, coerce: Hash
        end
      end
    end
  end
end
