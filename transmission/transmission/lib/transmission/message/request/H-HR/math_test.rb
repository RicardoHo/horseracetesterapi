# frozen_string_literal: true

module Transmission
  module Message
    module Request
      module HHr
        #
        # @author ben.b.wu
        # @since 0.1.0
        #
        # Response schema for query game action
        #
        class MathTestRequest < Base
          class InstanceInfo < Base
            property :id, coerce: String
          end

          property :player_token, coerce: String
          property :game_id, coerce: Integer
          property :property_id, coerce: Integer
          property :instance_info, coerce: InstanceInfo
          property :machine_token, coerce: String
          property :login_name, coerce: String
        end
      end
    end
  end
end
