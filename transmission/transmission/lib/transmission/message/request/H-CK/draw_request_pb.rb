# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: transmission/message/request/H-CK/draw_request.proto

require 'google/protobuf'

require 'transmission/message/shared/instance_info_pb'
require 'transmission/message/shared/round_info_pb'
require 'transmission/message/shared/balance_pb'
require 'transmission/message/shared/bet_option_with_bet_item_pb'
require 'transmission/message/shared/player_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_file("transmission/message/request/H-CK/draw_request.proto", :syntax => :proto3) do
    add_message "transmission.message.request.h_ck.InspectData" do
      optional :result_id, :string, 1
    end
    add_message "transmission.message.request.h_ck.DrawRequest" do
      optional :instance_info, :message, 1, "transmission.message.shared.InstanceInfo"
      optional :round_info, :message, 2, "transmission.message.shared.RoundInfo"
      optional :balance, :message, 3, "transmission.message.shared.Balance"
      repeated :bet_options, :message, 4, "transmission.message.shared.BetOptionWithBetItem"
      optional :denom, :int32, 5
      optional :inspect_data, :message, 6, "transmission.message.request.h_ck.InspectData"
      optional :player, :message, 7, "transmission.message.shared.Player"
      repeated :selected_id_list, :int32, 8
      optional :multiplier, :int32, 9
    end
  end
end

module Transmission
  module Message
    module Request
      module HCk
        InspectData = ::Google::Protobuf::DescriptorPool.generated_pool.lookup("transmission.message.request.h_ck.InspectData").msgclass
        DrawRequest = ::Google::Protobuf::DescriptorPool.generated_pool.lookup("transmission.message.request.h_ck.DrawRequest").msgclass
      end
    end
  end
end
