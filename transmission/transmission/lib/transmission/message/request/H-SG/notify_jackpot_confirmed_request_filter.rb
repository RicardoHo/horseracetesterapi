# frozen_string_literal: true

module Transmission
  module Message
    module Request
      module HSg
        #
        # @author Richard Fong
        # @since 0.9.0
        #
        # Response schema for notify jackpot confirmed action
        #
        class NotifyJackpotConfirmedRequest < Base
          class Table < Base
            property :round_info, coerce: Hash
            property :stage, coerce: Hash
          end

          class Balance < Base
            property :cash, coerce: Integer
            property :credit, coerce: Integer
          end

          property :table, coerce: Table
          property :balance, coerce: Balance
          property :session_id, coerce: String
        end
      end
    end
  end
end
