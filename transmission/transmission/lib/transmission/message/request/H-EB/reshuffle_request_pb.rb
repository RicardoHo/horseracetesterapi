# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: transmission/message/request/H-EB/reshuffle_request.proto

require 'google/protobuf'

require 'transmission/message/shared/instance_info_pb'
require 'transmission/message/shared/round_info_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_file("transmission/message/request/H-EB/reshuffle_request.proto", :syntax => :proto3) do
    add_message "transmission.message.request.h_eb.ReshuffleRequest" do
      optional :instance_info, :message, 1, "transmission.message.shared.InstanceInfo"
      optional :round_info, :message, 2, "transmission.message.shared.RoundInfo"
    end
  end
end

module Transmission
  module Message
    module Request
      module HEb
        ReshuffleRequest = ::Google::Protobuf::DescriptorPool.generated_pool.lookup("transmission.message.request.h_eb.ReshuffleRequest").msgclass
      end
    end
  end
end
