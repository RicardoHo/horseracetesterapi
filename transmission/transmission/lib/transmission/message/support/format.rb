# frozen_string_literal: true

module Transmission
  module Message
    module Format
      BINARY = { format: 'binary' }.to_json.unpack('C*')
      JSON   = { format: 'json' }.to_json
    end
  end
end
