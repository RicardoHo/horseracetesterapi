# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: transmission/message/response/H-CK/draw_response.proto

require 'google/protobuf'

require 'transmission/message/shared/round_info_pb'
require 'transmission/message/shared/stage_pb'
require 'transmission/message/shared/amount_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_file("transmission/message/response/H-CK/draw_response.proto", :syntax => :proto3) do
    add_message "transmission.message.response.h_ck.DrawResponse" do
      optional :table, :message, 1, "transmission.message.response.h_ck.DrawResponse.Table"
      repeated :bet_options, :message, 2, "transmission.message.response.h_ck.DrawResponse.BetOption"
      optional :player, :message, 3, "transmission.message.response.h_ck.DrawResponse.Player"
      optional :balance, :message, 4, "transmission.message.response.h_ck.DrawResponse.Balance"
      optional :before_result_balance, :message, 5, "transmission.message.response.h_ck.DrawResponse.Balance"
      optional :session_id, :string, 6
    end
    add_message "transmission.message.response.h_ck.DrawResponse.Balance" do
      optional :cash, :int32, 1
      optional :credit, :int32, 2
    end
    add_message "transmission.message.response.h_ck.DrawResponse.ScoreCard" do
      optional :point, :int32, 1
      optional :bet_option, :message, 2, "transmission.message.response.h_ck.DrawResponse.ScoreCard.BetOption"
      optional :win_type, :string, 3
      optional :win_level, :string, 4
    end
    add_message "transmission.message.response.h_ck.DrawResponse.ScoreCard.BetOption" do
      optional :name, :string, 1
      optional :bet_item_value, :int32, 2
      optional :bet_amount, :message, 3, "transmission.message.shared.Amount"
      optional :payout, :message, 4, "transmission.message.shared.Amount"
      optional :odds, :string, 5
    end
    add_message "transmission.message.response.h_ck.DrawResponse.Table" do
      optional :round_info, :message, 1, "transmission.message.shared.RoundInfo"
      optional :stage, :message, 2, "transmission.message.shared.Stage"
      repeated :dices, :int32, 3
      optional :score_card, :message, 4, "transmission.message.response.h_ck.DrawResponse.ScoreCard"
    end
    add_message "transmission.message.response.h_ck.DrawResponse.BetOption" do
      optional :name, :string, 1
      optional :bet_amount, :message, 2, "transmission.message.shared.Amount"
      optional :payout, :message, 3, "transmission.message.shared.Amount"
      optional :type, :string, 4
      optional :bet_item, :message, 5, "transmission.message.response.h_ck.DrawResponse.BetOption.BetItem"
    end
    add_message "transmission.message.response.h_ck.DrawResponse.BetOption.BetItem" do
      optional :value, :int32, 1
    end
    add_message "transmission.message.response.h_ck.DrawResponse.Player" do
      optional :action_win, :message, 1, "transmission.message.shared.Amount"
      optional :chip_index, :int32, 2
    end
  end
end

module Transmission
  module Message
    module Response
      module HCk
        DrawResponse = ::Google::Protobuf::DescriptorPool.generated_pool.lookup("transmission.message.response.h_ck.DrawResponse").msgclass
        DrawResponse::Balance = ::Google::Protobuf::DescriptorPool.generated_pool.lookup("transmission.message.response.h_ck.DrawResponse.Balance").msgclass
        DrawResponse::ScoreCard = ::Google::Protobuf::DescriptorPool.generated_pool.lookup("transmission.message.response.h_ck.DrawResponse.ScoreCard").msgclass
        DrawResponse::ScoreCard::BetOption = ::Google::Protobuf::DescriptorPool.generated_pool.lookup("transmission.message.response.h_ck.DrawResponse.ScoreCard.BetOption").msgclass
        DrawResponse::Table = ::Google::Protobuf::DescriptorPool.generated_pool.lookup("transmission.message.response.h_ck.DrawResponse.Table").msgclass
        DrawResponse::BetOption = ::Google::Protobuf::DescriptorPool.generated_pool.lookup("transmission.message.response.h_ck.DrawResponse.BetOption").msgclass
        DrawResponse::BetOption::BetItem = ::Google::Protobuf::DescriptorPool.generated_pool.lookup("transmission.message.response.h_ck.DrawResponse.BetOption.BetItem").msgclass
        DrawResponse::Player = ::Google::Protobuf::DescriptorPool.generated_pool.lookup("transmission.message.response.h_ck.DrawResponse.Player").msgclass
      end
    end
  end
end
