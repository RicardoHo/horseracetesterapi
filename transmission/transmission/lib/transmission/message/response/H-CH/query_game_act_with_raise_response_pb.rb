# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: transmission/message/response/H-CH/query_game_act_with_raise_response.proto

require 'google/protobuf'

require 'transmission/message/shared/round_info_pb'
require 'transmission/message/shared/stage_pb'
require 'transmission/message/shared/denom_info_pb'
require 'transmission/message/shared/amount_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_file("transmission/message/response/H-CH/query_game_act_with_raise_response.proto", :syntax => :proto3) do
    add_message "transmission.message.response.h_ch.QueryGameActWithRaiseResponse" do
      optional :table, :message, 1, "transmission.message.response.h_ch.QueryGameActWithRaiseResponse.Table"
      repeated :bet_options, :message, 2, "transmission.message.response.h_ch.QueryGameActWithRaiseResponse.BetOption"
      optional :dealer, :message, 3, "transmission.message.response.h_ch.QueryGameActWithRaiseResponse.Dealer"
      optional :player, :message, 4, "transmission.message.response.h_ch.QueryGameActWithRaiseResponse.Player"
      optional :balance, :message, 5, "transmission.message.response.h_ch.QueryGameActWithRaiseResponse.Balance"
      optional :session_id, :string, 6
      optional :system_completed, :bool, 7
    end
    add_message "transmission.message.response.h_ch.QueryGameActWithRaiseResponse.Balance" do
      optional :mode, :string, 1
      optional :currency, :string, 2
      optional :cash, :int32, 3
      optional :credit, :int32, 4
    end
    add_message "transmission.message.response.h_ch.QueryGameActWithRaiseResponse.Table" do
      optional :round_info, :message, 1, "transmission.message.shared.RoundInfo"
      optional :stage, :message, 2, "transmission.message.shared.Stage"
      optional :denom_info, :message, 3, "transmission.message.shared.DenomInfo"
      repeated :public_card, :string, 4
    end
    add_message "transmission.message.response.h_ch.QueryGameActWithRaiseResponse.BetOption" do
      optional :name, :string, 1
      optional :max_bet, :message, 2, "transmission.message.shared.Amount"
      optional :min_bet, :message, 3, "transmission.message.shared.Amount"
      optional :bet_amount, :message, 4, "transmission.message.shared.Amount"
      optional :payout, :message, 5, "transmission.message.shared.Amount"
      optional :type, :string, 6
      optional :dealer_qualify, :bool, 7
    end
    add_message "transmission.message.response.h_ch.QueryGameActWithRaiseResponse.Dealer" do
      optional :rank, :string, 1
      repeated :hand, :string, 2
      repeated :card, :string, 3
    end
    add_message "transmission.message.response.h_ch.QueryGameActWithRaiseResponse.Player" do
      optional :rank, :string, 1
      optional :aa_rank, :string, 2
      repeated :card, :string, 3
      repeated :hand, :string, 4
      optional :action_win, :message, 5, "transmission.message.shared.Amount"
    end
  end
end

module Transmission
  module Message
    module Response
      module HCh
        QueryGameActWithRaiseResponse = ::Google::Protobuf::DescriptorPool.generated_pool.lookup("transmission.message.response.h_ch.QueryGameActWithRaiseResponse").msgclass
        QueryGameActWithRaiseResponse::Balance = ::Google::Protobuf::DescriptorPool.generated_pool.lookup("transmission.message.response.h_ch.QueryGameActWithRaiseResponse.Balance").msgclass
        QueryGameActWithRaiseResponse::Table = ::Google::Protobuf::DescriptorPool.generated_pool.lookup("transmission.message.response.h_ch.QueryGameActWithRaiseResponse.Table").msgclass
        QueryGameActWithRaiseResponse::BetOption = ::Google::Protobuf::DescriptorPool.generated_pool.lookup("transmission.message.response.h_ch.QueryGameActWithRaiseResponse.BetOption").msgclass
        QueryGameActWithRaiseResponse::Dealer = ::Google::Protobuf::DescriptorPool.generated_pool.lookup("transmission.message.response.h_ch.QueryGameActWithRaiseResponse.Dealer").msgclass
        QueryGameActWithRaiseResponse::Player = ::Google::Protobuf::DescriptorPool.generated_pool.lookup("transmission.message.response.h_ch.QueryGameActWithRaiseResponse.Player").msgclass
      end
    end
  end
end
