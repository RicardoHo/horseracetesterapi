# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: transmission/message/entry.proto

require 'google/protobuf'

Google::Protobuf::DescriptorPool.generated_pool.build do
  add_file("transmission/message/entry.proto", :syntax => :proto3) do
    add_message "transmission.message.Entry" do
      optional :action, :string, 1
      optional :data_type, :string, 2
      optional :data, :bytes, 3
    end
  end
end

module Transmission
  module Message
    Entry = ::Google::Protobuf::DescriptorPool.generated_pool.lookup("transmission.message.Entry").msgclass
  end
end
