# frozen_string_literal: true

Clutch::System::EntitySystem.namespace_for_watch = :'transmission.component'

# require all system's file here
Dir[File.join(__dir__, 'system', '**.rb')].each { |file| require file }
