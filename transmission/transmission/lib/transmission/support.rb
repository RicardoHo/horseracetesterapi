# frozen_string_literal: true

# require all support's file here
Dir[File.join(__dir__, 'support', '**.rb')].each { |file| require file }
