# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.7.0
    #
    # Bet item of a bet option (area),
    # select one value from a range
    #
    class ValueTypeBetItem < Clutch::Component
      pack :property, name: :bet_item

      attribute :value
      attr_reader :range
      property :range, getter: proc { |represented:, **| represented.range.to_s }

      def initialize(range)
        @range = range
      end

      def load(data)
        @value = data.dig(:value)
        @dirty = false
      end
    end
  end
end
