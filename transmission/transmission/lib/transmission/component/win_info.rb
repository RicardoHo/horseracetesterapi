# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # The win info component
    #
    class WinInfo < Clutch::Component
      attribute :type
      attribute :odds
      attribute :dealer_qualify
    end
  end
end
