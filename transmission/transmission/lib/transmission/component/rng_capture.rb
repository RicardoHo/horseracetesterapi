# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Ivan Lao
    # @since 1.0.0.rc1
    #
    #
    #
    class RngCapture < Clutch::Component
      pack :property, name: :RNG

      attribute :result_list, [], type: :collection, as: :rng_result
      attribute :id, '2_', type: :property, as: :rng_ref_id

      #
      # record rng result to the result list
      #
      # @param [Array] result
      #
      def record(result)
        result_list.push(result)
      end
    end
  end
end
