# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.6.0
    #
    # The payout index component for lottery
    #
    class PayoutIndex < Clutch::Component
      attribute :value, nil
    end
  end
end
