# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Use to store cards and rank of a aa rank
    #
    class AaRank < Clutch::Component
      attribute :cards, as: :aa_card
      attribute :rank, as: :aa_rank
    end
  end
end
