# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Richard Fong
    # @since 0.8.0
    #
    # Box info describe which pocket
    # the ball finally drop down
    #
    class PocketInfo < Clutch::Component
      pack :property, name: :pocket_info

      attribute :pocket_index, as: :index
      attribute :odds
    end
  end
end
