# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # A burn card indicator indicates the table will burns card
    # after reshuffle and contains burn infomation
    #
    class BurnCardIndicator < Clutch::Component
      pack :property, name: :'shoe.burn'

      attribute :draw
      attribute :total
    end
  end
end
