# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Richard Fong
    # @since 0.8.0
    #
    # Save the data about the selected risk level
    #
    class RiskLevel < Clutch::Component
      pack :property, name: :risk_level

      attribute :levels, [],
        type: :collection
      attribute :risk_level_index, as: :index

      def initialize(levels, risk_level_index)
        @levels           = levels
        @risk_level_index = risk_level_index
      end

      def load(data)
        @levels           = data[:levels]
        @risk_level_index = data[:risk_level_index]
      end
    end
  end
end
