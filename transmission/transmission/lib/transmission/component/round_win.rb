# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Win amount on a round
    #
    class RoundWin < Clutch::Component
      pack :property, name: :round_win

      attribute :cash, 0
      attribute :credit, 0
    end
  end
end
