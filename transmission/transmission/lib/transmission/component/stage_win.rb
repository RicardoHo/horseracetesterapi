# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Win amount on a stage
    #
    class StageWin < Clutch::Component
      pack :property, name: :stage_win

      attribute :cash, 0
      attribute :credit, 0
    end
  end
end
