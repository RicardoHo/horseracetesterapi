# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # The bet amount component
    #
    class BetAmount < Clutch::Component
      attribute :cash
      attribute :credit

      pack :property, name: :bet_amount
    end
  end
end
