# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 1.0.0
    #
    # Chip component represent the chip in table game
    #
    class Chip < Clutch::Component
      attribute :selected_index, as: :chip_index
    end
  end
end
