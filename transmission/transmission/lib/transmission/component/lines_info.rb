# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Richard Fong
    # @since 0.8.0
    #
    # Save the data number of lines
    #
    class LinesInfo < Clutch::Component
      pack :property, name: :lines_info

      attribute :lines
      attribute :line_index, as: :index
      attribute :line

      def initialize(lines, line_index)
        @lines      = lines
        @line_index = line_index
      end
    end
  end
end
