# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.1.0
    # revamp since 0.3.0
    #
    # Shoe contains multiple deck of cards and cut card,
    # and each shoe has its own id
    # cards are stores as card pool which describe the amount of each card
    #
    class Shoe < Clutch::Component
      pack :property, name: :shoe

      attribute :id
      # @return [Hash<Symbol, Integer>]
      # @example
      #   { 'As': 8, 'Ad': 8, 'Ac': 8, 'Ah': 8 ...... }
      attribute :card_pool

      attr_reader :num_of_deck
      attr_reader :cut_card_valid_range
      attr_reader :cut_card_range
      attr_reader :cut_card_pos
      property :cut_card_pos
      property :cut_card_reached,
        getter: ->(represented:, **) { represented.reach_cut_card? }
      property :remaining_amount

      def initialize(num_of_deck, valid_range)
        @id                   = 0
        @num_of_deck          = num_of_deck
        @cut_card_valid_range = valid_range
      end

      #
      # init the cut card range value
      #
      # @param [Range] range
      #
      def init_cut_card_range(range)
        @cut_card_range = range
      end

      #
      # check is the shoe is empty
      #
      # @return [Boolean]
      #
      def empty?
        @card_pool.nil?
      end

      #
      # update the shoe by pass a updated card pool
      #
      # @param [Hash<Symbol, Integer>] updated_card_pool
      #
      def update(updated_card_pool)
        return if @card_pool == updated_card_pool

        @card_pool = updated_card_pool
      end

      #
      # shuffle the shoe by change id, reset card pool and place cut card
      #
      def shuffle
        @id       += 1
        @card_pool = Util::ShoeCardsUtil.card_pool(@num_of_deck)
        place_cut_card
      end

      #
      # draw position for cut card by range
      #
      def place_cut_card
        @cut_card_pos = rand(@cut_card_range)
      end

      #
      # the remaining card amount of the shoe
      #
      # @return [Integer]
      #
      def remaining_amount
        card_pool.values.sum
      end

      #
      # check is reach cut card position
      #
      # @return [Boolean]
      #
      def reach_cut_card?
        remaining_amount < cut_card_pos
      end

      #
      # load data from given source
      #
      # @param [Hash] data shoe data
      #
      # @return [Transmission::Component::Shoe]
      #
      def load(data)
        @id           = data.fetch(:id)
        @card_pool    = data.fetch(:card_pool)
        @cut_card_pos = data.fetch(:cut_card_pos)

        self
      end
    end
  end
end
