# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.7.0
    #
    # Store score card data of the game
    #
    class ScoreBoard < Clutch::Component
      pack :property, name: :score_board

      attribute :history, [],
        type: :collection,
        render_empty: false
      attr_reader :max_amount
      property :max_amount

      def initialize(max_amount = Float::INFINITY)
        @max_amount = max_amount
      end

      def record(score)
        @dirty = true
        history.push(score)

        history.shift if history.size > max_amount
      end

      def load(data)
        @history = data.dig(:history)
        @dirty   = false
      end
    end
  end
end
