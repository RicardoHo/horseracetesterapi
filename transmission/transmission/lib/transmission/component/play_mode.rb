# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author ben.b.wu
    # @since 0.1.0
    #
    # The horse racing play_mode component
    #
    class PlayMode < Clutch::Component
      attribute :play_mode
    end
  end
end
