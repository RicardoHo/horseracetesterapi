# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Ben B Wu
    # @since 0.1.0
    #
    # To store hand's result in score card format
    #
    class HrScoreCard < Clutch::Component
      pack :property, name: :score_card

      attribute :win_indices

      def load(data)
        @win_indices = data[:win_indices]
      end
    end
  end
end
