# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Denom info describe the available denoms
    # and current selected or default denom
    #
    class DenomInfo < Clutch::Component
      pack :property, name: :denom_info

      attribute :denoms
      attribute :denom_index, as: :index
      attribute :denom
    end
  end
end
