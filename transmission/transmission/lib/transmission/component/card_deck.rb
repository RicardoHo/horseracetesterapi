# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # Contains a deck of cards
    # @example Shuffle
    #   deck.reset
    #
    class CardDeck < Clutch::Component
      attribute :card_pool, [], type: :property

      def initialize(with_joker = false)
        @with_joker = with_joker
      end

      def reset
        super
        @card_pool = Util::ShoeCardsUtil.card_pool(1, with_joker: @with_joker)
      end

      def with_joker?
        @with_joker
      end

      #
      # update the deck by pass a updated card pool
      #
      # @param [Hash<Symbol, Integer>] updated_card_pool
      #
      def update(updated_card_pool)
        return if @card_pool == updated_card_pool

        @card_pool = updated_card_pool
      end

      #
      # the rest card count of the deck
      #
      # @return [Integer]
      #
      def remaining_amount
        card_pool.values.sum
      end
    end
  end
end
