# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author ben.b.wu
    # @since 0.1.0
    #
    # The horse racing result component
    #
    class HrLastBet < Clutch::Component
      attribute :last_bet, {'dual_forecast' => [], 'forecast' => []} 
      # attribute :last_bet_options, {}
    end
  end
end
