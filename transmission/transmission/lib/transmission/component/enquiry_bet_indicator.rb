# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # indicate enquiry bet information
    #
    class EnquiryBetIndicator < Clutch::Component
      pack :property, name: :enquiry_bet_indicator

      attribute :enable
      attribute :bet_credit
    end
  end
end
