# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.4.0
    #
    # Contains a deck of card faces
    # no suit information included
    # @example Shuffle
    #   deck.reset
    #
    class CardFaceDeck < Clutch::Component
      attribute :card_pool,
        Util::ShoeCardsUtil.card_face_pool(1),
        type: :property

      #
      # update the deck by pass a updated card pool
      #
      # @param [Hash<Symbol, Integer>] updated_card_pool
      #
      def update(updated_card_pool)
        return if @card_pool == updated_card_pool

        @card_pool = updated_card_pool
      end

      #
      # the rest card count of the deck
      #
      # @return [Integer]
      #
      def remaining_amount
        card_pool.values.sum
      end
    end
  end
end
