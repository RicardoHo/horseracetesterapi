# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Richard Fong
    # @since 0.8.0
    #
    # Path info describe the path
    # for the ball drop down
    #
    class PathInfo < Clutch::Component
      pack :property, name: :path_info

      attribute :path_left_right, [],
        type: :collection
      attribute :path_lines_index, [],
        type: :collection

      def load(data)
        @path_left_right  = data.dig(:path_left_right)
        @path_lines_index = data.dig(:path_lines_index)
      end
    end
  end
end
