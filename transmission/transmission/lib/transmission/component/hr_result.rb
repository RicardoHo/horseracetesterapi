# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author ben.b.wu
    # @since 0.1.0
    #
    # The horse racing result component
    #
    class HrResult < Clutch::Component
      attribute :play_mode
      attribute :win_indices
      attribute :win_reference
      attribute :win_options
      attribute :video_file
      attribute :total_bet_amt
      attribute :total_payout_amt
      attribute :payout_details
    end
  end
end
