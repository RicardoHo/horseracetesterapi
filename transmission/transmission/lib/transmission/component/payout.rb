# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # The payout component
    #
    class Payout < Clutch::Component
      attribute :cash, 0
      attribute :credit, 0

      pack :property, name: :payout

      def load(data)
        @cash   = data[:cash]
        @credit = data[:credit]
      end
    end
  end
end
