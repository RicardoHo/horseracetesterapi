# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # The label of bet area
    #
    class BetAreaLabel < Clutch::LabelComponent
      title :bet_options

      attr_reader :name
      property :name

      def initialize(name)
        @name = name
      end
    end
  end
end
