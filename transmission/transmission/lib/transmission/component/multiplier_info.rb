# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # multiplier info describe the available multipliers
    # and current selected or default multiplier
    #
    class MultiplierInfo < Clutch::Component
      pack :property, name: :multiplier_info

      attribute :multipliers, [],
        type: :collection
      attribute :multiplier_index, as: :index
      attribute :multiplier
    end
  end
end
