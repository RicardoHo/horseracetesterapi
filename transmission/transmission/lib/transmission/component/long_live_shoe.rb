# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # LongLiveShoe contains all card that can
    # be deal to player or public,
    # and will keep alive across round
    # until cut-card appear
    #
    class LongLiveShoe < Clutch::Component
      pack :property, name: :shoe

      attr_reader :card_pool
      attr_reader :cut_card_pos
      attribute :id
      attribute :cards, type: :collection
      property :remaining_amount
      property :cut_card_pos,
        skip_render: ->(represented:, **) { !represented.reach_cut_card? }

      def initialize(cards)
        @id = 0
        @card_pool = cards
        shuffle
      end

      #
      # Exchange the current shoe to new one
      # by shuffle the card from card pool
      # and place cut card to the shoe
      #
      # @return [Transmission::Component::LongLiveShoe]
      #
      def shuffle
        @id += 1
        @cards = @card_pool.shuffle
        place_cut_card

        self
      end

      #
      # pop one card from the top of the shoe
      # @example
      #   shoe.pop => 'Td'
      #
      # @return [String]
      #
      def pop
        @cards.pop
      end

      def place_cut_card
        @cut_card_pos = Util::RNGUtil.random(9, rng: @game_engin.rng) + 12
      end

      #
      # the remaining card amount of the shoe
      #
      # @return [Integer]
      #
      def remaining_amount
        @cards.size
      end

      #
      # check is reach cut card position
      #
      # @return [Boolean]
      #
      def reach_cut_card?
        remaining_amount < cut_card_pos
      end
    end
  end
end
