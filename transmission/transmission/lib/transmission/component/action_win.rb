# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Total win amount on a action
    #
    class ActionWin < Clutch::Component
      pack :property, name: :action_win

      attribute :cash, 0
      attribute :credit, 0
    end
  end
end
