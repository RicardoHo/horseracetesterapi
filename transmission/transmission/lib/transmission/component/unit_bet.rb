# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # The unit_bet component
    #
    class UnitBet < Clutch::Component
      pack :property, name: :unit_bet

      attribute :cash
      attribute :credit

      def initialize(credit)
        @credit = credit
      end
    end
  end
end
