# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # The label of table
    #
    class TableLabel < Clutch::LabelComponent
      title :table
    end
  end
end
