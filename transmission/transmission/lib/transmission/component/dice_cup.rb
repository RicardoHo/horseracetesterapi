# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.7.0
    #
    # dice cup contains n dice(s) and
    # the rolled dice(s) value
    #
    class DiceCup < Clutch::Component
      attr_reader :num_of_dice
      attribute :dices, [],
        type: :collection,
        render_empty: false

      def initialize(num_of_dice)
        @num_of_dice = num_of_dice
      end

      def load(dices)
        @dices = dices
        @dirty = false
      end
    end
  end
end
