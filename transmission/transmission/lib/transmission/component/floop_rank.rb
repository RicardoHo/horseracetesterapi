# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Use to store rank of a poker hand at floop
    #
    class FloopRank < Clutch::Component
      attribute :rank
    end
  end
end
