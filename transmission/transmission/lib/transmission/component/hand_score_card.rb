# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # To store hand's result in score card format
    #
    class HandScoreCard < Clutch::Component
      pack :property, name: :hand_score_card

      attribute :winner
      attribute :pair
      attribute :nature
    end
  end
end
