# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # indicate skip hand related data
    #
    class SkipHandIndicator < Clutch::Component
      pack :property, name: :skip_hand_indicator
      attribute :count, 0
      property :skipable,
        getter: ->(represented:, **) { represented.skipable? }

      def initialize(limit)
        @count = 0
        @limit = limit
      end

      #
      # count skip times
      #
      def skip
        @count += 1
      end

      #
      # indicate is skip count not reach limit
      #
      # @return [Boolean]
      #
      def skipable?
        @count < @limit
      end
    end
  end
end
