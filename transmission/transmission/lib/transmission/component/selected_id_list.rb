# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # Save the id that player selected
    #
    class SelectedIdList < Clutch::Component
      pack :property, name: :selected_id_list

      attribute :list, [],
        type: :collection

      def load(data)
        @list = data[:list]
      end
    end
  end
end
