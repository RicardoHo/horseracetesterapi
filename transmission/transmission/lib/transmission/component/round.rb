# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Round component to indicate the on going and  upcoming round of current game
    #
    class Round < Clutch::Component
      pack :property, name: :round_info

      attribute :id
    end
  end
end
