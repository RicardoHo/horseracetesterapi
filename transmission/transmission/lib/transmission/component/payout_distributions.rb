# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.6.0
    #
    # Payout distributions inforamtion for lottery
    #
    class PayoutDistributions < Clutch::Component
      attribute :list, [], as: :payout_distributions

      def record(data)
        list.push(data)
      end
    end
  end
end
