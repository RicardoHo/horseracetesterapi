# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # The upper limit of bet amount
    #
    class MaxBet < Clutch::Component
      attr_accessor :cash
      attr_accessor :credit
      property :cash
      property :credit

      pack :property, name: :max_bet

      def initialize(credit = nil)
        @credit = credit
      end
    end
  end
end
