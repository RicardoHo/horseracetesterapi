# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.6.0
    #
    # The total win cash component for lottery
    #
    class TotalWinCash < Clutch::Component
      attribute :value, nil, as: :total_win_amt
    end
  end
end
