# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # The win type component
    #
    class WinType < Clutch::Component
      attribute :type
    end
  end
end
