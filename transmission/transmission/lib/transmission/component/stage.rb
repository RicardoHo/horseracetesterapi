# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Stage component to indicate the on going and  upcoming stage of current game
    #
    class Stage < Clutch::Component
      pack :property, name: :stage

      attribute :current, Enum::Stage::BASE_GAME
      attribute :next
    end
  end
end
