# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # Save the data about id
    #
    class IdBox < Clutch::Component
      pack :property, name: :id_box

      attribute :hit_counter
      attribute :hit_id, [],
        type: :collection
      attribute :result_id, [],
        type: :collection
      attribute :hit_free_game

      def load(data)
        @hit_counter   = data[:hit_counter]
        @hit_id        = data[:hit_id]
        @result_id     = data[:result_id]
        @hit_free_game = data[:hit_free_game]
      end
    end
  end
end
