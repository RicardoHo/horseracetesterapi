# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Use to store cards and rank of a best hand
    #
    class BestHand < Clutch::Component
      attribute :cards, [],
        type: :collection,
        as: :hand,
        render_empty: false
      attribute :rank
    end
  end
end
