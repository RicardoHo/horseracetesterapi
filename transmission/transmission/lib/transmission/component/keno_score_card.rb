# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # To store keno result in score card format
    #
    class KenoScoreCard < Clutch::Component
      pack :property, name: :score_card

      attribute :bet_option
      attribute :payout
      attribute :hit_counter, as: :hit
      attribute :odds
      attribute :win_level
      attribute :stage

      def load(data)
        @bet_option  = data[:bet_option]
        @payout      = data[:payout]
        @hit_counter = data[:hit_counter]
        @odds        = data[:odds]
        @win_level   = data[:win_level]
        @stage       = data[:stage]
      end
    end
  end
end
