# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @author Jason Lei
    # @since 0.1.0
    #
    # Hand card describe cards that palyer holds
    #
    class HandCard < Clutch::Component
      attribute :cards, [],
        type: :collection,
        as: :card,
        render_empty: false
      attribute :equivalent_cards, [],
        type: :collection,
        as: :equivalent_card,
        render_empty: false

      attr_reader :min_hold
      attr_reader :max_hold

      def initialize(max_hold, min_hold = nil)
        @max_hold = max_hold
        @min_hold = min_hold ? min_hold : max_hold
      end

      #
      # collect new card into held card pool if
      # current card pool size not reach limit
      # @example
      #   hand_card.collect('da') => nil, cannot collect
      #   hand_card.collect('da') => hand_card, collected
      #
      # @param [String] card
      #
      # @return [Transmission::Component::HandCard] if can collect
      # @return [NilClass] if already reach limit
      #
      def collect(card)
        return if reach_limit?

        @dirty = true
        cards.push(card)
        equivalent_cards.push(card)

        self
      end

      #
      # collect batch of new cards into held card pool
      # @example
      #   hand_card.collect_batch(['As', 'Jh])
      #
      # @param [Array<String>] cards
      #
      def collect_batch(cards)
        cards.each { |card| collect(card) }
      end

      #
      # determine is card pool size reach the limit
      #
      # @return [Boolean]
      #
      def reach_limit?
        cards.size == @max_hold
      end
    end
  end
end
