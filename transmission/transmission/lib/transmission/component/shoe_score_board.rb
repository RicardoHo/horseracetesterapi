# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # Store all hand's score card data of the shoe
    #
    class ShoeScoreBoard < Clutch::Component
      pack :property, name: :shoe_score_board

      attr_accessor :shoe_id
      property :shoe_id
      attribute :history, [],
        type: :collection,
        render_empty: false

      def record(hand_score)
        @dirty = true
        history.push(hand_score)
      end

      def clear
        @shoe_id = nil
        @history = []
      end
    end
  end
end
