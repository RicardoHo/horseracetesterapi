# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.6.0
    #
    # The label of game context
    #
    class GameContextLabel < Clutch::LabelComponent
      title :game_context
    end
  end
end
