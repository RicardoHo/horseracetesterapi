# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # Count the free game
    #
    class FreeGameMeter < Clutch::Component
      pack :property, name: :free_game_meter

      attribute :counter, 0
    end
  end
end
