# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Richard Fong
    # @since 0.8.0
    #
    # ball hole contains n ball(s) and
    # the drop ball(s) value
    #
    class BallHole < Clutch::Component
      attr_reader :num_of_ball

      def initialize(num_of_ball)
        @num_of_ball = num_of_ball
      end
    end
  end
end
