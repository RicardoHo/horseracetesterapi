# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # The payout component
    #
    class FreeGamePayout < Clutch::Component
      attribute :cash, 0
      attribute :credit, 0

      pack :property, name: :free_game_payout

      def load(data)
        @cash   = data[:cash]
        @credit = data[:credit]
      end
    end
  end
end
