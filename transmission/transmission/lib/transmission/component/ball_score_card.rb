# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Richard Fong
    # @since 0.8.0
    #
    # To store ball result in score card format
    #
    class BallScoreCard < Clutch::Component
      pack :property, name: :score_card

      attribute :bet_option
      attribute :win_level

      def load(data)
        @bet_option = data[:bet_option]
        @win_level  = data[:win_level]
      end
    end
  end
end
