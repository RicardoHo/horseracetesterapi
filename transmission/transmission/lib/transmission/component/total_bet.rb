# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.6.0
    #
    # The total bet component for lottery
    #
    class TotalBet < Clutch::Component
      attribute :denom
      attribute :credit, 0, as: :total_bet_credit
    end
  end
end
