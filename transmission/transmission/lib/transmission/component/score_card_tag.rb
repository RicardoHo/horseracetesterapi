# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # The label of score card
    #
    class ScoreCardTag < Clutch::Component
      pack :property, name: :score_card_tag

      attribute :name, readable: false
      attribute :dirty, false, readable: false

      def initialize(name)
        @name = name
      end
    end
  end
end
