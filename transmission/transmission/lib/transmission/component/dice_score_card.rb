# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.7.0
    #
    # To store dice result in score card format
    #
    class DiceScoreCard < Clutch::Component
      pack :property, name: :score_card

      attribute :point
      attribute :win_type
      attribute :bet_option
      attribute :win_level

      def load(data)
        @point      = data.dig(:point)
        @win_type   = data.dig(:win_type)
        @bet_option = data.dig(:bet_option)
      end
    end
  end
end
