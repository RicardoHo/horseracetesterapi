# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author ben.b.wu
    # @since 0.1.0
    #
    # The horse racing paytable component
    #
    class HrPaytable < Clutch::Component
      attribute :paytable_int   # int, used by the backend to calculate payouts
      attribute :hr_paytable    # float, used by the frontend to show to players
    end
  end
end
