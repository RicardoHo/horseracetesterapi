# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # To store hand's result in score card format
    #
    class SanGongScoreCard < Clutch::Component
      pack :property, name: :score_card

      attribute :winner
      attribute :point
      attribute :payout_list, [],
        type: :collection
      attribute :win_type

      def load(data)
        @winner      = data[:winner]
        @point       = data[:point]
        @payout_list = data[:payout_list]
        @win_type    = data[:win_type]
      end
    end
  end
end
