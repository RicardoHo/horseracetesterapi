# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Public card means the card on board that every player can use
    #
    class PublicCard < HandCard
      attribute :cards, [],
        type: :collection,
        as: :public_card,
        render_empty: false
      attribute :equivalent_cards, [],
        type: :collection,
        as: :equivalent_card,
        render_empty: false
    end
  end
end
