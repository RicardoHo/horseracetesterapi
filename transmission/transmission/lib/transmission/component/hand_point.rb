# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # points of hand card(s)
    #
    class HandPoint < Clutch::Component
      attribute :value, as: :point
    end
  end
end
