# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # The label of player
    #
    class PlayerLabel < Clutch::LabelComponent
      attr_reader :name

      def initialize(name)
        @name = name
      end

      def title
        name
      end
    end
  end
end
