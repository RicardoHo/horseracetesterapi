# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # For contain discard cards
    #
    class DiscardTray < Clutch::Component
      attribute :used_cards, [], type: :collection

      #
      # record used card to the tray list
      #
      # @param [Integer] used_card
      #
      def record(used_card)
        used_cards.push(used_card)
      end
    end
  end
end
