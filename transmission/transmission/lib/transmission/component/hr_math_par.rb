# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author ben.b.wu
    # @since 0.1.0
    #
    # The horse racing paytable component
    #
    class HrMathPar < Clutch::Component
      attribute :group_idx
      attribute :modifications
      attribute :shuffle_idx
      attribute :shuffle_map
    end
  end
end
