# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.6.0
    #
    # The JackpotContribute component for lottery
    #
    class JackpotContribute < Clutch::Component
      attribute :enable, true, as: :contribute
    end
  end
end
