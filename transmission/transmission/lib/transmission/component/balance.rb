# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Player's balance component, described by mode, cash and credit
    #
    class Balance < Clutch::Component
      pack :property, name: :balance

      attribute :cash
      attribute :credit

      attr_reader :mode
      property :mode

      def initialize
        @mode = Enum::BalanceMode::CASH
      end
    end
  end
end
