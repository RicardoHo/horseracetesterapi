# frozen_string_literal: true

module Transmission
  module Component
    #
    # @author Richard Fong
    # @since 0.9.8
    #
    # The win type list component
    #
    class WinTypeList < Clutch::Component
      attribute :type_list, [],
        type: :collection

      def initialize(type_list)
        @type_list = type_list
      end
    end
  end
end
