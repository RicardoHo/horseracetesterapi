# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Find best hand in terms of rank for all player
    #
    class BestHandByRank < Clutch::System::CommandSystem
      watch :hand_card, :public_card, :best_hand
      command :raise, :find_best
      command :fold, :find_best

      private

      def find_best(entity)
        best_hand   = entity.find(Component::BestHand)
        hand_card   = entity.find(Component::HandCard)
        public_card = entity.find(Component::PublicCard)

        best_hand.cards, best_hand.rank = Util::PokerHandUtil
          .best_hand(hand_card.cards + public_card.cards)
      end
    end
  end
end
