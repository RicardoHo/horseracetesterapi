# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # Reset bet area to stand by state
    #
    class ResetBetAreaWithCondition < Clutch::System::CleanupSystem
      watch :bet_amount, :payout, :current_payout, :base_game_payout, :free_game_payout
      cleanup :reset, condition: :valid?

      def valid?(_entity)
        @command.casecmp?('draw')
      end
    end
  end
end
