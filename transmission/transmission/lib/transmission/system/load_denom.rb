# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Load denom from game engine when resume game
    #
    class LoadDenom < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :denom_info
      command :query_game, :on_load

      private

      def executable?
        super && loadable?
      end

      def before_all
        super
        load_save
        @denom = @save.dig(:table, :denom_info, :denom)
      end

      #
      # load the latest player selected denom
      # if the denom set has been changed and
      # not cleared rounds, then skip the load.
      #
      # @param [Clutch::Entity] entity
      #
      def on_load(entity)
        denom_info = entity.find(Component::DenomInfo)
        return unless denom_info.denoms.index(@denom)

        denom_info.denom = @denom
        denom_info.denom_index = denom_info.denoms.index(@denom)
      end
    end
  end
end
