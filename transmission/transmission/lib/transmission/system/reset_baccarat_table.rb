# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # Reset table to stand by state
    #
    class ResetBaccaratTable < Clutch::System::CleanupSystem
      watch :stage, :hand_score_card, :burn_card_indicator
      cleanup :reset
    end
  end
end
