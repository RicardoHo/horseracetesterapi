# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # Reset skip hand indicator
    #
    class ResetSkipHandIndicator < Clutch::System::CleanupSystem
      watch :skip_hand_indicator
      cleanup :reset, condition: :baccarat_deal?

      def baccarat_deal?(_entity)
        @command.casecmp?('baccarat_deal')
      end
    end
  end
end
