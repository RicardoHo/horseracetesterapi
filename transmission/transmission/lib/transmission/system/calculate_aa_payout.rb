# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # calculate payout of aa bet area by the first 5 card's rank
    #
    class CalculateAaPayout < Clutch::System::CommandSystem
      watch :bet_amount, :payout, :win_type
      reference :aa_rank
      command :raise, :calculate

      private

      def before_all
        super
        @aa_paytable ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
          .dig(:paytable, :aa)
        @aa_rank = references[:aa_rank].first
      end

      def calculate(entity)
        bet_amount = entity.find(Component::BetAmount)
        return unless bet_amount.cash

        payout        = entity.find(Component::Payout)
        win_type      = entity.find(Component::WinType)
        multiplier    = if win_type.type == Enum::WinType::LOSE
          0
        else
          compose_multiplier
        end

        payout.cash   = multiplier * bet_amount.cash
        payout.credit = multiplier * bet_amount.credit
      end

      def compose_multiplier
        @aa_paytable.dig(@aa_rank.rank.snake_case.to_sym, :odds)
      end
    end
  end
end
