# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # Load free game meter from game engine when resume game
    #
    class LoadFreeGameMeter < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :free_game_meter
      command :query_game, :on_load

      private

      def executable?
        super && loadable?
      end

      def before_all
        super
        load_save
        @counter = @save.dig(:table, :free_game_meter, :counter)
      end

      def on_load(entity)
        free_game_meter = entity.find(Component::FreeGameMeter)

        free_game_meter.counter = @counter
      end
    end
  end
end
