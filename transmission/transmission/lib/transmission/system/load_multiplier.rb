# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # Load multiplier from game engine when resume game
    #
    class LoadMultiplier < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :multiplier_info
      command :query_game, :on_load

      private

      def executable?
        super && loadable?
      end

      def before_all
        super
        load_save
        @multiplier = @save.dig(:table, :multiplier_info, :multiplier)
      end

      #
      # load the latest player selected multiplier
      # if the multiplier set has been changed and
      # not cleared rounds, then skip the load.
      #
      # @param [Clutch::Entity] entity
      #
      def on_load(entity)
        multiplier_info = entity.find(Component::MultiplierInfo)
        return unless multiplier_info.multipliers.index(@multiplier)

        multiplier_info.multiplier = @multiplier
        multiplier_info.multiplier_index = multiplier_info.multipliers.index(@multiplier)
      end
    end
  end
end
