# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Reset bet area to stand by state
    #
    class ResetScoreCard < Clutch::System::CleanupSystem
      watch :hand_score_card
      cleanup :reset
    end
  end
end
