# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # accumulate all bet area's payout to action win
    #
    class AccumulateCurrentPayout < Clutch::System::ReactiveSystem
      watch :current_payout
      reference :action_win

      private

      def executable?
        super && !@command.casecmp?('query_game') && !@command.casecmp?('draw_finish')
      end

      def before_all
        super
        @action_win ||= references[:action_win].first
      end

      def process(entity)
        current_payout = entity.find(Component::CurrentPayout)

        @action_win.cash   += current_payout.cash
        @action_win.credit += current_payout.credit
      end
    end
  end
end
