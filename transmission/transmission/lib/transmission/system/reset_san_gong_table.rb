# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # Reset table to stand by state
    #
    class ResetSanGongTable < Clutch::System::CleanupSystem
      watch :stage, :card_deck, :san_gong_score_card
      cleanup :reset
    end
  end
end
