# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # Cleanup action win
    #
    class CleanupActionWinWithCondition < Clutch::System::CleanupSystem
      watch :action_win
      cleanup :reset, condition: :valid?

      def valid?(_entity)
        @command.casecmp?('draw')
      end
    end
  end
end
