# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Reset bet area to stand by state
    #
    class ResetPlayerHand < Clutch::System::CleanupSystem
      watch :hand_card, :public_card, :best_hand
      cleanup :reset, condition: :deal?

      def deal?(_entity)
        @command.casecmp?('deal')
      end
    end
  end
end
