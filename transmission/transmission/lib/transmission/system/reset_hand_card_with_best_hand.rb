# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Reset player hand card with best hand
    #
    class ResetHandCardWithBestHand < Clutch::System::CleanupSystem
      watch :hand_card, :best_hand
      cleanup :reset, condition: :bet?

      def bet?(_entity)
        @command.casecmp?('bet')
      end
    end
  end
end
