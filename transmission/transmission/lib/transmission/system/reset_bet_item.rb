# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.7.0
    #
    # Reset bet item to stand by state
    #
    class ResetBetItem < Clutch::System::CleanupSystem
      watch :value_type_bet_item
      cleanup :reset
    end
  end
end
