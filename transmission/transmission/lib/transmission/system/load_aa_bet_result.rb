# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Load aa bet result from game engine when resume game
    #
    class LoadAaBetResult < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :payout, :win_type
      command :query_game, :on_load

      private

      def executable?
        super && loadable?
      end

      def before_all
        super
        load_save
        @bet_options_data = @save.fetch(:bet_options)
      end

      def on_load(entity)
        payout         = entity.find(Component::Payout)
        win_type       = entity.find(Component::WinType)
        bet_option_dta = @bet_options_data.find do |opt|
          opt[:name].casecmp?('aa')
        end
        return unless bet_option_dta

        payout.cash   = bet_option_dta.dig(:payout, :cash)
        payout.credit = bet_option_dta.dig(:payout, :credit)
        win_type.type = bet_option_dta.dig(:type)
      end
    end
  end
end
