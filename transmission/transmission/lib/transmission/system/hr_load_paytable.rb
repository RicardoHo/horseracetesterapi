# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Ben B Wu
    # @since 0.1.0
    #
    # Load paytable from game engine when resume game
    #
    class HrLoadPaytable < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :hr_paytable
      command :query_game, :on_load

      private

      def executable?
        super && loadable?
      end

      def before_all
        super
        load_save
        @paytable = @save.dig(:table, :hr_paytable)
        @paytable_int = @save.dig(:table, :paytable_int)
      end

      def on_load(entity)
        hr_paytable = entity.find(Component::HrPaytable)

        hr_paytable.hr_paytable = @paytable
        hr_paytable.paytable_int = @paytable_int
      end
    end
  end
end
