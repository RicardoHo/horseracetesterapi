# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Deal public cards, public cards are visable for all player
    #
    class DealtPublicCard < Clutch::System::CommandSystem
      watch :public_card
      reference :card_deck

      command :deal,  :process_dealt
      command :raise, :process_dealt
      command :fold,  :process_dealt

      private

      #
      # @todo
      # get the num of card by game config via ConfigUtil
      #
      def before_all
        super
        @deck     ||= references[:card_deck].first
        num_of_card = num_of_card_by_command
        dealt_public_cards(num_of_card)
      end

      #
      # clear the new_public_cards after all entity process
      #
      def after_all
        @new_public_cards.clear
      end

      #
      # get number of cards for different command
      #
      # @return [Integer]
      #
      def num_of_card_by_command
        case @command.to_sym
        when :deal
          3
        when :raise, :fold
          2
        end
      end

      #
      # dealt public cards to shared pool
      #
      # @param [Integer] num_of_card
      #
      def dealt_public_cards(num_of_card)
        @new_public_cards, @deck.card_pool = Util::RNGUtil
          .draw_multiple_from_weight_table(
            @deck.card_pool,
            num_of_card,
            rng: @game_engine.weight_table
          )
      end

      #
      # dealt public card main logic
      #
      # @param [Clutch::Entity] entity
      #
      def process_dealt(entity)
        public_card = entity.find(Component::PublicCard)
        @new_public_cards.each do |card|
          public_card.collect(card)
        end
      end
    end
  end
end
