# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Ben B Wu
    # @since 0.1.0
    #
    # Load pocket from game engine when resume game
    #
    class HrLoadResult < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :hr_result
      command :query_game, :on_load

      private

      def executable?
        super && loadable?
      end

      def before_all
        super
        load_save
        @play_mode = @save.dig(:table, :play_mode)
        @win_indices = @save.dig(:table, :win_indices)
        @win_reference = @save.dig(:table, :win_reference)
        @video_file = @save.dig(:table, :video_file)
        @win_options = @save.dig(:table, :win_options)
        @total_bet_amt = @save.dig(:table, :total_bet_amt)
        @total_payout_amt = @save.dig(:table, :total_payout_amt)
        @payout_details = @save.dig(:table, :payout_details)
      end

      def on_load(entity)
        hr_result = entity.find(Component::HrResult)

        hr_result.play_mode = @play_mode
        hr_result.win_indices = @win_indices
        hr_result.win_reference = @win_reference
        hr_result.video_file = @video_file
        hr_result.win_options = @win_options
        hr_result.total_bet_amt = @total_bet_amt
        hr_result.total_payout_amt = @total_payout_amt
        hr_result.payout_details = @payout_details
      end
    end
  end
end
