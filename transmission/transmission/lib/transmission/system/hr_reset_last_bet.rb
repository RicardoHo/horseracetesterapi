# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Ben B Wu
    # @since 0.1.0
    #
    # Reset last bet for horse racing
    #
    class HrResetLastBet < Clutch::System::CleanupSystem
      watch :hr_last_bet
      cleanup :reset, condition: :valid?

      def valid?(_entity)
        @command.casecmp?('query_game')
      end
    end
  end
end
