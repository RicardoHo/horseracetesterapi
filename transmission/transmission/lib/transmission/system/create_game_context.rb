# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.6.0
    #
    # Create game context related components
    #
    class CreateGameContext < Clutch::System::InitializeSystem
      private

      def process
        attribute = @game_def.game_config.game_object.attribute[:game_context]
        create_game_context(attribute)
      end

      def create_game_context(attribute)
        components = begin
          components_attr = attribute.fetch(:components)
          components_attr.each_with_object([]) do |(key, value), comps|
            init_data = comp_init_data(value)
            comps.push(
              Transmission::Component
                .const_get(key.to_s.camel_case)
                .new(*init_data)
            )
          end
        end
        create_entity(*components)
      end

      def comp_init_data(config)
        return [] unless config

        init_config = config.fetch(:init, [])
        case init_config
        when Array
          init_config
        when Hash
          [
            send(
              "init_data_from_#{init_config.fetch(:from)}",
              init_config.fetch(:name),
              init_config.fetch(:method),
              init_config.fetch(:args)
            ),
          ]
        end
      end

      def init_data_from_module(name, method, args)
        Transmission.const_get(name.constanize).send(method, *args)
      end
    end
  end
end
