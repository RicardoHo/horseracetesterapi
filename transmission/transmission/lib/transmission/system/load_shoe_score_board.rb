# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # Load shoe score board from game engine when resume
    #
    class LoadShoeScoreBoard < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :shoe_score_board, :shoe
      command :query_game, :on_load

      private

      def executable?
        super && instance_loadable?
      end

      def before_all
        super
        load_instance_save
      end

      def on_load(entity)
        shoe  = entity.find(Component::Shoe)
        board = entity.find(Component::ShoeScoreBoard)
        board.history = @instance_save.dig(:shoe_score_board, :history)
        board.shoe_id = shoe.id
      end
    end
  end
end
