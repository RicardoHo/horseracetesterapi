# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Player fold card system which means the player lose anyway.
    #
    class FoldCard < Clutch::System::CommandSystem
      watch :win_info
      command :fold, :fold_card

      private

      def fold_card(entity)
        win_info = entity.find(Component::WinInfo)
        win_info.type = Enum::WinType::LOSE
      end
    end
  end
end
