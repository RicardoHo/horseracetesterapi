# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # Shuffle the shoe if cut card appear
    #
    class ShuffleShoe < Clutch::System::CommandSystem
      watch :shoe
      command :query_game, :shuffle_shoe
      command :reshuffle, :shuffle_shoe

      private

      #
      # shuffle the shoe so that all rest system
      # can use a new show instead used one
      #
      # @param [Clutch::Entity] entity
      #
      def shuffle_shoe(entity)
        shoe = entity.find(Component::Shoe)
        shoe.shuffle if shoe.empty? || shoe.reach_cut_card?
      end
    end
  end
end
