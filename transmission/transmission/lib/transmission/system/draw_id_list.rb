# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # Draw the result numbers
    #
    class DrawIdList < Clutch::System::ReactiveSystem
      watch :selected_id_list
      reference :free_game_meter, :stage, :id_box

      private

      def executable?
        super && !@command.casecmp?('query_game') && !@command.casecmp?('draw_finish')
      end

      def before_all
        super
        @free_game_meter  ||= references[:free_game_meter].first
        @stage            ||= references[:stage].first
        @id_box           ||= references[:id_box].first
        @system_draw_size ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
          .fetch(:system_draw_size)
        @id_upper_limit   ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
          .fetch(:id_upper_limit)
      end

      def process(entity)
        selected_id_list = entity.find(Component::SelectedIdList)

        @id_box.result_id = Util::RNGUtil
          .random_multiple(
            @system_draw_size,
            @id_upper_limit,
            repeat: false, rng: @game_engine.rng
          )
          .map { |num| num + 1 }
        @id_box.hit_id = selected_id_list.list & @id_box.result_id
        @id_box.hit_counter = @id_box.hit_id.length
      end
    end
  end
end
