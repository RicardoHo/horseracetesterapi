# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Create player entitiy
    #
    class CreatePlayer < Clutch::System::InitializeSystem
      private

      def process
        players_attribute =
          @game_def.game_config.game_object.attribute.fetch(:player)

        players_attribute.each do |player_attribute|
          create_player(player_attribute)
        end
      end

      #
      # create player entity by given player attribute
      #
      # @param [Hash] player_attribute
      #
      # @return [Clutch::Entity]
      #
      def create_player(player_attribute)
        components = begin
          components_attr = player_attribute.fetch(:components)
          components_attr.each_with_object([]) do |(key, value), comps|
            init_data = value&.fetch(:init, []) || []
            comps.push(
              Transmission::Component
                .const_get(key.to_s.camel_case)
                .new(*init_data)
            )
          end
        end
        create_entity(*components)
      end
    end
  end
end
