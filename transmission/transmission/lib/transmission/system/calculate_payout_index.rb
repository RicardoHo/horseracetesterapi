# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.6.0
    #
    # Record
    #
    class CalculatePayoutIndex < Clutch::System::ReactiveSystem
      watch :best_hand
      reference :payout_index

      private

      def before_all
        super
        @paytable ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
          .dig(:paytable, :ante)

        @payout_index = references[:payout_index].first
      end

      def process(entity)
        @payout_index.value = @paytable
          .keys
          .index(entity.find(Component::BestHand).rank.to_sym)
      end
    end
  end
end
