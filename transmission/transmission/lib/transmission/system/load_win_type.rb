# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Load win type from game engine when resume game
    #
    class LoadWinType < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :win_type, :bet_area_label
      command :query_game, :on_load

      private

      def executable?
        super && loadable?
      end

      def before_all
        super
        load_save
        @bet_options_data = @save.fetch(:bet_options)
      end

      def on_load(entity)
        win_type        = entity.find(Component::WinType)
        bet_area_label  = entity.find(Component::BetAreaLabel)
        bet_option_data = @bet_options_data
          .find { |opt| bet_area_label.name.casecmp?(opt[:name]) }

        win_type.type = bet_option_data.dig(:type)
      end
    end
  end
end
