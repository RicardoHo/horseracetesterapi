# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.4.0
    #
    # Reset table to stand by state
    #
    class ResetLulufaTable < Clutch::System::CleanupSystem
      watch :stage, :card_face_deck
      cleanup :reset
    end
  end
end
