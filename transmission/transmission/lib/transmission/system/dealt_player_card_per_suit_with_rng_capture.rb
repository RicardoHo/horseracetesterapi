# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Ivan Lao
    # @since 1.0.0.rc1
    #
    # Add a rng capture on DealtPlayerCardPerSuit
    #
    class DealtPlayerCardPerSuitWithRngCapture < Clutch::System::CommandSystem
      watch :hand_card
      reference :card_face_deck, :rng_capture

      command :bet, :process_dealt

      private

      def before_all
        super
        @deck ||= references[:card_face_deck].first
        @rng_capture ||= references[:rng_capture].first
      end

      #
      # dealt card core logic, invoke for each entity
      #
      # @param [Clutch::Entity] entity
      #
      def process_dealt(entity)
        hand_card = entity.find(Component::HandCard)
        faces, @deck.card_pool = Util::RNGUtil
          .draw_multiple_from_weight_table(
            @deck.card_pool,
            hand_card.min_hold,
            replacement: true,
            rng: @game_engine.weight_table,
            rng_capture: @rng_capture,
          )
        suits = Util::ShoeCardsUtil::SUITS.shuffle
        hand_card.collect_batch(faces.map { |face| face + suits.shift })
      end
    end
  end
end
