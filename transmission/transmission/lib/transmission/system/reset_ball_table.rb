# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.8.0
    #
    # Reset ball table to stand by state
    #
    class ResetBallTable < Clutch::System::CleanupSystem
      watch :ball_score_card, :path_info
      cleanup :reset
    end
  end
end
