# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # Check selected bet item from wagered bet area
    #
    class CheckSelectedIdList < Clutch::System::CommandSystem
      watch :bet_amount, :bet_area_label
      reference :keno_score_card, :selected_id_list

      command :draw,           :selected_id_list
      command :draw_free_game, :selected_id_list

      private

      def before_all
        super
        @score_card                    ||= references[:keno_score_card].first
        @selected_id_list              ||= references[:selected_id_list].first
        @paytable                      ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
          .fetch(:paytable)
        @selected_id_list_length_range ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
          .fetch(:selected_id_list_length_range)
      end

      def selected_id_list(entity)
        bet_credit = entity.find(Component::BetAmount).credit
        return unless bet_credit

        bet_name = entity.find(Component::BetAreaLabel).name
        @selected_id_list.list = if @input.respond_to?(:selected_id_list)
          @input.selected_id_list.to_a
        else
          @selected_id_list.list
        end

        unless @selected_id_list_length_range.cover?(@selected_id_list.list.length)
          raise SelectNumbersSizeNotInRangeError,
            "Select number set size not in range [#{@selected_id_list_length_range}]"
        end

        @score_card.bet_option = {
          name: bet_name,
          bet_amount: {
            credit: bet_credit,
            cash: entity.find(Component::BetAmount).cash,
          },
        }
      end
    end
  end
end
