# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Ben B Wu
    # @since 0.1.0
    #
    # Load pocket from game engine when resume game
    #
    class HrLoadMathPar < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :hr_math_par
      command :query_game, :on_load

      private

      def executable?
        super && loadable?
      end

      def before_all
        super
        load_save
        @group_idx = @save.dig(:table, :group_idx)
        @shuffle_map = @save.dig(:table, :shuffle_map)
      end

      def on_load(entity)
        math_par = entity.find(Component::HrMathPar)
        math_par.group_idx = @group_idx
        math_par.shuffle_map = @shuffle_map
      end
    end
  end
end
