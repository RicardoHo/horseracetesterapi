# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Load player hand from game engine when resume game
    #
    class LoadPlayerHand < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :hand_card, :best_hand, :floop_rank, :player_label
      command :query_game, :on_load

      private

      def executable?
        super && loadable?
      end

      def before_all
        super
        load_save
      end

      def on_load(entity)
        player_label = entity.find(Component::PlayerLabel)
        hand_card    = entity.find(Component::HandCard)
        best_hand    = entity.find(Component::BestHand)
        floop_rank   = entity.find(Component::FloopRank)
        player_data  = @save
          .fetch(player_label.name.to_sym)

        hand_card.cards = player_data.fetch(:card)
        if player_data.key?(:hand)
          best_hand.cards = player_data.fetch(:hand)
          best_hand.rank  = player_data.fetch(:rank)
        else
          floop_rank.rank = player_data.fetch(:rank)
        end
      end
    end
  end
end
