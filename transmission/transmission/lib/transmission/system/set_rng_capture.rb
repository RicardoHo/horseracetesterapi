# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Ivan Lao
    # @since 1.0.0.rc1
    #
    #
    #
    class SetRngCapture < Clutch::System::CommandSystem
      watch :round

      command :deal, :store_rng_id
      command :bet,  :store_rng_id
      command :draw, :store_rng_id
      command :multiple_bet, :store_rng_id
      command :query_game, :store_rng_id
      command :new_game, :store_rng_id

      private

      def store_rng_id(entity)
        round = entity.find(Component::Round)

        @game_engine.rng_capture[:rng_result] = []
        @game_engine.rng_capture[:rng_ref_id] = "2_#{round.id}"
      end
    end
  end
end
