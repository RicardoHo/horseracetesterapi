# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # Load score card
    #
    class LoadKenoScoreCard < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :keno_score_card
      command :query_game, :on_load

      private

      def executable?
        super && loadable?
      end

      def before_all
        super
        load_save
        @score_card_data = @save.dig(:table, :score_card)
      end

      def on_load(entity)
        entity
          .find(Component::KenoScoreCard)
          .load(@score_card_data)
      end
    end
  end
end
