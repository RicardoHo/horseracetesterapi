# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Jason Lei
    # @since 0.2.0
    #
    # calculate payout of ante bet area according to best hand when finish deal.
    #
    class CalculateAntePayout < Clutch::System::ReactiveSystem
      watch :bet_amount, :payout
      reference :best_hand

      private

      def before_all
        super
        @ante_paytable ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
          .dig(:paytable, :ante)

        @best_hand = references[:best_hand].first
      end

      def process(entity)
        bet_amount = entity.find(Component::BetAmount)
        return unless bet_amount.cash

        payout        = entity.find(Component::Payout)
        multiplier    = compose_multiplier || 0

        payout.cash   = (multiplier * bet_amount.cash).to_i
        payout.credit = (multiplier * bet_amount.credit).to_i
      end

      def compose_multiplier
        @ante_paytable.dig(@best_hand.rank.snake_case.to_sym, :odds)
      end
    end
  end
end
