# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Load aa rank from game engine when resume game
    #
    class LoadAaRank < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :aa_rank, :player_label
      command :query_game, :on_load

      private

      def executable?
        super && loadable?
      end

      def before_all
        super
        load_save
      end

      def on_load(entity)
        player_label = entity.find(Component::PlayerLabel)
        aa_rank      = entity.find(Component::AaRank)
        player_data  = @save.fetch(player_label.name.to_sym)

        aa_rank.cards = player_data.fetch(:aa_card)
        aa_rank.rank  = player_data.fetch(:aa_rank)
      end
    end
  end
end
