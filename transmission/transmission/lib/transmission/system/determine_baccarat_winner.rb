# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # To determine the winner of baccarat hand by
    # compare the points
    #
    class DetermineBaccaratWinner < Clutch::System::ReactiveSystem
      watch :player_label, :hand_point
      reference :hand_score_card

      private

      def before_all
        super
        @hand_score_card ||= references[:hand_score_card].first
        @current_max_point = 0
      end

      def process(entity)
        point = entity.find(Component::HandPoint).value
        if point > @current_max_point
          @current_max_point = point
          @hand_score_card.winner = entity
            .find(Component::PlayerLabel)
            .name
            .upcase
        elsif point == @current_max_point
          @hand_score_card.winner = Enum::Winner::TIE
        end
      end
    end
  end
end
