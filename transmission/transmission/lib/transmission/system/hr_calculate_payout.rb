# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Ben B Wu
    # @since 0.1.0
    #
    # calculate horse racing payout 
    #
    class HrCalculatePayout < Clutch::System::ReactiveSystem
      watch :bet_amount, :payout, :bet_area_label
      reference :hr_result, :hr_paytable

      private

      def before_all
        super

        @hr_result = references[:hr_result].first
        @hr_paytable = references[:hr_paytable].first
        # p @hr_paytable


        @odds_divisor ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
          .dig(:odds_divisor)
      end


      def process(entity)
        bet_name = entity.find(Component::BetAreaLabel).name
        bet_amount = entity.find(Component::BetAmount)

        if bet_amount.credit == 0 && bet_amount.cash == 0
          return
        end

        payout_index = entity.find(Component::PayoutIndex)
        payout_index.value = nil 

        payout = entity.find(Component::Payout)
        @hr_result.total_bet_amt += bet_amount.credit
        p_idx = 0
        case bet_name
          # win
          when @hr_result.win_reference[0]
            idx = @hr_result.win_indices[0]
            pt = @hr_paytable.paytable_int[:win]

            payout.credit = bet_amount.credit * pt[idx] / @odds_divisor
            payout.cash = bet_amount.cash * pt[idx] / @odds_divisor
            @hr_result.payout_details['win'] = payout.credit
            p_idx = 100 + (idx + 1) * 10

          # place1
          when @hr_result.win_reference[1]
            idx = @hr_result.win_indices[0]
            pt = @hr_paytable.paytable_int[:place]
            payout.credit = bet_amount.credit * pt[idx] / @odds_divisor
            payout.cash = bet_amount.cash * pt[idx] / @odds_divisor
            @hr_result.payout_details['place'][0] = payout.credit
            p_idx = 200 + (idx + 1) * 10

          # place2
          when @hr_result.win_reference[2]
            idx = @hr_result.win_indices[1]
            pt = @hr_paytable.paytable_int[:place]
            payout.credit = bet_amount.credit * pt[idx] / @odds_divisor
            payout.cash = bet_amount.cash * pt[idx] / @odds_divisor
            @hr_result.payout_details['place'][1] = payout.credit
            p_idx = 200 + (idx + 1) * 10

          # dual forecast
          when @hr_result.win_reference[3]
            if @hr_result.win_indices[0] > @hr_result.win_indices[1]
              idx = @hr_result.win_indices[1] * @hr_paytable.paytable_int[:win].size + @hr_result.win_indices[0]
              p_idx = 300 + (@hr_result.win_indices[1] + 1) * 10 + (@hr_result.win_indices[0] + 1)
            else
              idx = @hr_result.win_indices[0] * @hr_paytable.paytable_int[:win].size + @hr_result.win_indices[1]
              p_idx = 300 + (@hr_result.win_indices[0] + 1) * 10 + (@hr_result.win_indices[1] + 1)
            end
            pt = @hr_paytable.paytable_int[:dual_forecast]
            #p idx, bet_amount.credit, pt[idx]
            payout.credit = bet_amount.credit * pt[idx] / @odds_divisor
            payout.cash = bet_amount.cash * pt[idx] / @odds_divisor
            @hr_result.payout_details['dual_forecast'] = payout.credit

          # forecast
          when @hr_result.win_reference[4]
            idx = @hr_result.win_indices[0] * @hr_paytable.paytable_int[:win].size + @hr_result.win_indices[1]
            pt = @hr_paytable.paytable_int[:forecast]
            payout.credit = bet_amount.credit * pt[idx] / @odds_divisor
            payout.cash = bet_amount.cash * pt[idx] / @odds_divisor
            @hr_result.payout_details['forecast'] = payout.credit
            p_idx = 400 + (@hr_result.win_indices[0] + 1) * 10 + (@hr_result.win_indices[1] + 1)

          else
            payout.credit = 0
        end

        if payout.credit > 0
          result = {}
          result['name'] = bet_name
          result['bet_amount'] = {'cash' => bet_amount.cash, 'credit' => bet_amount.credit}
          result['payout'] = {'cash' => payout.cash, 'credit' => payout.credit}
          @hr_result.win_options << result
          @hr_result.total_payout_amt += payout.credit

          payout_index.value = p_idx
        end
      end
    end
  end
end
