# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.2.2
    #
    # Handle raise bet of H-CH
    #
    class RaiseBet < Clutch::System::CommandSystem
      include Helper::BetHelper

      watch :bet_area_label, :bet_amount, :max_bet, :min_bet
      command :raise, :raise_bet
      reference :denom_info

      private

      def before_all
        super
        @ante_bet_comp ||= active_entities.find do |entity|
          entity.find(Component::BetAreaLabel).name == 'ante'
        end.find(Component::BetAmount)
        @denom_info = references[:denom_info].first
      end

      #
      # Entry of raise command
      #
      # @param [Clutch::Entity] entity
      #
      def raise_bet(entity)
        bet_name = entity.find(Component::BetAreaLabel).name
        return unless bet_name.casecmp?('raise')

        on_bet(@ante_bet_comp.credit * 2, entity)
      end
    end
  end
end
