# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Cleanup aa bet win type value
    #
    class CleanupWinType < Clutch::System::CleanupSystem
      watch :win_type
      cleanup :reset, condition: :deal?

      def deal?(_entity)
        @command.casecmp?('deal') ||
        @command.casecmp?('baccarat_deal') ||
        @command.casecmp?('roll')
      end
    end
  end
end
