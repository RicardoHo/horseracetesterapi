# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.8.0
    #
    # Load line from game engine when resume game
    #
    class LoadLine < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :lines_info
      command :query_game, :on_load

      private

      def executable?
        super && loadable?
      end

      def before_all
        super
        load_save
        @line_index = @save.dig(:table, :lines_info, :index)
      end

      def on_load(entity)
        lines_info = entity.find(Component::LinesInfo)

        lines_info.line_index = @line_index
      end
    end
  end
end
