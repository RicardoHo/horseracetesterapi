# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # Reset table to stand by state
    #
    class ResetFafafaTable < Clutch::System::CleanupSystem
      watch :stage, :card_deck
      cleanup :reset, condition: :bet?

      def bet?(_entity)
        @command.casecmp?('bet')
      end
    end
  end
end
