# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Ben B Wu
    # @since 0.1.0
    #
    # Load pocket from game engine when resume game
    #
    class HrLoadLastBet < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :hr_last_bet
      command :query_game, :on_load

      private

      def executable?
        super && loadable?
      end

      def before_all
        super
        load_save
        @last_bet = @save.dig(:table, :last_bet)
      end

      def on_load(entity)
        last_bet = entity.find(Component::HrLastBet)
        last_bet_options = entity.find(Component::HrLastBetOptions)
        last_bet.last_bet = {'dual_forecast' => [], 'forecast' => []}
        last_bet_options.last_bet_options = @last_bet
      end
    end
  end
end
