# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.4.0
    #
    # To dealt card to all player - the participant of the game
    # and will deal same amount of card for each suit
    #
    class DealtPlayerCardPerSuit < Clutch::System::CommandSystem
      watch :hand_card
      reference :card_face_deck

      command :bet, :process_dealt

      private

      def before_all
        super
        @deck ||= references[:card_face_deck].first
      end

      #
      # dealt card core logic, invoke for each entity
      #
      # @param [Clutch::Entity] entity
      #
      def process_dealt(entity)
        hand_card = entity.find(Component::HandCard)
        faces, @deck.card_pool = Util::RNGUtil
          .draw_multiple_from_weight_table(
            @deck.card_pool,
            hand_card.min_hold,
            replacement: true,
            rng: @game_engine.weight_table
          )
        suits = Util::ShoeCardsUtil::SUITS.shuffle
        hand_card.collect_batch(faces.map { |face| face + suits.shift })
      end
    end
  end
end
