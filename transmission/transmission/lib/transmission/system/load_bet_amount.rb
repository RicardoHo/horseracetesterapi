# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Load bet amount from game engine when resume game
    #
    class LoadBetAmount < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :bet_amount, :bet_area_label
      command :query_game, :on_load

      private

      def executable?
        super && loadable?
      end

      def before_all
        super
        load_save
        @bet_options_data = @save.fetch(:bet_options)
      end

      def on_load(entity)
        bet_amount      = entity.find(Component::BetAmount)
        bet_area_label  = entity.find(Component::BetAreaLabel)
        bet_amount_data = find_bet_amount_data(bet_area_label.name)
        return unless bet_amount_data

        bet_amount.cash   = bet_amount_data.dig(:cash)
        bet_amount.credit = bet_amount_data.dig(:credit)
      end

      def find_bet_amount_data(bet_area_name)
        if @bet_options_data.is_a?(Array)
          @bet_options_data.find do |opt|
            bet_area_name.casecmp?(opt[:name])
          end.dig(:bet_amount)
        elsif @bet_options_data.is_a?(Hash)
          @bet_options_data.dig(:bet_amount)
        end
      end
    end
  end
end
