# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # Dealt card to all player - the participant of the game
    # and the hand will treat as skip which means the score board
    # will record it but no hand was form
    #
    class SkipHand < Clutch::System::CommandSystem
      watch :shoe, :shoe_score_board, :skip_hand_indicator
      reference :hand_card

      command :skip_hand, :process

      private

      def before_all
        super
        @hand_card_comp_list ||= references[:hand_card]
      end

      def process(entity)
        indicator = entity.find(Component::SkipHandIndicator)
        raise ReachSkipLimitError unless indicator.skipable?

        board = entity.find(Component::ShoeScoreBoard)
        raise InvalidSkipError if board.history.size > indicator.count

        shoe = entity.find(Component::Shoe)
        indicator.skip
        @hand_card_comp_list.each do |hand_card|
          hand_card.cards, shoe.card_pool = Util::RNGUtil
            .draw_multiple_from_weight_table(
              shoe.card_pool,
              hand_card.min_hold,
              rng: @game_engine.weight_table
            )
        end
      end
    end
  end
end
