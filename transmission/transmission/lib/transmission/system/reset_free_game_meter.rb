# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # Reset free_game_meter by condition
    #
    class ResetFreeGameMeter < Clutch::System::CleanupSystem
      watch :free_game_meter
      cleanup :reset, condition: :valid?

      def valid?(entity)
        #
        # check the event suitable for game sequence
        # before reset the free game meter
        #
        counter = entity.find(Component::FreeGameMeter).counter
        if counter != 0 && @command.casecmp?('draw')
          raise GameSequenceWrongError,
            "Game sequence wrong, can not draw during free game"
        end
        @command.casecmp?('draw')
      end
    end
  end
end
