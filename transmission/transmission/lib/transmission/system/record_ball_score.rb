# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.8.0
    #
    # Record current ball score to score board
    #
    class RecordBallScore < Clutch::System::ReactiveSystem
      watch :ball_score_card, :score_board

      private

      def executable?
        # HACK: avoid duplicate score recording when resume game
        super && !@command.casecmp?('query_game')
      end

      def process(entity)
        board = entity.find(Component::ScoreBoard)
        board.record(
          entity.find(Component::BallScoreCard).to_hash.fetch(:score_card)
        )
      end
    end
  end
end
