# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Reset bet area to stand by state
    #
    class ResetBetArea < Clutch::System::CleanupSystem
      watch :bet_amount, :payout
      cleanup :reset, condition: :valid?

      def valid?(_entity)
        event_name = %w[
          deal
          bet
          baccarat_deal
          roll
          drop
          draw
          multiple_bet
        ]
        event_name.include?(@command.downcase)
      end
    end
  end
end
