# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # Clear all record in the score board
    #
    class ClearScoreBoard < Clutch::System::CommandSystem
      watch :shoe, :shoe_score_board
      command :query_game, :clear
      command :reshuffle, :clear

      private

      #
      # Handler for reshuffle event
      #
      # @param [Clutch::Entity] entity
      #
      def clear(entity)
        shoe  = entity.find(Component::Shoe)
        board = entity.find(Component::ShoeScoreBoard)
        return if shoe.id == board.shoe_id

        board.clear
        board.shoe_id = shoe.id
      end
    end
  end
end
