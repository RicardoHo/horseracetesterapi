# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Load dealt public card from game engine when resume game
    #
    class LoadPublicCard < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :public_card
      command :query_game, :on_load

      private

      def executable?
        super && loadable?
      end

      def before_all
        super
        load_save
        @public_card = @save.dig(:table, :public_card)
      end

      def on_load(entity)
        public_card = entity.find(Component::PublicCard)
        public_card.cards = @public_card
      end
    end
  end
end
