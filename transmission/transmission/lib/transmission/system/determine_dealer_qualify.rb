# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # determine is dealer hand qualify
    #
    class DetermineDealerQualify < Clutch::System::CommandSystem
      watch :best_hand, :player_label
      reference :win_info
      command :raise, :determine
      command :fold, :determine

      private

      def before_all
        super
        @math_model ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
        @ante_paytable  ||= @math_model.dig(:paytable, :ante)
        @qualify_rule   ||= @math_model.dig(:qualify)
        @qualify_score  ||= @qualify_rule.dig(:score)
        @rule_high_card ||= @qualify_rule
          .dig(:constraint, :highest_card, :face)
        @win_info_list    = references[:win_info]
      end

      def after_all
        @win_info_list.each do |win_info_comp|
          win_info_comp.dealer_qualify = @dealer_qualify
        end
        super
      end

      def determine(entity)
        entity_name = entity.find(Component::PlayerLabel).name
        return unless entity_name == 'dealer'

        best_hand = entity.find(Component::BestHand)
        @dealer_qualify = qualify?(best_hand)
      end

      def meet_high_card_constraint?(dealer_high_card)
        Util::PokerHandUtil.card_face_value(dealer_high_card) >= @rule_high_card
      end

      def qualify?(dealer_best_hand)
        hand_score = @ante_paytable
          .dig(dealer_best_hand.rank.snake_case.to_sym, :score)
        hand_score > @qualify_score \
        || (
          hand_score == @qualify_score \
          && meet_high_card_constraint?(dealer_best_hand.cards[0])
        )
      end
    end
  end
end
