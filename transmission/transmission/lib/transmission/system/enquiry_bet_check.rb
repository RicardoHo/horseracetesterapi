# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # Check is able to bet on both player and banker
    # and the bet amount is valid if player do so
    #
    class EnquiryBetCheck < Clutch::System::CommandSystem
      include Helper::BetHelper

      watch :bet_area_label, :bet_amount
      reference :enquiry_bet_indicator
      command :baccarat_deal, :check

      private

      def before_all
        super
        @enquiry_bet_recorder     = -1
        @rest_bet_area_betted     = false
        @player_banker_bet_credit = nil
        @enquiry_bet_indicator ||= references[:enquiry_bet_indicator].first
      end

      #
      # Entry of baccarat deal command
      #
      # @param [Clutch::Entity] entity
      #
      def check(entity)
        bet_credit = entity.find(Component::BetAmount).credit
        return unless bet_credit

        bet_area_name = entity.find(Component::BetAreaLabel).name
        if %w[banker player].include?(bet_area_name)
          @enquiry_bet_recorder      += 1
          @player_banker_bet_credit ||= bet_credit
        elsif bet_credit
          @rest_bet_area_betted = true
        end

        if @enquiry_bet_recorder == 1
          raise EnquiryBetWithOtherBetError if @rest_bet_area_betted
          raise EnquiryBetNotEnableError unless @enquiry_bet_indicator.enable
          raise EnquiryBetAmountInvalidError \
            if bet_credit != @enquiry_bet_indicator.bet_credit ||
               @player_banker_bet_credit != @enquiry_bet_indicator.bet_credit
        end
      end
    end
  end
end
