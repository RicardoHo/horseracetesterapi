# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # Record current keno score to score board
    #
    class RecordKenoScore < Clutch::System::ReactiveSystem
      watch :keno_score_card, :score_board
      reference :stage

      private

      def executable?
        # HACK: avoid duplicate score recording when resume game
        super && !@command.casecmp?('query_game') && !@command.casecmp?('draw_finish')
      end

      def before_all
        super
        @stage ||= references[:stage].first
      end

      def process(entity)
        score_card = entity.find(Component::KenoScoreCard)
        score_card.stage = @stage.to_hash.fetch(:stage)
        board = entity.find(Component::ScoreBoard)
        board.record(
          score_card.to_hash.fetch(:score_card)
        )
      end
    end
  end
end
