# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # calculate payout of san gong
    #
    class CalculateSanGongPayout < Clutch::System::ReactiveSystem
      watch :bet_amount, :payout, :bet_area_label
      reference :san_gong_score_card

      private

      def before_all
        super
        @score_card  ||= references[:san_gong_score_card].first
        @paytable    ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
          .fetch(:paytable)
        @odds_divsior ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
          .fetch(:odds_divsior)
      end

      def process(entity)
        bet_amount = entity.find(Component::BetAmount)
        return unless bet_amount.cash

        bet_name = entity.find(Component::BetAreaLabel).name
        payout = entity.find(Component::Payout)

        if @score_card.payout_list.include?("total_zero") && bet_name.match?(/total_*/)
          payout.cash   = bet_amount.cash
          payout.credit = bet_amount.credit
        end

        return unless @score_card.payout_list.include?(bet_name)

        odds = @paytable.dig(bet_name.to_sym, :odds)
        payout.cash   = bet_amount.cash * odds / @odds_divsior
        payout.credit = bet_amount.credit * odds / @odds_divsior
      end
    end
  end
end
