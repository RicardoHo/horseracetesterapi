# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # compose win type of each bet area which has been bet
    # and reference hand score card for decide the win type
    #
    class ComposeWinType < Clutch::System::ReactiveSystem
      watch :bet_area_label, :bet_amount, :win_type
      reference :hand_score_card

      private

      def before_all
        super
        @hand_score_card ||= references[:hand_score_card].first
      end

      def process(entity)
        bet_amount = entity.find(Component::BetAmount)
        return if bet_amount.credit.zero?

        win_type = entity.find(Component::WinType)
        bet_name = entity.find(Component::BetAreaLabel).name

        subject_name, pair = bet_name.split('_')
        win_type.type = if pair
          pair_win_type(subject_name)
        else
          normal_win_type(subject_name)
        end
      end

      def pair_win_type(subject_name)
        if @hand_score_card.pair&.casecmp?(Enum::Winner::BOTH) ||
            @hand_score_card.pair&.casecmp?(subject_name)
          Enum::WinType::WIN
        else
          Enum::WinType::LOSE
        end
      end

      def normal_win_type(subject_name)
        if subject_name.casecmp?(@hand_score_card.winner)
          Enum::WinType::WIN
        elsif @hand_score_card.winner.casecmp?(Enum::Winner::TIE)
          Enum::WinType::PUSH
        else
          Enum::WinType::LOSE
        end
      end
    end
  end
end
