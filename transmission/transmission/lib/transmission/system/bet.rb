# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Handle bet
    #
    class Bet < Clutch::System::CommandSystem
      include Helper::BetHelper

      watch :bet_area_label, :bet_amount, :max_bet, :min_bet
      reference :denom_info

      command :deal,            :ante_and_aa_bet
      command :bet,             :ante
      command :baccarat_deal,   :bet_on_bet_area
      command :roll,            :bet_on_bet_area
      command :drop,            :bet_on_bet_area
      command :draw,            :bet_on_bet_area
      command :multiple_bet,    :bet_on_bet_area

      private

      def executable?
        super && @input.bet_options
      end

      def before_all
        super
        @denom_info = references[:denom_info].first
      end

      #
      # Entry of deal command
      #
      # @param [Clutch::Entity] entity
      #
      def ante_and_aa_bet(entity)
        bet(:ante, entity, must_present: true)
        bet(:aa, entity)
      end

      #
      # Entry of bet command
      #
      # @param [Clutch::Entity] entity
      #
      def ante(entity)
        bet(:ante, entity, must_present: true)
      end

      #
      # Entry of baccarat deal command
      #
      # @param [Clutch::Entity] entity
      #
      def baccarat_bet(entity)
        name = entity.find(Component::BetAreaLabel).name
        bet(name.to_sym, entity)
      end
      alias_method :bet_on_bet_area, :baccarat_bet
    end
  end
end
