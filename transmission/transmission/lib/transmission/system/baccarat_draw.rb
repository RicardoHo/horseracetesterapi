# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # Baccarat draw means to dealt third card to
    # player and banker if needed by drawing rule of baccarat
    #
    class BaccaratDraw < Clutch::System::ReactiveSystem
      watch :player_label, :hand_card, :hand_point
      reference :shoe, :hand_score_card

      private

      def before_all
        super
        @shoe ||= references[:shoe].first
        @hand_score_card ||= references[:hand_score_card].first
        @participants = {}
      end

      def after_all
        super
        return if active_entities.empty?
        return if @participants.empty?

        draw_for_player if @participants.key?(:player)
        draw_for_banker if @participants.key?(:banker) &&
                           @participants.key?(:player)
      end

      #
      # Handler for baccarat_deal action
      #
      # @param [Clutch::Entity] entity
      #
      def process(entity)
        # in case of resume from last round, 3 cards was on hand
        return if entity.find(Component::HandCard).reach_limit?

        name = entity.find(Component::PlayerLabel).name
        @participants[name.to_sym] = entity
      end

      #
      # determine is banker need draw card or not
      #
      # @param [Clutch::Entity] banker
      # @param [Integer] player_draw_point
      #
      # @return [<Type>] <description>
      #
      def banker_need_draw?(banker, player_draw_point)
        return false if @hand_score_card.nature

        case banker.find(Component::HandPoint).value
        when 0..2
          true
        when 3
          [nil, 0, 1, 2, 3, 4, 5, 6, 7, 9].include?(player_draw_point)
        when 4
          [nil, 2, 3, 4, 5, 6, 7].include?(player_draw_point)
        when 5
          [nil, 4, 5, 6, 7].include?(player_draw_point)
        when 6
          [6, 7].include?(player_draw_point)
        else
          false
        end
      end

      #
      # dealt draw card to participant
      #
      # @param [Clutch::Entity] entity
      #
      def dealt_draw_card(entity)
        hand_card = entity.find(Component::HandCard)
        cards, @shoe.card_pool = Util::RNGUtil
          .draw_multiple_from_weight_table(
            @shoe.card_pool,
            hand_card.max_hold - hand_card.min_hold,
            rng: @game_engine.weight_table
          )
        cards.each { |card| hand_card.collect(card) }
      end

      #
      # handle draw for player
      #
      def draw_for_player
        player = @participants[:player]
        return if @hand_score_card.nature ||
                  player.find(Component::HandPoint).value > 5

        dealt_draw_card(player)
      end

      #
      # handle draw for banker
      # the draw for banekr needs depends on player
      #
      def draw_for_banker
        player = @participants[:player]
        banker = @participants[:banker]
        player_draw_point = Util::PokerHandUtil
          .card_baccarat_point(player.find(Component::HandCard).cards[2])
        return unless banker_need_draw?(banker, player_draw_point)

        dealt_draw_card(banker)
      end
    end
  end
end
