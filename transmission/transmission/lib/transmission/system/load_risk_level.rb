# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.8.0
    #
    # Load risk level from game engine when resume game
    #
    class LoadRiskLevel < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :risk_level
      command :query_game, :on_load

      private

      def executable?
        super && loadable?
      end

      def before_all
        super
        load_save
        @risk_level_index = @save.dig(:table, :risk_level, :index)
      end

      def on_load(entity)
        risk_level = entity.find(Component::RiskLevel)

        risk_level.risk_level_index = @risk_level_index
      end
    end
  end
end
