# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # append the evaluate value of draw card and append it to hand point
    #
    class AppendDrawToHandPoint < Clutch::System::ReactiveSystem
      watch :hand_card, :hand_point

      private

      #
      # main logic
      #
      # @param [Clutch::Entity] entity
      #
      def process(entity)
        draw_card = entity.find(Component::HandCard).cards[2]
        return unless draw_card

        hand_point = entity.find(Component::HandPoint)
        hand_point.value = (
          hand_point.value +
          Util::PokerHandUtil.card_baccarat_point(draw_card)
        ) % 10
      end
    end
  end
end
