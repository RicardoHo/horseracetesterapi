# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # To dealt card to all player - the participant of the game
    # from the shoe
    #
    class DealtPlayerCardFromShoe < Clutch::System::CommandSystem
      watch :hand_card
      reference :shoe

      command :baccarat_deal, :process_dealt

      private

      def before_all
        super
        @shoe ||= references[:shoe].first
      end

      #
      # dealt card core logic, invoke for each entity
      #
      # @param [Clutch::Entity] entity
      #
      def process_dealt(entity)
        hand_card = entity.find(Component::HandCard)
        hand_card.cards, @shoe.card_pool = Util::RNGUtil
          .draw_multiple_from_weight_table(
            @shoe.card_pool,
            hand_card.min_hold,
            rng: @game_engine.weight_table
          )
      end
    end
  end
end
