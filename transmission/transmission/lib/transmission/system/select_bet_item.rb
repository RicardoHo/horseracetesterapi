# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.7.0
    #
    # Select bet item from wagered bet area
    #
    class SelectBetItem < Clutch::System::ReactiveSystem
      watch :value_type_bet_item, :bet_amount, :bet_area_label
      reference :dice_score_card

      private

      def executable?
        super && !@command.casecmp?('query_game')
      end

      def before_process
        super
        @score_card ||= references[:dice_score_card].first
        @paytable   ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
          .dig(:paytable)
      end

      def process(entity)
        bet_credit = entity.find(Component::BetAmount).credit
        return unless bet_credit

        bet_name = entity.find(Component::BetAreaLabel).name
        bet_item = entity.find(Component::ValueTypeBetItem)
        selected_value = @input.bet_options.find do |opt|
          opt.name.casecmp?(bet_name)
        end.bet_item.value
        raise BetItemNotInRangeError unless bet_item.range.include?(selected_value)

        bet_item.value = selected_value
        @score_card.bet_option = {
          name: bet_name,
          bet_item_value: bet_item.value,
          odds: @paytable.dig(bet_name.to_sym, bet_item.value, :odds),
          bet_amount: {
            credit: bet_credit,
            cash: entity.find(Component::BetAmount).cash,
          },
        }
      end
    end
  end
end
