# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # adjust the free game meter by different case
    #
    class AdjustFreeGameMeter < Clutch::System::ReactiveSystem
      watch :id_box
      reference :stage, :free_game_meter, :selected_id_list

      private

      def executable?
        super && !@command.casecmp?('query_game') && !@command.casecmp?('draw_finish')
      end

      def before_process
        super
        @stage               ||= references[:stage].first
        @free_game_meter     ||= references[:free_game_meter].first
        @selected_id_list    ||= references[:selected_id_list].first
        @max_free_game_count ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
          .fetch(:max_free_game_count)
        @paytable            ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
          .fetch(:paytable)
      end

      def process(entity)
        id_box = entity.find(Component::IdBox)

        odds = @paytable.dig(@selected_id_list.list.length, id_box.hit_counter, :odds)
        last_number = id_box.result_id.last
        #
        #
        # case 1 : Current in Free Game, cannot trigger Free Game
        #
        #
        if @stage.current.casecmp?(Enum::Stage::FREE_GAME)
          @free_game_meter.counter = @free_game_meter.counter - 1
          id_box.hit_free_game = false
        #
        #
        # case 2 : Current in Base Game, trigger Free Game
        #
        #
        elsif id_box.hit_id.include?(last_number) && odds > 0
          @free_game_meter.counter = @max_free_game_count
          id_box.hit_free_game = true
        #
        #
        # case 3 : Current in Base Game,not trigger Free Game
        #
        #
        else
          @free_game_meter.counter = @free_game_meter.counter
          id_box.hit_free_game = false
        end
      end
    end
  end
end
