# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Ben B Wu
    # @since 0.1.0
    #
    # Handle bet
    #
    class HrNewRound < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :hr_paytable
      reference :denom_info

      command :query_game, :hr_new_round
      command :new_game, :hr_new_round

      private

      # def executable?
      #   super && (!loadable?).tap{|x|p x}
      #   #super && (!resume?).tap{|x|p x}
      #   # super && (!@game_engine.resume_round?).tap{|x|p x}
      # end

      def before_all
        super
        @par ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
          .dig(:parameters)
 
        @odds_divisor ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
          .dig(:odds_divisor)
      end

      # Get whole table size
      def get_table_size(table)
        table_size = 0
        table.each_value do |item|
          table_size += item.size
        end
        table_size
      end

      # Add each modification to each element in the original table to generate a new table
      #
      # @param [Hash] table0
      # @param [Array] modifications
      def modify_table(original_paytable, modifications)
        table_size = get_table_size(original_paytable)
        if modifications.size() != table_size
          puts("Error: table_size: #{table_size} != modification_size: #{modifications.size}")
          return nil
        end

        i = 0
        modified_paytable = {}
        original_paytable.each_key do |key|
          modified_paytable[key] = Array.new(original_paytable.size)
          original_paytable[key].each_index do |idx|
            # puts modifications[i]
            # table[key][idx] = (table0[key][idx] + modifications[i].to_f) / @odds_divisor
            modified_paytable[key][idx] = original_paytable[key][idx] + modifications[i].to_i
            i += 1
          end
        end
        modified_paytable
      end

      # Generate forecast-type 2-D square table in horse racing from an 1-D array
      #
      # @param [Array] list
      def gen_square(list, n)
        square = Array.new(n) {Array.new(n) {0}}

        idx = 0
        square.each_index do |i|
          square[i].each_index do |j|
            next if j == i

            square[i][j] = list[idx]
            idx += 1
          end
        end
        square
      end

      # Map forecast-type 2-D table with new indexes
      # This function generates new 2-D mapped table
      #
      # @param [Array 2D] square
      # @param [Array] new_idx
      def map_square(square, new_idx)
        new_square = Array.new(square.size()) {Array.new(square.size()) {0}}
        new_square.each_index do |i|
          new_square[i].each_index do |j|
            new_square[new_idx[i]][new_idx[j]] = square[i][j]
          end
        end
        new_square
      end

      # Generate dual-forecast-type 2-D square table in horse racing from an 1-D array
      #
      # @param [Array] list
      def gen_trangle(list, n)
        square = Array.new(n) {Array.new(n) {0}}

        idx = 0
        square.each_index do |i|
          square[i].each_index do |j|
            if j <= i
              next
            end
            square[i][j] = list[idx]
            idx += 1
          end
        end
        square
      end

      # Map dual-forecast-type 2-D table with new indexes
      # This function generates new 2-D mapped table
      #
      # @param [Array 2D] square
      # @param [Array] new_idx
      def map_trangle(square, new_idx)
        new_square = Array.new(square.size()) {Array.new(square.size()) {0}}
        new_square.each_index do |i|
          new_square[i].each_index do |j|
            if j <= i
              next
            end

            new_i = new_idx[i]
            new_j = new_idx[j]
            if new_j <= new_i
              new_i, new_j = new_j, new_i
            end
            new_square[new_i][new_j] = square[i][j]
          end
        end
        new_square
      end

      # Map 1-D array with new indexes
      # This function generates new 1-D mapped array
      #
      # @param [Array] list
      # @param [Array] new_idx
      def map_array(list, new_idx)
        new_list = Array.new(list.size()){0}
        new_list.each_index do |k|
          # new_list[k] = list[new_idx[k]]
          new_list[new_idx[k]] = list[k]
        end
        new_list
      end

      # Reshape 2-D parts of the table to 1-D array
      #
      # @param [Hash] table
      def reshape_1d(table)
        tmp = []
        table.each do |e|
          tmp += e
        end
        tmp
      end

      # Map horse racing table with new indexes
      # This function generates new mapped table
      #
      # @param [Hash] original_paytable
      # @param [Array] shuffle_map
      def map_hr_table(original_paytable, shuffle_map)
        n = original_paytable[:win].size
        new_paytable = {}

        new_paytable[:win] = map_array(original_paytable[:win], shuffle_map)
        new_paytable[:place] = map_array(original_paytable[:place], shuffle_map)

        dual_forecast_2d = gen_trangle(original_paytable[:dual_forecast], n)
        new_paytable[:dual_forecast] = reshape_1d(map_trangle(dual_forecast_2d, shuffle_map))

        forecast_2d = gen_square(original_paytable[:forecast], n)
        new_paytable[:forecast] = reshape_1d(map_square(forecast_2d, shuffle_map))
        new_paytable
      end

      # generate permutation from an index number
      #
      # @param [Int] idx
      # @param [Int] n, n is the length of the permutation
      def gen_perm(idx, n)
        permuted = [0] * n
        elems = (0...n).to_a

        # for i in 0...n
        (0...n).each do |i|
          k = idx % (n - i)
          idx /= n-i
          permuted[i] = elems[k]
          elems[k] = elems[n-i-1]
        end
        permuted
      end

      # generate new float table from the original integer table
      #
      # @param [Hash] table_int
      def gen_float_table(table_int)
        table_float = {}
        table_float['win'] = Array.new(table_int[:win].size, 0.0)
        table_int[:win].each_index {|i| table_float['win'][i] = table_int[:win][i].to_f / @odds_divisor}

        table_float['place'] = Array.new(table_int[:place].size, 0.0)
        table_int[:place].each_index {|i| table_float['place'][i] = table_int[:place][i].to_f / @odds_divisor}

        table_float['dual_forecast'] = Array.new(table_int[:dual_forecast].size, 0.0)
        table_int[:dual_forecast].each_index do |i| 
          table_float['dual_forecast'][i] = table_int[:dual_forecast][i].to_f / @odds_divisor
        end

        table_float['forecast'] = Array.new(table_int[:forecast].size, 0.0)
        table_int[:forecast].each_index do |i| 
          table_float['forecast'][i] = table_int[:forecast][i].to_f / @odds_divisor
        end

        table_float
      end

      # new round of horse racing
      def hr_new_round(entity)
        # when resuming, new paytable should not be generated, so the last-round paytale is reserved.
        return if loadable? && @command.casecmp?('query_game')

        hr_math_par = entity.find(Component::HrMathPar)
        hr_paytable = entity.find(Component::HrPaytable)

        # use a random group from 10 group of parameters
        group_idx = Util::RNGUtil.random(
          @par.length, 
          rng: @game_engine.rng,) 
          # rng_capture: @game_engine.rng_capture)

        # group_idx = 1
        hr_math_par.group_idx = group_idx

        paytable = @par.dig(group_idx, :paytable)
        modification_weight = @par.dig(group_idx, :modification_weight_table)

        table_size = get_table_size(paytable)
        modifications = Util::RNGUtil.draw_multiple_from_weight_table(
          modification_weight,
          table_size,
          replacement: true,
          rng: @game_engine.weight_table, 
          # rng_capture: @game_engine.rng_capture
        ).first
        hr_math_par.modifications = modifications

        # add modifications to each element
        modified_paytable = modify_table(paytable, modifications)

        # shuffle: generates a shuffle_map and use it to shuffle the whole paytable
        n = paytable[:win].length
        shuffle_idx = Util::RNGUtil.random(
          (1..n).inject(:*), 
          rng: @game_engine.rng,)
          # rng_capture: @game_engine.rng_capture)
        shuffle_map = gen_perm(shuffle_idx, n)

        # shuffle_map = Array[0, 1, 2, 3, 4, 5, 6]
        hr_math_par.shuffle_map = shuffle_map
        hr_math_par.shuffle_idx = shuffle_idx

        map_table = map_hr_table(modified_paytable, shuffle_map)

        hr_paytable.paytable_int = map_table
        hr_paytable.hr_paytable = gen_float_table(map_table)
      end
    end
  end
end
