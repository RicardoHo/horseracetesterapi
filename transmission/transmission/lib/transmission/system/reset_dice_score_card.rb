# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.7.0
    #
    # Reset score card to stand by state
    #
    class ResetDiceScoreCard < Clutch::System::CleanupSystem
      watch :dice_score_card
      cleanup :reset
    end
  end
end
