# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 1.0.0
    #
    # Handle player selected chip
    #
    class SetSelectedChip < Clutch::System::CommandSystem
      watch :chip
      command :deal, :save_chip
      command :bet, :save_chip
      command :baccarat_deal, :save_chip
      command :roll, :save_chip
      command :drop, :save_chip
      command :draw, :save_chip

      private

      def executable?
        super && @input.player&.chip_index
      end

      #
      # Entry method
      #
      # @param [Clutch::Entity] entity
      #
      def save_chip(entity)
        chip = entity.find(Component::Chip)
        chip.selected_index = @input.player.chip_index
      end
    end
  end
end
