# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # To dealt card to all player - the participant of the game
    #
    class DealtPlayerCard < Clutch::System::CommandSystem
      watch :hand_card
      reference :card_deck

      command :deal, :process_dealt
      command :bet,  :process_dealt
      command :multiple_bet, :process_dealt

      private

      def before_all
        super
        @deck ||= references[:card_deck].first
      end

      #
      # dealt card core logic, invoke for each entity
      #
      # @param [Clutch::Entity] entity
      #
      def process_dealt(entity)
        hand_card = entity.find(Component::HandCard)
        cards, @deck.card_pool = Util::RNGUtil
          .draw_multiple_from_weight_table(
            @deck.card_pool,
            hand_card.min_hold,
            rng: @game_engine.weight_table
          )
        hand_card.collect_batch(cards)
      end
    end
  end
end
