# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Compare all player's hand to find the win hand
    #
    class HandCompare < Clutch::System::ReactiveSystem
      watch :best_hand, :player_label
      reference :win_info

      private

      def executable?
        super && @command.casecmp?('raise')
      end

      def before_all
        super
        @math_model ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
        @ante_paytable  ||= @math_model.dig(:paytable, :ante)
        @win_info_list    = references[:win_info]
        @win_entity_name  = nil
        @win_entity_odds  = 0
        @win_entity_score = 0
      end

      def after_all
        win_type = case @win_entity_name
        when 'player'
          Enum::WinType::WIN
        when 'dealer'
          Enum::WinType::LOSE
        when 'pair'
          Enum::WinType::PUSH
        end
        @win_info_list.each do |win_info|
          win_info.type = win_info.dealer_qualify ? win_type : Enum::WinType::WIN
          win_info.odds = @win_entity_odds
        end
        super
      end

      def process(entity)
        entity_name = entity.find(Component::PlayerLabel).name
        best_hand   = entity.find(Component::BestHand)

        current_odds    = @ante_paytable
          .dig(best_hand.rank.snake_case.to_sym, :odds)
        current_score   = @ante_paytable
          .dig(best_hand.rank.snake_case.to_sym, :score)
        case hand_compare(current_score, best_hand.cards)
        when 1
          @win_entity_name  = entity_name
          @win_entity_odds  = current_odds
          @win_entity_score = current_score
          @win_entity_cards = best_hand.cards
        when 0
          @win_entity_name  = 'pair'
        end
      end

      def hand_compare(current_score, cards)
        return 1 if current_score > @win_entity_score
        return -1 if current_score < @win_entity_score

        card_values(cards) <=> card_values(@win_entity_cards)
      end

      def card_values(cards)
        cards.map do |card|
          Util::PokerHandUtil.card_face_value(card)
        end
      end
    end
  end
end
