# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.8.0
    #
    # Select bet item from wagered bet area
    #
    class BallSelectBetItem < Clutch::System::ReactiveSystem
      watch :bet_amount, :bet_area_label
      reference :ball_score_card, :lines_info, :risk_level

      private

      def executable?
        super && @command.casecmp?('drop')
      end

      def before_process
        super
        @score_card ||= references[:ball_score_card].first
        @lines_info ||= references[:lines_info].first
        @risk_level ||= references[:risk_level].first
      end

      def process(entity)
        bet_credit = entity.find(Component::BetAmount).credit
        return unless bet_credit

        bet_name = entity.find(Component::BetAreaLabel).name

        selected_risk_level = @risk_level.levels.find do |name|
          name.casecmp?(@input.selected_risk_level)
        end

        unless selected_risk_level
          raise SelectedRiskLevelNotInOptionsError,
            "Select risk level #{@input.selected_risk_level} not in options."
        end

        unless @lines_info.lines.include?(@input.selected_line)
          raise SelectedLineNotInRangeError,
            "Select line #{@input.selected_line} not in range #{[@lines_info.lines.min, @lines_info.lines.max]}."
        end

        @risk_level.risk_level_index = @risk_level.levels.index(selected_risk_level)
        @lines_info.line = @input.selected_line
        @lines_info.line_index = @lines_info.lines.to_a.index(@lines_info.line)
        @score_card.bet_option = {
          selected_bet_options: {
            name: bet_name,
            amount: {
              credit: bet_credit,
              cash: entity.find(Component::BetAmount).cash,
            },
          },
          selected_risk_level: selected_risk_level,
          selected_line: @lines_info.line,
        }
      end
    end
  end
end
