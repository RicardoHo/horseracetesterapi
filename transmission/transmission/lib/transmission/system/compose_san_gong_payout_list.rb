# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # To determine the winner of baccarat hand by
    # compare the points
    #
    class ComposeSanGongPayoutList < Clutch::System::ReactiveSystem
      watch :player_label, :hand_card, :hand_point
      reference :san_gong_score_card, :score_card_tag

      private

      def before_all
        super
        @score_card ||= references[:san_gong_score_card].first
        @score_card_tag ||= references[:score_card_tag].first
      end

      def process(entity)
        @score_card.payout_list
        player_name = entity.find(Component::PlayerLabel).name
        cards = entity.find(Component::HandCard).cards
        point = entity.find(Component::HandPoint).value

        if @score_card.winner == player_name
          @score_card.payout_list.append(player_name)
          case point
          when 9
            @score_card.payout_list.append("total_9")
          when 7..8
            @score_card.payout_list.append("total_7_8")
          when 1..6
            @score_card.payout_list.append("total_1_6")
          else
            @score_card.payout_list.append("total_zero")
          end
        end

        if point.even?
          @score_card.payout_list.append("#{player_name}_even")
        else
          @score_card.payout_list.append("#{player_name}_odd")
        end

        @score_card.payout_list.append("#{player_name}_pair") if pair?(cards)
        @score_card_tag.dirty = true
      end

      def pair?(cards)
        cards.map { |card| card[0] }
          .group_by { |face| face }
          .count { |_k, v| v.size >= 2 } > 0
      end
    end
  end
end
