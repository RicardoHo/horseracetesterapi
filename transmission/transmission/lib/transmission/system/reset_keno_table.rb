# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # Reset keno table to stand by state
    #
    class ResetKenoTable < Clutch::System::CleanupSystem
      watch :keno_score_card, :id_box
      cleanup :reset
    end
  end
end
