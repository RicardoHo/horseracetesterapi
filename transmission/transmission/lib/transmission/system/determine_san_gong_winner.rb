# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # To determine the winner of san gong hand
    #
    #
    class DetermineSanGongWinner < Clutch::System::ReactiveSystem
      watch :player_label, :hand_card, :hand_point
      reference :san_gong_score_card, :win_type_list

      private

      def before_all
        super
        @score_card    ||= references[:san_gong_score_card].first
        @win_type_list ||= references[:win_type_list].first
      end

      def process(entity)
        player_name = entity.find(Component::PlayerLabel).name
        cards = entity.find(Component::HandCard).cards
        point = entity.find(Component::HandPoint).value

        @player_items ||= {}

        @player_items[player_name.to_sym] = {
          name: player_name,
          cards: cards,
          point: point,
          best_card: cards.max { |a, b| Util::PokerHandUtil.card_compare(a, b, ignore_suit: false, low_ace: true) },
        }
      end

      def after_all
        return unless @player_items

        @score_card.winner = three_of_a_kind_winner ||
          three_picture_card_winner ||
          hand_value_winner

        @score_card.point = @player_items.dig(@score_card.winner.to_sym, :point)
        super
      end

      #
      # determine the winner who win by hand card have 3_of_a_kind
      #
      # @return [String] return the winner name or nil for do not have winner
      #
      def three_of_a_kind_winner
        winner = @player_items.select do |_player, items|
          face = items.fetch(:cards).first[0]
          items.fetch(:cards).all? { |card| card[0].casecmp?(face) }
        end&.keys

        case winner.length
        when 1
          @score_card.win_type = @win_type_list.type_list[0]
          winner.first.to_s
        when 2
          @score_card.win_type = @win_type_list.type_list[0]
          best_card_winner
        end
      end

      #
      # determine the winner who win by hand card have 3 picture card
      #
      # @return [String] return the winner name or nil for both of two player have/don't have 3 picture card
      #
      def three_picture_card_winner
        winner = @player_items.select do |_player, items|
          items.fetch(:cards).all? { |card| Util::PokerHandUtil.picture_card?(card[0]) }
        end&.keys

        case winner.length
        when 1
          @score_card.win_type = @win_type_list.type_list[1]
          winner.first.to_s
        when 2
          @score_card.win_type = @win_type_list.type_list[1]
          best_card_winner
        end
      end

      #
      # determine the winner who win by hand value
      #
      # @return [String] return the winner name or nil for both of two player have same hand value
      #
      def hand_value_winner
        @score_card.win_type = @win_type_list.type_list[2]

        return best_card_winner if @player_items.values.first.fetch(:point) == @player_items.values.last.fetch(:point)

        @player_items.values.max do |a, b|
          a.fetch(:point) <=> b.fetch(:point)
        end.fetch(:name)
      end

      #
      # determine the winner who win by hand best card
      #
      # @return [String] return the winner name
      #
      def best_card_winner
        @player_items.values.max do |a, b|
          Util::PokerHandUtil.card_compare(a.fetch(:best_card), b.fetch(:best_card), ignore_suit: false, low_ace: true)
        end.fetch(:name)
      end
    end
  end
end
