# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # Burn cards of new shoe, the amount of burns is
    # decided by the face value of first card draw
    #
    class BurnCardByDraw < Clutch::System::CommandSystem
      watch :burn_card_indicator, :shoe
      command :query_game, :burn_card
      command :reshuffle,  :burn_card

      private

      def burn_card(entity)
        @shoe = entity.find(Component::Shoe)
        return if @shoe.remaining_amount < 416

        card, @shoe.card_pool = Util::RNGUtil
          .draw_from_weight_table(
            @shoe.card_pool,
            rng: @game_engine.weight_table
          )
        face_value = Util::PokerHandUtil.card_face_value(card, low_ace: true)
        _cards, @shoe.card_pool = Util::RNGUtil
          .draw_multiple_from_weight_table(
            @shoe.card_pool,
            face_value,
            rng: @game_engine.weight_table
          )

        burn_card_machine = entity.find(Component::BurnCardIndicator)
        burn_card_machine.draw  = card
        burn_card_machine.total = face_value + 1
      end
    end
  end
end
