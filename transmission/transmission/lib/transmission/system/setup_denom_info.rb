# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Setup default denom info by game engine
    #
    class SetupDenomInfo < Clutch::System::CommandSystem
      watch :denom_info
      command :query_game, :setup

      private

      #
      # setup the denom set and denom index to DenomInfo
      #
      # @param [Clutch::Entity] entity
      #
      def setup(entity)
        denom_info = entity.find(Component::DenomInfo)

        denom_info.denoms      = @game_engine.game_config.denoms.value
        denom_info.denom_index = @game_engine.game_config.denoms.default_index
        denom_info.denom       = denom_info.denoms[denom_info.denom_index]
      end
    end
  end
end
