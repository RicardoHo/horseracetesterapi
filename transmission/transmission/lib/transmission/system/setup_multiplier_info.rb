# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # Setup default multiplier info by game engine
    #
    class SetupMultiplierInfo < Clutch::System::CommandSystem
      watch :multiplier_info
      command :query_game, :setup

      private

      #
      # setup the multiplier set and multiplier index to MultiplierInfo
      #
      # @param [Clutch::Entity] entity
      #
      def setup(entity)
        multiplier_info = entity.find(Component::MultiplierInfo)

        multiplier_config = @game_engine
          .game_config
          .extra.dig(:multipliers)
        raise MultiplierSetNotFoundError unless multiplier_config

        multiplier_config = multiplier_config.split(",")

        raise MultiplierSetNotAllIntegerError unless
          multiplier_config.all? { |num_str| num_str.match?(/^(\d)+$/) }

        multiplier_info.multipliers = multiplier_config.map(&:to_i)
        raise MultiplierSetNotFoundError unless
          multiplier_info.multipliers

        raise MultiplierSetNotAllPositiveError unless
          multiplier_info.multipliers.all?(&:positive?)

        multiplier_info.multiplier_index = @game_engine.game_config.extra.dig(:multipliers_default_index)
        raise MultiplierDefaultIndexNotFoundError unless
          multiplier_info.multiplier_index

        raise MultiplierDefaultIndexNotIntegerError unless
          multiplier_info.multiplier_index.is_a?(Integer)

        raise MultiplierDefaultIndexNotInRangeError unless
          (0...multiplier_info.multipliers.size).cover?(multiplier_info.multiplier_index)

        multiplier_info.multiplier = multiplier_info.multipliers[multiplier_info.multiplier_index]
      end
    end
  end
end
