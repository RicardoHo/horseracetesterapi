# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Cleanup action win
    #
    class CleanupActionWin < Clutch::System::CleanupSystem
      watch :action_win
      cleanup :reset
    end
  end
end
