# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.6.0
    #
    # Reset game context components
    #
    class ResetGameContext < Clutch::System::CleanupSystem
      watch :payout_distributions
      cleanup :reset
    end
  end
end
