# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Compose AA bet's win result
    # Deal command will predict the win type by aa rank
    # Fold command direct treat the AA bet as lose
    #
    class ComposeAaWin < Clutch::System::CommandSystem
      watch :win_type, :bet_amount
      reference :aa_rank
      command :deal, :predict
      command :fold, :voided

      private

      def before_all
        super
        @aa_paytable ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
          .dig(:paytable, :aa)
        @aa_rank = references[:aa_rank].first
      end

      def predict(entity)
        bet_amount = entity.find(Component::BetAmount)
        return unless bet_amount.cash

        win_type   = entity.find(Component::WinType)
        pay_item   = @aa_paytable.dig(@aa_rank.rank.snake_case.to_sym)
        win_type.type = if pay_item && meet_constraint?(pay_item)
          Enum::WinType::WIN
        else
          Enum::WinType::LOSE
        end
      end

      def voided(entity)
        win_type = entity.find(Component::WinType)
        win_type.type = Enum::WinType::LOSE
      end

      def meet_constraint?(pay_item)
        constraint = pay_item.dig(:constraint)
        return true unless constraint

        Util::PokerHandUtil.card_compare(
          @aa_rank.cards[0],
          constraint.dig(:highest_card, :face)
        ) > -1
      end
    end
  end
end
