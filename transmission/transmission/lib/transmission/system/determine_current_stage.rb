# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # determine the current stage on free game meter
    #
    class DetermineCurrentStage < Clutch::System::CommandSystem
      watch :stage, :free_game_meter

      command :draw,            :determine_current_stage
      command :draw_free_game,  :determine_current_stage

      private

      def determine_current_stage(entity)
        stage   = entity.find(Component::Stage)
        counter = entity.find(Component::FreeGameMeter).counter

        stage.current = if counter > 0
          Enum::Stage::FREE_GAME
        else
          Enum::Stage::BASE_GAME
        end
      end
    end
  end
end
