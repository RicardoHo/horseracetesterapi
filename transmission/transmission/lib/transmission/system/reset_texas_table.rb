# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    # rename since 0.3.0
    #
    # Reset table to stand by state
    #
    class ResetTexasTable < Clutch::System::CleanupSystem
      watch :public_card, :stage, :card_deck
      cleanup :reset, condition: :deal?

      def deal?(_entity)
        @command.casecmp?('deal')
      end
    end
  end
end
