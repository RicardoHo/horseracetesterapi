# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # accumulate all bet area's payout to action win
    #
    class AccumulatePayout < Clutch::System::ReactiveSystem
      watch :payout
      reference :action_win

      private

      def before_all
        super
        @action_win ||= references[:action_win].first
      end

      def process(entity)
        payout = entity.find(Component::Payout)

        @action_win.cash   += payout.cash
        @action_win.credit += payout.credit
      end
    end
  end
end
