# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.2.0
    #
    # Setup min and max bet from game engine
    #
    class SetupMinMaxBet < Clutch::System::CommandSystem
      watch :bet_area_label, :max_bet, :min_bet
      reference :denom_info
      command :query_game, :setup

      private

      def before_all
        super
        @denom_info ||= references[:denom_info].first
      end

      #
      # setup the max bet credit amount
      #
      # @param [Clutch::Entity] entity
      #
      def setup(entity)
        bet_area_label = entity.find(Component::BetAreaLabel)
        max_bet_comp   = entity.find(Component::MaxBet)
        min_bet_comp   = entity.find(Component::MinBet)

        if max_bet_comp.credit.nil?
          max_bet_comp.credit = retrieve_limit_bet(
            :"max_#{bet_area_label.name}_bet_in_credit"
          )
        end

        if min_bet_comp.credit.nil?
          min_bet_comp.credit = retrieve_limit_bet(
            :"min_#{bet_area_label.name}_bet_in_credit"
          )
        end

        max_bet_comp.cash   = @denom_info.denoms.max * max_bet_comp.credit
        min_bet_comp.cash   = @denom_info.denoms.min * min_bet_comp.credit
      end

      def retrieve_limit_bet(data_name)
        if respond_to?(data_name, true)
          send(data_name)
        elsif @game_engine.game_config.extra.key?(data_name)
          @game_engine.game_config.extra.fetch(data_name)
        else
          @game_engine.game_config.extra.fetch(:"#{data_name.to_s.split('_')[0]}_bet_in_credit")
        end
      end

      def max_raise_bet_in_credit
        @game_engine.game_config.extra.fetch(:max_ante_bet_in_credit) * 2
      end

      def min_raise_bet_in_credit
        @game_engine.game_config.extra.fetch(:min_ante_bet_in_credit) * 2
      end
    end
  end
end
