# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    #
    #
    class CheckGameSequence < Clutch::System::CommandSystem
      watch :free_game_meter

      command :draw_free_game, :check_draw_free_game
      command :draw_finish,    :check_draw_finish

      private

      def check_draw_free_game(entity)
        counter = entity.find(Component::FreeGameMeter).counter
        if counter == 0
          raise GameSequenceWrongError,
            "Game sequence wrong, you not trigger free game"
        end
      end

      def check_draw_finish(entity)
        counter = entity.find(Component::FreeGameMeter).counter
        unless counter == 0
          raise GameSequenceWrongError,
            "Game sequence wrong, free game not finish"
        end
      end
    end
  end
end
