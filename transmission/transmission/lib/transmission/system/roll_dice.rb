# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.7.0
    #
    # Roll dice and calculate the points
    #
    class RollDice < Clutch::System::ReactiveSystem
      watch :dice_score_card, :dice_cup

      private

      def process(entity)
        dice_cup   = entity.find(Component::DiceCup)
        score_card = entity.find(Component::DiceScoreCard)

        dice_cup.dices = Util::RNGUtil
          .random_multiple(dice_cup.num_of_dice, 6, repeat: true, rng: @game_engine.rng)
          .map { |dice| dice + 1 }
        score_card.point = dice_cup.dices.sum
      end
    end
  end
end
