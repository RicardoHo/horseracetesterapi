# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # Setup default unit bet info by game engine
    #
    class SetupUnitBet < Clutch::System::CommandSystem
      watch :unit_bet
      command :query_game, :setup

      private

      #
      # setup the unit bet credit to UnitBet
      #
      # @param [Clutch::Entity] entity
      #
      def setup(entity)
        unit_bet = entity.find(Component::UnitBet)

        unit_bet.credit = @game_engine.game_config.extra[:unit_bet_in_credit]
        raise UnitBetNotFoundError unless unit_bet.credit
      end
    end
  end
end
