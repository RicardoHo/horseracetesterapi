# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Load stage from game engine when resume game
    #
    class LoadStage < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :stage
      command :query_game, :on_load

      private

      def executable?
        super && resume?
      end

      def before_all
        super
        load_save
        @stage_data = @save.dig(:table, :stage)
      end

      def on_load(entity)
        stage = entity.find(Component::Stage)
        stage.current = @stage_data.fetch(:current)
        stage.next    = @stage_data.dig(:next)
      end
    end
  end
end
