# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Create bet area components
    #
    class CreateBetArea < Clutch::System::InitializeSystem
      private

      def process
        game_object_attribute = @game_def.game_config.game_object.attribute
        game_object_attribute.dig(:bet_area).each do |bet_area_attribute|
          create_bet_area(bet_area_attribute)
        end
      end

      #
      # create bet area entity and it's components
      #
      # @param [String] bet_area_attribute
      #
      # @return [Clutch::Entity]
      #
      def create_bet_area(bet_area_attribute)
        components = begin
          components_attr = bet_area_attribute.fetch(:components)
          components_attr.each_with_object([]) do |(key, value), comps|
            init_data = value&.fetch(:init, []) || []
            comps.push(
              Transmission::Component
                .const_get(key.to_s.camel_case)
                .new(*init_data)
            )
          end
        end
        create_entity(*components)
      end
    end
  end
end
