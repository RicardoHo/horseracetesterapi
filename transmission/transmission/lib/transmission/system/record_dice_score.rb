# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.7.0
    #
    # Record current dice score to score board
    #
    class RecordDiceScore < Clutch::System::ReactiveSystem
      watch :dice_score_card, :score_board

      private

      def executable?
        # HACK: avoid duplicate score recording when resume game
        super && !@command.casecmp?('query_game')
      end

      def process(entity)
        board = entity.find(Component::ScoreBoard)
        board.record(
          entity.find(Component::DiceScoreCard).to_hash.fetch(:score_card)
        )
      end
    end
  end
end
