# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # evaluate the value of hand and store it as point
    #
    class EvaluateHandValue < Clutch::System::ReactiveSystem
      watch :hand_card, :hand_point

      private

      #
      # Handler for baccarat_deal
      #
      # @param [Clutch::Entity] entity
      #
      def process(entity)
        hand_card  = entity.find(Component::HandCard)
        hand_point = entity.find(Component::HandPoint)

        hand_point.value = hand_card.cards.first(hand_card.min_hold).sum do |card|
          Util::PokerHandUtil.card_baccarat_point(card)
        end % 10
      end
    end
  end
end
