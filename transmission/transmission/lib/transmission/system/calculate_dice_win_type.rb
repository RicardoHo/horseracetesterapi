# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.7.0
    #
    # calculate the win type base on dice result
    #
    class CalculateDiceWinType < Clutch::System::ReactiveSystem
      watch :dice_score_card

      private

      def process(entity)
        score_card = entity.find(Component::DiceScoreCard)

        win_flag = send(
          :"#{score_card.bet_option.dig(:name)}?",
          score_card.point,
          score_card.bet_option.dig(:bet_item_value)
        )
        score_card.win_type = win_flag ? Enum::WinType::WIN : Enum::WinType::LOSE
      end

      def under?(dice_points, bet_point)
        dice_points < bet_point
      end

      def over?(dice_points, bet_point)
        dice_points > bet_point
      end

      def tie?(dice_points, bet_point)
        dice_points == bet_point
      end
    end
  end
end
