# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.7.0
    #
    # Load bet item from game engine when resume game
    #
    class LoadValueTypeBetItem < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :value_type_bet_item, :bet_area_label
      command :query_game, :on_load

      private

      def executable?
        super && loadable?
      end

      def before_all
        super
        load_save
        @bet_options_data = @save.dig(:bet_options)
      end

      def on_load(entity)
        bet_area_label = entity.find(Component::BetAreaLabel)
        bet_item_data  = find_bet_item_data(bet_area_label.name)
        return unless bet_item_data

        entity
          .find(Component::ValueTypeBetItem)
          .load(bet_item_data)
      end

      def find_bet_item_data(bet_area_name)
        if @bet_options_data.is_a?(Array)
          @bet_options_data.find do |opt|
            bet_area_name.casecmp?(opt[:name])
          end.dig(:bet_item)
        elsif @bet_options_data.is_a?(Hash)
          @bet_options_data.dig(:bet_item)
        end
      end
    end
  end
end
