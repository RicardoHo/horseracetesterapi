# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # Load skip hand indicator from game engine when resume
    #
    class LoadSkipHandIndicator < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :skip_hand_indicator
      command :query_game, :on_load

      private

      def executable?
        super && instance_loadable?
      end

      def before_all
        super
        load_instance_save
      end

      def on_load(entity)
        indicator = entity.find(Component::SkipHandIndicator)
        indicator.count = @instance_save.dig(:skip_hand_indicator, :count)
      end
    end
  end
end
