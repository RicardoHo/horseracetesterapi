# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Calculate the rank by dealt cards at first bet,
    # public cards included.
    #
    class AaBetRank < Clutch::System::CommandSystem
      watch :hand_card, :public_card, :aa_rank
      command :deal, :calculate_rank

      private

      def calculate_rank(entity)
        aa_rank     = entity.find(Component::AaRank)
        hand_card   = entity.find(Component::HandCard)
        public_card = entity.find(Component::PublicCard)

        aa_rank.cards, aa_rank.rank = Util::PokerHandUtil
          .best_hand(hand_card.cards + public_card.cards)
      end
    end
  end
end
