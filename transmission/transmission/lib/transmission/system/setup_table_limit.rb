# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # Setup table limit which means the table min and max bet
    #
    class SetupTableLimit < Clutch::System::CommandSystem
      watch :table_label, :max_bet, :min_bet
      reference :denom_info
      command :query_game, :setup

      private

      def before_all
        super
        @denom_info ||= references[:denom_info].first
      end

      #
      # setup the max and min bet credit amount
      #
      # @param [Clutch::Entity] entity
      #
      def setup(entity)
        max_bet_comp = entity.find(Component::MaxBet)
        min_bet_comp = entity.find(Component::MinBet)

        max_bet_comp.credit =
          @game_engine.game_config.extra.fetch(:max_table_bet_in_credit)
        min_bet_comp.credit =
          @game_engine.game_config.extra.fetch(:min_table_bet_in_credit)
        max_bet_comp.cash   = @denom_info.denoms.max * max_bet_comp.credit
        min_bet_comp.cash   = @denom_info.denoms.min * min_bet_comp.credit
      end
    end
  end
end
