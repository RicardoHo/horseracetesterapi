# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # To determine is the first two cards on hand is nature or not
    # then store the result to hand score card
    #
    class DetermineNature < Clutch::System::ReactiveSystem
      watch :player_label, :hand_point
      reference :hand_score_card

      private

      def before_all
        super
        @hand_score_card ||= references[:hand_score_card].first
      end

      def process(entity)
        hand_point = entity.find(Component::HandPoint).value
        return if hand_point < 8

        @hand_score_card.nature = if @hand_score_card.nature
          Enum::Winner::BOTH
        else
          entity.find(Component::PlayerLabel).name.upcase
        end
      end
    end
  end
end
