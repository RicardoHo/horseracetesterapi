# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.6.0
    #
    # Record
    #
    class RecordLotteryTotalPayout < Clutch::System::ReactiveSystem
      watch :action_win
      reference :total_win_cash

      private

      def before_all
        super
        @total_win_cash ||= references[:total_win_cash].first
      end

      def process(entity)
        @total_win_cash.value = entity.find(Component::ActionWin).cash
      end
    end
  end
end
