# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.5.0
    #
    # substitute joker card to other poker card
    class SubstituteJokerCard < Clutch::System::ReactiveSystem
      include Helper::PokerHandHelper

      watch :hand_card

      private

      def process(entity)
        @paytable ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
          .dig(:paytable, :ante)

        @hand_card = entity.find(Component::HandCard)
        return unless Util::ShoeCardsUtil.contain_joker?(@hand_card.cards)

        @non_joker_cards = @hand_card.cards.select do |card|
          !card.include?('Joker')
        end
        @joker_cards = @hand_card.cards - @non_joker_cards
        substitute_joker
      end

      def substitute_joker
        joker_indexes = @joker_cards.map do |card|
          @hand_card.cards.index(card)
        end
        list = @joker_cards.each_with_object([]) do |joker, result|
          color = joker[-1].downcase
          cards = Util::ShoeCardsUtil::JOKER_CARDS_MAP[color.to_sym] - @non_joker_cards
          result.push(cards)
        end

        substitute_cards = nil
        current_best_rank_idx = @paytable.size
        candidate_cards_comb = list[0].product(*list[1..-1])
        candidate_cards_comb.each do |candidate_cards|
          @card_faces = (@non_joker_cards + candidate_cards).map do |card|
            Util::PokerHandUtil.card_face_value(card)
          end.sort_by(&:-@)
          @card_suits = (@non_joker_cards + candidate_cards).map { |card| card[1] }
          current_best_rank = @paytable.keys[0..current_best_rank_idx - 1].find do |rank|
            send(:"#{rank}?") && fulfill_constraint?(rank)
          end
          next unless current_best_rank

          current_best_rank_idx = @paytable.keys.index(current_best_rank)
          substitute_cards = candidate_cards
          break if current_best_rank_idx == 0
        end

        substitute_cards = candidate_cards_comb.sample unless substitute_cards
        joker_indexes.each do |index|
          @hand_card.equivalent_cards[index] = substitute_cards.shift
        end
      end
    end
  end
end
