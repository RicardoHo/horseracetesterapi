# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # Reset burn card indicator
    #
    class ResetBurnCardIndicator < Clutch::System::CleanupSystem
      watch :burn_card_indicator
      cleanup :reset
    end
  end
end
