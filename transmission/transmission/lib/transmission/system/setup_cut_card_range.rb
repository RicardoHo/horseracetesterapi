# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # Setup cut card range from operation config
    #
    class SetupCutCardRange < Clutch::System::CommandSystem
      watch :shoe
      command :query_game, :setup

      private

      #
      # query game setup entry
      #
      # @param [Clutch::Entity] entity
      #
      def setup(entity)
        shoe = entity.find(Component::Shoe)
        from = @game_engine.game_config.extra.fetch(:cut_card_range_from)
        to   = @game_engine.game_config.extra.fetch(:cut_card_range_to)
        raise CutCardRangeParameterError if !to.is_a?(Integer) || !from.is_a?(Integer)
        raise CutCardRangeParameterError if to < from
        raise CutCardRangeParameterError unless shoe.cut_card_valid_range.include?(to)
        raise CutCardRangeParameterError unless shoe.cut_card_valid_range.include?(from)

        shoe.init_cut_card_range(from..to)
      end
    end
  end
end
