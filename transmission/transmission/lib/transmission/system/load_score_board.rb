# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.7.0
    #
    # Load score board from game engine when resume game
    #
    class LoadScoreBoard < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :score_board
      command :query_game, :on_load

      private

      def executable?
        super && loadable?
      end

      def before_all
        super
        load_save
        @board_data = @save.dig(:table, :score_board)
      end

      def on_load(entity)
        entity
          .find(Component::ScoreBoard)
          .load(@board_data)
      end
    end
  end
end
