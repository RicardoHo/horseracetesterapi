# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Jason Lei
    # @since 0.2.0
    #
    # Load player hand and card combination from game engine when resume game
    #
    class LoadPlayerHandWithBestHand < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :hand_card, :best_hand, :player_label
      command :query_game, :on_load

      private

      def executable?
        super && loadable?
      end

      def before_all
        super
        load_save
      end

      def on_load(entity)
        player_label      = entity.find(Component::PlayerLabel)
        hand_card         = entity.find(Component::HandCard)
        best_hand         = entity.find(Component::BestHand)
        player_data = @save
          .fetch(player_label.name.to_sym)

        hand_card.collect_batch(player_data.fetch(:card))
        best_hand.rank = player_data.fetch(:rank)
      end
    end
  end
end
