# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # set round from game engine
    #
    class SetRound < Clutch::System::CommandSystem
      watch :round
      command :query_game, :last_or_new_round_id
      command :deal, :new_round_id
      command :bet,  :new_round_id
      command :baccarat_deal, :new_round_id
      command :roll, :new_round_id
      command :drop, :new_round_id
      command :draw, :new_round_id
      command :multiple_bet, :new_round_id

      private

      def last_or_new_round_id(entity)
        round = entity.find(Component::Round)
        round.id = if @game_engine.new_round?
          @game_engine.last_round.ref_id
        else
          @game_engine.current_round.ref_id
        end
      end

      #
      # set round
      #
      # @param [Clutch::Entity] entity
      #
      def new_round_id(entity)
        round = entity.find(Component::Round)
        round.id = @game_engine.current_round.ref_id
      end
    end
  end
end
