# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # Determine the next stage
    #
    class DetermineNextStageByFreeGameMeter < Clutch::System::ReactiveSystem
      watch :free_game_meter
      reference :stage

      private

      def before_process
        super
        @stage ||= references[:stage].first
      end

      def process(entity)
        free_game_meter = entity.find(Component::FreeGameMeter)

        @stage.next = if free_game_meter.counter > 0
          Enum::Stage::FREE_GAME
        else
          Enum::Stage::BASE_GAME
        end
      end
    end
  end
end
