# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # Handle bet
    #
    class FreeGameBet < Clutch::System::CommandSystem
      watch :bet_amount
      reference :denom_info

      command :draw_free_game, :free_game_bet_options

      private

      def before_all
        super
        @denom_info = references[:denom_info].first
      end

      def free_game_bet_options(entity)
        bet_amount = entity.find(Component::BetAmount)

        bet_amount.cash = bet_amount.credit * @denom_info.denom
      end
    end
  end
end
