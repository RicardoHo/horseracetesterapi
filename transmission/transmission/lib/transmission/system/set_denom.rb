# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Handle player selected denom
    #
    class SetDenom < Clutch::System::CommandSystem
      watch :denom_info
      command :deal, :save_denom
      command :bet, :save_denom
      command :baccarat_deal, :save_denom
      command :roll, :save_denom
      command :drop, :save_denom
      command :draw, :save_denom
      command :multiple_bet, :save_denom

      private

      def executable?
        super && @input.denom
      end

      #
      # Entry of deal command
      #
      # @param [Clutch::Entity] entity
      #
      def save_denom(entity)
        denom_info = entity.find(Component::DenomInfo)

        denom_info.denom       = @input.denom
        denom_info.denom_index = denom_info.denoms.index(@input.denom)

        raise DenomNotInRangeError unless denom_info.denom_index
      end
    end
  end
end
