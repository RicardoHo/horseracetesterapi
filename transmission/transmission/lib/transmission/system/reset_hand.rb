# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # Reset hand to empty
    #
    class ResetHand < Clutch::System::CleanupSystem
      watch :hand_card
      cleanup :reset
    end
  end
end
