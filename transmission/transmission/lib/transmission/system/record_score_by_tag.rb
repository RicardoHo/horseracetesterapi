# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # Record current hand score to score board
    #
    class RecordScoreByTag < Clutch::System::ReactiveSystem
      watch :score_card_tag, :score_board

      private

      def executable?
        super && !@command.casecmp?('query_game')
      end

      def process(entity)
        score_card_name = entity.find(Component::ScoreCardTag).name
        board = entity.find(Component::ScoreBoard)
        board.record(
          entity
            .find(Component.const_get(score_card_name.camel_case))
            .to_hash
            .fetch(:score_card)
        )
      end
    end
  end
end
