# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.8.0
    #
    # Load path from game engine when resume game
    #
    class LoadPath < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :path_info
      command :query_game, :on_load

      private

      def executable?
        super && loadable?
      end

      def before_all
        super
        load_save
        @path_left_right = @save.dig(:table, :path_info, :path_left_right)
      end

      def on_load(entity)
        path_info = entity.find(Component::PathInfo)

        path_info.path_left_right = @path_left_right
      end
    end
  end
end
