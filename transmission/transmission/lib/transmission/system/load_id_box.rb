# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # Load number box from game engine when resume game
    #
    class LoadIdBox < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :id_box
      command :query_game, :on_load

      private

      def executable?
        super && loadable?
      end

      def before_all
        super
        load_save
        @hit_counter   = @save.dig(:table, :id_box, :hit_counter)
        @hit_id        = @save.dig(:table, :id_box, :hit_id)
        @result_id     = @save.dig(:table, :id_box, :result_id)
        @hit_free_game = @save.dig(:table, :id_box, :hit_free_game)
      end

      def on_load(entity)
        id_box = entity.find(Component::IdBox)

        id_box.hit_counter   = @hit_counter
        id_box.hit_id        = @hit_id
        id_box.result_id     = @result_id
        id_box.hit_free_game = @hit_free_game
      end
    end
  end
end
