# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Ben B Wu
    # @since 0.1.0
    #
    # Generate bet result
    #
    class HrGenResult < Clutch::System::CommandSystem
      watch :hr_result, :hr_last_bet
      reference :denom_info, :hr_score_card, :score_card_tag, :hr_math_par, :hr_paytable

      command :multiple_bet, :hr_gen_result

      private

      #def executable?
      #  super && @input.bet_options
      #end

      def before_all
        super
        @par ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
          .dig(:parameters)
        # @bet_options = @input.bet_options
        @score_card  ||= references[:hr_score_card].first
        @score_card_tag  ||= references[:score_card_tag].first
        @hr_math_par ||= references[:hr_math_par].first
      end

      def update_last_bet(entity, hr_result)
        last_bet = entity.find(Component::HrLastBet)

        hr_result.play_mode = @input.play_mode
        # p hr_result.play_mode
        last_bet.last_bet[@input.play_mode] = []

        @input.bet_options.each do |e|
          last_bet.last_bet[@input.play_mode] << {'name' => e.name, 'bet_amount' => {'credit' => e.bet_amount.credit}}
        end
      end

      def save_rng_capture(*items)
        @game_engine.rng_capture[:rng_result].push(@hr_math_par.group_idx)
        @game_engine.rng_capture[:rng_result].push(@hr_math_par.modifications)
        @game_engine.rng_capture[:rng_result].push(@hr_math_par.shuffle_idx)
        items.each do |item|
          @game_engine.rng_capture[:rng_result].push(item)
        end
      end

      # Generate bet result of Horse Racing game
      #
      def hr_gen_result(entity)
        hr_result = entity.find(Component::HrResult)

        # update last bet information
        update_last_bet(entity, hr_result)

        # generate the result of this round
        paytable = @par.dig(@hr_math_par.group_idx, :paytable)
        round_weight = @par.dig(@hr_math_par.group_idx, :round_weight_table)
        original_idx = Util::RNGUtil.draw_from_weight_table(
          round_weight, 
          rng: @game_engine.weight_table
        ).first.to_i
        n = paytable[:win].size
        place1 = original_idx / n    # winner
        place2 = original_idx % n    # the second

        # load shuffle map 
        shuffle_map = @hr_math_par.shuffle_map

        # calculate new indices of the won 
        place1 = shuffle_map[place1]
        place2 = shuffle_map[place2]
        forecast_idx = place1 * n + place2
        dual_forecast_idx = forecast_idx
        if place1 > place2
          dual_forecast_idx = place2 * n + place1
        end

        i = Util::RNGUtil.random(
          5, 
          rng: @game_engine.rng,)
          # rng_capture: @game_engine.rng_capture)
        suffix = ('a'.ord + i).chr

        hr_result.win_indices = [place1, place2]
        hr_result.win_reference = ["win_#{place1}", "place_#{place1}", "place_#{place2}", "dual_forecast_#{dual_forecast_idx}", "forecast_#{forecast_idx}"]
        hr_result.video_file = "horseracing#{place1+1}#{place2+1}#{suffix}.mp4"
        hr_result.win_options = []
        hr_result.total_bet_amt = 0
        hr_result.total_payout_amt = 0
        hr_result.payout_details = {'win' => 0.0, 'place' => [0.0, 0.0], 'dual_forecast' => 0.0, 'forecast' => 0.0}
        @score_card.win_indices = hr_result.win_indices
        @score_card_tag.dirty = true
        
        save_rng_capture(original_idx, i)
      end
    end
  end
end
