# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # Handle player selected multiplier
    #
    class SetMultiplier < Clutch::System::CommandSystem
      watch :multiplier_info
      command :draw, :save_multiplier
      command :multiple_bet, :save_multiplier

      private

      def executable?
        super && @input.multiplier
      end

      #
      # setup the multiplier to MultiplierInfo
      #
      # @param [Clutch::Entity] entity
      #
      def save_multiplier(entity)
        multiplier_info = entity.find(Component::MultiplierInfo)

        multiplier_info.multiplier       = @input.multiplier
        multiplier_info.multiplier_index = multiplier_info.multipliers.index(@input.multiplier)

        raise MultiplierNotInRangeError unless multiplier_info.multiplier_index
      end
    end
  end
end
