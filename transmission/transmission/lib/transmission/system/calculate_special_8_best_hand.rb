# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Jason Lei
    # @since 0.2.0
    #
    # Calculate best hand in game 888, speical handle on card 8.

    class CalculateSpecial8BestHand < Clutch::System::ReactiveSystem
      watch :hand_card, :best_hand

      private

      def executable?
        super && @command.casecmp?('bet')
      end

      def process(entity)
        @ante_paytable ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
          .dig(:paytable, :ante)

        best_hand = entity.find(Component::BestHand)
        hand_card = entity.find(Component::HandCard)

        best_hand.rank = calculate_best_hand(hand_card.cards)
      end

      # @param  [Array] cards
      # @return [String] the type of combination
      def calculate_best_hand(cards)
        @ante_paytable.keys.each do |rank|
          rank_str = rank.to_s.sub('3', 'three')
          return rank.to_s if send(:"#{rank_str}?", cards)
        end
        'nothing'
      end

      #
      # @param [Array] cards
      #
      # @return [Boolean]
      #
      def triple_8?(cards)
        face_value = cards.map { |card| Util::PokerHandUtil.card_face_value(card) }
        return false unless face_value.count(8) == 3

        true
      end

      #
      # @param [Array] cards
      #
      # @return [Boolean]
      #
      def double_8?(cards)
        face_value = cards.map { |card| Util::PokerHandUtil.card_face_value(card) }
        return false unless face_value.count(8) == 2

        true
      end

      #
      # @param [Array] cards
      #
      # @return [Boolean]
      #
      def three_of_a_kind?(cards)
        face_value = cards.map { |card| Util::PokerHandUtil.card_face_value(card) }
        return false unless face_value.uniq.length == 1

        true
      end

      #
      # @param [Array] cards
      #
      # @return [Boolean]
      #
      def straight_flush_with_8?(cards)
        face_value = cards.map { |card| Util::PokerHandUtil.card_face_value(card) }

        return false unless straight?(cards) \
          && flush?(cards) \
          && face_value.count(8) == 1

        true
      end

      #
      # @param [Array] cards
      #
      # @return [Boolean]
      #
      def straight_with_8?(cards)
        face_value = cards.map { |card| Util::PokerHandUtil.card_face_value(card) }

        return false unless straight?(cards) \
          && face_value.count(8) == 1

        true
      end

      #
      # @param [Array] cards
      #
      # @return [Boolean]
      #
      def flush_with_8?(cards)
        face_value = cards.map { |card| Util::PokerHandUtil.card_face_value(card) }

        return false unless flush?(cards) \
          && face_value.count(8) == 1

        true
      end

      #
      # @param [Array] cards a set of cards
      #
      # @return [Boolean]
      #
      def straight_flush?(cards)
        return false unless straight?(cards) \
          && flush?(cards)

        true
      end

      #
      # @param  [Array] cards
      #
      # @return [Boolean]
      #
      def straight?(cards)
        face_value = cards.map { |card| Util::PokerHandUtil.card_face_value(card) }
        face_value << Util::PokerHandUtil::LOW_ACE_VALUE \
          if cards.find { |card| card[0] == 'A' }
        face_value.sort!
        (0..face_value.size - cards.size).map do |idx|
          face_value[idx, cards.size].each_cons(2).map do |x, y|
            (y - x) == 1
          end.all?
        end.any?
      end

      #
      # @param [Array] cards
      #
      # @return [Boolean]
      #
      def flush?(cards)
        card_suit = cards.map { |card| card[1] }
        card_suit.uniq.size == 1
      end

      #
      # @param [Array] cards
      #
      # @return [Boolean]
      #
      def single_8?(cards)
        face_value = cards.map { |card| Util::PokerHandUtil.card_face_value(card) }
        face_value.count(8) == 1
      end
    end
  end
end
