# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.6.0
    #
    # Record
    #
    class RecordLotteryDenom < Clutch::System::ReactiveSystem
      watch :denom_info
      reference :total_bet

      private

      def before_all
        super
        @total_bet ||= references[:total_bet].first
      end

      def process(entity)
        @total_bet.denom = entity.find(Component::DenomInfo).denom
      end
    end
  end
end
