# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # Load payout from game engine when resume game
    #
    class LoadPayout < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :payout, :bet_area_label
      command :query_game, :on_load
      reference :current_payout, :base_game_payout, :free_game_payout

      private

      def executable?
        super && loadable?
      end

      def before_all
        super
        load_save
        @bet_options_data = @save.fetch(:bet_options)
        @current_payout   ||= references[:current_payout]&.first
        @base_game_payout ||= references[:base_game_payout]&.first
        @free_game_payout ||= references[:free_game_payout]&.first
      end

      def on_load(entity)
        payout         = entity.find(Component::Payout)
        bet_area_label = entity.find(Component::BetAreaLabel)

        payout_data = find_payout_data(bet_area_label.name)
        return unless payout_data

        payout.load(payout_data[:payout])

        @current_payout&.load(payout_data[:current_payout])
        @base_game_payout&.load(payout_data[:base_game_payout])
        @free_game_payout&.load(payout_data[:free_game_payout])
      end

      def find_payout_data(bet_area_name)
        if @bet_options_data.is_a?(Array)
          @bet_options_data.find do |opt|
            bet_area_name.casecmp?(opt[:name])
          end
        elsif @bet_options_data.is_a?(Hash)
          @bet_options_data
        end
      end
    end
  end
end
