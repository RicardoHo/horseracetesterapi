# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Reset bet area to stand by state
    #
    class ResetAaRank < Clutch::System::CleanupSystem
      watch :aa_rank
      cleanup :reset, condition: :deal?

      def deal?(_entity)
        @command.casecmp?('deal')
      end
    end
  end
end
