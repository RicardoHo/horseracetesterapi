# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.8.0
    #
    # determine the win level base on win odds
    #
    class DetermineBallWinLevelByOdds < Clutch::System::ReactiveSystem
      watch :ball_score_card

      private

      def before_process
        super
        @win_level_rules ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
          .fetch(:win_level)
      end

      def process(entity)
        score_card = entity.find(Component::BallScoreCard)

        score_card.win_level = @win_level_rules.find do |rule|
          score_card.bet_option.fetch(:odds) >= rule.fetch(:min_odds)
        end.fetch(:level)
      end
    end
  end
end
