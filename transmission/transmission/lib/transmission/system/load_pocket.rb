# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.8.0
    #
    # Load pocket from game engine when resume game
    #
    class LoadPocket < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :pocket_info
      command :query_game, :on_load

      private

      def executable?
        super && loadable?
      end

      def before_all
        super
        load_save
        @pocket_index = @save.dig(:table, :pocket_info, :index)
        @odds         = @save.dig(:table, :pocket_info, :odds)
      end

      def on_load(entity)
        pocket_info = entity.find(Component::PocketInfo)

        pocket_info.pocket_index = @pocket_index
        pocket_info.odds         = @odds
      end
    end
  end
end
