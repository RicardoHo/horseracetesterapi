# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.6.0
    #
    # Record
    #
    class RecordLotteryTotalBet < Clutch::System::ReactiveSystem
      watch :bet_amount
      reference :total_bet

      private

      def before_all
        super
        @total_bet ||= references[:total_bet].first
      end

      def process(entity)
        @total_bet.credit += entity.find(Component::BetAmount).credit
      end
    end
  end
end
