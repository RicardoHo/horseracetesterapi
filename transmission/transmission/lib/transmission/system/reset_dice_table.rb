# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.7.0
    #
    # Reset dice table to stand by state
    #
    class ResetDiceTable < Clutch::System::CleanupSystem
      watch :dice_score_card, :dice_cup
      cleanup :reset
    end
  end
end
