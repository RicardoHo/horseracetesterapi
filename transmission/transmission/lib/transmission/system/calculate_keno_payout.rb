# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # calculate payout of keno
    #
    class CalculateKenoPayout < Clutch::System::ReactiveSystem
      watch :keno_score_card, :id_box, :stage, :selected_id_list
      reference :bet_amount, :payout, :bet_area_label, :current_payout, :base_game_payout, :free_game_payout

      private

      def executable?
        super && !@command.casecmp?('query_game') && !@command.casecmp?('draw_finish')
      end

      def before_all
        super
        @bet_amount       ||= references[:bet_amount].first
        @payout           ||= references[:payout].first
        @bet_area_label   ||= references[:bet_area_label].first
        @current_payout   ||= references[:current_payout].first
        @base_game_payout ||= references[:base_game_payout].first
        @free_game_payout ||= references[:free_game_payout].first
        @paytable         ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
          .fetch(:paytable)
      end

      def process(entity)
        return unless @bet_amount.cash

        score_card = entity.find(Component::KenoScoreCard)
        return if score_card.bet_option[:name] != @bet_area_label.name

        id_box = entity.find(Component::IdBox)
        stage = entity.find(Component::Stage)
        selected_id_list = entity.find(Component::SelectedIdList)

        choose = selected_id_list.list.size
        hit_counter = id_box.hit_counter
        odds = @paytable.dig(choose, hit_counter, :odds)

        odds *= 2 if stage.current.casecmp?(Enum::Stage::FREE_GAME)

        @current_payout.cash   = (@bet_amount.cash * odds).to_i
        @current_payout.credit = (@bet_amount.credit * odds).to_i

        if stage.current.casecmp?(Enum::Stage::FREE_GAME)
          @free_game_payout.cash   += @current_payout.cash
          @free_game_payout.credit += @current_payout.credit
        else
          @base_game_payout.cash   = @current_payout.cash
          @base_game_payout.credit = @current_payout.credit
        end

        @payout.cash   += @current_payout.cash
        @payout.credit += @current_payout.credit
        score_card.payout = {
          name: @bet_area_label.name,
          payout_amount: {
            credit: @current_payout.cash,
            cash: @current_payout.credit,
          },
        }
        score_card.hit_counter = hit_counter
        score_card.odds = odds
      end
    end
  end
end
