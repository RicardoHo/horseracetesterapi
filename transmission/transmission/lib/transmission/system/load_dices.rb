# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.7.0
    #
    # Load dices' point from game engine when resume game
    #
    class LoadDices < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :dice_cup
      command :query_game, :on_load

      private

      def executable?
        super && loadable?
      end

      def before_all
        super
        load_save
        @dice_points = @save.dig(:table, :dices)
      end

      def on_load(entity)
        entity
          .find(Component::DiceCup)
          .load(@dice_points)
      end
    end
  end
end
