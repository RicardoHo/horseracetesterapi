# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Cleanup win info
    #
    class CleanupWinInfo < Clutch::System::CleanupSystem
      watch :win_info
      cleanup :reset, condition: :deal?

      def deal?(_entity)
        @command.casecmp?('deal')
      end
    end
  end
end
