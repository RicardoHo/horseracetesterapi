# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # Record current hand score to score board
    #
    class RecordHandScore < Clutch::System::ReactiveSystem
      watch :hand_score_card, :shoe_score_board

      private

      def process(entity)
        # HACK: avoid duplicate score recording when resume game
        return if @command.casecmp?('query_game')

        board = entity.find(Component::ShoeScoreBoard)
        board.record(
          entity.find(Component::HandScoreCard).to_hash.fetch(:hand_score_card)
        )
      end
    end
  end
end
