# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # validate multiplier
    #
    class ValidateMultiplier < Clutch::System::CommandSystem
      watch :unit_bet
      reference :bet_amount, :bet_area_label

      command :draw, :validate_multiplier

      private

      def executable?
        super && @input.multiplier && @input.bet_options
      end

      def before_all
        super
        @bet_amount = references[:bet_amount].first
        @bet_area_label = references[:bet_area_label].first
      end

      def validate_multiplier(entity)
        unit_bet = entity.find(Component::UnitBet)

        bet_input = @input.bet_options.find do |opt|
          opt.name.casecmp?(@bet_area_label.name)
        end

        unless bet_input
          raise MissingBetParamsError,
            "Missing #{@bet_area_label.name} bet input data"
        end

        unless bet_input.bet_amount.credit == unit_bet.credit * @input.multiplier
          raise BetAmountNotMatchForMultiplierValueError,
            "Bet amount #{bet_input.bet_amount.credit} not match for multiplier value"
        end
      end
    end
  end
end
