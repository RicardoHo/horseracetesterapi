# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.8.0
    #
    # Change selected_bet_options to bet_options
    #
    class SelectedBet < Clutch::System::CommandSystem
      watch :bet_area_label, :bet_amount, :max_bet, :min_bet

      command :drop, :change_selected_bet_options_to_bet_options

      private

      def executable?
        super && @input.selected_bet_options
      end

      def change_selected_bet_options_to_bet_options(_entity)
        @input.selected_bet_options.each do |bet_option|
          bet_option.bet_amount = bet_option.amount
        end
        @input.bet_options = @input.selected_bet_options
      end
    end
  end
end
