# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    # revamp since 0.3.0
    #
    # Load used card from game engine when resume game
    #
    class LoadUsedCard < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :card_deck
      command :query_game, :on_load

      private

      def executable?
        super && loadable?
      end

      def before_all
        super
        load_save
        @card_pool  = @save.dig(:table, :card_pool)
        # compatible to previous version save data
        @used_cards = @save.dig(:table, :used_cards)
      end

      def on_load(entity)
        deck = entity.find(Component::CardDeck)

        if @card_pool
          deck.card_pool = @card_pool
        else
          @used_cards.each do |card|
            deck.card_pool[card.to_sym] -= 1
          end
        end
      end
    end
  end
end
