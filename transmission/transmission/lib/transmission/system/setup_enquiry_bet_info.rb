# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # Setup enquiry bet infomation
    #
    class SetupEnquiryBetInfo < Clutch::System::CommandSystem
      watch :enquiry_bet_indicator
      command :query_game, :setup

      reference :denom_info

      private

      def before_all
        super
        @denom_info ||= references[:denom_info].first
      end

      #
      # query game setup entry
      #
      # @param [Clutch::Entity] entity
      #
      def setup(entity)
        config    = @game_engine.game_config.extra
        indicator = entity.find(Component::EnquiryBetIndicator)

        indicator.enable = config.fetch(:enquiry_bet_enable)
        if indicator.enable
          indicator.bet_credit = config.fetch(:min_banker_bet_in_credit)
        end
      end
    end
  end
end
