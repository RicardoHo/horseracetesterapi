# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # Check all bets in table fulfill table limit or not
    #
    class TableLimitCheck < Clutch::System::CommandSystem
      include Helper::BetHelper

      watch :bet_area_label, :bet_amount
      reference :table_label, :max_bet, :min_bet

      command :baccarat_deal, :collect_bet

      private

      def before_all
        super
        @total_bet_credit       = 0
        @table_max_bet_credit ||= references[:max_bet].first.credit
        @table_min_bet_credit ||= references[:min_bet].first.credit
      end

      def after_all
        super
        unless @total_bet_credit.between?(@table_min_bet_credit, @table_max_bet_credit)
          raise TableBetsNotInRangeError
        end
      end

      #
      # Entry of baccarat deal command
      #
      # @param [Clutch::Entity] entity
      #
      def collect_bet(entity)
        @total_bet_credit += entity.find(Component::BetAmount).credit || 0
      end
    end
  end
end
