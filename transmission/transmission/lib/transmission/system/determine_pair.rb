# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # To determine is the first two cards on hand is pair or not
    # then store the result to hand score card
    #
    class DeterminePair < Clutch::System::ReactiveSystem
      watch :player_label, :hand_card
      reference :hand_score_card

      private

      def before_all
        super
        @hand_score_card ||= references[:hand_score_card].first
      end

      def process(entity)
        cards = entity.find(Component::HandCard).cards
        return unless pair?(cards[0], cards[1])

        @hand_score_card.pair = if @hand_score_card.pair
          Enum::Winner::BOTH
        else
          entity.find(Component::PlayerLabel).name.upcase
        end
      end

      def pair?(first_card, second_card)
        first_card[0] == second_card[0]
      end
    end
  end
end
