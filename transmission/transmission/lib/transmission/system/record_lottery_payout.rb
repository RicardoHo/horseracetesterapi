# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.6.0
    #
    # Record
    #
    class RecordLotteryPayout < Clutch::System::ReactiveSystem
      watch :payout, :payout_index
      reference :payout_distributions

      private

      def before_all
        super
        @payout_distributions ||= references[:payout_distributions].first
      end

      def process(entity)
        payout_cash  = entity.find(Component::Payout).cash
        payout_index = entity.find(Component::PayoutIndex).value
        return unless payout_cash && payout_index

        @payout_distributions.record(
          index: payout_index,
          win_amt: payout_cash
        )
      end
    end
  end
end
