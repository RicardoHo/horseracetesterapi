# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # Load san gong player hand from game engine when resume game
    #
    class LoadSanGongPlayerHand < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :player_label, :hand_card, :hand_point
      command :query_game, :on_load

      private

      def executable?
        super && loadable?
      end

      def before_all
        super
        load_save
      end

      def on_load(entity)
        player_label = entity.find(Component::PlayerLabel)
        hand_card    = entity.find(Component::HandCard)
        hand_point   = entity.find(Component::HandPoint)
        player_data  = @save
          .fetch(player_label.name.to_sym)

        hand_card.cards  = player_data.fetch(:card)
        hand_point.value = player_data.fetch(:point)
      end
    end
  end
end
