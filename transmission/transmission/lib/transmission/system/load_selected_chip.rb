# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 1.0.0
    #
    # Load chip index from game engine if loadable
    #
    class LoadSelectedChip < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :chip
      command :query_game, :on_load

      private

      def executable?
        super && loadable?
      end

      def before_all
        super
        load_save
        @chip_index_save = @save.dig(:player, :chip_index)
      end

      def on_load(entity)
        chip = entity.find(Component::Chip)
        chip.selected_index = @chip_index_save || 0
      end
    end
  end
end
