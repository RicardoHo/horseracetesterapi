# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.4.0
    #
    # Calculate best hand
    class CalculateBestHand < Clutch::System::ReactiveSystem
      include Helper::PokerHandHelper

      watch :hand_card, :best_hand

      private

      def executable?
        super && @command.casecmp?('bet')
      end

      def process(entity)
        @paytable ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
          .dig(:paytable, :ante)

        best_hand = entity.find(Component::BestHand)
        hand_card = entity.find(Component::HandCard)

        best_hand.rank = calculate_best_hand(hand_card.equivalent_cards)
      end

      #
      # calculate the best poker hand in terms of rank
      #
      # @param  [Array] cards
      #
      # @return [String] the type of rank or nothing
      #
      def calculate_best_hand(cards)
        @card_faces = cards
          .map { |card| Util::PokerHandUtil.card_face_value(card) }
          .sort_by(&:-@)
        @card_suits = cards.map { |card| card[1] }
        @paytable.keys.find do |rank|
          send(:"#{rank}?") && fulfill_constraint?(rank)
        end&.to_s || 'nothing'
      end
    end
  end
end
