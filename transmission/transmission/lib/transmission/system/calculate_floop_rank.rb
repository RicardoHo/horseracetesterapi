# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Calculate the rank of poker hand of each player at floop
    #
    class CalculateFloopRank < Clutch::System::CommandSystem
      watch :hand_card, :public_card, :floop_rank
      command :deal, :calculate

      private

      def calculate(entity)
        floop_rank  = entity.find(Component::FloopRank)
        hand_card   = entity.find(Component::HandCard)
        public_card = entity.find(Component::PublicCard)

        _, floop_rank.rank = Util::PokerHandUtil
          .best_hand(hand_card.cards + public_card.cards)
      end
    end
  end
end
