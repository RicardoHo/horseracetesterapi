# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.8.0
    #
    # Random to decide the box and generate the path to let the ball drop down
    #
    class DropBall < Clutch::System::ReactiveSystem
      watch :path_info, :pocket_info, :risk_level, :lines_info

      private

      def executable?
        super && @command.casecmp?('drop')
      end

      def before_all
        super
        @weight_table ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
          .fetch(:weight_table)
      end

      def process(entity)
        path_info   = entity.find(Component::PathInfo)
        pocket_info = entity.find(Component::PocketInfo)
        risk_level  = entity.find(Component::RiskLevel)
        lines_info  = entity.find(Component::LinesInfo)
        return unless lines_info.line

        pocket_info.pocket_index = Util::RNGUtil
          .draw_from_weight_table(
            @weight_table.dig(
              lines_info.line,
              risk_level.levels[risk_level.risk_level_index].to_sym
            ),
            rng: @game_engine.weight_table
          )
          .first
          .to_i
        path_left  = Array.new(lines_info.line - pocket_info.pocket_index, 0)
        path_right = Array.new(pocket_info.pocket_index, 1)
        path_info.path_left_right = (path_left + path_right).shuffle
      end
    end
  end
end
