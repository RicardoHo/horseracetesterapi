# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.6.1
    #
    # Setup min and max bet
    #
    class SetupStaticMinMaxBet < Clutch::System::CommandSystem
      watch :max_bet, :min_bet
      reference :denom_info
      command :query_game, :setup

      private

      def before_all
        super
        @denom_info ||= references[:denom_info].first
      end

      #
      # setup the max bet credit amount
      #
      # @param [Clutch::Entity] entity
      #
      def setup(entity)
        max_bet_comp = entity.find(Component::MaxBet)
        min_bet_comp = entity.find(Component::MinBet)
        max_bet_comp.cash   = @denom_info.denoms.max * max_bet_comp.credit
        min_bet_comp.cash   = @denom_info.denoms.min * min_bet_comp.credit
      end
    end
  end
end
