# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # calculate payout of each bet area by the winner's
    # poker hand and the bet amount
    #
    class CalculatePayout < Clutch::System::ReactiveSystem
      watch :bet_area_label, :bet_amount, :payout, :win_info

      private

      def before_all
        super
        @math_model ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
        @raise_paytable ||= @math_model.dig(:paytable, :raise)
      end

      def process(entity)
        win_info = entity.find(Component::WinInfo)
        return if !win_info.type || win_info.type == Enum::WinType::LOSE

        bet_label  = entity.find(Component::BetAreaLabel)
        bet_amount = entity.find(Component::BetAmount)
        payout     = entity.find(Component::Payout)
        multiplier = send("#{bet_label.name}_multiplier", win_info)

        payout.cash   = multiplier * bet_amount.cash
        payout.credit = multiplier * bet_amount.credit
      end

      def ante_multiplier(win_info)
        if win_info.type == Enum::WinType::PUSH
          1
        else
          win_info.odds
        end
      end

      def raise_multiplier(win_info)
        win_info.type = Enum::WinType::PUSH unless win_info.dealer_qualify
        @raise_paytable.dig(win_info.type.downcase.to_sym, :odds)
      end
    end
  end
end
