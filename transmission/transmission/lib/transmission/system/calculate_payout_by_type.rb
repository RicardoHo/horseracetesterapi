# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # calculate payout of each bet area by the
    # win type, paytable and the bet amount
    #
    class CalculatePayoutByType < Clutch::System::ReactiveSystem
      watch :bet_area_label, :bet_amount, :payout, :win_type

      private

      def before_all
        super
        @math_model ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
      end

      def process(entity)
        win_type = entity.find(Component::WinType)
        return if win_type.type.nil? || win_type.type == Enum::WinType::LOSE

        bet_amount = entity.find(Component::BetAmount)
        bet_name   = entity.find(Component::BetAreaLabel).name
        multiplier = @math_model.dig(
          :paytable,
          bet_name.to_sym,
          win_type.type.snake_case.to_sym
        )

        entity.find(Component::Payout).tap do |payout|
          payout.cash   = (multiplier * bet_amount.cash).to_i
          payout.credit = (multiplier * bet_amount.credit).to_i
        end
      end
    end
  end
end
