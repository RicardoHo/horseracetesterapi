# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.7.0
    #
    # determine the win level base on win odds
    #
    class DetermineWinLevelByOdds < Clutch::System::ReactiveSystem
      watch :dice_score_card

      private

      def before_process
        super
        @win_level_rules ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
          .dig(:win_level)
      end

      def process(entity)
        score_card = entity.find(Component::DiceScoreCard)
        return if score_card.win_type == Enum::WinType::LOSE

        score_card.win_level = @win_level_rules.find do |rule|
          score_card.bet_option.fetch(:odds) >= rule.fetch(:min_odds)
        end.fetch(:level)
      end
    end
  end
end
