# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.3.0
    #
    # Load shoe from game engine when resume
    #
    class LoadShoe < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :shoe
      command :query_game, :on_load

      private

      def executable?
        super && instance_loadable?
      end

      def before_all
        super
        load_instance_save
      end

      def on_load(entity)
        shoe = entity.find(Component::Shoe)

        shoe.load(
          @instance_save.dig(:shoe)
        )
      end
    end
  end
end
