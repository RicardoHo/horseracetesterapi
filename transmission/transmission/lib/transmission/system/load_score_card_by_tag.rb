# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # Load score card
    #
    class LoadScoreCardByTag < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :score_card_tag
      command :query_game, :on_load

      private

      def executable?
        super && loadable?
      end

      def before_all
        super
        load_save
        @score_card_data = @save.dig(:table, :score_card)
      end

      def on_load(entity)
        score_card_name = entity.find(Component::ScoreCardTag).name
        entity
          .find(Component.const_get(score_card_name.camel_case))
          .load(@score_card_data)
      end
    end
  end
end
