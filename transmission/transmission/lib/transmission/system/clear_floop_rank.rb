# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Clear floop rank data at raise bet
    #
    class ClearFloopRank < Clutch::System::CleanupSystem
      watch :floop_rank
      cleanup :reset, condition: :raise_bet?

      def raise_bet?(_entity)
        @command.casecmp?('raise')
      end
    end
  end
end
