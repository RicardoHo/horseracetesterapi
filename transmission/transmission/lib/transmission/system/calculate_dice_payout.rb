# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Alpha Huang
    # @since 0.7.0
    #
    # calculate payout of dice game
    #
    class CalculateDicePayout < Clutch::System::ReactiveSystem
      watch :bet_amount, :payout, :bet_area_label
      reference :dice_score_card

      private

      def before_all
        super
        @score_card ||= references[:dice_score_card].first
      end

      def process(entity)
        bet_amount = entity.find(Component::BetAmount)
        return unless bet_amount.cash

        bet_name = entity.find(Component::BetAreaLabel).name
        return if @score_card.bet_option.dig(:name) != bet_name

        return if @score_card.win_type == Enum::WinType::LOSE

        odds = @score_card.bet_option.dig(:odds)
        payout = entity.find(Component::Payout)
        payout.cash   = (bet_amount.cash * odds).to_i
        payout.credit = (bet_amount.credit * odds).to_i
        @score_card.bet_option[:payout] = {
          credit: payout.credit,
          cash: payout.cash,
        }
      end
    end
  end
end
