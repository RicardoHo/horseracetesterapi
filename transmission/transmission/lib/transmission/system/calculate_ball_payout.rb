# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.8.0
    #
    # calculate payout of ball game
    #
    class CalculateBallPayout < Clutch::System::ReactiveSystem
      watch :bet_amount, :payout, :bet_area_label
      reference :ball_score_card, :lines_info, :pocket_info

      private

      def before_all
        super
        @score_card   ||= references[:ball_score_card].first
        @lines_info   ||= references[:lines_info].first
        @pocket_info  ||= references[:pocket_info].first
        @paytable     ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
          .fetch(:paytable)
      end

      def process(entity)
        bet_amount = entity.find(Component::BetAmount)
        return unless bet_amount.cash

        bet_name = entity.find(Component::BetAreaLabel).name
        return if @score_card.bet_option.dig(:selected_bet_options, :name) != bet_name

        return unless @lines_info.line

        selected_risk_level = @score_card.bet_option.dig(:selected_risk_level)
        odds = @paytable.dig(
          @lines_info.line,
          selected_risk_level.to_sym,
          @pocket_info.pocket_index,
          :odds
        )
        @pocket_info.odds = odds.to_s
        payout = entity.find(Component::Payout)
        payout.cash   = (bet_amount.cash * odds).to_i
        payout.credit = (bet_amount.credit * odds).to_i
        @score_card.bet_option[:odds] = odds
        @score_card.bet_option[:payout] = {
          name: bet_name,
          amount: {
            credit: payout.credit,
            cash: payout.cash,
          },
        }
      end
    end
  end
end
