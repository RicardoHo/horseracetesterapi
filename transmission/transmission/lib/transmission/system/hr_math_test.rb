# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Ben B Wu
    # @since 0.1.0
    #
    # Handle bet
    #
    require 'csv'

    class HrMathTest < Clutch::System::CommandSystem

      watch :hr_paytable
      reference :denom_info

      command :math_test, :hr_math_test

      private

      def before_all
        super
        @par ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
          .dig(:parameters)
 
        @odds_divisor ||= @game_def
          .math_model
          .model(@game_engine.game_config.math_name)
          .dig(:odds_divisor)
      end

      # generate permutation from an index number
      #
      # @param [Int] idx
      # @param [Int] n, n is the length of the permutation
      def gen_perm(idx, n)
        permuted = [0] * n
        elems = (0...n).to_a

        # for i in 0...n
        (0...n).each do |i|
          k = idx % (n - i)
          idx /= n-i
          permuted[i] = elems[k]
          elems[k] = elems[n-i-1]
        end
        permuted
      end

      def hr_math_test(entity)
        n = 100_000_000
        # n = 100
        distribution = {}

        (0...5040).each do |x|
          distribution[gen_perm(x, 7)] = 0
        end

        (0...n).each do |x|
          if x % 1000000 == 0
            p x
          end
          # shuffle_idx = Util::RNGUtil.random(5040, rng: @game_engine.rng)
          shuffle_idx = Util::RNGUtil.random((1..7).inject(:*), rng: @game_engine.rng)
          # p shuffle_idx
          shuffle_map = gen_perm(shuffle_idx, 7)
          distribution[shuffle_map] += 1
        end

        p distribution
        CSV.open('math/report/hr_math.csv', 'wb') {|csv| distribution.to_a.each {|e| csv << e}}
      end

    end
  end
end
