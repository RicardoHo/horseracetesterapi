# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Richard Fong
    # @since 0.9.0
    #
    # Load number box from game engine when resume game
    #
    class LoadSelectedIdList < Clutch::System::CommandSystem
      include Helper::LoadGameHelper

      watch :selected_id_list
      command :query_game, :on_load

      private

      def executable?
        super && loadable?
      end

      def before_all
        super
        load_save
        @list = @save.dig(:table, :selected_id_list, :list)
      end

      def on_load(entity)
        selected_id_list = entity.find(Component::SelectedIdList)

        selected_id_list.list = @list
      end
    end
  end
end
