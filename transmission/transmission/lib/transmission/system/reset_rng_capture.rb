# frozen_string_literal: true

module Transmission
  module System
    #
    # @author Ivan Lao
    # @since 1.0.0.rc1
    #
    # Reset rng capture
    #
    class ResetRngCapture < Clutch::System::CleanupSystem
      watch :rng_capture
      cleanup :reset
    end
  end
end
