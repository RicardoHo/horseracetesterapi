# frozen_string_literal: true

require 'd13n'

require 'clutch'

# game engine Api client
# require_relative '../pkg/pinion-api/lib/pinion-api.rb'
# require 'pinion-api'

# RNG Api client
# require_relative '../pkg/wheel-api/lib/wheel-api.rb'
# require 'wheel-api'

require 'transmission/version'

require 'multi_json'
require 'pp'
require 'hashie'

module Transmission
  extend D13n::Application::ClassMethods
  # D13n defined logger, config and config's default configuration 'default_source' in Transmission, you can use
  # Transmission.config=
  # to assign another configurator or logger.
  #
  # To config default configuration, you can use
  # Transmission.default_source=
  # to assign some default configuration in application start. To define the default configuration,
  Transmission.default_source = {
    env: {
      default: 'production',
      public: true,
      type: String,
      description: 'Defines service running environment.',
    },
    game_engine_url: {
      default: 'http://axle-app',
      public: true,
      type: String,
      description: 'Defines the game engine url.',
    },
    rng_url: {
      default: 'http://wheel-app',
      public: true,
      type: String,
      description: 'Defines the game engine url.',
    },
    system_complete_host_keyword: {
      default: 'belt',
      public: true,
      type: String,
      description: 'Defines the keyword of system complete request host.',
    },
  }
  #    :'service.broker.host' => {
  #        :default => 'localhost',
  #        :public => true,
  #        :type => String,
  #        :allowed_from_server => true,
  #        :description => 'Broker Host Name.'
  #    },
  #    :'service.broker.port' => {
  #        :default => '61614',
  #        :public => true,
  #        :type => String,
  #        :allowed_from_server => true,
  #        :description => 'Broker Host Port.'
  #    },
  #    :'service.broker.username' => {
  #        :default => 'laxino_service',
  #        :public => true,
  #        :type => String,
  #        :allowed_from_server => true,
  #        :description => 'Broker Host Service Username.'
  #    },
  #    :'service.broker.password' => {
  #        :default => 'laxino_service',
  #        :public => true,
  #        :type => String,
  #        :allowed_from_server => true,
  #        :description => 'Broker Host Password.'
  #    }
  #   }
  #
  # More information about configuration pls refer config_kit repository: http://stash.mo.laxino.com/users/ben.wu/repos/config_kit/browse
  # Or AxleApi Configuration documentation: http://stash.mo.laxino.com/projects/G2/repos/idc_config/browse

  class <<self
    #
    # root path of the project
    #
    # @return [String]
    #
    def root
      File.expand_path('..', __dir__)
    end

    #
    # path of game definition folder
    #
    # @return [String]
    #
    def game_definition_path
      File.join(root, 'game_definition')
    end

    #
    # Service running environment
    #
    # @return [String]
    #
    def env
      Transmission.config[:env]
    end

    #
    # Log message at info level
    #
    # @param [String] msg
    #
    def log_info(msg)
      logger.info(msg)
    end

    #
    # Log message at debug level, only valid at non-production env
    #
    # @param [String] msg
    #
    def log_debug(msg)
      logger.debug(msg) unless env == 'production'
    end

    #
    # Log message at error level with specific format
    #
    # @param [String] error
    #
    def log_error(error)
      logger.error(
        error.message + "\n" + error.backtrace[0..15].join("\n")
      )
    end

    #
    # Log elasped time between two calls for same given name
    # example:
    #   log_time(:process)
    #   \// process code here
    #   log_time(:process)
    #
    # @param [String] name
    # @param [Integer] id
    #
    def log_time(name, id)
      @time_records ||= {}

      record_key = :"#{name}_#{id}"
      if @time_records.key?(record_key)
        end_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
        elapsed  = (end_time - @time_records[record_key]) * 1000.0
        log_info(
          caller_id: id,
          "#{name}": "#{elapsed.round(2)} ms"
        )
        @time_records.delete(record_key)
      else
        @time_records[record_key] = Process.clock_gettime(Process::CLOCK_MONOTONIC)
      end
    end
  end
end

require 'transmission/core_ext/hash'
require 'transmission/core_ext/string'
require 'transmission/core_ext/array'
require 'transmission/message'
require 'transmission/service'
require 'transmission/api'
require 'transmission/error'
require 'transmission/support'
require 'transmission/util'
require 'transmission/helper'
require 'transmission/component'
require 'transmission/system'
require 'transmission/game_definition'
require 'transmission/driver'
