# frozen_string_literal: true

module RequestHelper
  def query_game_request
    {
      action: 'query_game',
      data_type: 'query_game',
      data: {
        player_token: 'fake_token',
        game_id: 862467050,
        property_id: 8600,
      },
    }
  end

  def bet_request
    {
      action: 'bet',
      data_type: 'bet',
      data: {
        balance: {
          mode: 'CASH',
        },
        denom: 100,
        round_info: {
          id: "CADSADSAD",
        },
        bet_options: [
          {
            name: 'ante',
            bet_amount: {
              credit: 1,
            },
          },
        ],
      },
    }
  end
end

World(RequestHelper)

Given("Receive a bet request") do
  @request = bet_request
end

Given("Have not played in the past") do
  allow_any_instance_of(Transmission::GameEngineWrapper)
    .to receive_message_chain(:last_round, :last_action)
    .and_return({})
end

Given("Have played in the past which the last round cards is 8d,7d,9d") do
  allow_any_instance_of(Transmission::GameEngineWrapper)
    .to receive_message_chain(:new_round?)
    .and_return(true)
  allow_any_instance_of(Transmission::GameEngineWrapper)
    .to receive_message_chain(:last_round, :last_action)
    .and_return(
        table: {
          round_info: {
            id: "123",
          },
          stage: {
            current: "baseGame",
          },
          denom_info: {
            denoms: [100, 200, 500, 1000, 50000],
            index: 0,
            denom: 100,
          },
        },
        player: {
          rank: "straight_flush_with_8",
          action_win: {
            cash: 9900,
            credit: 99,
          },
          card: ["8d", "7d", "9d"],
        },
        bet_options: [
          {
            name: "ante",
            max_bet: {
              cash: 999999999,
            },
            min_bet: {
              cash: 1,
            },
            bet_amount: {
              cash: 100,
              credit: 1,
            },
            payout: {
              cash: 9900,
              credit: 99,
            },
          },
        ]
      )
  allow_any_instance_of(Transmission::GameEngineWrapper)
    .to receive_message_chain(:instance_data)
    .and_return(
        last_command: 'bet',
      )
end

When("H-888 has opened") do
  @gamebox_driver = Transmission::Driver::GameboxDriver.new(862467050)
end

When("Finished query game") do
  @request = query_game_request
  @response = JSON.parse(@gamebox_driver.update(@request.to_json)).deep_snakize_keys!
end

When("The cards is {string}") do |cards|
  @request[:data][:inspect_data] = {
    player_card: cards,
  }
  @response = JSON.parse(@gamebox_driver.update(@request.to_json)).deep_snakize_keys!
end

Then("The response's rank is {string}") do |expect_rank|
  actual_rank = @response[:data][:player][:rank]
  expect(actual_rank).to eq(expect_rank)
end

Then("The response's payout cash is {int}") do |expected_payout_cash|
  expected_payout_cash = expected_payout_cash.to_i
  actual_payout = @response[:data][:player][:action_win]
  expect(actual_payout[:cash]).to eq(expected_payout_cash)
end

Then("The response's payout credit is {int}") do |expected_payout_credit|
  expected_payout_credit = expected_payout_credit.to_i
  actual_payout = @response[:data][:player][:action_win]
  expect(actual_payout[:credit]).to eq(expected_payout_credit)
end

Then("Player would not show last round rank and last card") do
  player_data = @response.dig(:data, :player)
  expect(player_data).to(be_nil)
end

Then("Player will show last round rank and last card") do
  player_data = @response.dig(:data, :player)
  expect(player_data).not_to(be_nil)
  expect(player_data[:rank]).to eq("straight_flush_with_8")
  expect(player_data[:card]).to eq(["8d", "7d", "9d"])
end
