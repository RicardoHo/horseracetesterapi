# frozen_string_literal: true

ENV['TRANSMISSION_ENV'] ||= 'development'

$LOAD_PATH.unshift(
  File.join(File.expand_path('../../development/', __dir__), 'lib')
)

require 'transmission_dev/api'
require 'transmission'
require 'transmission_dev'
require 'transmission_dev/support'
require 'transmission_dev/util'
require 'transmission_dev/driver'

require 'cucumber/rspec/doubles'
