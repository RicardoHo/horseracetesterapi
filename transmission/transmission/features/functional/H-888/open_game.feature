Feature: H-888 open game
  H-888 open game, the situation can divide into first open game and show last round

  Scenario: Open game for the first time
    Given Have not played in the past
    When  H-888 has opened
    And   Finished query game
    Then  Player would not show last round rank and last card

  Scenario: Have played game and show last round in open game
    Given Have played in the past which the last round cards is 8d,7d,9d
    When  H-888 has opened
    And   Finished query game
    Then  Player will show last round rank and last card
