Feature: H-888 game play
  If Receive request, it should return a correct response payout.

  Background:
    When H-888 has opened
    And  Finished query game

  Scenario Outline: Bet and win
    Given Receive a bet request
    When The cards is "<cards>"
    Then The response's rank is "<expect_rank>"
    And  The response's payout cash is <expect_payout_cash>
    And  The response's payout credit is <expect_payout_credit>

  Examples:
    | cards    | expect_rank            | expect_payout_cash |  expect_payout_credit |
    | 8h,8d,8s | triple_8				        |  16800             |  168                  |
    | 8h,7h,6h | straight_flush_with_8	|  9900              |  99                   |
    | 5h,7h,6h | straight_flush			    |  3300              |  33                   |
    | 3h,3s,3d | three_of_a_kind				    |  2800              |  28                   |
    | 8h,7d,6s | straight_with_8		    |  1500              |  15                   |
    | 2h,5h,8h | flush_with_8			      |  900               |  9                    |
    | 8h,8d,6h | double_8				        |  800               |  8                    |
    | 2h,3d,4s | straight				        |  500               |  5                    |
    | 2h,5h,6h | flush					        |  300               |  3                    |
    | 8h,2d,6h | single_8				        |  100               |  1                    |

  Scenario Outline: Bet and no win
    Given Receive a bet request
    When The cards is "<cards>"
    Then The response's rank is "<expect_rank>"
    And  The response's payout cash is <expect_payout_cash>
    And  The response's payout credit is <expect_payout_credit>

  Examples:
    | cards    | expect_rank           | expect_payout_cash |  expect_payout_credit |
    | 2h,4d,5s | nothing               | 0                  |  0                    |
