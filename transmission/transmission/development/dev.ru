# frozen_string_literal: true

ENV['TRANSMISSION_ENV'] ||= 'development'
ENV['TRANSMISSION_RNG_URL'] = 'http://localhost:4000'
if ENV['OPS']
  ENV['TRANSMISSION_PORT'] = ENV['OPS'] * 4
end

$LOAD_PATH.unshift(
  File.join(File.expand_path('..', __dir__), 'lib'),
  File.join(__dir__, 'lib')
)
require 'hashie'
require 'transmission_dev/api'
require 'transmission'
require 'transmission_dev'
# require 'transmission_dev/util'
require 'transmission_dev/support/game_engine_instance_struct'
require 'transmission_dev/support/game_engine_wrapper'
Transmission::Inspection::Inspecter.inspect_classes.each do |klass|
  Transmission::System.const_get(klass.class_name.to_sym).prepend(klass)
end

Transmission::Service.run!(
  port: Transmission.config[:port],
  host: Transmission.config[:host]
)
