# frozen_string_literal: true

ENV['TRANSMISSION_ENV'] ||= 'development'
# ENV['TRANSMISSION_RNG_URL'] ||= 'http://localhost:5000'

$LOAD_PATH.unshift(
  File.join(File.expand_path('..', __dir__), 'lib'),
  File.join(__dir__, 'lib')
)
require 'transmission_dev/api'
require 'transmission'
require 'transmission_dev'
require 'transmission_dev/support'
require 'transmission_dev/driver'

if ENV['TRANSMISSION_RNG_URL']
  require_relative 'lib/transmission_dev/util/math_report_generator.rb'
else
  require 'transmission_dev/util'
end

require 'ruby-progressbar'
require 'axlsx'

def find_game_id(math_code_name)
  Transmission::Util::GameDefinitionUtil
    .send(:code_name_lut)
    .select do |_game_id, code_name|
      code_name == math_code_name
    end
    .keys.first
end

begin
  math_code_name = ENV.fetch('MATH_CODE_NAME')
  math_dir       = File.join(Transmission.root, 'math')
  report_dir     = File.join(math_dir, 'report')
  report_path    = File.join(report_dir, "#{math_code_name}_%srounds_%s.xlsx")
  FileUtils.mkdir_p(report_dir)

  D13n.enable_dry_run

  game_id      = find_game_id(math_code_name)
  info_mod     = Transmission::Math.const_get(
    math_code_name.camel_case + '::Info'
  )
  validator    = Transmission::Driver::MathValidateDriver.new(info_mod, game_id, report_path)
  strategy_mod = Transmission::Math.const_get(
    math_code_name.camel_case + '::Strategy'
  )
  validator.extend(strategy_mod)
  validator.run
rescue SystemExit, Interrupt
  Transmission::Util::MathReportGenerator.to_csv(
    validator.report_data,
    format(report_path, validator.progress, Time.now.to_s.tr(':', '-'))
  )

  puts "\nexit!"
end
