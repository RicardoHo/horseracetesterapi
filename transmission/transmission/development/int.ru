# frozen_string_literal: true

ENV['TRANSMISSION_ENV'] ||= 'development'
ENV['TRANSMISSION_GAME_ENGINE_URL'] ||= 'http://10.10.5.138:3000'
ENV['TRANSMISSION_RNG_URL'] ||= 'http://localhost:5000'

$LOAD_PATH.unshift(
  File.join(File.expand_path('..', __dir__), 'lib'),
  File.join(__dir__, 'lib')
)
require 'transmission_dev/api'
require 'transmission'
require 'transmission_dev'
Transmission::Inspection::Inspecter.inspect_classes.each do |klass|
  Transmission::System.const_get(klass.class_name.to_sym).prepend(klass)
end

Transmission::Service.run!(
  port: Transmission.config[:port],
  host: Transmission.config[:host]
)
