# frozen_string_literal: true

module Transmission
  class GameEngineWrapper
    JPS_LOCK_AMT = 1_000_000
    CONFIG_PATH  = File.join(__dir__, 'operation_config')

    module MockGameEngineApiClientMethod
      def open; end

      def update
        instance_data[:round_data].shift
      end

      def bet(bet_opts:)
        current_round.init   = false
        current_round.opened = true
        current_round.closed = false
        self.balance -= bet_opts.values.sum
        {
          balance: balance,
        }
      end

      def result; end

      def take_win(payout_opts: nil)
        current_round.current_action = nil
        payout_amt = payout_opts&.values&.sum || 0
        self.balance += payout_amt
        if payout_amt >= JPS_LOCK_AMT
          current_round.hit_jps = true
          {
            jps_info: {
              win_id: '123123',
              state: 'hit',
            },
          }
        else
          current_round.hit_jps = false
          current_round.init   = true
          current_round.opened = false
          current_round.closed = true
          {
            balance: self.balance,
          }
        end
      end
    end

    def self.game_configs(game_id:, property_id:)
      # property_id has no usage in dev code
      property_id.to_s
      code_name = Util::GameDefinitionUtil.send(:code_name_lut)[game_id]
      config_data = MultiJson.load(
        File.read(
          File.join(CONFIG_PATH, "#{code_name.snake_case}.json")
        ),
        symbolize_keys: true
      )
      {
        denoms: config_data.dig(:game_config, :denoms),
        **config_data.slice(:currency),
      }
    end

    def new_round?
      false
      # true      #resume game
    end

    def resume_round?
      false
    end

    def create(_player_token = nil, _instance_id = nil, property_id = nil, login_name = nil, _machine_token = nil)
      code_name = Util::GameDefinitionUtil.send(:code_name_lut)[@game_id]
      json_data = MultiJson.load(
        File.read(
          File.join(CONFIG_PATH, "#{code_name.snake_case}.json")
        ),
        symbolize_keys: true
      )
      @game_engine ||= GameEngineInstanceStruct.new(json_data)
      @game_engine.extend(MockGameEngineApiClientMethod)
      @rng_capture = {
        rng_ref_id: '',
        rng_result: [],
      }
      on_open
    end

    def close
      true
    end

    def on_bet
      @game_engine.current_round.denom   = @game_engine.current_round.current_action.dig(:table, :denom_info, :denom)
      @game_engine.before_result_balance = @game_engine.balance
      @game_engine.balance -= retrieve_bet_opts.values.sum
      @running_data[:before_result_balance] = {
        cash: @game_engine.before_result_balance,
        credit: @game_engine.before_result_balance / @game_engine.current_round.denom,
        currency: @game_engine.currency,
      }
      @running_data[:balance] = {
        cash: @game_engine.balance,
        credit: @game_engine.balance / @game_engine.current_round.denom,
        currency: @game_engine.currency,
      }
    end

    def on_result
      @game_engine.instance_data[:save_state] = Enum::SaveState::RESULT
      @game_engine.result
    end

    def on_take_win
      @game_engine.balance += retrieve_payout_opts.values.sum
      @running_data[:balance] = {
        cash: @game_engine.balance,
        credit: @game_engine.balance / @game_engine.current_round.denom,
        currency: @game_engine.currency,
      }
    end

    def on_update
      @game_engine.instance_data[:save_state] = Enum::SaveState::UPDATE
      @game_engine.update
    end
  end
end
