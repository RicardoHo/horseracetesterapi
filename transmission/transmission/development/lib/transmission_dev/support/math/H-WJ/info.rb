# frozen_string_literal: true

module Transmission
  module Math
    module HWj
      module Info
        PAGES_INFO = {
          base_game: {
            heads: [
              'Player Hand',
              'Hit',
              'Bet',
              'Total Payout',
            ],
            pks: [
              'Player Hand',
            ],
          },
        }

        REPORT_INFO = {
          code_name: 'H-WJ',
          pages_info: PAGES_INFO,
        }
      end
    end
  end
end
