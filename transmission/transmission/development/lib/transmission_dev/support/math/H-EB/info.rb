# frozen_string_literal: true

module Transmission
  module Math
    module HEb
      module Info
        PAGES_INFO = {
          base_game_deal: {
            heads: [
              'Bet Item',
              'Hit',
              'Total Bet',
              'Total Payout',
            ],
            pks: [
              'Bet Item',
            ],
          },
        }

        REPORT_INFO = {
          code_name: 'H-EB',
          pages_info: PAGES_INFO,
        }
      end
    end
  end
end
