# frozen_string_literal: true

module Transmission
  module Math
    module HEb
      module Strategy
        extend self

        def query_game
          {
            action: 'query_game',
            data_type: 'query_game',
            data: {
              player_token: 'fake_token',
              game_id: @game_id,
              property_id: 8600,
              login_name: 'math_validater',
            },
          }
        end

        def after_query_game
          @round_finish = true

          {
            action: 'baccarat_deal',
            data_type: 'baccarat_deal',
            data: {
              balance: {
                mode: 'CASH',
              },
              denom: 100,
              bet_options: [
                {
                  name: 'player',
                  bet_amount: {
                    credit: 100,
                  },
                },
                {
                  name: 'banker',
                  bet_amount: {
                    credit: 100,
                  },
                },
                {
                  name: 'player_pair',
                  bet_amount: {
                    credit: 100,
                  },
                },
                {
                  name: 'banker_pair',
                  bet_amount: {
                    credit: 100,
                  },
                },
                {
                  name: 'tie',
                  bet_amount: {
                    credit: 100,
                  },
                },
              ],
            },
          }
        end
        alias_method :after_reshuffle, :after_query_game

        def reshuffle
          {
            action: 'reshuffle',
            data_type: 'reshuffle',
            data: {},
          }
        end

        def after_baccarat_deal
          if @result.dig(:data, :table, :shoe, :cut_card_reached)
            reshuffle
          else
            after_query_game
          end
        end

        def collect_data
          return unless @result.fetch(:action) == 'baccarat_deal'

          @result_data = @result.fetch(:data)
          collect_bet_and_result
        end

        private

        def compose_bet_opt_row_data(opt)
          [
            opt.dig(:name),
            opt.dig(:payout, :credit) > 0 ? 1 : 0,
            opt.dig(:bet_amount, :credit),
            opt.dig(:payout, :credit),
          ]
        end

        def collect_bet_and_result
          @result_data.fetch(:bet_options).each do |opt|
            @report_data.push_row_to(:base_game_deal, compose_bet_opt_row_data(opt))
          end
        end
      end
    end
  end
end
