# frozen_string_literal: true

module Transmission
  module Math
    module HHr
      module Strategy
        extend self

        def query_game
          {
            action: 'query_game',
            data_type: 'query_game',
            data: {
              player_token: 'fake_token',
              game_id: @game_id,
              property_id: 10,
              login_name: 'math_validater',
            },
          }
        end

        def after_query_game
          @round_finish = true

          {
            action: 'multipleBet',
            data_type: 'multipleBet',
            data: {
              denom: 1,
              play_mode: "forecast",
              bet_options:
              [
                {
                  name: "win_0",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "win_1",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "win_2",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "win_3",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "win_4",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "win_5",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "win_6",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "place_0",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "place_1",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "place_2",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "place_3",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "place_4",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "place_5",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "place_6",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_1",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_2",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_3",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_4",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_5",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_6",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_7",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_9",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_10",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_11",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_12",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_13",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_14",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_15",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_17",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_18",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_19",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_20",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_21",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_22",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_23",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_25",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_26",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_27",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_28",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_29",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_30",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_31",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_33",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_34",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_35",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_36",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_37",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_38",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_39",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_41",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_42",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_43",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_44",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_45",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_46",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "forecast_47",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "dual_forecast_1",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "dual_forecast_2",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "dual_forecast_3",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "dual_forecast_4",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "dual_forecast_5",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "dual_forecast_6",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "dual_forecast_9",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "dual_forecast_10",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "dual_forecast_11",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "dual_forecast_12",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "dual_forecast_13",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "dual_forecast_17",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "dual_forecast_18",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "dual_forecast_19",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "dual_forecast_20",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "dual_forecast_25",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "dual_forecast_26",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "dual_forecast_27",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "dual_forecast_33",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "dual_forecast_34",
                  betAmount: {
                    credit: 100,
                  },
                },
                {
                  name: "dual_forecast_41",
                  betAmount: {
                    credit: 100,
                  },
                },

              ],
            },
          }
        end

        def after_multiple_bet
          after_query_game
        end

        def collect_data
          return unless @result.fetch(:action) == 'multiple_bet'

          @result_data = @result.fetch(:data)
          bet_options = @result_data.dig(:table, :last_bet, :forecast)
          win_options = @result_data.dig(:table, :win_options)
          win_hash = {}
          win_options.each do |win_opt|
            win_hash[win_opt[:name]] = win_opt[:payout][:credit]
          end
          # p win_hash
          win_reference = @result_data.dig(:table, :win_reference)
          # p win_options, win_reference
          bet_options.each do |bet_opt|
            # p bet_opt[:name]
            payout = 0
            hit = win_reference.include?(bet_opt[:name])
            if hit
              payout = win_hash[bet_opt[:name]]
            end
            row = [
              bet_opt.dig(:name),
              hit ? 1 : 0,
              bet_opt.dig(:bet_amount, :credit),
              payout,
            ]
            @report_data.push_row_to(:base_game_deal, row)
            # p row
          end

        end

        private

        # def compose_bet_opt_row_data(opt)
        #   [
        #     opt.dig(:name),
        #     opt.dig(:payout, :credit) > 0 ? 1 : 0,
        #     opt.dig(:bet_amount, :credit),
        #     opt.dig(:payout, :credit),
        #   ]
        # end

        # def collect_bet_and_result
        #   @result_data.fetch(:bet_options).each do |opt|
        #     @report_data.push_row_to(:base_game_deal, compose_bet_opt_row_data(opt))
        #   end
        # end
      end
    end
  end
end
