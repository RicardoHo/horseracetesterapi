# frozen_string_literal: true

module Transmission
  module Math
    module HSg
      module Strategy
        extend self

        def query_game
          {
            action: 'query_game',
            data_type: 'query_game',
            data: {
              player_token: 'fake_token',
              game_id: @game_id,
              property_id: 8600,
              login_name: 'math_validater',
            },
          }
        end

        def after_query_game
          @round_finish = true

          {
            action: 'multiple_bet',
            data_type: 'multiple_bet',
            data: {
              denom: 1,
              multiplier: 1,
              bet_options: [
                {
                  name: 'dragon',
                  bet_amount: {
                    credit: 100,
                  },
                },
                {
                  name: 'phoenix',
                  bet_amount: {
                    credit: 100,
                  },
                },
                {
                  name: 'dragon_even',
                  bet_amount: {
                    credit: 100,
                  },
                },
                {
                  name: 'dragon_odd',
                  bet_amount: {
                    credit: 100,
                  },
                },
                {
                  name: 'dragon_pair',
                  bet_amount: {
                    credit: 100,
                  },
                },
                {
                  name: 'phoenix_even',
                  bet_amount: {
                    credit: 100,
                  },
                },
                {
                  name: 'phoenix_odd',
                  bet_amount: {
                    credit: 100,
                  },
                },
                {
                  name: 'phoenix_pair',
                  bet_amount: {
                    credit: 100,
                  },
                },
                {
                  name: 'total_9',
                  bet_amount: {
                    credit: 100,
                  },
                },
                {
                  name: 'total_7_8',
                  bet_amount: {
                    credit: 100,
                  },
                },
                {
                  name: 'total_1_6',
                  bet_amount: {
                    credit: 100,
                  },
                },
              ],
            },
          }
        end

        def after_multiple_bet
          after_query_game
        end

        def collect_data
          return unless @result.fetch(:action) == 'multiple_bet'

          @result_data = @result.fetch(:data)
          collect_bet_and_result
        end

        private

        def compose_bet_opt_row_data(opt)
          [
            opt.dig(:name),
            opt.dig(:payout, :credit) > 0 ? 1 : 0,
            opt.dig(:bet_amount, :credit),
            opt.dig(:payout, :credit),
          ]
        end

        def collect_bet_and_result
          @result_data.fetch(:bet_options).each do |opt|
            @report_data.push_row_to(:base_game_deal, compose_bet_opt_row_data(opt))
          end
        end
      end
    end
  end
end
