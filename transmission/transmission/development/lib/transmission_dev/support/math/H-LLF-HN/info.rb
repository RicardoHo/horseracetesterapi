# frozen_string_literal: true

module Transmission
  module Math
    module HLlfHn
      module Info
        PAGES_INFO = {
          base_game: {
            heads: [
              'Player Hand',
              'Hit',
              'Bet',
              'Total Payout',
            ],
            pks: [
              'Player Hand',
            ],
          },
        }

        REPORT_INFO = {
          code_name: 'H-LLF-HN',
          pages_info: PAGES_INFO,
        }
      end
    end
  end
end
