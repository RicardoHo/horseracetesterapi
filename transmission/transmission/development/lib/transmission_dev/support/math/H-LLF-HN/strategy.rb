# frozen_string_literal: true

module Transmission
  module Math
    module HLlfHn
      module Strategy
        extend self

        def query_game
          {
            action: 'query_game',
            data_type: 'query_game',
            data: {
              player_token: 'fake_token',
              game_id: @game_id,
              property_id: 8600,
              login_name: 'math_validater',
            },
          }
        end

        def after_query_game
          @round_finish = true

          {
            action: 'bet',
            data_type: 'bet',
            data: {
              balance: {
                mode: 'CASH',
              },
              denom: 100,
              bet_options: [
                {
                  name: 'ante',
                  bet_amount: {
                    credit: 10,
                  },
                },
              ],
            },
          }
        end

        def after_bet
          after_query_game
        end

        def collect_data
          return unless @result.fetch(:action) == 'bet'

          @result_data = @result.fetch(:data)
          collect_bet_data
        end

        private

        def compose_bet_row_data
          ante_payout = @result_data.fetch(:bet_options).dig(:payout, :credit)

          [
            @result_data.dig(:player, :rank),
            1,
            10,
            ante_payout,
          ]
        end

        def collect_bet_data
          @report_data.push_row_to(:base_game, compose_bet_row_data)
        end
      end
    end
  end
end
