# frozen_string_literal: true

module Transmission
  module Math
    class ReportData
      def initialize(report_info)
        @code_name = report_info.fetch(:code_name)
        pages_info = report_info.fetch(:pages_info)
        @pages = pages_info.keys
        @pages.each do |page|
          add_page(page, pages_info[page])
        end
      end

      #
      # to add a new page
      #
      # @param [String] page
      # @param [Array<String>] heads
      #
      def add_page(page, info)
        instance_variable_set(:"@#{page}", PageData.new(info))
      end

      def push_row_to(page, row)
        instance_variable_get(:"@#{page}").push(row)
      end

      def inspect
        @pages.each_with_object({}) do |page_name, hash|
          hash[page_name] = instance_variable_get(:"@#{page_name}").inspect
        end
      end
    end
  end
end
