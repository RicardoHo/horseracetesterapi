# frozen_string_literal: true

module Transmission
  module Math
    module HFc
      module Strategy
        extend self

        BET_COMBS = {
          over: 3..11,
          under: 3..11,
          tie: 2..12,
        }
        NUM_BET_OPTION = BET_COMBS.values.sum(&:size)

        def init_counter
          @counter = 0
          @max_bet_counter = @progress.total / NUM_BET_OPTION
          @current_bet_option = BET_COMBS.keys.first
          @current_bet_choose = BET_COMBS[@current_bet_option].first
        end

        def query_game
          init_counter
          {
            action: 'query_game',
            data_type: 'query_game',
            data: {
              player_token: 'fake_token',
              game_id: @game_id,
              property_id: 8600,
            },
          }
        end

        def after_query_game
          @round_finish = true

          {
            action: 'roll',
            data_type: 'roll',
            data: {
              denom: 100,
              bet_options: [
                {
                  name: @current_bet_option.to_s,
                  bet_amount: {
                    credit: 100,
                  },
                  bet_item: {
                    value: @current_bet_choose,
                  },
                },
              ],
            },
          }
        end

        def after_roll
          after_query_game
        end

        def collect_data
          return unless @result.fetch(:action) == 'roll'

          counter_click
          @result_data = @result.fetch(:data)
          collect_bet_data
        end

        private

        def counter_click
          @counter += 1
          return unless @counter == @max_bet_counter

          @counter = 0
          if @current_bet_choose == BET_COMBS[@current_bet_option].last
            next_bet_option
          else
            @current_bet_choose += 1
          end
        end

        def next_bet_option
          @current_bet_option =
            BET_COMBS.keys.rotate(BET_COMBS.keys.index(@current_bet_option) + 1).first
          @current_bet_choose = BET_COMBS[@current_bet_option].first
        end

        def compose_bet_row_data
          payout = @result_data.fetch(:bet_options).find do |opt|
            opt.dig(:name) == @current_bet_option.to_s
          end.dig(:payout, :credit)
          hit = payout.zero? ? 0 : 1

          [
            @current_bet_option.to_s.capitalize,
            @current_bet_choose,
            1,
            hit,
            100,
            payout,
          ]
        end

        def collect_bet_data
          @report_data.push_row_to(:base_game, compose_bet_row_data)
        end
      end
    end
  end
end
