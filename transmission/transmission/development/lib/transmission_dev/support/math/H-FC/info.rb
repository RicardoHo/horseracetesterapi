# frozen_string_literal: true

module Transmission
  module Math
    module HFc
      module Info
        PAGES_INFO = {
          base_game: {
            heads: [
              'Bet Option',
              'Choose',
              'Count',
              'Hit',
              'Bet in Credit',
              'Total Payout in Credit',
            ],
            pks: [
              'Bet Option',
              'Choose',
            ],
          },
        }

        REPORT_INFO = {
          code_name: 'H-FC',
          pages_info: PAGES_INFO,
        }
      end
    end
  end
end
