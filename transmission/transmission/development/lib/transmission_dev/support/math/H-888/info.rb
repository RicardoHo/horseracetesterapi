# frozen_string_literal: true

module Transmission
  module Math
    module H888
      module Info
        PAGES_INFO = {
          base_game: {
            heads: [
              'Player Hand',
              'Hit',
              'Bet',
              'Total Payout',
            ],
            pks: [
              'Player Hand',
            ],
          },
        }

        REPORT_INFO = {
          code_name: 'H-888',
          pages_info: PAGES_INFO,
        }
      end
    end
  end
end
