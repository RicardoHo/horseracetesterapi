# frozen_string_literal: true

module Transmission
  module Math
    module HNp
      module Strategy
        extend self

        BET_COMBS = {
          # low: 8..12,
          # normal: 8..12,
          high: 8..12,
        }
        NUM_BET_OPTION = BET_COMBS.values.sum(&:size)

        def init_counter
          @counter = 0
          @max_bet_counter = @progress.total / NUM_BET_OPTION
          @current_bet_option = BET_COMBS.keys.first
          @current_bet_choose = BET_COMBS[@current_bet_option].first
        end

        def query_game
          init_counter
          {
            action: 'query_game',
            data_type: 'query_game',
            data: {
              player_token: 'fake_token',
              game_id: @game_id,
              property_id: 8600,
            },
          }
        end

        def after_query_game
          @round_finish = true
          {
            action: 'drop',
            data_type: 'drop',
            data: {
              denom: 1,
              selectedLine: @current_bet_choose,
              selectedRiskLevel: @current_bet_option.to_s,
              selectedBetOptions: [
                {
                  name: "default",
                  amount: {
                    credit: 10,
                  },
                },
              ],
            },
          }
        end

        def after_drop
          after_query_game
        end

        # def after_drop
        #   @round_finish = true

        #   {
        #     action: "dropFinish",
        #     dataType: "dropFinish",
        #     data: {
        #       roundInfoWithFinish: [
        #         {
        #           roundInfo: {
        #             id: "xxxx",
        #           },
        #           finish: true,
        #         },
        #       ],
        #     },
        #   }
        # end

        # def after_drop_finish
        #   after_query_game
        # end

        def collect_data
          return unless @result.fetch(:action) == 'drop'

          counter_click
          @result_data = @result.fetch(:data)
          collect_bet_data
        end

        private

        def counter_click
          @counter += 1
          return unless @counter == @max_bet_counter

          @counter = 0
          if @current_bet_choose == BET_COMBS[@current_bet_option].last
            next_bet_option
          else
            @current_bet_choose += 1
          end
        end

        def next_bet_option
          @current_bet_option =
            BET_COMBS.keys.rotate(BET_COMBS.keys.index(@current_bet_option) + 1).first
          @current_bet_choose = BET_COMBS[@current_bet_option].first
        end

        def compose_bet_row_data
          @result_data.dig(:selected_bet_options).each do |opt|
            @payout = @result_data.dig(:payouts).find do |option|
              opt.dig(:name) == option.dig(:name)
            end.dig(:amount, :credit)
          end
          # pocket_index = @result_data.dig(:result, :pocket, :index)
          [
            @current_bet_option.to_s.capitalize,
            @current_bet_choose,
            # pocket_index,
            1,
            10,
            @payout,
          ]
        end

        def collect_bet_data
          @report_data.push_row_to(:base_game, compose_bet_row_data)
        end
      end
    end
  end
end
