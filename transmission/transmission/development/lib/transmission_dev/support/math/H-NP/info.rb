# frozen_string_literal: true

module Transmission
  module Math
    module HNp
      module Info
        PAGES_INFO = {
          base_game: {
            heads: [
              'Bet Option',
              'Lines',
              # 'Pocket index',
              'Count',
              'Bet in Credit',
              'Total Payout in Credit',
            ],
            pks: [
              'Bet Option',
              'Lines',
              # 'Pocket index',
            ],
          },
        }

        REPORT_INFO = {
          code_name: 'H-NP',
          pages_info: PAGES_INFO,
        }
      end
    end
  end
end
