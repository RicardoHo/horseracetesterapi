# frozen_string_literal: true

module Transmission
  module Math
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Page data consists by heads and rows
    # heads is the head line
    # rows is corresponding data to the heads in row
    # example:
    #   heads: name, number
    #   rows:  Foo,  123-456
    #          Boo,  456-123
    #
    class PageData
      def initialize(heads:, pks:, **opts)
        @heads    = heads
        @pk_idxs  = pks.map { |pk| @heads.index(pk) }
        @opt_idxs = opts.each_with_object({}) do |(opt_name, list), hash|
          hash[opt_name] = list.map { |head| @heads.index(head) }
        end
        @rows     = {}
      end

      #
      # push one row to the end of all row data
      #
      # @param [Array] data
      #
      def push(data)
        pk_string = @pk_idxs.map { |idx| data[idx] }.join('-')
        if @rows.key?(pk_string)
          accumulate_data_to_row(@rows[pk_string], data)
        else
          @rows[pk_string] = data
        end
        # @rows.push(data[0..@heads.size])
      end

      #
      # convert page data to hash, shows all rows data by
      # integrate heads into each row
      # example:
      #   [
      #     { name: 'Foo', number: '123-456' }
      #     { name: 'Boo', number: '456-123' }
      #   ]
      #
      # @return [Hash]
      #
      def to_h
        @rows.values.map do |row_data|
          Hash[@heads.zip(row_data)]
        end
      end

      def inspect
        total = @heads.map.with_index do |_head, idx|
          next if @pk_idxs.include?(idx)
          next if @opt_idxs.values.flatten.include?(idx)

          @rows.values.sum { |row| row[idx] }
        end

        page_data = @rows.values.dup
        page_data.insert(0, @heads)
        page_data.push(total)
      end

      def accumulate_data_to_row(row, data)
        row.map!.with_index do |cell, idx|
          if @pk_idxs.include?(idx)
            cell
          elsif @opt_idxs.dig(:max)&.include?(idx)
            [cell, data[idx]].max
          else
            cell + data[idx]
          end
        end
      end
    end
  end
end
