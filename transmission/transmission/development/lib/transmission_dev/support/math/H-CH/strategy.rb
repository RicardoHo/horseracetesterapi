# frozen_string_literal: true

module Transmission
  module Math
    module HCh
      module Strategy
        extend self

        WEIGHT_TABLE = {
          raise: 8199,
          fold: 1801,
        }
        ENDING_ACTIONS = %w{raise fold}

        def query_game
          {
            action: 'query_game',
            data_type: 'query_game',
            data: {
              player_token: 'fake_token',
              game_id: @game_id,
              property_id: 8600,
              login_name: 'math_validater',
            },
          }
        end

        def after_query_game
          {
            action: 'deal',
            data_type: 'deal',
            data: {
              balance: {
                mode: 'CASH',
              },
              denom: 100,
              bet_options: [
                {
                  name: 'ante',
                  bet_amount: {
                    credit: 1,
                  },
                },
                {
                  name: 'aa',
                  bet_amount: {
                    credit: 1,
                  },
                },
              ],
            },
          }
        end
        alias_method :after_raise, :after_query_game
        alias_method :after_fold, :after_query_game

        def do_raise
          {
            action: 'raise',
            data_type: 'raise',
            data: {
              balance: {
                mode: 'CASH',
              },
              bet_options: [
                {
                  name: 'raise',
                  bet_amount: {
                    credit: 2,
                  },
                },
              ],
            },
          }
        end

        def do_fold
          {
            action: 'fold',
            data_type: 'fold',
            data: {},
          }
        end

        def after_deal
          @round_finish = true

          @rng ||= @gamebox_driver.instance_variable_get(:@game_engine).weight_table
          action, _table = Util::RNGUtil.draw_from_weight_table(WEIGHT_TABLE, rng: @rng)
          send(:"do_#{action}")
        end

        def collect_data
          return unless ENDING_ACTIONS.include?(@result.fetch(:action))

          @result_data = @result.fetch(:data)
          collect_aa
          collect_ante
        end

        private

        def aa_type(aa_rank)
          case aa_rank
          when 'pair', 'two_pair', 'three_of_a_kind', 'straight'
            'aces_or_better_to_straight'
          else
            aa_rank
          end
        end

        def compose_aa_row_data
          aa_bet = @result_data.fetch(:bet_options).find do |opt|
            opt[:name] == 'aa'
          end
          return ['nothing', 1, 1, 0, @result.fetch(:action)] if aa_bet.fetch(:type) == Enum::WinType::LOSE

          [
            aa_type(@result_data.dig(:player, :aa_rank)),
            1,
            1,
            aa_bet.dig(:payout, :credit),
            @result.fetch(:action),
          ]
        end

        def compose_ante_row_data
          ante_payout = @result_data.fetch(:bet_options).find do |opt|
            opt[:name] == 'ante'
          end.dig(:payout, :credit)
          raise_payout = @result_data.fetch(:bet_options).find do |opt|
            opt[:name] == 'raise'
          end.dig(:payout, :credit)

          [
            @result_data.dig(:player, :rank),
            @result_data.dig(:dealer, :rank),
            1,
            3,
            ante_payout,
            raise_payout,
            ante_payout + raise_payout,
            @result.fetch(:action),
          ]
        end

        def collect_aa
          aa_data = compose_aa_row_data
          return unless aa_data

          @report_data.push_row_to(:base_game_aa, aa_data)
        end

        def collect_ante
          @report_data.push_row_to(:base_game_ante, compose_ante_row_data)
        end
      end
    end
  end
end
