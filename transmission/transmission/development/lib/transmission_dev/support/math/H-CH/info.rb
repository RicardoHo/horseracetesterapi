# frozen_string_literal: true

module Transmission
  module Math
    module HCh
      module Info
        PAGES_INFO = {
          base_game_aa: {
            heads: [
              'AA Type',
              'Hit',
              'Bet',
              'Payout',
              'Fold/Raise',
            ],
            pks: [
              'AA Type',
              'Fold/Raise',
            ],
          },
          base_game_ante: {
            heads: [
              'Player Hand',
              'Dealer Hand',
              'Hit',
              'Bet',
              'Ante Payout',
              'Raise Payout',
              'Total Payout',
              'Fold/Raise',
            ],
            pks: [
              'Player Hand',
              'Dealer Hand',
              'Fold/Raise',
            ],
          },
        }

        REPORT_INFO = {
          code_name: 'H-CH',
          pages_info: PAGES_INFO,
        }
      end
    end
  end
end
