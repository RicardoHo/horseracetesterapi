# frozen_string_literal: true

module Transmission
  module Math
    module HCk
      module Strategy
        extend self

        BET_COMBS = {
          # 3 => [10, 20, 30],
          # 4 => [10, 20, 30, 40],
          # 5 => [10, 20, 30, 40, 50],
          # 6 => [10, 20, 30, 40, 50, 60],
          # 7 => [10, 20, 30, 40, 50, 60, 70],
          # 8 => [10, 20, 30, 40, 50, 60, 70, 80],
          # 9 => [10, 20, 30, 40, 50, 60, 70, 80, 65],
          10 => [10, 20, 30, 40, 50, 60, 70, 80, 65, 75],
        }
        NUM_BET_OPTION = BET_COMBS.keys.size

        def init_counter
          @counter = 0
          @max_bet_counter = @progress.total / NUM_BET_OPTION
          @current_bet_option = BET_COMBS.keys.first
          @current_bet_number_set = BET_COMBS[@current_bet_option]
        end

        def query_game
          init_counter
          {
            action: 'query_game',
            data_type: 'query_game',
            data: {
              player_token: 'fake_token',
              game_id: @game_id,
              property_id: 8600,
            },
          }
        end

        def after_query_game
          @round_finish = true
          {
            action: 'draw',
            data_type: 'draw',
            data: {
              denom: 1,
              selected_id_list: @current_bet_number_set,
              bet_options: [
                {
                  name: "default",
                  bet_amount: {
                    credit: 10,
                  },
                },
              ],
            },
          }
        end

        def draw_free_game
          {
            action: 'draw_free_game',
            data_type: 'draw_free_game',
            data: {},
          }
        end

        def after_draw
          if @result.dig(:data, :table, :stage, :next) == "freeGame"
            draw_free_game
          else
            after_query_game
          end
        end
        alias_method :after_draw_free_game, :after_draw

        def collect_data
          @hit =
            @result
              .dig(:data, :table, :id_box, :hit_counter) if @result.dig(:data, :table, :stage, :current) == "baseGame"
          return unless @result.dig(:data, :table, :stage, :next) == "baseGame"

          @result_data = @result.fetch(:data)
          collect_bet_data
          counter_click
        end

        private

        def counter_click
          @counter += 1
          return unless @counter == @max_bet_counter

          @counter = 0
          next_bet_option
        end

        def next_bet_option
          @current_bet_option =
            BET_COMBS.keys.rotate(BET_COMBS.keys.index(@current_bet_option) + 1).first
          @current_bet_number_set = BET_COMBS[@current_bet_option]
        end

        def compose_bet_row_data
          @base_game_payout = @result_data.dig(:bet_options, :base_game_payout, :credit)
          @free_game_payout = @result_data.dig(:bet_options, :free_game_payout, :credit)
          @total_payout = @result_data.dig(:bet_options, :payout, :credit)
          @free_game_count = @result_data.dig(:table, :stage, :current) == "freeGame" ? 1 : 0

          [
            @current_bet_option,
            @hit,
            1,
            @free_game_count,
            10,
            @base_game_payout,
            @free_game_payout,
            @total_payout,
          ]
        end

        def collect_bet_data
          @report_data.push_row_to(:base_game, compose_bet_row_data)
        end
      end
    end
  end
end
