# frozen_string_literal: true

module Transmission
  module Math
    module HCk
      module Info
        PAGES_INFO = {
          base_game: {
            heads: [
              'Choose',
              'Hit',
              'Count',
              'Free Game Count',
              'Bet in Credit',
              'Base Game Payout in Credit',
              'Free Game Payout in Credit',
              'Total Payout in Credit',
            ],
            pks: [
              'Choose',
              'Hit',
            ],
          },
        }

        REPORT_INFO = {
          code_name: 'H-CK',
          pages_info: PAGES_INFO,
        }
      end
    end
  end
end
