# frozen_string_literal: true

module Transmission
  module Math
    module HWjHn
      module Info
        PAGES_INFO = {
          base_game: {
            heads: [
              'Player Hand',
              'Hit',
              'Bet',
              'Total Payout',
            ],
            pks: [
              'Player Hand',
            ],
          },
        }

        REPORT_INFO = {
          code_name: 'H-WJ-HN',
          pages_info: PAGES_INFO,
        }
      end
    end
  end
end
