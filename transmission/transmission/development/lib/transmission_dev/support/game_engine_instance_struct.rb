# frozen_string_literal: true

module Transmission
  class CurrentRound < Hashie::Dash
    include Hashie::Extensions::Dash::Coercion
    include Hashie::Extensions::IndifferentAccess
    include Hashie::Extensions::MethodQuery

    property :ref_id, coerce: String
    property :current_action, coerce: Hash
    property :denom, default: nil
    property :opened, default: false
    property :prog_payout_info, default: nil
    property :hit_prog_payout, default: false
    property :hit_jps, default: false
    property :jps_info, default: nil
    property :init, default: true
    property :closed, default: false
  end

  class LastRound < Hashie::Dash
    include Hashie::Extensions::Dash::Coercion
    include Hashie::Extensions::IndifferentAccess
    include Hashie::Extensions::MethodQuery

    property :ref_id, coerce: String
    property :last_action, coerce: Hash
    property :system_completed?, default: true
  end

  class Denoms < Hashie::Dash
    include Hashie::Extensions::Dash::Coercion
    include Hashie::Extensions::IndifferentAccess
    include Hashie::Extensions::MethodQuery

    property :value, coerce: Array
    property :default_index, coerce: Integer
  end

  class GameConfig < Hashie::Dash
    include Hashie::Extensions::Dash::Coercion
    include Hashie::Extensions::IndifferentAccess
    include Hashie::Extensions::MethodQuery

    property :denoms, coerce: Denoms
    property :math_name, coerce: String
    property :extra, coerce: Hash
  end

  class GameEngineInstanceStruct < Hashie::Dash
    include Hashie::Extensions::Dash::Coercion
    include Hashie::Extensions::IndifferentAccess
    include Hashie::Extensions::MethodQuery

    property :current_round, coerce: CurrentRound
    property :instance_data, coerce: Hash
    property :last_round, coerce: LastRound, default: nil
    property :session_id, coerce: String
    property :instance_id, coerce: String
    property :currency, coerce: String
    property :game_config, coerce: GameConfig
    property :test_mode
    property :balance, default: 1_000_000
    property :before_result_balance, default: 1_000_000
    property :weight_table, coerce: Hash
  end
end
