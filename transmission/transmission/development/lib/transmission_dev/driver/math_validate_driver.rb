# frozen_string_literal: true

module Transmission
  module Driver
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # Driver for run math validation on development
    #
    class MathValidateDriver
      extend Forwardable

      def_delegators :@progress, :progress
      attr_reader :report_data

      def initialize(info_mod, game_id, report_path)
        @game_id = game_id
        @report_path = report_path
        @report_data = Math::ReportData.new(info_mod::REPORT_INFO)
        @gamebox_driver = GameboxDriver.new(@game_id)
      end

      def run
        @progress = ProgressBar.create(
          total: ENV.fetch('MATH_CYCLE', 5_000_000).to_i,
          throttle_rate: 0.5,
          format: '%a |%e | Progress: %p%%, %c rounds | Average: %r round/sec'
        )
        report_interval = ENV.fetch('REPORT_INTERVAL', @progress.total).to_i

        @event = query_game.to_json
        on_update
        until @progress.finished?
          run_one_round

          next unless (@progress.progress % report_interval).zero? || @progress.finished?

          Transmission::Util::MathReportGenerator.to_csv(
            @report_data,
            format(
              @report_path,
              @progress.progress,
              Time.now.to_s.tr(':', '-')
            )
          )
        end
      end

      private

      def before_update
        @event = send(:"after_#{@result.fetch(:action)}").to_json
      end

      def on_update
        @result = JSON.parse(@gamebox_driver.update(@event)).deep_snakize_keys!
        @result[:action] = @result[:action].snake_case

        raise @result.to_s if @result[:data_type] == 'error'
      end

      def after_update
        collect_data
      end

      def run_one_action
        before_update
        on_update
        after_update
      end

      def run_one_round
        run_one_action until @round_finish
      ensure
        @round_finish = false
        @progress.increment
      end
    end
  end
end
