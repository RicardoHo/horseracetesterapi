# frozen_string_literal: true

module Transmission
  module Util
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # A util to generate given report data to file
    #
    module MathReportGenerator
      extend self

      def to_csv(report_data, file_path = 'report.xlsx')
        xlsx = Axlsx::Package.new
        report_data.inspect.each do |page_name, rows|
          xlsx.workbook.add_worksheet(name: page_name.to_s.camel_case) do |sheet|
            rows.each { |row| sheet.add_row(row) }
          end
        end
        xlsx.use_shared_strings = true
        xlsx.serialize(file_path)
      end
    end
  end
end
