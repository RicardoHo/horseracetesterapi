# frozen_string_literal: true

module Transmission
  module Util
    #
    # @author Alpha Huang
    # @since 0.1.0
    #
    # random number generator util
    #
    module RNGUtil
      extend self

      #
      # generate a random number in range 0 to upper limit
      #
      # @param [Integer] upper_limit
      #
      # @return [Integer]
      #
      def random(upper_limit, rng:, rng_capture: nil)
        return 0 if upper_limit == 0

        rng.nil?
        rand(upper_limit)
      end

      #
      # generate a list of random number in range 0 to upper limit - 1
      #
      # @param [Integer] num_of_random
      # @param [Integer] upper_limit
      #
      # @return [Array<Integer>]
      #
      def random_multiple(num_of_random, upper_limit, repeat:, rng:, rng_capture: nil)
        rng.nil?
        return [0] * num_of_random if upper_limit <= 1

        if repeat
          num_of_random.times.map { rand(upper_limit) }
        else
          (0..upper_limit - 1).to_a.sample(num_of_random)
        end
      end

      #
      # generate list of random number in range 0 to upper limit
      #
      # @param [Array<Integer>] upper_limit_list
      #
      # @return [Array<Integer>]
      #
      def batch_random(upper_limit_list, rng:)
        rng.nil?
        upper_limit_list.map { |upper_limit| rand(upper_limit) }
      end

      #
      # random draw a symbol base on weight table
      # set replacement to true for avoid modify original table
      #
      # @param [Hash<Symbol, Integer>] weight_table
      # @param [Boolean] replacement
      #
      # @return [Integer]
      #
      def draw_from_weight_table(weight_table, replacement: false, rng:, rng_capture: nil)
        result_in_list, table = draw_multiple_from_weight_table(
          weight_table,
          1,
          replacement: replacement,
          rng: rng,
          rng_capture: rng_capture
        )

        [result_in_list[0], table]
      end

      #
      # random draw a list of symbol base on weight table
      # set replacement to true for avoid modify original table
      #
      # @param [Hash<Symbol, Integer>] weight_table
      # @param [Boolean] replacement
      #
      # @return [Integer]
      #
      def draw_multiple_from_weight_table(weight_table, num, replacement: false, rng:, rng_capture: nil)
        rng.nil?
        rng_capture.nil?
        list = weight_table.each_with_object([]) do |(key, value), arr|
          arr.push(*([key] * value))
        end

        if replacement
          [Array.new(num) { list.sample.to_s }, weight_table]
        else
          [
            Array.new(num) { list.delete_at(rand(list.size)).to_s },
            list
              .group_by { |v| v }
              .each_with_object({}) { |(k, v), h| h[k] = v.size },
          ]
        end
      end
    end
  end
end
