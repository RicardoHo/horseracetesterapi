# frozen_string_literal: true

# require all inspection's file here
require 'transmission_dev/inspection/inspecter'
Dir[File.join(__dir__, 'inspection', '**', '*.rb')].each { |file| require file }
