# frozen_string_literal: true

module Transmission
  module Api
    class Service < Sinatra::Base
      register Sinatra::Reloader
    end
  end
end
