# frozen_string_literal: true

module Transmission
  module Inspection
    class InspectConfigStore
      attr_reader :data_store
      attr_reader :approach_config_store

      def initialize
        @data_store = {}
        @approach_config_store = {}
      end

      def init_approach(approach_name)
        @approach_config_store[approach_name] = {
          data_list: [],
          data_struct: nil,
          handler: nil,
          handler_guard: nil,
        }

        @approach_config_store[approach_name]
      end
    end

    module Inspecter
      def self.included(klass)
        @inspect_classes ||= []
        @inspect_classes.push(klass)

        klass.extend(ClassMethod)
      end

      def self.inspect_classes
        @inspect_classes
      end

      def load_inspect_data(config_store)
        return nil if !@input.respond_to?(:inspect_data) || @input.inspect_data.nil?

        config_store.approach_config_store.each do |name, config|
          return name, config[:data_struct].new(
            @input.inspect_data.to_h.transform_keys(&:to_s).slice(*config.dig(:data_list))
          )
        rescue Dry::Struct::Error
          next
        end

        nil
      end

      module ClassMethod
        def class_name
          name.split('::').last
        end

        def data(name, type)
          data_store = @curr_config_store.data_store
          data_store[name.to_s] = type
        end

        def data_list(*data_list)
          data_store = @curr_config_store.data_store
          data_list.map!(&:to_s)
          return unless data_list.all? { |data_name| data_store.key?(data_name) }

          @curr_approach_store[:data_list].push(*data_list)
        end

        def handler(name, guard: nil)
          @curr_approach_store[:handler]       = name
          @curr_approach_store[:handler_guard] = guard
        end

        def approach(name)
          @curr_approach_store = @curr_config_store.init_approach(name)
          yield

          data_list  = @curr_approach_store[:data_list]
          data_store = @curr_config_store.data_store
          @curr_approach_store[:data_struct] = Class.new(Dry::Struct) do
            transform_keys(&:to_sym)
            data_list.each do |data_name|
              attribute data_name.to_sym, data_store[data_name]
            end
          end
        end

        def inspect_method(method_name)
          @curr_method_name  = method_name.to_sym
          @curr_config_store = InspectConfigStore.new
          @insp_config_store ||= {}
          @insp_config_store[method_name.to_sym] = @curr_config_store
          yield

          class_name   = self.class_name
          config_store = @curr_config_store
          appr_store   = config_store.approach_config_store
          class_eval do
            define_method(method_name) do |*args|
              approach_name, @inspect_data = load_inspect_data(config_store)
              if @inspect_data && send(appr_store[approach_name][:handler_guard], *args)
                Transmission.log_info(<<~INFO.tr("\n", ''))
                  Invoke inspection logic for method #{method_name}
                   of class #{class_name} by approach #{approach_name}
                INFO
                send(appr_store[approach_name][:handler], *args)
              else
                super(*args)
              end
            ensure
              @inspect_data = nil
            end
          end
        end
      end
    end
  end
end
