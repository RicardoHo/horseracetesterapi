# frozen_string_literal: true

module Transmission
  module Inspection
    module System
      #
      # @author Richard Fong
      # @since 0.9.0
      #
      # For insepction
      #
      #
      module DrawIdList
        include Inspecter

        inspect_method :process do
          data :result_id, Message::Filter::Types::String

          approach :by_result_id do
            data_list :result_id
            handler :inspect_by_result_id, guard: :data_valid_for_result_id?
          end
        end

        def inspect_by_result_id(entity)
          selected_id_list = entity.find(Component::SelectedIdList)

          @id_box.result_id = @input
            .inspect_data
            &.send(:result_id)
            &.split(",")
            &.map { |num_str| num_str.to_i }

          if @id_box.result_id
            @id_box.hit_id = selected_id_list.list & @id_box.result_id
            @id_box.hit_counter = @id_box.hit_id.length
          end
        end

        def data_valid_for_result_id?(_entity)
          result_id = @input
            .inspect_data
            &.send(:result_id)
            &.split(",")
            &.map { |num_str| num_str.to_i }
          (result_id - (0..@id_upper_limit).to_a).empty? && result_id.length == @system_draw_size
        end
      end
    end
  end
end
