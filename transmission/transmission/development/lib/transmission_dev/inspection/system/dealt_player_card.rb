# frozen_string_literal: true

module Transmission
  module Inspection
    module System
      #
      # @author Alpha Huang
      # @since 1.0.0
      #
      # For insepction
      # To dealt card to all player - the participant of the game
      #
      module DealtPlayerCard
        include Inspecter

        inspect_method :process_dealt do
          data :player_card, Message::Filter::Types::String
          data :banker_card, Message::Filter::Types::String.meta(omittable: true)
          data :dealer_card, Message::Filter::Types::String.meta(omittable: true)
          data :dragon_card, Message::Filter::Types::String
          data :phoenix_card, Message::Filter::Types::String

          approach :by_card do
            data_list :player_card, :banker_card, :dealer_card
            handler :inspect_by_card, guard: :data_valid_for_card?
          end
          approach :by_dragon_phoenix_card do
            data_list :dragon_card, :phoenix_card
            handler :inspect_by_card, guard: :data_valid_for_card?
          end
        end

        def data_valid_for_card?(entity)
          player_name = entity.find(Component::PlayerLabel).name
          hand_card   = entity.find(Component::HandCard)
          card_counts = @inspect_data
            .send("#{player_name}_card")
            .split(',')
            .slice(0...hand_card.min_hold)
            .group_by(&:itself)
            .map { |card, list| [card, list.size] }
            .to_h
          card_counts&.all? { |card, counts| @deck.card_pool.fetch(card.to_sym, 0) >= counts }
        end

        def inspect_by_card(entity)
          player_name = entity.find(Component::PlayerLabel).name
          hand_card = entity.find(Component::HandCard)
          cards     = @inspect_data.send("#{player_name}_card").split(',').slice(0...hand_card.min_hold)
          cards.each do |card|
            @deck.card_pool[card.to_sym] -= 1
            hand_card.collect(card)
          end
        end
      end
    end
  end
end
