# frozen_string_literal: true

module Transmission
  module Inspection
    module System
      #
      # @author Alpha Huang
      # @since 1.0.0
      #
      # For insepction
      # Dealt public cards, public cards are visable for all player
      #
      module RollDice
        include Inspecter

        inspect_method :process do
          data :dices, Message::Filter::Types::String

          approach :by_dices do
            data_list :dices
            handler :inspect_by_dices, guard: :data_valid_for_dices?
          end
        end

        def inspect_by_dices(entity)
          dice_cup   = entity.find(Component::DiceCup)
          score_card = entity.find(Component::DiceScoreCard)

          dices = @input
            .inspect_data
            &.dig(:dices)
            &.split(',')
            &.slice(0...dice_cup.num_of_dice)
            &.map { |dice_str| dice_str.to_i }
          if dices
            dice_cup.dices   = dices
            score_card.point = dice_cup.dices.sum
          end
        end

        def data_valid_for_dices?(entity)
          dice_cup = entity.find(Component::DiceCup)
          dices = @input.inspect_data&.dig(:dices)&.split(',')
          dices ||= []
          dices.size == dice_cup.num_of_dice
        end
      end
    end
  end
end
