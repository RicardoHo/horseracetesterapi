# frozen_string_literal: true

module Transmission
  module Inspection
    module System
      #
      # @author Alpha Huang
      # @since 1.0.0
      #
      # For insepction
      # Dealt public cards, public cards are visable for all player
      #
      module DealtPublicCard
        include Inspecter

        inspect_method :dealt_public_cards do
          data :public_card, Message::Filter::Types::String

          approach :by_card do
            data_list :public_card
            handler :inspect_by_card, guard: :data_valid_for_card?
          end
        end

        def data_valid_for_card?(num_of_card)
          range = case @command
          when 'deal'
            0...num_of_card
          when 'raise', 'fold'
            3...(3 + num_of_card)
          end
          public_cards = @inspect_data
            .public_card
            .split(',')
            .slice(range)
          card_counts = public_cards
            .group_by(&:itself)
            .map { |card, list| [card, list.size] }
            .to_h
          return false unless card_counts&.all? { |card, counts| @deck.card_pool.fetch(card.to_sym, 0) >= counts }

          @new_public_cards = public_cards
          true
        end

        def inspect_by_card(_num_of_card)
          @new_public_cards.each do |card|
            @deck.card_pool[card.to_sym] -= 1
          end
        end
      end
    end
  end
end
