# frozen_string_literal: true

module Transmission
  module Inspection
    module System
      #
      # @author Alpha Huang
      # @since 1.0.0
      #
      # For insepction
      # To dealt card to all player - the participant of the game
      # and will deal same amount of card for each suit
      #
      module DealtPlayerCardPerSuit
        include Inspecter

        inspect_method :process_dealt do
          data :player_card, Message::Filter::Types::String

          approach :by_card do
            data_list :player_card
            handler :inspect_by_card, guard: :data_valid_for_card?
          end
        end

        def data_valid_for_card?(entity)
          hand_card   = entity.find(Component::HandCard)
          suit_counts = @inspect_data
            .player_card
            .split(',')
            .slice(0...hand_card.min_hold)
            .map { |card| card[1] }
            .uniq
            .size
          suit_counts == hand_card.min_hold
        end

        def inspect_by_card(entity)
          hand_card = entity.find(Component::HandCard)
          cards     = @inspect_data.player_card.split(',').slice(0...hand_card.min_hold)
          cards.each do |card|
            @deck.card_pool[card[0].to_sym] -= 1
            hand_card.collect(card)
          end
        end
      end
    end
  end
end
