# frozen_string_literal: true

module Transmission
  module Inspection
    module System
      #
      # @author Alpha Huang
      # @since 1.0.0
      #
      # For insepction
      # Baccarat draw means to dealt third card to
      # player and banker if needed by drawing rule of baccarat
      #
      module DealtPlayerCardFromShoe
        include Inspecter

        CHECK_LIST = {
          3 => [nil, 0, 1, 2, 3, 4, 5, 6, 7, 9],
          4 => [nil, 2, 3, 4, 5, 6, 7],
          5 => [nil, 4, 5, 6, 7],
          6 => [6, 7],
        }
        NO_DRAW_LIST = {
          3 => [8],
          4 => [1, 8, 9],
          5 => [1, 2, 3, 8, 9],
          6 => [nil, 1, 2, 3, 4, 5, 8, 9],
        }

        inspect_method :process_dealt do
          data :player_card, Message::Filter::Types::String
          data :banker_card, Message::Filter::Types::String
          data :banker_point, Message::Filter::Types::CoeInt
          data :player_point, Message::Filter::Types::CoeInt
          data :banker_nature, Message::Filter::Types::CoeBool.optional.default(false)
          data :player_nature, Message::Filter::Types::CoeBool.optional.default(false)
          data :banker_pair, Message::Filter::Types::CoeBool.optional.default(false)
          data :player_pair, Message::Filter::Types::CoeBool.optional.default(false)

          approach :by_point do
            data_list :banker_point, :player_point, :banker_nature,
              :player_nature, :banker_pair, :player_pair
            handler :inspect_by_point, guard: :data_valid_for_point?
          end

          approach :by_card do
            data_list :banker_card, :player_card
            handler :inspect_by_card, guard: :data_valid_for_card?
          end
        end

        def ava_card_face_commbinations(target_point_list, pair = false)
          first_face_value_list = (0..9).to_a.select do |first_face_value|
            @card_face_pool[first_face_value]
          end

          target_point_list.shuffle.each do |target_point|
            first_face_value_list.shuffle.each do |first_face_value|
              face_pool = @card_face_pool.dup
              next unless face_pool[first_face_value]

              face_pool[first_face_value] -= 1
              second_face_value = target_point - first_face_value
              second_face_value += 10 if second_face_value < 0
              next if pair && first_face_value != second_face_value
              next if face_pool[second_face_value].nil? || face_pool[second_face_value] < 1

              yield [first_face_value, second_face_value]
            end
          end
        end

        def find_baccarat_card(banker_point, banker_pair, banker_nature = false)
          target_point_list = (0..6).to_a.shuffle
          if banker_point.between?(8, 9) && banker_nature
            target_point_list = [banker_point]
          elsif banker_point == 7
            target_point_list.push(banker_point)
          end
          ava_card_face_commbinations(target_point_list, banker_pair) do |first_face_value, second_face_value|
            face_pool = @card_face_pool.dup
            face_pool[first_face_value]  -= 1
            face_pool[second_face_value] -= 1

            target_point = (first_face_value + second_face_value) % 10
            next if banker_nature && target_point != banker_point

            if target_point.between?(3, 6)
              if banker_point == target_point
                ava_face_list = NO_DRAW_LIST[target_point].shuffle
                until ava_face_list.empty? || @player_card_faces
                  @player_card_faces = find_player_first_two_cards(face_pool, ava_face_list.pop)
                end
              else
                ava_face_list = CHECK_LIST[target_point].shuffle
                until ava_face_list.empty? || @player_card_faces
                  @player_card_faces = find_player_first_two_cards(face_pool, ava_face_list.pop)
                end
                next if @player_card_faces.empty?

                third_face_value = banker_point - first_face_value - second_face_value
                third_face_value += 10 if third_face_value < 0
                next if face_pool[third_face_value].nil? || face_pool[third_face_value] < 1

                face_pool[third_face_value] -= 1
              end
            elsif target_point.between?(0, 2)
              third_face_value = banker_point - target_point
              third_face_value += 10 if third_face_value < 0
              next if face_pool[third_face_value].nil? || face_pool[third_face_value] < 1

              face_pool[third_face_value] -= 1
            end

            @card_face_pool = face_pool
            @card_face_pool.delete_if { |_, num| num == 0 }
            @banker_card_faces.push(first_face_value, second_face_value)
            break
          end
        end

        def find_player_first_two_cards(face_pool, third_face_value = nil)
          player_point = @inspect_data.player_point
          target_point = player_point - (third_face_value || 0)
          target_point += 10 if target_point < 0
          return [] unless third_face_value.nil? || target_point.between?(0..5)

          (0..9).to_a.shuffle.each do |first_face_value|
            face_pool = @card_face_pool.dup
            next if face_pool[first_face_value].nil? || face_pool[first_face_value] < 1

            face_pool[first_face_value] -= 1

            second_face_value = target_point - first_face_value
            second_face_value += 10 if second_face_value < 0
            next if face_pool[second_face_value].nil? || face_pool[second_face_value] < 1

            face_pool[second_face_value] -= 1

            face_pool = pool
            return [first_face_value, second_face_value]
          end
        end

        def update_card_face_pool
          @card_face_pool = @shoe.card_pool.each_with_object({}) do |(card, num), hash|
            card_point = Util::PokerHandUtil.card_baccarat_point(card.to_s[0])
            hash[card_point] ||= 0
            hash[card_point] += num
          end
        end

        def generate_cards_by_point
          update_card_face_pool
          @banker_card_faces = []
          @player_card_faces = []
          banker_point  = @inspect_data.banker_point
          banker_pair   = @inspect_data.banker_pair
          banker_nature = @inspect_data.banker_nature
          player_point  = @inspect_data.player_point
          player_pair   = @inspect_data.player_pair
          player_nature = @inspect_data.player_nature
          find_baccarat_card(banker_point, banker_pair, banker_nature) if banker_point
          if player_point && @player_card_faces.empty?
            target_point_list = (0..5).to_a.shuffle
            if player_point.between?(8, 9) && player_nature
              target_point_list = [player_point]
            elsif player_point.between?(6, 7)
              target_point_list.push(player_point)
            end
            ava_card_face_commbinations(target_point_list, player_pair) do |first_face_value, second_face_value|
              face_pool = @card_face_pool.dup
              face_pool[first_face_value] -= 1
              face_pool[second_face_value] -= 1

              target_point = (first_face_value + second_face_value) % 10
              if target_point.between?(0, 5)
                third_face_value = player_point - target_point
                third_face_value += 10 if third_face_value < 0
                next if face_pool[third_face_value].nil? || face_pool[third_face_value] < 1
              end

              @player_card_faces.push(first_face_value, second_face_value)
              break
            end
          end
        end

        def data_valid_for_point?(entity)
          player_name = entity.find(Component::PlayerLabel).name
          @inspect_data.send(:"#{player_name}_point") < 10
        end

        def inspect_by_point(entity)
          player_name = entity.find(Component::PlayerLabel).name
          generate_cards_by_point unless
            instance_variable_defined?(:"@#{player_name}_card_faces")

          hand_card = entity.find(Component::HandCard)
          instance_variable_get(:"@#{player_name}_card_faces").each do |face|
            list = if face.zero?
              ['T', 'J', 'Q', 'K']
            else
              [face == 1 ? 'A' : face]
            end.product(Util::ShoeCardsUtil::SUITS)
            list.shuffle!
            card = list.map(&:join).find do |cand_card|
              @shoe.card_pool[cand_card.to_sym]
            end
            @shoe.card_pool[card.to_sym] -= 1
            @shoe.card_pool.delete_if { |_, num| num == 0 }
            hand_card.collect(card)
          end
        ensure
          remove_instance_variable(:"@#{player_name}_card_faces")
        end

        def valid_cards?(card_counts)
          card_counts&.all? { |card, counts| @shoe.card_pool.fetch(card.to_sym, 0) >= counts }
        end

        def data_valid_for_card?(entity)
          player_name = entity.find(Component::PlayerLabel).name
          hand_card   = entity.find(Component::HandCard)
          card_counts = @inspect_data
            .send(:"#{player_name}_card")
            .split(',')
            .shift(hand_card.min_hold)
            .group_by(&:itself)
            .map { |card, list| [card, list.size] }
            .to_h
          valid_cards?(card_counts)
        end

        def inspect_by_card(entity)
          player_name = entity.find(Component::PlayerLabel).name
          hand_card   = entity.find(Component::HandCard)
          cards       = @inspect_data
            .send(:"#{player_name}_card")
            .split(',')
            .slice(0...hand_card.min_hold)
          cards.each do |card|
            @shoe.card_pool[card.to_sym] -= 1
            @shoe.card_pool.delete_if { |_, num| num == 0 }
            hand_card.collect(card)
          end
        end
      end
    end
  end
end
