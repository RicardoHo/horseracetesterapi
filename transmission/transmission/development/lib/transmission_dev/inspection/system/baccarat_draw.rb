# frozen_string_literal: true

module Transmission
  module Inspection
    module System
      #
      # @author Alpha Huang
      # @since 1.0.0
      #
      # For insepction
      # Baccarat draw means to dealt third card to
      # player and banker if needed by drawing rule of baccarat
      #
      module BaccaratDraw
        include Inspecter

        inspect_method :dealt_draw_card do
          data :player_card, Message::Filter::Types::String
          data :banker_card, Message::Filter::Types::String
          data :player_point, Message::Filter::Types::CoeInt
          data :banker_point, Message::Filter::Types::CoeInt

          approach :by_point do
            data_list :player_point, :banker_point
            handler :inspect_by_point, guard: :data_valid_for_point?
          end
          approach :by_card do
            data_list :player_card, :banker_card
            handler :inspect_by_card, guard: :data_valid_for_card?
          end
        end

        def valid_cards?(card_counts)
          card_counts&.all? { |card, counts| @shoe.card_pool.fetch(card.to_sym, 0) >= counts }
        end

        def data_valid_for_card?(entity)
          player_name = entity.find(Component::PlayerLabel).name
          hand_card   = entity.find(Component::HandCard)
          card_counts = @inspect_data
            .send(:"#{player_name}_card")
            .split(',')
            .pop(hand_card.max_hold - hand_card.min_hold)
            .group_by(&:itself)
            .map { |card, list| [card, list.size] }
            .to_h
          valid_cards?(card_counts)
        end

        def data_valid_for_point?(entity)
          player_name = entity.find(Component::PlayerLabel).name
          @inspect_data.send(:"#{player_name}_point") < 10
        end

        def inspect_by_card(entity)
          player_name = entity.find(Component::PlayerLabel).name
          hand_card   = entity.find(Component::HandCard)
          card        = @inspect_data
            .send(:"#{player_name}_card")
            .split(',')
            .dig(hand_card.min_hold)
          return unless card && @shoe.card_pool.fetch(card.to_sym, 0) > 0

          @shoe.card_pool[card.to_sym] -= 1
          @shoe.card_pool.delete_if { |_, num| num == 0 }
          hand_card.collect(card)
        end

        def inspect_by_point(entity)
          name          = entity.find(Component::PlayerLabel).name
          hand_card     = entity.find(Component::HandCard)
          total_point   = @inspect_data.send(:"#{name}_point")
          current_point = hand_card.cards.sum do |card_on_hand|
            Util::PokerHandUtil.card_baccarat_point(card_on_hand)
          end % 10
          face = total_point - current_point
          face += 10 if face < 0
          list = if face.zero?
            %w(T J Q K)
          else
            [face == 1 ? 'A' : face]
          end.product(Util::ShoeCardsUtil::SUITS)
          list.shuffle!
          card = list.map(&:join).find do |cand_card|
            @shoe.card_pool[cand_card.to_sym]
          end

          @shoe.card_pool[card.to_sym] -= 1
          @shoe.card_pool.delete_if { |_, num| num == 0 }
          hand_card.collect(card)
        end
      end
    end
  end
end
