# frozen_string_literal: true

module Transmission
  module Inspection
    module System
      #
      # @author Richard Fong
      # @since 0.8.0
      #
      # For insepction
      # Dealt public cards, public cards are visable for all player
      #
      module DropBall
        include Inspecter

        inspect_method :process do
          data :pocket_index, Message::Filter::Types::CoeInt

          approach :by_pocket_index do
            data_list :pocket_index
            handler :inspect_by_pocket_index, guard: :data_valid_for_pocket_index?
          end
        end

        def inspect_by_pocket_index(entity)
          path_info   = entity.find(Component::PathInfo)
          pocket_info = entity.find(Component::PocketInfo)
          lines_info  = entity.find(Component::LinesInfo)

          pocket_index = @input
            .inspect_data
            &.send(:pocket_index)
            &.to_i
          if pocket_index
            pocket_info.pocket_index = pocket_index
            path_left  = Array.new(lines_info.line - pocket_info.pocket_index, 0)
            path_right = Array.new(pocket_info.pocket_index, 1)
            path_info.path_left_right = (path_left + path_right).shuffle
          end
        end

        def data_valid_for_pocket_index?(entity)
          lines_info    = entity.find(Component::LinesInfo)
          pocket_index  = @input.inspect_data&.send(:pocket_index).to_i
          (0..lines_info.line).cover?(pocket_index)
        end
      end
    end
  end
end
