# frozen_string_literal: true

require 'zip'
require 'json'
require 'fileutils'
require_relative '../../lib/transmission/version'

PROJECT_PATH = File.expand_path('../..', __dir__)

class FilePacker
  def self.pack(file_list_path, zip_name)
    FileUtils.rm(zip_name, force: true)
    file_list = JSON.parse(File.read(file_list_path))

    Zip::File.open(zip_name, Zip::File::CREATE) do |zipfile|
      file_list.each do |relative_parent_path, file_names|
        if file_names.is_a?(String)
          files = Dir[File.join(PROJECT_PATH, relative_parent_path, '**', file_names)]
          files.each do |file|
            zipfile.add(file.gsub(PROJECT_PATH + '/', ''), file)
          end
        else
          base_path = if relative_parent_path == '@root'
            PROJECT_PATH
          else
            File.join(PROJECT_PATH, relative_parent_path)
          end
          file_names.each do |file_name|
            file = File.join(base_path, file_name)
            zipfile.add(file.gsub(PROJECT_PATH + '/', ''), file)
          end
        end
      end
    end
  end
end

game_name = ARGV[0]
critical_file_list_path = File.join(__dir__, 'game_critical_file_list')
input_list_paths = if game_name.nil?
  Dir[File.join(critical_file_list_path, '*.json')]
else
  File.join(__dir__, 'game_critical_file_list', game_name + '.json')
end

input_list_paths.each do |file_list_path|
  game_name = File.basename(file_list_path, '.json')
  output_zip_name = "service\##{game_name}\##{Transmission::VERSION}.zip"
  FilePacker.pack(file_list_path, output_zip_name)
end

FilePacker.pack(
  File.join(__dir__, 'source_code.json'),
  "transmission-#{Transmission::VERSION}.zip"
)
