[TOC]

# Changelog
All notable changes to this project will be documented in this file.

## Unreleased
### Added
  - New game H-CK / 2021-01-27
    - add Game, System, Math Config
    - add new system
    - add draw event
    - add draw free game event
    - add draw finish event
    - Math info and strategy
    - Add protobuf response messages
  - New game H-NP / 2020-10-16
    - add system
      -	load_path
      - load_boxes
      - load_ball_score_card
      - ball_select_bet_item
      - drop_ball
      - calculate_ball_payout
      - record_ball_score
      - reset_ball_table
    - add Drop event
    - Math info and strategy
    - Add protobuf response messages
###


## 0.9.0 / 2021-03-01
### Added
  - New game H-SG
    - add Game, System, Math Config
    - add new system
    - add multiple bet event
    - Math info and strategy

## 0.7.3 / 2020-10-28
### Changed
  - refactor config.ru

## 0.7.2 / 2020-10-14
### Changed
  - correct the wrong game critical file list
    - H-888
    - H-WJ
  - remove unused rtp 94.86% and 88.71% from H-888 and H-888-MINI
  - refactor config.ru

## 0.7.1 / 2020-09-22
### Fixed
  - fix the notify_jackpot_confirmed issue of H-FC
### Changed
  - Update math name of H-FC

## 0.7.0 / 2020-08-06
### Added
  - New game H-FC
    - add Roll event
    - resume after Roll
    - Math info and strategy
    - Add protobuf response messages

## 0.6.1 / 2020-07-01
### Added
  - Support Progressive payout
  - Support Jackpots
  - Support test (inspection) mode
### Changed
  - Change H-888 3_of_a_kind to three_of_a_kind
  - Seperate H-888 & H-888-MINI game definition config
  - Update read code name from game_config.yaml
  - Fix error on H-EB's inspection
  - fix H-888's message filter

## 0.6.0 / 2020-04-07
### Added
  - Support Lottery
### Changed
  - Break H-888, H-WJ and H-LLF to normal and HN version

## 0.5.2 / 2020-02-28
### Fixed
  - H-WJ resume no equivalent card issue
  - Include v0.3.2 fixed

## 0.5.1 / 2020-02-27
### Fixed
  - shuffle_shoe_spec test case setup issue
### Changed
  - PublicCard component provide equivalent_cards attribute

## 0.5.0 / 2020-02-27
### Added
  - New game H-WJ
    - substitue joker card system
    - Math info and strategy
### Changed
  - CardDeck component support Joker card
  - ShoeCardsUtil provide card pool with Joker cards
  - ShoeCardsUtil provide Joker color-suits map

## 0.4.0 / 2020-02-27
### Added
  - New game H-LLF
    - deal card per suit system
    - calculate best hand system
    - Math info and strategy
  - poker hand helper
  - ShoeCardsUtil provide card_face_pool API
  - extend ruby Array class

## 0.3.5 / 2020-02-28
### Fixed
  - win type not clean up in some case issue

## 0.3.4 / 2020-02-26
### Added
  - New math model 89.30% for H-888

## 0.3.3 / 2019-12-27
### Changed
  - check table limit by credit

## 0.3.2 / 2019-12-26
### Fixed
  - fix H-EB round id not update issue
### Changed
  - check bet limit by credit
  - max and min bet component provide credit attribute

## 0.3.1 / 2019-12-16
### Changed
  - Enquiry bet amount base on min table credit and min denom

## 0.3.0 / 2019-12-16
### Added
  - game title H-EB
    - Game, system and math model config
    - Shuffle shoe and burn card
    - Skip Hand event
    - Baccarat Deal event
    - Math info and strategy
  - Instance data and action data hndling by game title
  - RNGUtil provide weight table warpper method
### Changed
  - Revamp H-CH and H-888 shoe component and renamed as card deck
  - Remove card deck's shuffle system, replaced shuffle behavior by cleanup system (reset table)
  - Make request and response by game
  - optimize String#camel_case

## 0.2.12
### Changed
  - Fixed resume open round raise bet issue

## 0.2.10
### Added
  - game title H-EB
    - load aa bet result

## 0.2.9
### Changed
  - close game session if close socket normally

## 0.2.8
### Changed
  - Pinion API gem update
  - change system_complete_code to system_completed_code

## 0.2.8
### Changed
  - Only close game session when leave game properly

## 0.2.7
### Added
  - response contain currency in Balance
### Changed
  - Fix makeup game engine api call not trigger issue
  - H-888 math model model name changed to 94.86%
  - H-CH math model model name changed to 97.84%

## 0.2.6
### Changed
  - Fix mini 888 bet and take win issue

## 0.2.5
### Changed
  - Fine tune api service closing

## 0.2.4
### Added
  - Jenkinsfile

## 0.2.3
### Changed
  - Fix log_time call issue in system_complete_driver
  - Fix code style issue in set_round system

## 0.2.2
### Changed
  - Guaranteed raise bet
  - Log format
  - make SystemCompleteStrategy not required
### Added
  - Handle game engine error
  - system_completed field in all event

## 0.2.1
### Changed
  - game engine API client 0.0.10 -> 0.1.2
  - retreive system_complete_code and system_completed from game engine

## 0.2.0
### Added
  - game H-888:
    - bet event
    - math validation
  - functional test env
### Changed
  - game engine API client 0.0.8 -> 0.0.10
  - load property game config from game engine

## 0.1.0
### Added
  - api server
    - web socket handle
    - system complete handle
    - shutdown pre-hook
  - game driver
  - system complete driver
  - game H-CH:
    - deal event
    - raise event
    - fold event
  - math validator
