# frozen_string_literal: true

describe Transmission::System::SetMultiplier do
  subject do
    instance = described_class.new(nil, nil)
    instance.instance_variable_set(:@command, 'draw')

    instance
  end

  let(:multipliers) { [100, 200, 500, 1000, 50000] }
  let(:multiplier_index) { 0 }
  let(:component) do
    component = Transmission::Component::MultiplierInfo.new
    component.multipliers = multipliers
    component.multiplier_index = multiplier_index

    component
  end
  let(:entity) { Clutch::Entity.new(component) }

  it 'should be kind of CommandSystem' do
    expect(subject).to be_kind_of(Clutch::System::CommandSystem)
  end

  it 'should config watch component(s)' do
    expect_classes = [Transmission::Component::MultiplierInfo]
    expect(described_class::WATCHS).to eq(expect_classes)
  end

  context 'command' do
    it 'should config method to deal command' do
      expect(described_class.tasks['draw']).to eq(:save_multiplier)
    end

    it 'should config method to bet command' do
      expect(described_class.tasks['multiple_bet']).to eq(:save_multiplier)
    end
  end

  context '#executable?' do
    it 'should pass for input multiplier exist' do
      subject.instance_variable_set(:@entities, [entity])
      subject.instance_variable_set(:@command, 'draw')
      subject.instance_variable_set(:@input, OpenStruct.new(multiplier: 100))
      expect(subject.send(:executable?)).to be_truthy
    end

    it 'should not pass for input multiplier not exist' do
      subject.instance_variable_set(:@entities, [entity])
      subject.instance_variable_set(:@input, OpenStruct.new(multiplier: nil))
      expect(subject.send(:executable?)).to be_falsey
    end
  end

  context '#save_multiplier' do
    it 'should save player selected multiplier' do
      expect_multiplier = 200
      subject.instance_variable_set(:@input, OpenStruct.new(multiplier: expect_multiplier))

      subject.send(:save_multiplier, entity)
      expect(component.multiplier).to eq(expect_multiplier)
      expect(component.multiplier_index).to eq(1)
    end

    it 'should raise error for not in range error' do
      expect_multiplier = 10
      subject.instance_variable_set(:@input, OpenStruct.new(multiplier: expect_multiplier))

      expect { subject.send(:save_multiplier, entity) }
        .to raise_error(Transmission::MultiplierNotInRangeError)
    end
  end
end
