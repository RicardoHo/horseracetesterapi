# frozen_string_literal: true

describe Transmission::System::ComposeSanGongPayoutList do
  subject { described_class.new(nil, nil) }

  let(:dragon_player_label) { Transmission::Component::PlayerLabel.new('dragon').tap(&:changes_applied) }
  let(:phoenix_player_label) { Transmission::Component::PlayerLabel.new('phoenix').tap(&:changes_applied) }
  let(:dragon_hand_card) { Transmission::Component::HandCard.new(3).tap(&:changes_applied) }
  let(:phoenix_hand_card) { Transmission::Component::HandCard.new(3).tap(&:changes_applied) }
  let(:dragon_hand_point) { Transmission::Component::HandPoint.new.tap(&:changes_applied) }
  let(:phoenix_hand_point) { Transmission::Component::HandPoint.new.tap(&:changes_applied) }
  let(:dragon_entity) { Clutch::Entity.new(dragon_player_label, dragon_hand_card, dragon_hand_point) }
  let(:phoenix_entity) { Clutch::Entity.new(phoenix_player_label, phoenix_hand_card, phoenix_hand_point) }

  let(:type_list) { ["three_of_a_kind", "three_picture_cards", "hand_value"] }
  let(:score_card) { Transmission::Component::SanGongScoreCard.new.tap(&:changes_applied) }
  let(:score_card_tag) { Transmission::Component::ScoreCardTag.new('san_gong_score_card').tap(&:changes_applied) }

  subject do
    instance = described_class.new(nil, nil)
    instance.instance_variable_set(:@score_card, score_card)
    instance.instance_variable_set(:@score_card_tag, score_card_tag)

    instance
  end

  it 'should be kind of ReactiveSystem' do
    expect(subject).to be_kind_of(Clutch::System::ReactiveSystem)
  end

  it 'should config watch component(s)' do
    expect_classes = [
      Transmission::Component::PlayerLabel,
      Transmission::Component::HandCard,
      Transmission::Component::HandPoint,
    ]

    expect(described_class::WATCHS).to eq(expect_classes)
  end

  it 'should config reference component(s)' do
    expect_classes = [
      Transmission::Component::SanGongScoreCard,
      Transmission::Component::ScoreCardTag,
    ]

    expect(described_class::REFERENCE).to eq(expect_classes)
  end

  context '#before_all' do
    before 'each' do
      subject.instance_variable_set(:@entities, [dragon_entity, phoenix_entity])
      subject.entities_handle = [dragon_entity, phoenix_entity]
    end

    it 'should set instance variable @score_card' do
      subject.send(:before_all)
      expect(subject.instance_variable_get(:@score_card)).to eq(score_card)
    end

    it 'should set instance variable @score_card_tag' do
      subject.send(:before_all)
      expect(subject.instance_variable_get(:@score_card_tag)).to eq(score_card_tag)
    end
  end

  context '#process' do
    context 'winner hand point is total_9 case' do
      before 'all' do
        score_card.winner = 'dragon'

        dragon_hand_card.cards = ['4s', '5s', 'Ts']
        dragon_hand_point.value = 9
        phoenix_hand_card.cards = ['4s', '5s', 'As']
        phoenix_hand_point.value = 0
      end

      it 'should have "total_9" in payout_list' do
        subject.send(:process, dragon_entity)
        subject.send(:process, phoenix_entity)

        expect(subject.instance_variable_get(:@score_card).payout_list.include?('total_9')).to be(true)
      end
    end

    context 'winner hand point is total_7_8 case' do
      before 'all' do
        score_card.winner = 'dragon'

        dragon_hand_card.cards = ['3s', '4c', 'Ts']
        dragon_hand_point.value = 7
        phoenix_hand_card.cards = ['4s', '5s', 'As']
        phoenix_hand_point.value = 0
      end

      it 'should have "total_7_8" in payout_list' do
        subject.send(:process, dragon_entity)
        subject.send(:process, phoenix_entity)

        expect(subject.instance_variable_get(:@score_card).payout_list.include?('total_7_8')).to be(true)
      end
    end

    context 'winner hand point is total_1_6 case' do
      before 'all' do
        score_card.winner = 'dragon'

        dragon_hand_card.cards = ['3s', '2s', 'Ts']
        dragon_hand_point.value = 5
        phoenix_hand_card.cards = ['4s', '5s', 'As']
        phoenix_hand_point.value = 0
      end

      it 'should have "total_1_6" in payout_list' do
        subject.send(:process, dragon_entity)
        subject.send(:process, phoenix_entity)

        expect(subject.instance_variable_get(:@score_card).payout_list.include?('total_1_6')).to be(true)
      end
    end

    context 'winner hand point is total_zero case' do
      before 'all' do
        score_card.winner = 'dragon'

        dragon_hand_card.cards = ['Js', 'Qs', 'Ts']
        dragon_hand_point.value = 0
        phoenix_hand_card.cards = ['4s', '5s', 'As']
        phoenix_hand_point.value = 0
      end

      it 'should have "total_1_6" in payout_list' do
        subject.send(:process, dragon_entity)
        subject.send(:process, phoenix_entity)

        expect(subject.instance_variable_get(:@score_card).payout_list.include?('total_zero')).to be(true)
      end
    end

    context 'player hand point is even case' do
      before 'all' do
        score_card.winner = 'dragon'

        dragon_hand_card.cards = ['Js', 'Qs', 'Ts']
        dragon_hand_point.value = 0
        phoenix_hand_card.cards = ['4s', '5s', 'As']
        phoenix_hand_point.value = 0

        subject.send(:process, dragon_entity)
        subject.send(:process, phoenix_entity)
      end

      it 'should have "dragon_even" in payout_list' do
        expect(subject.instance_variable_get(:@score_card).payout_list.include?('dragon_even')).to be(true)
      end

      it 'should have "phoenix_even" in payout_list' do
        expect(subject.instance_variable_get(:@score_card).payout_list.include?('phoenix_even')).to be(true)
      end
    end

    context 'player hand point is odd case' do
      before 'all' do
        score_card.winner = 'dragon'

        dragon_hand_card.cards = ['Js', 'Qs', 'As']
        dragon_hand_point.value = 1
        phoenix_hand_card.cards = ['4s', '5s', '2s']
        phoenix_hand_point.value = 1

        subject.send(:process, dragon_entity)
        subject.send(:process, phoenix_entity)
      end

      it 'should have "dragon_odd" in payout_list' do
        expect(subject.instance_variable_get(:@score_card).payout_list.include?('dragon_odd')).to be(true)
      end

      it 'should have "phoenix_odd" in payout_list' do
        expect(subject.instance_variable_get(:@score_card).payout_list.include?('phoenix_odd')).to be(true)
      end
    end

    context 'player hand card have pair case' do
      before 'all' do
        score_card.winner = 'dragon'

        dragon_hand_card.cards = ['4c', '4s', 'As']
        dragon_hand_point.value = 9
        phoenix_hand_card.cards = ['5c', '5s', '2s']
        phoenix_hand_point.value = 2

        subject.send(:process, dragon_entity)
        subject.send(:process, phoenix_entity)
      end

      it 'should have "dragon_pair" in payout_list' do
        expect(subject.instance_variable_get(:@score_card).payout_list.include?('dragon_pair')).to be(true)
      end

      it 'should have "phoenix_pair" in payout_list' do
        expect(subject.instance_variable_get(:@score_card).payout_list.include?('phoenix_pair')).to be(true)
      end
    end
  end
end
