# frozen_string_literal: true

describe Transmission::System::SetupMinMaxBet do
  let(:game_engine) do
    wrapper = Transmission::GameEngineWrapper.new(978597675)
    wrapper.create(nil, nil)

    wrapper
  end
  let(:denoms) { game_engine.game_config.denoms.value }
  let(:denom_info) do
    denom_info = Transmission::Component::DenomInfo.new
    denom_info.denoms = denoms

    denom_info
  end

  subject do
    instance = described_class.new(nil, nil)
    instance.instance_variable_set(:@game_engine, game_engine)
    instance.instance_variable_set(:@denom_info, denom_info)

    instance
  end

  it 'should be kind of CommandSystem' do
    expect(subject).to be_kind_of(Clutch::System::CommandSystem)
  end

  it 'should config watch component(s)' do
    expect_classes = [
      Transmission::Component::BetAreaLabel,
      Transmission::Component::MaxBet,
      Transmission::Component::MinBet,
    ]
    expect(described_class::WATCHS).to eq(expect_classes)
  end

  context 'command' do
    it 'should config method to query_game command' do
      expect(described_class.tasks['query_game']).to eq(:setup)
    end
  end

  context '#setup' do
    it 'should setup the max/min bet credit amount' do
      bet_area_label = Transmission::Component::BetAreaLabel.new("default")
      max_bet_comp   = Transmission::Component::MaxBet.new(1000)
      min_bet_comp   = Transmission::Component::MinBet.new(10)

      entity = Clutch::Entity.new(bet_area_label, max_bet_comp, min_bet_comp)
      subject.send(:setup, entity)
      expect(max_bet_comp.cash).to eq(denoms.max * max_bet_comp.credit)
      expect(min_bet_comp.cash).to eq(denoms.min * min_bet_comp.credit)
    end
  end
end
