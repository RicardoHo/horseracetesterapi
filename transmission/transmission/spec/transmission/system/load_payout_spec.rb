# frozen_string_literal: true

# describe Transmission::System::LoadPayout do
#   subject { described_class.new(nil, nil) }
#   let(:game_engine) do
#     wrapper = Transmission::GameEngineWrapper.new(950404013)
#     wrapper.create(nil, nil)

#     wrapper
#   end
#   let(:payout_cash) { 500 }
#   let(:payout_credit) { 500 }
#   let(:current_payout_cash) { 100 }
#   let(:current_payout_credit) { 100 }
#   let(:base_game_payout_cash) { 200 }
#   let(:base_game_payout_credit) { 200 }
#   let(:free_game_payout_cash) { 300 }
#   let(:free_game_payout_credit) { 300 }
#   let(:payout) { Transmission::Component::Payout.new }
#   let(:current_payout) { Transmission::Component::CurrentPayout.new }
#   let(:base_game_payout) { Transmission::Component::BaseGamePayout.new }
#   let(:free_game_payout) { Transmission::Component::FreeGamePayout.new }
#   let(:bet_area_label) { Transmission::Component::BetAreaLabel.new('test') }
#   let(:entity) { Clutch::Entity.new(payout, bet_area_label) }

#   let(:payout_save_data) do
#     {
#       cash: payout_cash,
#       credit: payout_credit,
#     }
#   end
#   let(:current_payout_save_data) do
#     {
#       cash: current_payout_cash,
#       credit: current_payout_credit,
#     }
#   end
#   let(:base_game_payout_save_data) do
#     {
#       cash: base_game_payout_cash,
#       credit: base_game_payout_credit,
#     }
#   end
#   let(:free_game_payout_save_data) do
#     {
#       cash: free_game_payout_cash,
#       credit: free_game_payout_credit,
#     }
#   end
#   let(:save) do
#     {
#       bet_options: {
#         name: bet_area_label.name,
#         payout: payout_save_data,
#         current_payout: current_payout_save_data,
#         base_game_payout: base_game_payout_save_data,
#         free_game_payout: free_game_payout_save_data,
#       },
#     }
#   end

#   subject do
#     instance = described_class.new(nil, nil)
#     instance.instance_variable_set(:@current_payout, current_payout)
#     instance.instance_variable_set(:@base_game_payout, base_game_payout)
#     instance.instance_variable_set(:@free_game_payout, free_game_payout)

#     instance
#   end

#   it 'should be kind of CommandSystem' do
#     expect(subject).to be_kind_of(Clutch::System::CommandSystem)
#   end

#   it 'should config watch component(s)' do
#     expect_classes = [
#       Transmission::Component::Payout,
#       Transmission::Component::BetAreaLabel,
#     ]
#     expect(described_class::WATCHS).to eq(expect_classes)
#   end

#   it 'should config reference component(s)' do
#     expect_classes = [
#       Transmission::Component::CurrentPayout,
#       Transmission::Component::BaseGamePayout,
#       Transmission::Component::FreeGamePayout,
#     ]
#     expect(described_class::REFERENCE).to eq(expect_classes)
#   end

#   context 'command' do
#     it 'should config method to query_game command' do
#       expect(described_class.tasks['query_game']).to eq(:on_load)
#     end
#   end

#   context '#executable?' do
#     before 'all' do
#       subject.instance_variable_set(:@entities, [entity])
#       subject.instance_variable_set(:@command, 'query_game')
#     end

#     it 'should pass for resume round' do
#       allow(game_engine)
#         .to receive(:resume_round?)
#         .and_return(true)
#       subject.instance_variable_set(:@game_engine, game_engine)
#       expect(subject.send(:executable?)).to be(true)
#     end

#     it 'should pass for load last round' do
#       allow(game_engine)
#         .to receive(:new_round?)
#         .and_return(true)
#       subject.instance_variable_set(:@game_engine, game_engine)
#       expect(subject.send(:executable?)).to be(true)
#     end

#     it 'should not pass for new game' do
#       allow(game_engine)
#         .to receive(:new_round?)
#         .and_return(false)
#       allow(game_engine)
#         .to receive(:resume_round?)
#         .and_return(false)
#       subject.instance_variable_set(:@game_engine, game_engine)
#       expect(subject.send(:executable?)).to be(false)
#     end
#   end

#   context '#before_all' do
#     before 'all' do
#       subject.instance_variable_set(:@entities, [entity])
#       allow(game_engine)
#         .to receive(:new_round?)
#         .and_return(true)
#       allow(subject)
#         .to receive(:last_round_save)
#         .and_return(save)
#       subject.instance_variable_set(:@game_engine, game_engine)
#       subject.send(:before_all)
#     end

#     it 'should load save from game engine' do
#       expect(subject.instance_variable_get(:@save)).to eq(save)
#     end

#     it 'should load and cache stage data from game engine' do
#       expect(subject.instance_variable_get(:@bet_options_data)).to eq(save[:bet_options])
#     end

#     it 'should set instance variable @current_payout' do
#       expect(subject.instance_variable_get(:@current_payout))
#         .to be_instance_of(Transmission::Component::CurrentPayout)
#     end

#     it 'should set instance variable @base_game_payout' do
#       expect(subject.instance_variable_get(:@base_game_payout))
#         .to be_instance_of(Transmission::Component::BaseGamePayout)
#     end

#     it 'should set instance variable @free_game_payout' do
#       expect(subject.instance_variable_get(:@free_game_payout))
#         .to be_instance_of(Transmission::Component::FreeGamePayout)
#     end
#   end

#   context '#on_load' do
#     before 'all' do
#       expect(entity.find(Transmission::Component::Payout).cash)
#         .to eq(0)
#       expect(entity.find(Transmission::Component::Payout).credit)
#         .to eq(0)
#       expect(subject.instance_variable_get(:@current_payout).cash)
#         .to eq(0)
#       expect(subject.instance_variable_get(:@current_payout).credit)
#         .to eq(0)
#       expect(subject.instance_variable_get(:@base_game_payout).cash)
#         .to eq(0)
#       expect(subject.instance_variable_get(:@base_game_payout).credit)
#         .to eq(0)
#       expect(subject.instance_variable_get(:@free_game_payout).cash)
#         .to eq(0)
#       expect(subject.instance_variable_get(:@free_game_payout).credit)
#         .to eq(0)
#       subject.instance_variable_set(:@bet_options_data, save[:bet_options])
#       subject.send(:on_load, entity)
#     end

#     it 'should load bet amount' do
#       expect(entity.find(Transmission::Component::Payout).cash)
#         .to eq(payout_cash)
#       expect(entity.find(Transmission::Component::Payout).credit)
#         .to eq(payout_credit)
#       expect(subject.instance_variable_get(:@current_payout).cash)
#         .to eq(current_payout_cash)
#       expect(subject.instance_variable_get(:@current_payout).credit)
#         .to eq(current_payout_credit)
#       expect(subject.instance_variable_get(:@base_game_payout).cash)
#         .to eq(base_game_payout_cash)
#       expect(subject.instance_variable_get(:@base_game_payout).credit)
#         .to eq(base_game_payout_credit)
#       expect(subject.instance_variable_get(:@free_game_payout).cash)
#         .to eq(free_game_payout_cash)
#       expect(subject.instance_variable_get(:@free_game_payout).credit)
#         .to eq(free_game_payout_credit)
#     end
#   end

#   context '#find_bet_amount_data' do
#     it 'should find bet_amount_data from array' do
#       subject.instance_variable_set(:@bet_options_data, [save[:bet_options]])
#       expect(subject.send(:find_payout_data, bet_area_label.name))
#         .to eq(save[:bet_options])
#     end

#     it 'should find bet_amount_data from hash' do
#       subject.instance_variable_set(:@bet_options_data, save[:bet_options])
#       expect(subject.send(:find_payout_data, bet_area_label.name))
#         .to eq(save[:bet_options])
#     end
#   end
# end
