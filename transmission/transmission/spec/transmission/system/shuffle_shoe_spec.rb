# frozen_string_literal: true

describe Transmission::System::ShuffleShoe do
  let(:game_engine) do
    wrapper = Transmission::GameEngineWrapper.new(970328033)
    wrapper.create(nil, nil)

    wrapper
  end
  let(:shoe) do
    Transmission::Component::Shoe.new(8, 6..402)
      .tap { |shoe| shoe.init_cut_card_range(150..250) }
  end
  let(:entity) { Clutch::Entity.new(shoe) }

  subject do
    instance = described_class.new(nil, nil)
    instance.instance_variable_set(:@game_engine, game_engine)

    instance
  end

  it 'should be kind of CommandSystem' do
    expect(subject).to be_kind_of(Clutch::System::CommandSystem)
  end

  it 'should config watch component(s)' do
    expect_classes = [
      Transmission::Component::Shoe,
    ]
    expect(described_class::WATCHS).to eq(expect_classes)
  end

  context 'command' do
    it 'assosiate the entry method with query game event' do
      expect(described_class.tasks['query_game']).to eq(:shuffle_shoe)
    end

    it 'assosiate the entry method with reshuffle event' do
      expect(described_class.tasks['reshuffle']).to eq(:shuffle_shoe)
    end
  end

  context '#shuffle_shoe' do
    it 'shuffle the shoe when shoe empty' do
      initial_id = shoe.id
      subject.send(:shuffle_shoe, entity)
      expect(shoe.empty?).to be_falsey
      expect(shoe.id).to eq(initial_id + 1)
      expect(shoe.cut_card_pos).to be_kind_of(Integer)
    end

    it 'shuffle the shoe when reach cut card' do
      subject.send(:shuffle_shoe, entity)
      shoe.update(
        Hash[shoe.card_pool.to_a[0...(shoe.cut_card_pos / 8) - 1]]
      )
      expect(shoe.reach_cut_card?).to be_truthy

      previous_id = shoe.id
      previous_card_pool = shoe.card_pool
      subject.send(:shuffle_shoe, entity)
      expect(shoe.reach_cut_card?).to be_falsey
      expect(shoe.id).to eq(previous_id + 1)
      expect(shoe.card_pool).not_to eq(previous_card_pool)
    end

    it 'do not shuffle the shoe when not empty and not reach cut card' do
      subject.send(:shuffle_shoe, entity)
      previous_id = shoe.id

      subject.send(:shuffle_shoe, entity)
      expect(shoe.id).to eq(previous_id)
    end
  end
end
