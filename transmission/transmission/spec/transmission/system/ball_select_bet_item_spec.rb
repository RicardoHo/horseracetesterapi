# frozen_string_literal: true

describe Transmission::System::BallSelectBetItem do
  subject { described_class.new(nil, nil) }
  let(:lines) { 8..12 }
  let(:line_index) { 0 }
  let(:levels) { ["low", "normal", "high"] }
  let(:risk_level_index) { 1 }
  let(:valid_line) { 8 }
  let(:invalid_line) { 13 }
  let(:valid_risk_level) { 'low' }
  let(:invalid_risk_level) { 'lo' }
  let(:bet_amount) do
    Transmission::Component::BetAmount.new.tap { |bet| bet.credit = 100 }
  end
  let(:bet_area_label) { Transmission::Component::BetAreaLabel.new('test') }
  let(:entity) { Clutch::Entity.new(bet_amount, bet_area_label) }
  let(:score_card) { Transmission::Component::BallScoreCard.new }
  let(:lines_info) { Transmission::Component::LinesInfo.new(lines, line_index) }
  let(:risk_level) { Transmission::Component::RiskLevel.new(levels, risk_level_index) }
  let(:reference) do
    {
      ball_score_card: [score_card],
      lines_info: [lines_info],
      risk_level: [risk_level],
    }
  end

  it 'should be kind of ReactiveSystem' do
    expect(subject).to be_kind_of(Clutch::System::ReactiveSystem)
  end

  it 'should config watch component(s)' do
    expect_classes = [
      Transmission::Component::BetAmount,
      Transmission::Component::BetAreaLabel,
    ]
    expect(described_class::WATCHS).to eq(expect_classes)
  end

  it 'should config reference component(s)' do
    expect_classes = [
      Transmission::Component::BallScoreCard,
      Transmission::Component::LinesInfo,
      Transmission::Component::RiskLevel,
    ]
    expect(described_class::REFERENCE).to eq(expect_classes)
  end

  context '#executable?' do
    before 'each' do
    end

    it 'should pass for drop event' do
      subject.instance_variable_set(:@command, 'drop')
      expect(subject.send(:executable?)).to be(true)
    end
  end

  context '#before_process' do
    before 'all' do
      allow(subject)
        .to receive(:references)
        .and_return(reference)
      subject.send(:before_process)
    end

    it 'should set instance variable @score_card' do
      expect(subject.instance_variable_get(:@score_card)).to be_instance_of(Transmission::Component::BallScoreCard)
    end

    it 'should set instance variable @lines_info' do
      expect(subject.instance_variable_get(:@lines_info)).to be_instance_of(Transmission::Component::LinesInfo)
    end

    it 'should set instance variable @risk_level' do
      expect(subject.instance_variable_get(:@risk_level)).to be_instance_of(Transmission::Component::RiskLevel)
    end
  end

  context '#process' do
    context 'valid selected line value' do
      before 'all' do
        subject.instance_variable_set(
          :@input,
          OpenStruct.new(
            selected_line: valid_line,
            selected_risk_level: valid_risk_level,
            bet_options: [
              OpenStruct.new(
                name: 'test',
              ),
            ]
          )
        )
        subject.instance_variable_set(:@score_card, score_card)
        subject.instance_variable_set(:@lines_info, lines_info)
        subject.instance_variable_set(:@risk_level, risk_level)
      end

      it 'set the selected line to bet option' do
        expect(lines_info.line).to eq(nil)
        subject.send(:process, entity)
        expect(lines_info.line).to eq(valid_line)
        expect(score_card.bet_option[:selected_line]).to eq(valid_line)
      end
    end

    context 'invlid selected_line value' do
      before 'all' do
        subject.instance_variable_set(
          :@input,
          OpenStruct.new(
            selected_line: invalid_line,
            selected_risk_level: valid_risk_level,
            bet_options: [
              OpenStruct.new(
                name: 'test',
              ),
            ]
          )
        )
        subject.instance_variable_set(:@score_card, score_card)
        subject.instance_variable_set(:@lines_info, lines_info)
        subject.instance_variable_set(:@risk_level, risk_level)
      end

      it 'raise error' do
        expect { subject.send(:process, entity) }
          .to raise_error(Transmission::SelectedLineNotInRangeError)
      end
    end

    context 'valid selected risk level value' do
      before 'all' do
        subject.instance_variable_set(
          :@input,
          OpenStruct.new(
            selected_line: valid_line,
            selected_risk_level: valid_risk_level,
            bet_options: [
              OpenStruct.new(
                name: 'test',
              ),
            ]
          )
        )
        subject.instance_variable_set(:@score_card, score_card)
        subject.instance_variable_set(:@lines_info, lines_info)
        subject.instance_variable_set(:@risk_level, risk_level)
      end

      it 'set the selected risk level to bet option' do
        expect(risk_level.risk_level_index).to eq(1)
        subject.send(:process, entity)
        expect(risk_level.risk_level_index).to eq(0)
        expect(score_card.bet_option[:selected_risk_level]).to eq(valid_risk_level)
      end
    end

    context 'invlid selected_line value' do
      before 'all' do
        subject.instance_variable_set(
          :@input,
          OpenStruct.new(
            selected_line: valid_line,
            selected_risk_level: invalid_risk_level,
            bet_options: [
              OpenStruct.new(
                name: 'test',
              ),
            ]
          )
        )
        subject.instance_variable_set(:@score_card, score_card)
        subject.instance_variable_set(:@lines_info, lines_info)
        subject.instance_variable_set(:@risk_level, risk_level)
      end

      it 'raise error' do
        expect { subject.send(:process, entity) }
          .to raise_error(Transmission::SelectedRiskLevelNotInOptionsError)
      end
    end
  end
end
