# frozen_string_literal: true

describe Transmission::System::EvaluateHandValue do
  subject { described_class.new(nil, nil) }

  let(:hand_card) { Transmission::Component::HandCard.new(3).tap(&:changes_applied) }
  let(:hand_point) { Transmission::Component::HandPoint.new.tap(&:changes_applied) }
  let(:entity) { Clutch::Entity.new(hand_card, hand_point) }

  it 'should be kind of ReactiveSystem' do
    expect(subject).to be_kind_of(Clutch::System::ReactiveSystem)
  end

  it 'should config watch component(s)' do
    expect_classes = [
      Transmission::Component::HandCard,
      Transmission::Component::HandPoint,
    ]

    expect(described_class::WATCHS).to eq(expect_classes)
  end

  context '#process' do
    context 'when hand card are Js, Qs, Ks' do
      before 'all' do
        hand_card.cards = ['Js', 'Qs', 'Ks']
      end

      it 'should get hand point 0' do
        subject.send(:process, entity)
        expect(
          entity.find(Transmission::Component::HandPoint).value
        ).to eq(0)
      end
    end

    context 'when hand card are As, 2s, 3s' do
      before 'all' do
        hand_card.cards = ['As', '2s', '3s']
      end

      it 'should get hand point 6' do
        subject.send(:process, entity)
        expect(
          entity.find(Transmission::Component::HandPoint).value
        ).to eq(6)
      end
    end
  end
end
