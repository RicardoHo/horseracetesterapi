# frozen_string_literal: true

# describe Transmission::System::LoadSanGongPlayerHand do
#   subject { described_class.new(nil, nil) }
#   let(:game_engine) do
#     wrapper = Transmission::GameEngineWrapper.new(636816080)
#     wrapper.create(nil, nil)

#     wrapper
#   end
#   let(:cards) { ['As', '2s', '3s'] }
#   let(:value) { 6 }
#   let(:player_label) { Transmission::Component::PlayerLabel.new('player') }
#   let(:hand_card) { Transmission::Component::HandCard.new(3) }
#   let(:hand_point) { Transmission::Component::HandPoint.new }
#   let(:entity) { Clutch::Entity.new(player_label, hand_card, hand_point) }
#   let(:save) do
#     {
#       player: {
#         card: cards,
#         point: value,
#       },
#     }
#   end

#   it 'should be kind of CommandSystem' do
#     expect(subject).to be_kind_of(Clutch::System::CommandSystem)
#   end

#   it 'should config watch component(s)' do
#     expect_classes = [
#       Transmission::Component::PlayerLabel,
#       Transmission::Component::HandCard,
#       Transmission::Component::HandPoint,
#     ]
#     expect(described_class::WATCHS).to eq(expect_classes)
#   end

#   context 'command' do
#     it 'should config method to query_game command' do
#       expect(described_class.tasks['query_game']).to eq(:on_load)
#     end
#   end

#   context '#executable?' do
#     before 'all' do
#       subject.instance_variable_set(:@entities, [entity])
#       subject.instance_variable_set(:@command, 'query_game')
#     end

#     it 'should pass for resume round' do
#       allow(game_engine)
#         .to receive(:resume_round?)
#         .and_return(true)
#       subject.instance_variable_set(:@game_engine, game_engine)
#       expect(subject.send(:executable?)).to be(true)
#     end

#     it 'should pass for load last round' do
#       allow(game_engine)
#         .to receive(:new_round?)
#         .and_return(true)
#       subject.instance_variable_set(:@game_engine, game_engine)
#       expect(subject.send(:executable?)).to be(true)
#     end

#     it 'should not pass for new game' do
#       allow(game_engine)
#         .to receive(:new_round?)
#         .and_return(false)
#       allow(game_engine)
#         .to receive(:resume_round?)
#         .and_return(false)
#       subject.instance_variable_set(:@game_engine, game_engine)
#       expect(subject.send(:executable?)).to be(false)
#     end
#   end

#   context '#before_all' do
#     before 'all' do
#       subject.instance_variable_set(:@entities, [entity])
#       allow(game_engine)
#         .to receive(:new_round?)
#         .and_return(true)
#       allow(subject)
#         .to receive(:last_round_save)
#         .and_return(save)
#       subject.instance_variable_set(:@game_engine, game_engine)
#       subject.send(:before_all)
#     end

#     it 'should load save from game engine' do
#       expect(subject.instance_variable_get(:@save)).to eq(save)
#     end
#   end

#   context '#on_load' do
#     before 'all' do
#       subject.instance_variable_set(:@save, save)
#       expect(entity.find(Transmission::Component::HandCard).cards)
#         .to eq([])
#       expect(entity.find(Transmission::Component::HandPoint).value)
#         .to eq(nil)
#       subject.send(:on_load, entity)
#     end

#     it 'should load player hand card' do
#       expect(entity.find(Transmission::Component::HandCard).cards)
#         .to eq(['As', '2s', '3s'])
#     end

#     it 'should load player hand point' do
#       expect(entity.find(Transmission::Component::HandPoint).value)
#         .to eq(6)
#     end
#   end
# end
