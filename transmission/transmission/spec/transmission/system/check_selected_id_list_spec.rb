# frozen_string_literal: true

# describe Transmission::System::CheckSelectedIdList do
#   subject { described_class.new(nil, nil) }
#   let(:game_engine) do
#     wrapper = Transmission::GameEngineWrapper.new(950404013)
#     wrapper.create(nil, nil)

#     wrapper
#   end
#   let(:game_def) do
#     Transmission::GameDefinition.new(
#       Transmission::Util::GameDefinitionUtil
#       .load(950404013)
#     )
#   end
#   let(:paytable) do
#     game_def
#       .math_model
#       .model(game_engine.game_config.math_name)
#       .dig(:paytable)
#   end
#   let(:selected_id_list_length_range) do
#     game_def
#       .math_model
#       .model(game_engine.game_config.math_name)
#       .dig(:selected_id_list_length_range)
#   end

#   let(:valid_id_list) { [10, 20, 30, 40, 50] }
#   let(:invalid_id_list) { [10, 20] }
#   let(:bet_amount) { Transmission::Component::BetAmount.new }
#   let(:bet_area_label) { Transmission::Component::BetAreaLabel.new('test') }
#   let(:score_card) { Transmission::Component::KenoScoreCard.new }
#   let(:selected_id_list) { Transmission::Component::SelectedIdList.new }
#   let(:entity) { Clutch::Entity.new(bet_amount, bet_area_label) }
#   let(:expect_score_card) do
#     {
#       name: bet_area_label.name,
#       bet_amount: {
#         credit: 100,
#         cash: 100,
#       },
#     }
#   end

#   subject do
#     instance = described_class.new(nil, nil)
#     instance.instance_variable_set(:@score_card, score_card)
#     instance.instance_variable_set(:@selected_id_list, selected_id_list)
#     instance.instance_variable_set(:@paytable, paytable)
#     instance.instance_variable_set(:@selected_id_list_length_range, selected_id_list_length_range)

#     instance
#   end

#   it 'should be kind of CommandSystem' do
#     expect(subject).to be_kind_of(Clutch::System::CommandSystem)
#   end

#   it 'should config watch component(s)' do
#     expect_classes = [
#       Transmission::Component::BetAmount,
#       Transmission::Component::BetAreaLabel,
#     ]
#     expect(described_class::WATCHS).to match_array(expect_classes)
#   end

#   it 'should config reference component(s)' do
#     expect_classes = [
#       Transmission::Component::KenoScoreCard,
#       Transmission::Component::SelectedIdList,
#     ]
#     expect(described_class::REFERENCE).to match_array(expect_classes)
#   end

#   context 'command' do
#     it 'should config method to deal command' do
#       expect(described_class.tasks['draw']).to eq(:selected_id_list)
#     end

#     it 'should config method to draw free game command' do
#       expect(described_class.tasks['draw_free_game']).to eq(:selected_id_list)
#     end
#   end

#   context '#before_all' do
#     before 'each' do
#       subject.instance_variable_set(:@entities, [entity])
#       subject.entities_handle = [entity]

#       subject.send(:before_all)
#     end

#     it 'should set instance variable @score_card' do
#       expect(subject.instance_variable_get(:@score_card)).to eq(score_card)
#     end

#     it 'should set instance variable @selected_id_list' do
#       expect(subject.instance_variable_get(:@selected_id_list)).to eq(selected_id_list)
#     end

#     it 'should set instance variable @paytable' do
#       expect(subject.instance_variable_get(:@paytable)).to eq(paytable)
#     end

#     it 'should set instance variable @odds_divsior' do
#       expect(subject.instance_variable_get(:@selected_id_list_length_range)).to eq(selected_id_list_length_range)
#     end
#   end

#   context '#selected_id_list' do
#     it 'should return when dont have bet_credit' do
#       bet_amount.credit = nil

#       subject.send(:selected_id_list, entity)
#       expect(subject.instance_variable_get(:@score_card).bet_option).to eq(nil)
#     end

#     it 'should assign selected_id_list by input when input have it' do
#       bet_amount.credit = 100
#       subject.instance_variable_set(:@input, OpenStruct.new(selected_id_list: valid_id_list))

#       subject.send(:selected_id_list, entity)
#       expect(subject.instance_variable_get(:@selected_id_list).list).to eq(valid_id_list)
#     end

#     it 'should hold current selected_id_list when input dont have' do
#       bet_amount.credit = 100
#       selected_id_list.list = valid_id_list

#       subject.send(:selected_id_list, entity)
#       expect(subject.instance_variable_get(:@selected_id_list).list).to eq(valid_id_list)
#     end

#     it 'should raise error when selected_id_list length not in range' do
#       bet_amount.credit = 100
#       subject.instance_variable_set(:@input, OpenStruct.new(selected_id_list: invalid_id_list))

#       expect { subject.send(:selected_id_list, entity) }
#         .to raise_error(Transmission::SelectNumbersSizeNotInRangeError)
#     end

#     it 'should fill in the score card' do
#       bet_amount.credit = 100
#       bet_amount.cash = 100
#       subject.instance_variable_set(:@input, OpenStruct.new(selected_id_list: valid_id_list))

#       subject.send(:selected_id_list, entity)
#       expect(subject.instance_variable_get(:@score_card).bet_option).to eq(expect_score_card)
#     end
#   end
# end
