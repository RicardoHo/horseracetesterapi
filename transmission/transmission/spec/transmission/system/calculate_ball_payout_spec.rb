# frozen_string_literal: true

# describe Transmission::System::CalculateBallPayout do
#   subject { described_class.new(nil, nil) }
#   let(:game_engine) do
#     wrapper = Transmission::GameEngineWrapper.new(636816080)
#     wrapper.create(nil, nil)

#     wrapper
#   end
#   let(:game_def) do
#     Transmission::GameDefinition.new(
#       Transmission::Util::GameDefinitionUtil
#       .load(636816080)
#     )
#   end
#   let(:paytable) do
#     game_def
#       .math_model
#       .model(game_engine.game_config.math_name)
#       .dig(:paytable)
#   end
#   let(:bet_amount) { Transmission::Component::BetAmount.new.tap(&:changes_applied) }
#   let(:payout) { Transmission::Component::Payout.new.tap(&:changes_applied) }
#   let(:bet_area_label) { Transmission::Component::BetAreaLabel.new('test').tap(&:changes_applied) }
#   let(:entity) { Clutch::Entity.new(bet_amount, payout, bet_area_label) }
#   let(:score_card) do
#     score_card = Transmission::Component::BallScoreCard.new
#     score_card.bet_option = {
#       selected_bet_options: {
#         name: 'test',
#       },
#       selected_risk_level: 'low',
#     }

#     score_card
#   end
#   let(:lines) { 8..12 }
#   let(:line_index) { 0 }
#   let(:lines_info) { Transmission::Component::LinesInfo.new(lines, line_index) }
#   let(:pocket_info) { Transmission::Component::PocketInfo.new }
#   let(:reference) do
#     {
#       ball_score_card: [score_card],
#       lines_info: [lines_info],
#       pocket_info: [pocket_info],
#       paytable: [paytable],
#     }
#   end

#   subject do
#     instance = described_class.new(nil, nil)
#     instance.instance_variable_set(:@score_card, score_card)
#     instance.instance_variable_set(:@paytable, paytable)

#     instance
#   end

#   it 'should be kind of ReactiveSystem' do
#     expect(subject).to be_kind_of(Clutch::System::ReactiveSystem)
#   end

#   it 'should config watch component(s)' do
#     expect_classes = [
#       Transmission::Component::BetAmount,
#       Transmission::Component::Payout,
#       Transmission::Component::BetAreaLabel,
#     ]

#     expect(described_class::WATCHS).to eq(expect_classes)
#   end

#   it 'should config reference component(s)' do
#     expect_classes = [
#       Transmission::Component::BallScoreCard,
#       Transmission::Component::LinesInfo,
#       Transmission::Component::PocketInfo,
#     ]
#     expect(described_class::REFERENCE).to eq(expect_classes)
#   end

#   context '#before_all' do
#     before 'all' do
#       subject.instance_variable_set(:@entities, [entity])
#       subject.entities_handle = [entity]
#       allow(subject)
#         .to receive(:references)
#         .and_return(reference)
#       subject.send(:before_all)
#     end

#     it 'should set instance variable @score_card' do
#       expect(subject.instance_variable_get(:@score_card)).to be_instance_of(Transmission::Component::BallScoreCard)
#     end

#     it 'should set instance variable @lines_info' do
#       expect(subject.instance_variable_get(:@lines_info)).to be_instance_of(Transmission::Component::LinesInfo)
#     end

#     it 'should set instance variable @pocket_info' do
#       expect(subject.instance_variable_get(:@pocket_info)).to be_instance_of(Transmission::Component::PocketInfo)
#     end

#     it 'should set instance variable @paytable' do
#       expect(subject.instance_variable_get(:@paytable)).to eq(paytable)
#     end
#   end

#   context '#process' do
#     context 'when bet amount credit/cash is 10, line is 9, pocket index is 4' do
#       before 'all' do
#         bet_amount.credit = 10
#         bet_amount.cash   = 10
#         lines_info.line = 9
#         pocket_info.pocket_index = 4
#         subject.instance_variable_set(:@lines_info, lines_info)
#         subject.instance_variable_set(:@pocket_info, pocket_info)
#       end

#       it 'should get the odds (0.5) from paytable' do
#         subject.send(:process, entity)
#         expect(pocket_info.odds).to eq('0.5')
#       end

#       it 'should calculate the correct payout credit (5)' do
#         subject.send(:process, entity)
#         expect(entity.find(Transmission::Component::Payout).credit).to eq(5)
#         expect(entity.find(Transmission::Component::Payout).cash).to eq(5)
#       end
#     end
#   end
# end
