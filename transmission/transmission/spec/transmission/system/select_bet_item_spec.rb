# frozen_string_literal: true

describe Transmission::System::SelectBetItem do
  subject { described_class.new(nil, nil) }
  let(:range) { 1..12 }
  let(:valid_value) { 1 }
  let(:invalid_value) { 13 }
  let(:bet_amount) do
    Transmission::Component::BetAmount.new.tap { |bet| bet.credit = 100 }
  end
  let(:bet_area_label) { Transmission::Component::BetAreaLabel.new('test') }
  let(:bet_item) { Transmission::Component::ValueTypeBetItem.new(range) }
  let(:entity) { Clutch::Entity.new(bet_amount, bet_area_label, bet_item) }
  let(:score_card) { Transmission::Component::DiceScoreCard.new }
  let(:paytable) do
    {
      test: {
        1 => {
          odds: 1,
        },
      },
    }
  end

  it 'should be kind of ReactiveSystem' do
    expect(subject).to be_kind_of(Clutch::System::ReactiveSystem)
  end

  it 'should config watch component(s)' do
    expect_classes = [
      Transmission::Component::ValueTypeBetItem,
      Transmission::Component::BetAmount,
      Transmission::Component::BetAreaLabel,
    ]
    expect(described_class::WATCHS).to eq(expect_classes)
  end

  context '#process' do
    context 'valid bet item value' do
      before 'all' do
        subject.instance_variable_set(
          :@input,
          OpenStruct.new(
            bet_options: [
              OpenStruct.new(
                name: 'test',
                bet_item: OpenStruct.new(value: valid_value)
              ),
            ]
          )
        )
        subject.instance_variable_set(:@score_card, score_card)
        subject.instance_variable_set(:@paytable, paytable)
      end

      it 'set the selected value to bet item' do
        expect(bet_item.value).to eq(nil)
        subject.send(:process, entity)
        expect(bet_item.value).to eq(valid_value)
      end
    end

    context 'invlid bet item value' do
      before 'all' do
        subject.instance_variable_set(
          :@input,
          OpenStruct.new(
            bet_options: [
              OpenStruct.new(
                name: 'test',
                bet_item: OpenStruct.new(value: invalid_value)
              ),
            ]
          )
        )
      end

      it 'raise error' do
        expect { subject.send(:process, entity) }
          .to raise_error(Transmission::BetItemNotInRangeError)
      end
    end
  end
end
