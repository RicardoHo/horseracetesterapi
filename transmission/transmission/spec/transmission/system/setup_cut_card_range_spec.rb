# frozen_string_literal: true

describe Transmission::System::SetupCutCardRange do
  let(:game_engine) do
    wrapper = Transmission::GameEngineWrapper.new(970328033)
    wrapper.create(nil, nil)

    wrapper
  end
  let(:shoe) { Transmission::Component::Shoe.new(8, 6..402) }
  let(:entity) { Clutch::Entity.new(shoe) }

  subject do
    instance = described_class.new(nil, nil)
    instance.instance_variable_set(:@game_engine, game_engine)

    instance
  end

  it 'should be kind of CommandSystem' do
    expect(subject).to be_kind_of(Clutch::System::CommandSystem)
  end

  it 'should config watch component(s)' do
    expect_classes = [
      Transmission::Component::Shoe,
    ]
    expect(described_class::WATCHS).to eq(expect_classes)
  end

  context 'command' do
    it 'assosiate the entry method with query game event' do
      expect(described_class.tasks['query_game']).to eq(:setup)
    end
  end

  context '#setup' do
    it 'set the cut card range to the shoe' do
      extra_config = {
        cut_card_range_from: 150,
        cut_card_range_to: 250,
      }
      allow_any_instance_of(Transmission::GameEngineWrapper)
        .to receive_message_chain(:game_config, :extra)
        .and_return(extra_config)

      subject.send(:setup, entity)
      expect(shoe.cut_card_range.begin)
        .to eq(extra_config[:cut_card_range_from])
      expect(shoe.cut_card_range.end)
        .to eq(extra_config[:cut_card_range_to])
    end

    it 'raise argument error when the begin value is not integer' do
      extra_config = {
        cut_card_range_from: '150',
        cut_card_range_to: 250,
      }
      allow_any_instance_of(Transmission::GameEngineWrapper)
        .to receive_message_chain(:game_config, :extra)
        .and_return(extra_config)

      expect { subject.send(:setup, entity) }
        .to raise_error(Transmission::CutCardRangeParameterError)
    end

    it 'raise argument error when the begin value is not integer' do
      extra_config = {
        cut_card_range_from: 150,
        cut_card_range_to: '250',
      }
      allow_any_instance_of(Transmission::GameEngineWrapper)
        .to receive_message_chain(:game_config, :extra)
        .and_return(extra_config)

      expect { subject.send(:setup, entity) }
        .to raise_error(Transmission::CutCardRangeParameterError)
    end

    it 'raise error when the range is desc' do
      allow_any_instance_of(Transmission::GameEngineWrapper)
        .to receive_message_chain(:game_config, :extra)
        .and_return(
          cut_card_range_from: 250,
          cut_card_range_to: 150,
        )

      expect { subject.send(:setup, entity) }
        .to raise_error(Transmission::CutCardRangeParameterError)
    end

    it 'raise error when the begin value is less than 6' do
      allow_any_instance_of(Transmission::GameEngineWrapper)
        .to receive_message_chain(:game_config, :extra)
        .and_return(
          cut_card_range_from: 5,
          cut_card_range_to: 250,
        )

      expect { subject.send(:setup, entity) }
        .to raise_error(Transmission::CutCardRangeParameterError)
    end

    it 'raise error when the begin value is bigger than 402' do
      allow_any_instance_of(Transmission::GameEngineWrapper)
        .to receive_message_chain(:game_config, :extra)
        .and_return(
          cut_card_range_from: 403,
          cut_card_range_to: 403,
        )

      expect { subject.send(:setup, entity) }
        .to raise_error(Transmission::CutCardRangeParameterError)
    end

    it 'raise error when the end value is less than 6' do
      allow_any_instance_of(Transmission::GameEngineWrapper)
        .to receive_message_chain(:game_config, :extra)
        .and_return(
          cut_card_range_from: 5,
          cut_card_range_to: 5,
        )

      expect { subject.send(:setup, entity) }
        .to raise_error(Transmission::CutCardRangeParameterError)
    end

    it 'raise error when the end value is bigger than 402' do
      allow_any_instance_of(Transmission::GameEngineWrapper)
        .to receive_message_chain(:game_config, :extra)
        .and_return(
          cut_card_range_from: 150,
          cut_card_range_to: 403,
        )

      expect { subject.send(:setup, entity) }
        .to raise_error(Transmission::CutCardRangeParameterError)
    end
  end
end
