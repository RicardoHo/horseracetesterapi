# frozen_string_literal: true

describe Transmission::System::SetupDenomInfo do
  let(:game_engine) do
    wrapper = Transmission::GameEngineWrapper.new(978597675)
    wrapper.create(nil, nil)

    wrapper
  end
  let(:denoms) { game_engine.game_config.denoms.value }
  let(:denom_index) { game_engine.game_config.denoms.default_index }

  subject do
    instance = described_class.new(nil, nil)
    instance.instance_variable_set(:@game_engine, game_engine)

    instance
  end

  it 'should be kind of CommandSystem' do
    expect(subject).to be_kind_of(Clutch::System::CommandSystem)
  end

  it 'should config watch component(s)' do
    expect_classes = [Transmission::Component::DenomInfo]
    expect(described_class::WATCHS).to eq(expect_classes)
  end

  context 'command' do
    it 'should config method to query_game command' do
      expect(described_class.tasks['query_game']).to eq(:setup)
    end
  end

  context '#setup' do
    it 'should setup default denoms info' do
      component = Transmission::Component::DenomInfo.new
      entity = Clutch::Entity.new(component)
      subject.send(:setup, entity)
      expect(component.denom_index).to eq(denom_index)
      expect(component.denoms).to eq(denoms)
      expect(component.denom).to eq(denoms[denom_index])
    end
  end
end
