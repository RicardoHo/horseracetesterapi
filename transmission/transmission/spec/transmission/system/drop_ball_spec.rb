# frozen_string_literal: true

# describe Transmission::System::DropBall do
#   subject { described_class.new(nil, nil) }
#   let(:game_engine) do
#     wrapper = Transmission::GameEngineWrapper.new(636816080)
#     wrapper.create(nil, nil)

#     wrapper
#   end
#   let(:game_def) do
#     Transmission::GameDefinition.new(
#       Transmission::Util::GameDefinitionUtil
#       .load(636816080)
#     )
#   end
#   let(:weight_table) do
#     game_def
#       .math_model
#       .model(game_engine.game_config.math_name)
#       .dig(:weight_table)
#   end
#   let(:lines) { 8..12 }
#   let(:line_index) { 0 }
#   let(:levels) { ["low", "normal", "high"] }
#   let(:risk_level_index) { 1 }
#   let(:path_info) { Transmission::Component::PathInfo.new.tap(&:changes_applied) }
#   let(:pocket_info) { Transmission::Component::PocketInfo.new.tap(&:changes_applied) }
#   let(:risk_level) { Transmission::Component::RiskLevel.new(levels, risk_level_index).tap(&:changes_applied) }
#   let(:lines_info) { Transmission::Component::LinesInfo.new(lines, line_index).tap(&:changes_applied) }
#   let(:entity) { Clutch::Entity.new(path_info, pocket_info, risk_level, lines_info) }

#   subject do
#     instance = described_class.new(nil, nil)
#     instance.instance_variable_set(:@game_engine, game_engine)
#     instance.instance_variable_set(:@weight_table, weight_table)

#     instance
#   end

#   it 'should be kind of ReactiveSystem' do
#     expect(subject).to be_kind_of(Clutch::System::ReactiveSystem)
#   end

#   it 'should config watch component(s)' do
#     expect_classes = [
#       Transmission::Component::PathInfo,
#       Transmission::Component::PocketInfo,
#       Transmission::Component::RiskLevel,
#       Transmission::Component::LinesInfo,
#     ]

#     expect(described_class::WATCHS).to eq(expect_classes)
#   end

#   context '#executable?' do
#     it 'should pass for drop event' do
#       subject.instance_variable_set(:@command, 'drop')
#       expect(subject.send(:executable?)).to be(true)
#     end

#     it 'should not pass for query game event' do
#       subject.instance_variable_set(:@command, 'query_game')
#       expect(subject.send(:executable?)).to be(false)
#     end

#     it 'should not pass for drop finish event' do
#       subject.instance_variable_set(:@command, 'drop_finish')
#       expect(subject.send(:executable?)).to be(false)
#     end
#   end

#   context '#before_all' do
#     before 'each' do
#       subject.instance_variable_set(:@entities, [entity])
#       subject.entities_handle = [entity]
#     end

#     it 'should set instance variable @weight_table' do
#       subject.send(:before_all)
#       expect(subject.instance_variable_get(:@weight_table)).to eq(weight_table)
#     end
#   end

#   context '#process' do
#     context 'when line is 12, risk level is low' do
#       before 'all' do
#         lines_info.line = 12
#         risk_level.risk_level_index = 0
#         subject.instance_variable_set(:@lines_info, lines_info)
#         subject.instance_variable_set(:@risk_level, risk_level)
#       end

#       it 'should randomly to generate the pocket index' do
#         subject.send(:process, entity)
#         expect(
#           entity.find(Transmission::Component::PocketInfo).pocket_index
#         ).to be_between(0, 12).inclusive
#       end

#       it 'should generate the correct path' do
#         subject.send(:process, entity)
#         pocket_index = entity.find(Transmission::Component::PocketInfo).pocket_index
#         expect(
#           entity.find(Transmission::Component::PathInfo).path_left_right.sum
#         ).to eq(pocket_index)
#       end
#     end
#   end
# end
