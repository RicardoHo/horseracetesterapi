# frozen_string_literal: true

describe Transmission::System::DetermineSanGongWinner do
  subject { described_class.new(nil, nil) }

  let(:dragon_player_label) { Transmission::Component::PlayerLabel.new('dragon').tap(&:changes_applied) }
  let(:phoenix_player_label) { Transmission::Component::PlayerLabel.new('phoenix').tap(&:changes_applied) }
  let(:dragon_hand_card) do
    component = Transmission::Component::HandCard.new(3)
    component.cards = ['As', '2s', '3s']

    component
  end
  let(:phoenix_hand_card) do
    component = Transmission::Component::HandCard.new(3)
    component.cards = ['4s', '5s', '6s']

    component
  end
  let(:dragon_hand_point) do
    component = Transmission::Component::HandPoint.new
    component.value = 6

    component
  end
  let(:phoenix_hand_point) do
    component = Transmission::Component::HandPoint.new
    component.value = 5

    component
  end
  let(:dragon_entity) { Clutch::Entity.new(dragon_player_label, dragon_hand_card, dragon_hand_point) }
  let(:phoenix_entity) { Clutch::Entity.new(phoenix_player_label, phoenix_hand_card, phoenix_hand_point) }

  let(:type_list) { ["three_of_a_kind", "three_picture_cards", "hand_value"] }
  let(:score_card) { Transmission::Component::SanGongScoreCard.new.tap(&:changes_applied) }
  let(:win_type_list) { Transmission::Component::WinTypeList.new(type_list).tap(&:changes_applied) }

  let(:expect_player_items) do
    {
      dragon: {
        best_card: "3s",
        cards: ["As", "2s", "3s"],
        name: "dragon",
        point: 6,
      },
      phoenix: {
        best_card: "6s",
        cards: ["4s", "5s", "6s"],
        name: "phoenix",
        point: 5,
      },
    }
  end

  subject do
    instance = described_class.new(nil, nil)
    instance.instance_variable_set(:@score_card, score_card)
    instance.instance_variable_set(:@win_type_list, win_type_list)

    instance
  end

  it 'should be kind of ReactiveSystem' do
    expect(subject).to be_kind_of(Clutch::System::ReactiveSystem)
  end

  it 'should config watch component(s)' do
    expect_classes = [
      Transmission::Component::PlayerLabel,
      Transmission::Component::HandCard,
      Transmission::Component::HandPoint,
    ]

    expect(described_class::WATCHS).to eq(expect_classes)
  end

  it 'should config reference component(s)' do
    expect_classes = [
      Transmission::Component::SanGongScoreCard,
      Transmission::Component::WinTypeList,
    ]

    expect(described_class::REFERENCE).to eq(expect_classes)
  end

  context '#before_all' do
    before 'each' do
      subject.instance_variable_set(:@entities, [dragon_entity, phoenix_entity])
      subject.entities_handle = [dragon_entity, phoenix_entity]
    end

    it 'should set instance variable @score_card' do
      subject.send(:before_all)
      expect(subject.instance_variable_get(:@score_card)).to eq(score_card)
    end

    it 'should set instance variable @win_type_list' do
      subject.send(:before_all)
      expect(subject.instance_variable_get(:@win_type_list)).to eq(win_type_list)
    end
  end

  context '#process' do
    it 'should get the @player_items hash' do
      subject.send(:process, dragon_entity)
      subject.send(:process, phoenix_entity)

      expect(subject.instance_variable_get(:@player_items)).to eq(expect_player_items)
    end
  end

  context '#after_all' do
    before 'all' do
      subject.instance_variable_set(:@entities, [dragon_entity, phoenix_entity])
      subject.entities_handle = [dragon_entity, phoenix_entity]

      subject.instance_variable_set(:@player_items, expect_player_items)
    end

    it 'should get the winner' do
      subject.send(:after_all)

      expect(subject.instance_variable_get(:@score_card).winner).to eq('dragon')
      expect(subject.instance_variable_get(:@score_card).point).to eq(6)
    end
  end

  context '#three_of_a_kind_winner' do
    context 'just one player have three of a kind hand card' do
      before 'all' do
        player_items = {
          dragon: {
            cards: ["As", "2s", "3s"],
            name: "dragon",
            point: 6,
          },
          phoenix: {
            cards: ["5s", "5c", "5h"],
            name: "phoenix",
            point: 5,
          },
        }
        subject.instance_variable_set(:@player_items, player_items)
      end

      it 'should get the winner' do
        expect(subject.send(:three_of_a_kind_winner)).to eq('phoenix')
      end
    end

    context 'both player have three of a kind hand card' do
      before 'all' do
        player_items = {
          dragon: {
            best_card: "2s",
            cards: ["2s", "2c", "2h"],
            name: "dragon",
            point: 6,
          },
          phoenix: {
            best_card: "5s",
            cards: ["5s", "5c", "5h"],
            name: "phoenix",
            point: 5,
          },
        }
        subject.instance_variable_set(:@player_items, player_items)
      end

      it 'should get the winner win by best card' do
        expect(subject.send(:three_of_a_kind_winner)).to eq('phoenix')
      end
    end

    context 'both player do not have three of a kind hand card' do
      before 'all' do
        player_items = {
          dragon: {
            best_card: "2s",
            cards: ["As", "2c", "2h"],
            name: "dragon",
            point: 5,
          },
          phoenix: {
            best_card: "5s",
            cards: ["As", "5c", "5h"],
            name: "phoenix",
            point: 1,
          },
        }
        subject.instance_variable_set(:@player_items, player_items)
      end

      it 'should get nil' do
        expect(subject.send(:three_of_a_kind_winner)).to eq(nil)
      end
    end
  end

  context '#three_picture_card_winner' do
    context 'just one player have three picture card hand card' do
      before 'all' do
        player_items = {
          dragon: {
            cards: ["As", "2s", "3s"],
            name: "dragon",
            point: 6,
          },
          phoenix: {
            cards: ["Js", "Qc", "Kh"],
            name: "phoenix",
            point: 0,
          },
        }
        subject.instance_variable_set(:@player_items, player_items)
      end

      it 'should get the winner' do
        expect(subject.send(:three_picture_card_winner)).to eq('phoenix')
      end
    end

    context 'both player have three picture card hand card' do
      before 'all' do
        player_items = {
          dragon: {
            best_card: "Kh",
            cards: ["Js", "Qc", "Kh"],
            name: "dragon",
            point: 0,
          },
          phoenix: {
            best_card: "Kc",
            cards: ["Qs", "Kc", "Jh"],
            name: "phoenix",
            point: 0,
          },
        }
        subject.instance_variable_set(:@player_items, player_items)
      end

      it 'should get the winner win by best card' do
        expect(subject.send(:three_picture_card_winner)).to eq('dragon')
      end
    end

    context 'both player do not have three picture card hand card' do
      before 'all' do
        player_items = {
          dragon: {
            best_card: "2h",
            cards: ["As", "2c", "2h"],
            name: "dragon",
            point: 5,
          },
          phoenix: {
            best_card: "5h",
            cards: ["As", "5c", "5h"],
            name: "phoenix",
            point: 1,
          },
        }
        subject.instance_variable_set(:@player_items, player_items)
      end

      it 'should get nil' do
        expect(subject.send(:three_picture_card_winner)).to eq(nil)
      end
    end
  end

  context 'hand_value_winner' do
    context 'two player have different hand point' do
      before 'all' do
        player_items = {
          dragon: {
            cards: ["As", "2s", "3s"],
            name: "dragon",
            point: 6,
          },
          phoenix: {
            cards: ["4s", "5c", "6h"],
            name: "phoenix",
            point: 5,
          },
        }
        subject.instance_variable_set(:@player_items, player_items)
      end

      it 'should get the winner' do
        expect(subject.send(:hand_value_winner)).to eq('dragon')
      end
    end

    context 'two player have same hand point' do
      before 'all' do
        player_items = {
          dragon: {
            best_card: "3h",
            cards: ["As", "2c", "3h"],
            name: "dragon",
            point: 6,
          },
          phoenix: {
            best_card: "6h",
            cards: ["4s", "6c", "6h"],
            name: "phoenix",
            point: 6,
          },
        }
        subject.instance_variable_set(:@player_items, player_items)
      end

      it 'should get the winner win by best card' do
        expect(subject.send(:hand_value_winner)).to eq('phoenix')
      end
    end
  end
end
