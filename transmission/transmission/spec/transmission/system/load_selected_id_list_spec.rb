# frozen_string_literal: true

# describe Transmission::System::LoadSelectedIdList do
#   subject { described_class.new(nil, nil) }
#   let(:game_engine) do
#     wrapper = Transmission::GameEngineWrapper.new(950404013)
#     wrapper.create(nil, nil)

#     wrapper
#   end
#   let(:list) { [10, 20, 30] }
#   let(:selected_id_list) { Transmission::Component::SelectedIdList.new }
#   let(:entity) { Clutch::Entity.new(selected_id_list) }

#   let(:save) do
#     {
#       table: {
#         selected_id_list: {
#           list: list,
#         },
#       },
#     }
#   end

#   it 'should be kind of CommandSystem' do
#     expect(subject).to be_kind_of(Clutch::System::CommandSystem)
#   end

#   it 'should config watch component(s)' do
#     expect_classes = [Transmission::Component::SelectedIdList]
#     expect(described_class::WATCHS).to eq(expect_classes)
#   end

#   context 'command' do
#     it 'should config method to query_game command' do
#       expect(described_class.tasks['query_game']).to eq(:on_load)
#     end
#   end

#   context '#executable?' do
#     before 'all' do
#       subject.instance_variable_set(:@entities, [entity])
#       subject.instance_variable_set(:@command, 'query_game')
#     end

#     it 'should pass for resume round' do
#       allow(game_engine)
#         .to receive(:resume_round?)
#         .and_return(true)
#       subject.instance_variable_set(:@game_engine, game_engine)
#       expect(subject.send(:executable?)).to be(true)
#     end

#     it 'should pass for load last round' do
#       allow(game_engine)
#         .to receive(:new_round?)
#         .and_return(true)
#       subject.instance_variable_set(:@game_engine, game_engine)
#       expect(subject.send(:executable?)).to be(true)
#     end

#     it 'should not pass for new game' do
#       allow(game_engine)
#         .to receive(:new_round?)
#         .and_return(false)
#       allow(game_engine)
#         .to receive(:resume_round?)
#         .and_return(false)
#       subject.instance_variable_set(:@game_engine, game_engine)
#       expect(subject.send(:executable?)).to be(false)
#     end
#   end

#   context '#before_all' do
#     before 'all' do
#       subject.instance_variable_set(:@entities, [entity])
#       allow(game_engine)
#         .to receive(:new_round?)
#         .and_return(true)
#       allow(subject)
#         .to receive(:last_round_save)
#         .and_return(save)
#       subject.instance_variable_set(:@game_engine, game_engine)
#       subject.send(:before_all)
#     end

#     it 'should load save from game engine' do
#       expect(subject.instance_variable_get(:@save)).to eq(save)
#     end

#     it 'should load and cache stage data from game engine' do
#       expect(subject.instance_variable_get(:@list)).to eq(list)
#     end
#   end

#   context '#on_load' do
#     before 'all' do
#       expect(entity.find(Transmission::Component::SelectedIdList).list)
#         .to eq([])
#       subject.instance_variable_set(:@list, list)
#       subject.send(:on_load, entity)
#     end

#     it 'should load score_card' do
#       expect(entity.find(Transmission::Component::SelectedIdList).list)
#         .to eq(list)
#     end
#   end
# end
