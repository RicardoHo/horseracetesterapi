# frozen_string_literal: true

describe Transmission::System::ClearScoreBoard do
  let(:game_engine) do
    wrapper = Transmission::GameEngineWrapper.new(970328033)
    wrapper.create(nil, nil)

    wrapper
  end
  let(:shoe) { Transmission::Component::Shoe.new(8, 6..402) }
  let(:shoe_score_board) { Transmission::Component::ShoeScoreBoard.new }
  let(:entity) { Clutch::Entity.new(shoe, shoe_score_board) }

  subject do
    instance = described_class.new(nil, nil)
    instance.instance_variable_set(:@game_engine, game_engine)

    instance
  end

  it 'should be kind of CommandSystem' do
    expect(subject).to be_kind_of(Clutch::System::CommandSystem)
  end

  it 'should config watch component(s)' do
    expect_classes = [
      Transmission::Component::Shoe,
      Transmission::Component::ShoeScoreBoard,
    ]
    expect(described_class::WATCHS).to eq(expect_classes)
  end

  context 'command' do
    it 'assosiate the entry method with query game event' do
      expect(described_class.tasks['query_game']).to eq(:clear)
    end

    it 'assosiate the entry method with reshuffle event' do
      expect(described_class.tasks['reshuffle']).to eq(:clear)
    end
  end

  context '#clear' do
    before 'all' do
      shoe_score_board.shoe_id = shoe.id
      shoe_score_board.record('fake_hand_score')
    end

    it 'clear the score board after shoe shuffled' do
      shoe.shuffle
      expect(shoe_score_board.shoe_id).not_to eq(shoe.id)
      subject.send(:clear, entity)
      expect(shoe_score_board.shoe_id).to eq(shoe.id)
      expect(shoe_score_board.history).to be_empty
    end

    it 'do not clear the score board while shoe not change' do
      subject.send(:clear, entity)
      expect(shoe_score_board.shoe_id).to eq(shoe.id)
      expect(shoe_score_board.history).not_to be_empty
    end
  end
end
