# frozen_string_literal: true

describe Transmission::System::RecordScoreByTag do
  subject { described_class.new(nil, nil) }

  let(:score_card_tag) { Transmission::Component::ScoreCardTag.new('san_gong_score_card').tap(&:changes_applied) }
  let(:score_board) { Transmission::Component::ScoreBoard.new.tap(&:changes_applied) }
  let(:san_gong_score_card) { Transmission::Component::SanGongScoreCard.new.tap(&:changes_applied) }
  let(:entity) { Clutch::Entity.new(score_card_tag, score_board, san_gong_score_card) }

  it 'should be kind of ReactiveSystem' do
    expect(subject).to be_kind_of(Clutch::System::ReactiveSystem)
  end

  it 'should config watch component(s)' do
    expect_classes = [
      Transmission::Component::ScoreCardTag,
      Transmission::Component::ScoreBoard,
    ]

    expect(described_class::WATCHS).to eq(expect_classes)
  end

  context '#executable?' do
    it 'should pass for multiple_bet event' do
      subject.instance_variable_set(:@command, 'multiple_bet')
      expect(subject.send(:executable?)).to be(true)
    end

    it 'should not pass for query game event' do
      subject.instance_variable_set(:@command, 'query_game')
      expect(subject.send(:executable?)).to be(false)
    end
  end

  context '#process' do
    before 'all' do
      san_gong_score_card.winner = 'testing'
    end

    it 'should record the score card' do
      subject.send(:process, entity)
      expect(
        entity.find(Transmission::Component::ScoreBoard).history.first.dig(:winner)
      ).to eq('testing')
    end
  end
end
