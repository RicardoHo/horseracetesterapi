# frozen_string_literal: true

# describe Transmission::System::LoadRiskLevel do
#   subject { described_class.new(nil, nil) }
#   let(:game_engine) do
#     wrapper = Transmission::GameEngineWrapper.new(636816080)
#     wrapper.create(nil, nil)

#     wrapper
#   end
#   let(:levels) { ["low", "normal", "high"] }
#   let(:risk_level_index) { 1 }
#   let(:risk_level_index_default) { 0 }
#   let(:risk_level) { Transmission::Component::RiskLevel.new(levels, risk_level_index_default) }
#   let(:entity) { Clutch::Entity.new(risk_level) }
#   let(:save) do
#     {
#       table: {
#         risk_level: {
#           index: risk_level_index,
#         },
#       },
#     }
#   end

#   it 'should be kind of CommandSystem' do
#     expect(subject).to be_kind_of(Clutch::System::CommandSystem)
#   end

#   it 'should config watch component(s)' do
#     expect_classes = [Transmission::Component::RiskLevel]
#     expect(described_class::WATCHS).to eq(expect_classes)
#   end

#   context 'command' do
#     it 'should config method to query_game command' do
#       expect(described_class.tasks['query_game']).to eq(:on_load)
#     end
#   end

#   context '#executable?' do
#     before 'all' do
#       subject.instance_variable_set(:@entities, [entity])
#       subject.instance_variable_set(:@command, 'query_game')
#     end

#     it 'should pass for resume round' do
#       allow(game_engine)
#         .to receive(:resume_round?)
#         .and_return(true)
#       subject.instance_variable_set(:@game_engine, game_engine)
#       expect(subject.send(:executable?)).to be(true)
#     end

#     it 'should pass for load last round' do
#       allow(game_engine)
#         .to receive(:new_round?)
#         .and_return(true)
#       subject.instance_variable_set(:@game_engine, game_engine)
#       expect(subject.send(:executable?)).to be(true)
#     end

#     it 'should not pass for new game' do
#       allow(game_engine)
#         .to receive(:new_round?)
#         .and_return(false)
#       allow(game_engine)
#         .to receive(:resume_round?)
#         .and_return(false)
#       subject.instance_variable_set(:@game_engine, game_engine)
#       expect(subject.send(:executable?)).to be(false)
#     end
#   end

#   context '#before_all' do
#     before 'all' do
#       subject.instance_variable_set(:@entities, [entity])
#       allow(game_engine)
#         .to receive(:new_round?)
#         .and_return(true)
#       allow(subject)
#         .to receive(:last_round_save)
#         .and_return(save)
#       subject.instance_variable_set(:@game_engine, game_engine)
#       subject.send(:before_all)
#     end

#     it 'should load save from game engine' do
#       expect(subject.instance_variable_get(:@save)).to eq(save)
#     end

#     it 'should load and cache stage data from game engine' do
#       expect(subject.instance_variable_get(:@risk_level_index)).to eq(risk_level_index)
#     end
#   end

#   context '#on_load' do
#     before 'all' do
#       expect(entity.find(Transmission::Component::RiskLevel).risk_level_index)
#         .to eq(risk_level_index_default)
#       subject.instance_variable_set(:@risk_level_index, risk_level_index)
#       subject.send(:on_load, entity)
#     end

#     it 'should load risk level' do
#       expect(entity.find(Transmission::Component::RiskLevel).risk_level_index)
#         .to eq(risk_level_index)
#     end
#   end
# end
