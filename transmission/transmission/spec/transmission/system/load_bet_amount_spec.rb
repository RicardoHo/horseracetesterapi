# frozen_string_literal: true

# describe Transmission::System::LoadBetAmount do
#   subject { described_class.new(nil, nil) }
#   let(:game_engine) do
#     wrapper = Transmission::GameEngineWrapper.new(636816080)
#     wrapper.create(nil, nil)

#     wrapper
#   end
#   let(:bet_amount_cash) { 100 }
#   let(:bet_amount_credit) { 100 }
#   let(:bet_amount) { Transmission::Component::BetAmount.new }
#   let(:bet_area_label) { Transmission::Component::BetAreaLabel.new('test') }
#   let(:entity) { Clutch::Entity.new(bet_amount, bet_area_label) }
#   let(:save) do
#     {
#       bet_options: {
#         name: bet_area_label.name,
#         bet_amount: {
#           cash: bet_amount_cash,
#           credit: bet_amount_credit,
#         },
#       },
#     }
#   end

#   it 'should be kind of CommandSystem' do
#     expect(subject).to be_kind_of(Clutch::System::CommandSystem)
#   end

#   it 'should config watch component(s)' do
#     expect_classes = [
#       Transmission::Component::BetAmount,
#       Transmission::Component::BetAreaLabel,
#     ]
#     expect(described_class::WATCHS).to eq(expect_classes)
#   end

#   context 'command' do
#     it 'should config method to query_game command' do
#       expect(described_class.tasks['query_game']).to eq(:on_load)
#     end
#   end

#   context '#executable?' do
#     before 'all' do
#       subject.instance_variable_set(:@entities, [entity])
#       subject.instance_variable_set(:@command, 'query_game')
#     end

#     it 'should pass for resume round' do
#       allow(game_engine)
#         .to receive(:resume_round?)
#         .and_return(true)
#       subject.instance_variable_set(:@game_engine, game_engine)
#       expect(subject.send(:executable?)).to be(true)
#     end

#     it 'should pass for load last round' do
#       allow(game_engine)
#         .to receive(:new_round?)
#         .and_return(true)
#       subject.instance_variable_set(:@game_engine, game_engine)
#       expect(subject.send(:executable?)).to be(true)
#     end

#     it 'should not pass for new game' do
#       allow(game_engine)
#         .to receive(:new_round?)
#         .and_return(false)
#       allow(game_engine)
#         .to receive(:resume_round?)
#         .and_return(false)
#       subject.instance_variable_set(:@game_engine, game_engine)
#       expect(subject.send(:executable?)).to be(false)
#     end
#   end

#   context '#before_all' do
#     before 'all' do
#       subject.instance_variable_set(:@entities, [entity])
#       allow(game_engine)
#         .to receive(:new_round?)
#         .and_return(true)
#       allow(subject)
#         .to receive(:last_round_save)
#         .and_return(save)
#       subject.instance_variable_set(:@game_engine, game_engine)
#       subject.send(:before_all)
#     end

#     it 'should load save from game engine' do
#       expect(subject.instance_variable_get(:@save)).to eq(save)
#     end

#     it 'should load and cache stage data from game engine' do
#       expect(subject.instance_variable_get(:@bet_options_data)).to eq(save[:bet_options])
#     end
#   end

#   context '#on_load' do
#     before 'all' do
#       expect(entity.find(Transmission::Component::BetAmount).cash)
#         .to eq(nil)
#       expect(entity.find(Transmission::Component::BetAmount).credit)
#         .to eq(nil)
#       subject.instance_variable_set(:@bet_options_data, save[:bet_options])
#       subject.send(:on_load, entity)
#     end

#     it 'should load bet amount' do
#       expect(entity.find(Transmission::Component::BetAmount).cash)
#         .to eq(bet_amount_cash)
#       expect(entity.find(Transmission::Component::BetAmount).credit)
#         .to eq(bet_amount_credit)
#     end
#   end

#   context '#find_bet_amount_data' do
#     it 'should find bet_amount_data from array' do
#       subject.instance_variable_set(:@bet_options_data, [save[:bet_options]])
#       expect(subject.send(:find_bet_amount_data, bet_area_label.name)[:cash])
#         .to eq(bet_amount_cash)
#       expect(subject.send(:find_bet_amount_data, bet_area_label.name)[:credit])
#         .to eq(bet_amount_credit)
#     end

#     it 'should find bet_amount_data from hash' do
#       subject.instance_variable_set(:@bet_options_data, save[:bet_options])
#       expect(subject.send(:find_bet_amount_data, bet_area_label.name)[:cash])
#         .to eq(bet_amount_cash)
#       expect(subject.send(:find_bet_amount_data, bet_area_label.name)[:credit])
#         .to eq(bet_amount_credit)
#     end
#   end
# end
