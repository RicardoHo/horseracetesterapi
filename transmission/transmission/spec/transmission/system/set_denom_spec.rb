# frozen_string_literal: true

describe Transmission::System::SetDenom do
  subject do
    instance = described_class.new(nil, nil)
    instance.instance_variable_set(:@command, 'deal')

    instance
  end

  let(:denoms) { [100, 200, 500, 1000, 50000] }
  let(:denom_index) { 0 }
  let(:component) do
    component = Transmission::Component::DenomInfo.new
    component.denoms = denoms
    component.denom_index = denom_index

    component
  end
  let(:entity) { Clutch::Entity.new(component) }

  it 'should be kind of CommandSystem' do
    expect(subject).to be_kind_of(Clutch::System::CommandSystem)
  end

  it 'should config watch component(s)' do
    expect_classes = [Transmission::Component::DenomInfo]
    expect(described_class::WATCHS).to eq(expect_classes)
  end

  context 'command' do
    it 'should config method to deal command' do
      expect(described_class.tasks['deal']).to eq(:save_denom)
    end

    it 'should config method to bet command' do
      expect(described_class.tasks['bet']).to eq(:save_denom)
    end
  end

  context '#executable?' do
    it 'should pass for input denom exist' do
      subject.instance_variable_set(:@entities, [entity])
      subject.instance_variable_set(:@command, 'deal')
      subject.instance_variable_set(:@input, OpenStruct.new(denom: 100))
      expect(subject.send(:executable?)).to be_truthy
    end

    it 'should not pass for input denom not exist' do
      subject.instance_variable_set(:@entities, [entity])
      subject.instance_variable_set(:@input, OpenStruct.new(denom: nil))
      expect(subject.send(:executable?)).to be_falsey
    end
  end

  context '#save_denom' do
    it 'should save player selected denom' do
      expect_denom = 200
      subject.instance_variable_set(:@input, OpenStruct.new(denom: expect_denom))

      subject.send(:save_denom, entity)
      expect(component.denom).to eq(expect_denom)
      expect(component.denom_index).to eq(1)
    end

    it 'should raise error for not in range error' do
      expect_denom = 10
      subject.instance_variable_set(:@input, OpenStruct.new(denom: expect_denom))

      expect { subject.send(:save_denom, entity) }
        .to raise_error(Transmission::DenomNotInRangeError)
    end
  end
end
