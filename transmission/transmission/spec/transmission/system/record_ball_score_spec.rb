# frozen_string_literal: true

describe Transmission::System::RecordBallScore do
  subject { described_class.new(nil, nil) }
  let(:ball_score_card) { Transmission::Component::BallScoreCard.new }
  let(:score_board) { Transmission::Component::ScoreBoard.new(10) }
  let(:entity) { Clutch::Entity.new(ball_score_card, score_board) }

  it 'should be kind of ReactiveSystem' do
    expect(subject).to be_kind_of(Clutch::System::ReactiveSystem)
  end

  it 'should config watch component(s)' do
    expect_classes = [
      Transmission::Component::BallScoreCard,
      Transmission::Component::ScoreBoard,
    ]

    expect(described_class::WATCHS).to eq(expect_classes)
  end

  context '#executable?' do
    before 'each' do
      subject.instance_variable_set(:@entities, [entity])
    end

    it 'should not pass for query game event' do
      subject.instance_variable_set(:@command, 'query_game')
      expect(subject.send(:executable?)).to be(false)
    end
  end

  context '#process' do
    context 'when have a new score card' do
      before 'all' do
        ball_score_card.bet_option = {
          selected_bet_options: {
            name: 'test',
          },
        }
        @score_card = {
          bet_option: entity.find(Transmission::Component::BallScoreCard).bet_option,
        }
      end

      it 'should record in score board' do
        subject.send(:process, entity)
        expect(
          entity.find(Transmission::Component::ScoreBoard).history.first
        ).to eq(@score_card)
      end
    end

    context 'when insert histories over max limit' do
      before 'all' do
        ball_score_card.bet_option = {
          selected_bet_options: {
            name: 'test',
          },
        }
        times_for_insert_score_card = entity.find(Transmission::Component::ScoreBoard).max_amount
        times_for_insert_score_card.times.each { subject.send(:process, entity) }
      end

      it 'number of histories in score board should not over max limit' do
        subject.send(:process, entity)
        expect(
          entity.find(Transmission::Component::ScoreBoard).history.size
        ).to eq(entity.find(Transmission::Component::ScoreBoard).max_amount)
      end

      it 'score board should shift for the new insert' do
        ball_score_card.bet_option = {
          selected_bet_options: {
            name: 'shift record',
          },
        }
        @shift_score_card = {
          bet_option: entity.find(Transmission::Component::BallScoreCard).bet_option,
        }
        subject.send(:process, entity)
        expect(
          entity.find(Transmission::Component::ScoreBoard).history.last
        ).to eq(@shift_score_card)
      end
    end
  end
end
