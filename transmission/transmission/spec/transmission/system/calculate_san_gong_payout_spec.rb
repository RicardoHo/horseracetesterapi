# frozen_string_literal: true

describe Transmission::System::CalculateSanGongPayout do
  subject { described_class.new(nil, nil) }

  let(:game_engine) do
    wrapper = Transmission::GameEngineWrapper.new(622742907)
    wrapper.create(nil, nil)

    wrapper
  end
  let(:game_def) do
    Transmission::GameDefinition.new(
      Transmission::Util::GameDefinitionUtil
      .load(622742907)
    )
  end
  let(:paytable) do
    game_def
      .math_model
      .model(game_engine.game_config.math_name)
      .dig(:paytable)
  end
  let(:odds_divsior) do
    game_def
      .math_model
      .model(game_engine.game_config.math_name)
      .dig(:odds_divsior)
  end

  let(:bet_area_label_list) do
    [
      'dragon',
      'phoenix',
      'dragon_even',
      'dragon_odd',
      'dragon_pair',
      'phoenix_even',
      'phoenix_odd',
      'phoenix_pair',
      'total_9',
      'total_7_8',
      'total_1_6',
    ]
  end
  let(:bet_amount) { Transmission::Component::BetAmount.new.tap(&:changes_applied) }
  let(:payout) { Transmission::Component::Payout.new.tap(&:changes_applied) }
  let(:bet_area_label) { Transmission::Component::BetAreaLabel.new(bet_area_label_list[0]).tap(&:changes_applied) }
  let(:entity) { Clutch::Entity.new(bet_amount, payout, bet_area_label) }

  let(:type_list) { ["three_of_a_kind", "three_picture_cards", "hand_value"] }
  let(:score_card) { Transmission::Component::SanGongScoreCard.new.tap(&:changes_applied) }

  subject do
    instance = described_class.new(nil, nil)
    instance.instance_variable_set(:@score_card, score_card)
    instance.instance_variable_set(:@paytable, paytable)
    instance.instance_variable_set(:@odds_divsior, odds_divsior)

    instance
  end

  it 'should be kind of ReactiveSystem' do
    expect(subject).to be_kind_of(Clutch::System::ReactiveSystem)
  end

  it 'should config watch component(s)' do
    expect_classes = [
      Transmission::Component::BetAmount,
      Transmission::Component::Payout,
      Transmission::Component::BetAreaLabel,
    ]

    expect(described_class::WATCHS).to eq(expect_classes)
  end

  it 'should config reference component(s)' do
    expect_classes = [
      Transmission::Component::SanGongScoreCard,
    ]

    expect(described_class::REFERENCE).to eq(expect_classes)
  end

  context '#before_all' do
    before 'each' do
      subject.instance_variable_set(:@entities, [entity])
      subject.entities_handle = [entity]

      subject.send(:before_all)
    end

    it 'should set instance variable @score_card' do
      expect(subject.instance_variable_get(:@score_card)).to eq(score_card)
    end

    it 'should set instance variable @paytable' do
      expect(subject.instance_variable_get(:@paytable)).to eq(paytable)
    end

    it 'should set instance variable @odds_divsior' do
      expect(subject.instance_variable_get(:@odds_divsior)).to eq(odds_divsior)
    end
  end

  context '#process' do
    context 'payout_list does not include "total_zero"' do
      before 'all' do
        bet_amount.cash = 100
        bet_amount.credit = 100
        score_card.payout_list = bet_area_label_list
      end

      it 'should calculate the correctly payout' do
        bet_area_label_list.each do |label|
          bet_label = Transmission::Component::BetAreaLabel.new(label)
          ent = Clutch::Entity.new(bet_amount, payout, bet_label)
          subject.send(:process, ent)

          odds = subject.instance_variable_get(:@paytable).dig(label.to_sym, :odds)

          expect(ent.find(Transmission::Component::Payout).credit).to eq(bet_amount.credit * odds / odds_divsior)
          expect(ent.find(Transmission::Component::Payout).cash).to eq(bet_amount.cash * odds / odds_divsior)
        end
      end
    end

    context 'payout_list include "total_zero"' do
      before 'all' do
        bet_amount.cash = 100
        bet_amount.credit = 100
        score_card.payout_list = bet_area_label_list - ['total_9', 'total_7_8', 'total_1_6'] + ['total_zero']
      end

      it 'should calculate the correctly payout and return the bet amount in "total_*"' do
        bet_area_label_list.each do |label|
          bet_label = Transmission::Component::BetAreaLabel.new(label)
          ent = Clutch::Entity.new(bet_amount, payout, bet_label)
          subject.send(:process, ent)

          odds = subject.instance_variable_get(:@paytable).dig(label.to_sym, :odds)

          if label.match?(/total_*/)
            expect(ent.find(Transmission::Component::Payout).credit).to eq(bet_amount.credit)
            expect(ent.find(Transmission::Component::Payout).cash).to eq(bet_amount.cash)
          else
            expect(ent.find(Transmission::Component::Payout).credit).to eq(bet_amount.credit * odds / odds_divsior)
            expect(ent.find(Transmission::Component::Payout).cash).to eq(bet_amount.cash * odds / odds_divsior)
          end
        end
      end
    end
  end
end
