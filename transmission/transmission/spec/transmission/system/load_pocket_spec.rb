# frozen_string_literal: true

# describe Transmission::System::LoadPocket do
#   subject { described_class.new(nil, nil) }
#   let(:game_engine) do
#     wrapper = Transmission::GameEngineWrapper.new(636816080)
#     wrapper.create(nil, nil)

#     wrapper
#   end
#   let(:pocket_info) { Transmission::Component::PocketInfo.new }
#   let(:entity) { Clutch::Entity.new(pocket_info) }
#   let(:pocket_index) { 0 }
#   let(:odds) { 1 }
#   let(:save) do
#     {
#       table: {
#         pocket_info: {
#           index: pocket_index,
#           odds: odds,
#         },
#       },
#     }
#   end

#   it 'should be kind of CommandSystem' do
#     expect(subject).to be_kind_of(Clutch::System::CommandSystem)
#   end

#   it 'should config watch component(s)' do
#     expect_classes = [Transmission::Component::PocketInfo]
#     expect(described_class::WATCHS).to eq(expect_classes)
#   end

#   context 'command' do
#     it 'should config method to query_game command' do
#       expect(described_class.tasks['query_game']).to eq(:on_load)
#     end
#   end

#   context '#executable?' do
#     before 'all' do
#       subject.instance_variable_set(:@entities, [entity])
#       subject.instance_variable_set(:@command, 'query_game')
#     end

#     it 'should pass for resume round' do
#       allow(game_engine)
#         .to receive(:resume_round?)
#         .and_return(true)
#       subject.instance_variable_set(:@game_engine, game_engine)
#       expect(subject.send(:executable?)).to be(true)
#     end

#     it 'should pass for load last round' do
#       allow(game_engine)
#         .to receive(:new_round?)
#         .and_return(true)
#       subject.instance_variable_set(:@game_engine, game_engine)
#       expect(subject.send(:executable?)).to be(true)
#     end

#     it 'should not pass for new game' do
#       allow(game_engine)
#         .to receive(:new_round?)
#         .and_return(false)
#       allow(game_engine)
#         .to receive(:resume_round?)
#         .and_return(false)
#       subject.instance_variable_set(:@game_engine, game_engine)
#       expect(subject.send(:executable?)).to be(false)
#     end
#   end

#   context '#before_all' do
#     before 'all' do
#       subject.instance_variable_set(:@entities, [entity])
#       allow(game_engine)
#         .to receive(:new_round?)
#         .and_return(true)
#       allow(subject)
#         .to receive(:last_round_save)
#         .and_return(save)
#       subject.instance_variable_set(:@game_engine, game_engine)
#       subject.send(:before_all)
#     end

#     it 'should load save from game engine' do
#       expect(subject.instance_variable_get(:@save)).to eq(save)
#     end

#     it 'should load and cache stage data from game engine' do
#       expect(subject.instance_variable_get(:@pocket_index)).to eq(pocket_index)
#       expect(subject.instance_variable_get(:@odds)).to eq(odds)
#     end
#   end

#   context '#on_load' do
#     before 'all' do
#       expect(entity.find(Transmission::Component::PocketInfo).pocket_index)
#         .to eq(nil)
#       expect(entity.find(Transmission::Component::PocketInfo).odds)
#         .to eq(nil)
#       subject.instance_variable_set(:@pocket_index, pocket_index)
#       subject.instance_variable_set(:@odds, odds)
#       subject.send(:on_load, entity)
#     end

#     it 'should load pocket' do
#       expect(entity.find(Transmission::Component::PocketInfo).pocket_index)
#         .to eq(pocket_index)
#       expect(entity.find(Transmission::Component::PocketInfo).odds)
#         .to eq(odds)
#     end
#   end
# end
