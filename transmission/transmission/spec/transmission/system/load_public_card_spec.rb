# frozen_string_literal: true

describe Transmission::System::LoadPublicCard do
  subject { described_class.new(nil, nil) }

  it 'should be kind of CommandSystem' do
    expect(subject).to be_kind_of(Clutch::System::CommandSystem)
  end

  it 'should config watch component(s)' do
    expect_classes = [
      Transmission::Component::PublicCard,
    ]
    expect(described_class::WATCHS).to eq(expect_classes)
  end

  context 'command' do
    it 'should config method to query_game command' do
      expect(described_class.tasks['query_game']).to eq(:on_load)
    end
  end

  context '#executable?' do
    it 'should pass for resume round' do
    end

    it 'should pass for load last round' do
    end

    it 'should not pass for new game' do
    end
  end

  context '#before_all' do
    it 'should load save from game engine' do
    end

    it 'should load and cache stage data from game engine' do
    end
  end

  context '#on_load' do
    it 'should load player card' do
    end
  end
end
