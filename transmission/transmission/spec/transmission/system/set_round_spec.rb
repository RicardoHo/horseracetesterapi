# frozen_string_literal: true

describe Transmission::System::SetRound do
  let(:game_engine) do
    wrapper = Transmission::GameEngineWrapper.new(978597675)
    wrapper.create(nil, nil)

    wrapper
  end
  let(:round) { Transmission::Component::Round.new }
  let(:entity) { Clutch::Entity.new(round) }
  let(:current_round_id) { game_engine.current_round.ref_id }
  let(:last_round_id) { 123 }

  subject do
    instance = described_class.new(nil, nil)
    instance.instance_variable_set(:@game_engine, game_engine)

    instance
  end

  it 'should be kind of CommandSystem' do
    expect(subject).to be_kind_of(Clutch::System::CommandSystem)
  end

  it 'should config watch component(s)' do
    expect_classes = [Transmission::Component::Round]
    expect(described_class::WATCHS).to eq(expect_classes)
  end

  context 'command' do
    it 'should config method to query_game command' do
      expect(described_class.tasks['query_game']).to eq(:last_or_new_round_id)
    end

    it 'should config method to deal command' do
      expect(described_class.tasks['deal']).to eq(:new_round_id)
    end

    it 'should config method to bet command' do
      expect(described_class.tasks['bet']).to eq(:new_round_id)
    end

    it 'should config method to baccarat_deal command' do
      expect(described_class.tasks['baccarat_deal']).to eq(:new_round_id)
    end

    it 'should config method to roll command' do
      expect(described_class.tasks['roll']).to eq(:new_round_id)
    end

    it 'should config method to drop command' do
      expect(described_class.tasks['drop']).to eq(:new_round_id)
    end
  end

  context '#last_or_new_round_id' do
    context 'last round' do
      it 'should set last round ref id' do
        allow(game_engine.last_round)
          .to receive(:ref_id)
          .and_return(last_round_id)
        allow(game_engine)
          .to receive(:new_round?)
          .and_return(true)
        subject.send(:last_or_new_round_id, entity)
        expect(round.id).to eq(last_round_id)
      end
    end

    context 'new round' do
      it 'should set current round ref id' do
        allow(game_engine)
          .to receive(:new_round?)
          .and_return(false)
        subject.send(:last_or_new_round_id, entity)
        expect(round.id).to eq(current_round_id)
      end
    end
  end

  context '#new_round_id' do
    it 'should set current round ref id' do
      subject.send(:new_round_id, entity)
      expect(round.id).to eq(current_round_id)
    end
  end
end
