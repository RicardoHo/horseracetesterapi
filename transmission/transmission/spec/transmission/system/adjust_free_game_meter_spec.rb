# frozen_string_literal: true

# describe Transmission::System::AdjustFreeGameMeter do
#   subject { described_class.new(nil, nil) }
#   let(:game_engine) do
#     wrapper = Transmission::GameEngineWrapper.new(950404013)
#     wrapper.create(nil, nil)

#     wrapper
#   end
#   let(:game_def) do
#     Transmission::GameDefinition.new(
#       Transmission::Util::GameDefinitionUtil
#       .load(950404013)
#     )
#   end
#   let(:max_free_game_count) do
#     game_def
#       .math_model
#       .model(game_engine.game_config.math_name)
#       .dig(:max_free_game_count)
#   end
#   let(:paytable) do
#     game_def
#       .math_model
#       .model(game_engine.game_config.math_name)
#       .dig(:paytable)
#   end
#   let(:id_box) { Transmission::Component::IdBox.new.tap(&:changes_applied) }
#   let(:stage) { Transmission::Component::Stage.new.tap(&:changes_applied) }
#   let(:free_game_meter) { Transmission::Component::FreeGameMeter.new.tap(&:changes_applied) }
#   let(:selected_id_list) { Transmission::Component::SelectedIdList.new.tap(&:changes_applied) }
#   let(:entity) { Clutch::Entity.new(id_box) }

#   subject do
#     instance = described_class.new(nil, nil)
#     instance.instance_variable_set(:@game_engine, game_engine)
#     instance.instance_variable_set(:@game_def, game_def)
#     instance.instance_variable_set(:@stage, stage)
#     instance.instance_variable_set(:@free_game_meter, free_game_meter)
#     instance.instance_variable_set(:@selected_id_list, selected_id_list)

#     instance
#   end

#   it 'should be kind of ReactiveSystem' do
#     expect(subject).to be_kind_of(Clutch::System::ReactiveSystem)
#   end

#   it 'should config watch component(s)' do
#     expect_classes = [
#       Transmission::Component::IdBox,
#     ]

#     expect(described_class::WATCHS).to eq(expect_classes)
#   end

#   it 'should config reference component(s)' do
#     expect_classes = [
#       Transmission::Component::Stage,
#       Transmission::Component::FreeGameMeter,
#       Transmission::Component::SelectedIdList,
#     ]
#     expect(described_class::REFERENCE).to match_array(expect_classes)
#   end

#   context '#executable?' do
#     it 'should pass for draw event' do
#       subject.instance_variable_set(:@command, 'draw')
#       expect(subject.send(:executable?)).to be(true)
#     end

#     it 'should not pass for query game event' do
#       subject.instance_variable_set(:@command, 'query_game')
#       expect(subject.send(:executable?)).to be(false)
#     end

#     it 'should not pass for draw finish event' do
#       subject.instance_variable_set(:@command, 'draw_finish')
#       expect(subject.send(:executable?)).to be(false)
#     end
#   end

#   context '#before_process' do
#     before 'each' do
#       subject.instance_variable_set(:@entities, [entity])
#       subject.entities_handle = [entity]

#       subject.send(:before_process)
#     end

#     it 'should set instance variable @stage' do
#       expect(subject.instance_variable_get(:@stage)).to eq(stage)
#     end

#     it 'should set instance variable @free_game_meter' do
#       expect(subject.instance_variable_get(:@free_game_meter)).to eq(free_game_meter)
#     end

#     it 'should set instance variable @selected_id_list' do
#       expect(subject.instance_variable_get(:@selected_id_list)).to eq(selected_id_list)
#     end

#     it 'should set instance variable @max_free_game_count' do
#       expect(subject.instance_variable_get(:@max_free_game_count)).to eq(max_free_game_count)
#     end

#     it 'should set instance variable @system_draw_size' do
#       expect(subject.instance_variable_get(:@paytable)).to eq(paytable)
#     end
#   end

#   context '#process' do
#     before 'all' do
#       subject.instance_variable_set(:@max_free_game_count, max_free_game_count)
#       subject.instance_variable_set(:@paytable, paytable)
#     end

#     context 'current round is Free Game' do
#       before 'all' do
#         stage.current = 'freeGame'
#         free_game_meter.counter = max_free_game_count

#         subject.send(:process, entity)
#       end

#       it 'should not trigger Free Game' do
#         expect(subject.instance_variable_get(:@free_game_meter).counter).to eq(max_free_game_count - 1)
#         expect(entity.find(Transmission::Component::IdBox).hit_free_game).to be(nil)
#       end
#     end

#     context 'current round is Free Game' do
#       before 'all' do
#         stage.current = 'baseGame'
#         free_game_meter.counter = 0
#       end

#       it 'should set the meter to max when hit free game' do
#         id_box.result_id = [10, 20, 30]
#         id_box.hit_id = [10, 20, 30]
#         id_box.hit_counter = 3
#         selected_id_list.list = [10, 20, 30]
#         subject.send(:process, entity)

#         expect(subject.instance_variable_get(:@free_game_meter).counter).to eq(max_free_game_count)
#         expect(entity.find(Transmission::Component::IdBox).hit_free_game).to be(true)
#       end

#       it 'should set the meter to 0 when do not hit free game' do
#         id_box.result_id = [10, 20, 30, 40]
#         id_box.hit_id = [10, 20, 30]
#         id_box.hit_counter = 3
#         selected_id_list.list = [10, 20, 30]
#         subject.send(:process, entity)

#         expect(subject.instance_variable_get(:@free_game_meter).counter).to eq(0)
#         expect(entity.find(Transmission::Component::IdBox).hit_free_game).to be(nil)
#       end
#     end
#   end
# end
