# frozen_string_literal: true

# describe Transmission::System::DrawIdList do
#   subject { described_class.new(nil, nil) }
#   let(:game_engine) do
#     wrapper = Transmission::GameEngineWrapper.new(950404013)
#     wrapper.create(nil, nil)

#     wrapper
#   end
#   let(:game_def) do
#     Transmission::GameDefinition.new(
#       Transmission::Util::GameDefinitionUtil
#       .load(950404013)
#     )
#   end
#   let(:system_draw_size) do
#     game_def
#       .math_model
#       .model(game_engine.game_config.math_name)
#       .dig(:system_draw_size)
#   end
#   let(:id_upper_limit) do
#     game_def
#       .math_model
#       .model(game_engine.game_config.math_name)
#       .dig(:id_upper_limit)
#   end
#   let(:selected_id_list) { Transmission::Component::SelectedIdList.new.tap(&:changes_applied) }
#   let(:free_game_meter) { Transmission::Component::FreeGameMeter.new.tap(&:changes_applied) }
#   let(:stage) { Transmission::Component::Stage.new.tap(&:changes_applied) }
#   let(:id_box) { Transmission::Component::IdBox.new.tap(&:changes_applied) }
#   let(:entity) { Clutch::Entity.new(selected_id_list) }

#   subject do
#     instance = described_class.new(nil, nil)
#     instance.instance_variable_set(:@game_engine, game_engine)
#     instance.instance_variable_set(:@game_def, game_def)
#     instance.instance_variable_set(:@free_game_meter, free_game_meter)
#     instance.instance_variable_set(:@stage, stage)
#     instance.instance_variable_set(:@id_box, id_box)

#     instance
#   end

#   it 'should be kind of ReactiveSystem' do
#     expect(subject).to be_kind_of(Clutch::System::ReactiveSystem)
#   end

#   it 'should config watch component(s)' do
#     expect_classes = [
#       Transmission::Component::SelectedIdList,
#     ]

#     expect(described_class::WATCHS).to eq(expect_classes)
#   end

#   it 'should config reference component(s)' do
#     expect_classes = [
#       Transmission::Component::FreeGameMeter,
#       Transmission::Component::Stage,
#       Transmission::Component::IdBox,
#     ]
#     expect(described_class::REFERENCE).to match_array(expect_classes)
#   end

#   context '#executable?' do
#     it 'should pass for draw event' do
#       subject.instance_variable_set(:@command, 'draw')
#       expect(subject.send(:executable?)).to be(true)
#     end

#     it 'should not pass for query game event' do
#       subject.instance_variable_set(:@command, 'query_game')
#       expect(subject.send(:executable?)).to be(false)
#     end

#     it 'should not pass for draw finish event' do
#       subject.instance_variable_set(:@command, 'draw_finish')
#       expect(subject.send(:executable?)).to be(false)
#     end
#   end

#   context '#before_all' do
#     before 'each' do
#       subject.instance_variable_set(:@entities, [entity])
#       subject.entities_handle = [entity]

#       subject.send(:before_all)
#     end

#     it 'should set instance variable @system_draw_size' do
#       expect(subject.instance_variable_get(:@system_draw_size)).to eq(system_draw_size)
#     end

#     it 'should set instance variable @id_upper_limit' do
#       expect(subject.instance_variable_get(:@id_upper_limit)).to eq(id_upper_limit)
#     end

#     it 'should set instance variable @free_game_meter' do
#       expect(subject.instance_variable_get(:@free_game_meter)).to eq(free_game_meter)
#     end

#     it 'should set instance variable @stage' do
#       expect(subject.instance_variable_get(:@stage)).to eq(stage)
#     end

#     it 'should set instance variable @id_box' do
#       expect(subject.instance_variable_get(:@id_box)).to eq(id_box)
#     end
#   end

#   context '#process' do
#     before 'all' do
#       selected_id_list.list = [10, 20, 30, 40, 50]
#       subject.instance_variable_set(:@system_draw_size, system_draw_size)
#       subject.instance_variable_set(:@id_upper_limit, id_upper_limit)
#     end

#     it 'should randomly to generate result id list' do
#       subject.send(:process, entity)

#       expect(subject.instance_variable_get(:@id_box).result_id.length).to eq(system_draw_size)

#       subject.instance_variable_get(:@id_box).result_id.each do |id|
#         expect(id).to be_between(0, id_upper_limit).inclusive
#       end
#     end

#     it 'should find out hit id list and how many id were hit' do
#       allow(id_box)
#         .to receive(:result_id)
#         .and_return([10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29])
#       subject.send(:process, entity)

#       expect(subject.instance_variable_get(:@id_box).hit_id)
#         .to eq([10, 20])

#       expect(subject.instance_variable_get(:@id_box).hit_counter)
#         .to eq(2)
#     end
#   end
# end
