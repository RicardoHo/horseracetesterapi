# frozen_string_literal: true

describe Transmission::System::CalculateSpecial8BestHand do
  subject { described_class.new(nil, nil) }

  let(:entity) do
    Clutch::Entity.new(
      Transmission::Component::HandCard.new(3),
      Transmission::Component::BestHand.new
    )
  end

  let(:ante_paytable) do
    {
      "triple_8": {
        "odds": 168,
      },
      "straight_flush_with_8": {
        "odds": 99,
      },
      "straight_flush": {
        "odds": 33,
      },
      "three_of_a_kind": {
        "odds": 28,
      },
      "straight_with_8": {
        "odds": 15,
      },
      "flush_with_8": {
        "odds": 9,
      },
      "double_8": {
        "odds": 8,
      },
      "straight": {
        "odds": 5,
      },
      "flush": {
        "odds": 3,
      },
      "single_8": {
        "odds": 1,
      },
    }.deep_symbolize_keys
  end

  it 'should be kind of ReactiveSystem' do
    expect(subject).to be_kind_of(Clutch::System::ReactiveSystem)
  end

  it 'should config watch component(s)' do
    expect_classes = []
    expect_classes << Transmission::Component::HandCard
    expect_classes << Transmission::Component::BestHand

    expect(described_class::WATCHS).to eq(expect_classes)
  end

  context '#executable?' do
    it 'should pass for bet event' do
      subject.instance_variable_set(:@command, 'bet')
      expect(subject.send(:executable?)).to be_truthy
    end
  end

  # context '#process' do
  #   it 'should call calculate_best_hand once' do
  #     expect(subject).to receive(:calculate_best_hand).once
  #     subject.send(:process, entity)
  #   end
  # end

  context '#triple_8?' do
    before 'all' do
      subject.instance_variable_set(:@ante_paytable, ante_paytable)
    end

    it 'should return false when cards is [As, 3d, 4h]' do
      cards = ['As', '3d', '4h']
      expect(subject.send(:triple_8?, cards)).to eql(false)
    end

    it 'should return false when cards is [8s, Ad, 2h]' do
      cards = ['8s', 'Ad', '2h']
      expect(subject.send(:triple_8?, cards)).to eql(false)
    end

    it 'should return false when cards is [8s, 8d, 3h]' do
      cards = ['8s', '8d', '3h']
      expect(subject.send(:triple_8?, cards)).to eql(false)
    end

    it 'should return true when cards is [8s, 8d, 8h]' do
      cards = ['8s', '8d', '8h']
      expect(subject.send(:triple_8?, cards)).to eql(true)
    end
  end

  context '#double_8?' do
    before 'all' do
      subject.instance_variable_set(:@ante_paytable, ante_paytable)
    end

    it 'should return false when cards is [As, 3d, 4h]' do
      cards = ['As', '3d', '4h']
      expect(subject.send(:double_8?, cards)).to eql(false)
    end

    it 'should return false when cards is [8s, Ad, 2h]' do
      cards = ['8s', 'Ad', '2h']
      expect(subject.send(:double_8?, cards)).to eql(false)
    end

    it 'should return true when cards is [8s, 8d, 3h]' do
      cards = ['8s', '8d', '3h']
      expect(subject.send(:double_8?, cards)).to eql(true)
    end

    it 'should return false when cards is [8s, 8d, 8h]' do
      cards = ['8s', '8d', '8h']
      expect(subject.send(:double_8?, cards)).to eql(false)
    end
  end

  context '#single_8?' do
    before 'all' do
      subject.instance_variable_set(:@ante_paytable, ante_paytable)
    end

    it 'should return false when cards is [As, 3d, 4h]' do
      cards = ['As', '3d', '4h']
      expect(subject.send(:single_8?, cards)).to eql(false)
    end

    it 'should return true when cards is [8s, Ad, 2h]' do
      cards = ['8s', 'Ad', '2h']
      expect(subject.send(:single_8?, cards)).to eql(true)
    end

    it 'should return false when cards is [8s, 8d, 3h]' do
      cards = ['8s', '8d', '3h']
      expect(subject.send(:single_8?, cards)).to eql(false)
    end

    it 'should return false when cards is [8s, 8d, 8h]' do
      cards = ['8s', '8d', '8h']
      expect(subject.send(:single_8?, cards)).to eql(false)
    end
  end

  context '#three_of_a_kind?' do
    before 'all' do
      subject.instance_variable_set(:@ante_paytable, ante_paytable)
    end

    it 'should return false when cards is [3s, 4d, 5h]' do
      cards = ['3s', '4d', '5h']
      expect(subject.send(:three_of_a_kind?, cards)).to eql(false)
    end

    it 'should return false when cards is [3s, 3d, 5h]' do
      cards = ['3s', '3d', '5h']
      expect(subject.send(:three_of_a_kind?, cards)).to eql(false)
    end

    it 'should return true when cards is [3s, 3d, 3h]' do
      cards = ['3s', '3d', '3h']
      expect(subject.send(:three_of_a_kind?, cards)).to eql(true)
    end
  end

  context '#straight_flush_with_8?' do
    before 'all' do
      subject.instance_variable_set(:@ante_paytable, ante_paytable)
    end

    it 'should return false when cards is [7s, 8d, 9h]' do
      cards = ['7s', '8d', '9h']
      expect(subject.send(:straight_flush_with_8?, cards)).to eql(false)
    end

    it 'should return true when cards is [7s, 8s, 9s]' do
      cards = ['7s', '8s', '9s']
      expect(subject.send(:straight_flush_with_8?, cards)).to eql(true)
    end
  end

  context '#straight_with_8?' do
    before 'all' do
      subject.instance_variable_set(:@ante_paytable, ante_paytable)
    end

    it 'should return false when cards is [6s, 7d, 5h]' do
      cards = ['6s', '7d', '5h']
      expect(subject.send(:straight_with_8?, cards)).to eql(false)
    end

    it 'should return true when cards is [7s, 8d, 9s]' do
      cards = ['7s', '8d', '9s']
      expect(subject.send(:straight_with_8?, cards)).to eql(true)
    end
  end

  context '#flush_with_8?' do
    before 'all' do
      subject.instance_variable_set(:@ante_paytable, ante_paytable)
    end

    it 'should return false when cards is [7s, 8d, 9s]' do
      cards = ['7s', '8d', '9s']
      expect(subject.send(:flush_with_8?, cards)).to eql(false)
    end

    it 'should return true when cards is [7s, 8s, 9s]' do
      cards = ['7s', '8s', '9s']
      expect(subject.send(:flush_with_8?, cards)).to eql(true)
    end
  end

  context '#straight_flush?' do
    before 'all' do
      subject.instance_variable_set(:@ante_paytable, ante_paytable)
    end

    it 'should return false when cards is [4s, 5d, 6h]' do
      cards = ['4s', '5d', '6h']
      expect(subject.send(:straight_flush?, cards)).to eql(false)
    end

    it 'should return true when cards is [7s, 8s, 9s]' do
      cards = ['4s', '5s', '6s']
      expect(subject.send(:straight_flush?, cards)).to eql(true)
    end
  end

  context '#straight?' do
    before 'all' do
      subject.instance_variable_set(:@ante_paytable, ante_paytable)
    end

    it 'should return false when cards is [As, 3d, 4h]' do
      cards = ['As', '3d', '4h']
      expect(subject.send(:straight?, cards)).to eql(false)
    end

    it 'should return false when cards is [Ks, Ad, 2h]' do
      cards = ['Ks', 'Ad', '2h']
      expect(subject.send(:straight?, cards)).to eql(false)
    end

    it 'should return true when cards is [As, 2d, 3h]' do
      cards = ['As', '2d', '3h']
      expect(subject.send(:straight?, cards)).to eql(true)
    end

    it 'should return true when cards is [7s, 8d, 9h]' do
      cards = ['7s', '8d', '9h']
      expect(subject.send(:straight?, cards)).to eql(true)
    end

    it 'should return true when cards is [Qs, Kd, Ah]' do
      cards = ['Qs', 'Kd', 'Ah']
      expect(subject.send(:straight?, cards)).to eql(true)
    end
  end

  context '#flush?' do
    before 'all' do
      subject.instance_variable_set(:@ante_paytable, ante_paytable)
    end

    it 'should return false when cards is [As, 3d, 4h]' do
      cards = ['As', '3d', '4h']
      expect(subject.send(:flush?, cards)).to eql(false)
    end

    it 'should return false when cards is [As, 3s, 4h]' do
      cards = ['As', '3s', '4h']
      expect(subject.send(:flush?, cards)).to eql(false)
    end

    it 'should return true when cards is [Ah, 2h, 3h]' do
      cards = ['Ah', '2h', '3h']
      expect(subject.send(:flush?, cards)).to eql(true)
    end

    it 'should return true when cards is [Ad, 2d, 3d]' do
      cards = ['Ad', '2d', '3d']
      expect(subject.send(:flush?, cards)).to eql(true)
    end

    it 'should return true when cards is [As, 2s, 3s]' do
      cards = ['As', '2s', '3s']
      expect(subject.send(:flush?, cards)).to eql(true)
    end

    it 'should return true when cards is [Ac, 2c, 3c]' do
      cards = ['Ac', '2c', '3c']
      expect(subject.send(:flush?, cards)).to eql(true)
    end
  end

  context '#calculate_best_hand' do
    before 'all' do
      subject.instance_variable_set(:@ante_paytable, ante_paytable)
    end

    it 'should return \'nothing\' when cards is [As, 3d, 4h]' do
      cards = ['As', '3d', '4h']
      expect(subject.send(:calculate_best_hand, cards)).to eql('nothing')
    end

    it 'should return \'triple_8\' when cards is [8s, 8d, 8h]' do
      cards = ['8s', '8d', '8h']
      expect(subject.send(:calculate_best_hand, cards)).to eql('triple_8')
    end

    it 'should return \'double_8\' when cards is [8s, 8d, Ah]' do
      cards = ['8s', '8d', 'Ah']
      expect(subject.send(:calculate_best_hand, cards)).to eql('double_8')
    end

    it 'should return \'three_of_a_kind\' when cards is [As, Ad, Ah]' do
      cards = ['As', 'Ad', 'Ah']
      expect(subject.send(:calculate_best_hand, cards)).to eql('three_of_a_kind')
    end

    it 'should return \'straight\' when cards is [As, 2d, 3h]' do
      cards = ['As', '2d', '3h']
      expect(subject.send(:calculate_best_hand, cards)).to eql('straight')
    end

    it 'should return \'straight\' when cards is [As, 2d, 3h]' do
      cards = ['As', '2d', '3h']
      expect(subject.send(:calculate_best_hand, cards)).to eql('straight')
    end

    it 'should return \'straight\' when cards is [Qh, Kd, Ah]' do
      cards = ['Qh', 'Kd', 'Ah']
      expect(subject.send(:calculate_best_hand, cards)).to eql('straight')
    end

    it 'should return \'straight_flush\' when cards is [Ah, 2h, 3h]' do
      cards = ['Ah', '2h', '3h']
      expect(subject.send(:calculate_best_hand, cards)).to eql('straight_flush')
    end

    it 'should return \'straight_with_8\' when cards is [8s, 7d, 6h]' do
      cards = ['8s', '7d', '6h']
      expect(subject.send(:calculate_best_hand, cards)).to eql('straight_with_8')
    end

    it 'should return \'flush_with_8\' when cards is [Ah, 3h, 8h]' do
      cards = ['Ah', '3h', '8h']
      expect(subject.send(:calculate_best_hand, cards)).to eql('flush_with_8')
    end

    it 'should return \'straight_flush_with_8\' when cards is [7h, 8h, 6h]' do
      cards = ['7h', '8h', '6h']
      expect(subject.send(:calculate_best_hand, cards)).to eql('straight_flush_with_8')
    end

    it 'should return \'single_8\' when cards is [Ah, 8s, Kd]' do
      cards = ['Ah', '8s', 'Kd']
      expect(subject.send(:calculate_best_hand, cards)).to eql('single_8')
    end
  end
end
