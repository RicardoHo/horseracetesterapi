# frozen_string_literal: true

describe Transmission::System::Bet do
  subject { described_class.new(nil, nil) }
  let(:bet_area_label_test) { Transmission::Component::BetAreaLabel.new('test').tap(&:changes_applied) }
  let(:bet_area_label_ante) { Transmission::Component::BetAreaLabel.new('ante').tap(&:changes_applied) }
  let(:bet_area_label_aa) { Transmission::Component::BetAreaLabel.new('aa').tap(&:changes_applied) }
  let(:bet_amount) { Transmission::Component::BetAmount.new.tap(&:changes_applied) }
  let(:max_bet) { Transmission::Component::MaxBet.new(500).tap(&:changes_applied) }
  let(:min_bet) { Transmission::Component::MinBet.new(100).tap(&:changes_applied) }
  let(:entity_test) { Clutch::Entity.new(bet_area_label_test, bet_amount, max_bet, min_bet) }
  let(:entity_ante) { Clutch::Entity.new(bet_area_label_ante, bet_amount, max_bet, min_bet) }
  let(:entity_aa) { Clutch::Entity.new(bet_area_label_aa, bet_amount, max_bet, min_bet) }
  let(:denom_info) do
    denom_info = Transmission::Component::DenomInfo.new
    denom_info.denoms = [1, 5, 10, 50, 100]
    denom_info.denom_index = 0
    denom_info.denom = 1

    denom_info
  end

  it 'should be kind of CommandSystem' do
    expect(subject).to be_kind_of(Clutch::System::CommandSystem)
  end

  it 'should config watch component(s)' do
    expect_classes = [
      Transmission::Component::BetAreaLabel,
      Transmission::Component::BetAmount,
      Transmission::Component::MaxBet,
      Transmission::Component::MinBet,
    ]
    expect(described_class::WATCHS).to eq(expect_classes)
  end

  it 'should config reference component(s)' do
    expect_classes = [
      Transmission::Component::DenomInfo,
    ]
    expect(described_class::REFERENCE).to eq(expect_classes)
  end

  context 'command' do
    it 'should config method to deal command' do
      expect(described_class.tasks['deal']).to eq(:ante_and_aa_bet)
    end

    it 'should config method to bet command' do
      expect(described_class.tasks['bet']).to eq(:ante)
    end

    it 'should config method to baccarat_deal command' do
      expect(described_class.tasks['baccarat_deal']).to eq(:bet_on_bet_area)
    end

    it 'should config method to roll command' do
      expect(described_class.tasks['roll']).to eq(:bet_on_bet_area)
    end

    it 'should config method to drop command' do
      expect(described_class.tasks['drop']).to eq(:bet_on_bet_area)
    end
  end

  context '#executable?' do
    before 'all' do
      subject.instance_variable_set(:@entities, [entity_test, entity_ante, entity_aa])
    end

    it 'should pass for input bet_options exist' do
      subject.instance_variable_set(:@command, 'deal')
      subject.instance_variable_set(:@input, OpenStruct.new(bet_options: OpenStruct.new(name: 'test')))
      expect(subject.send(:executable?)).to be_truthy
    end

    it 'should not pass for input bet_options not exist' do
      subject.instance_variable_set(:@command, 'deal')
      subject.instance_variable_set(:@input, OpenStruct.new(bet_options: nil))
      expect(subject.send(:executable?)).to be_falsey
    end
  end

  context '#before_all' do
    before 'all' do
      subject.instance_variable_set(:@entities, [entity_test, entity_ante, entity_aa])
      allow(subject)
        .to receive(:references)
        .and_return(denom_info: [denom_info])
      subject.send(:before_all)
    end

    it 'should set instance variable @denom_info' do
      expect(subject.instance_variable_get(:@denom_info)).to be_instance_of(Transmission::Component::DenomInfo)
    end
  end

  context 'bet' do
    before 'all' do
      subject.instance_variable_set(:@denom_info, denom_info)
    end

    context '#ante_and_aa_bet' do
      it 'should set bet amount' do
        subject.instance_variable_set(
          :@input,
          OpenStruct.new(
            bet_options: [
              OpenStruct.new(
                name: 'ante',
                bet_amount: OpenStruct.new(credit: 200)
              ),
              OpenStruct.new(
                name: 'aa',
                bet_amount: OpenStruct.new(credit: 100)
              ),
            ]
          )
        )
        subject.send(:ante_and_aa_bet, entity_ante)
        expect(entity_ante.find(Transmission::Component::BetAmount).cash).to eq(200)
        expect(entity_ante.find(Transmission::Component::BetAmount).credit).to eq(200)
        subject.send(:ante_and_aa_bet, entity_aa)
        expect(entity_aa.find(Transmission::Component::BetAmount).cash).to eq(100)
        expect(entity_aa.find(Transmission::Component::BetAmount).credit).to eq(100)
      end
    end

    context '#ante' do
      it 'should set bet amount' do
        subject.instance_variable_set(
          :@input,
          OpenStruct.new(
            bet_options: [
              OpenStruct.new(
                name: 'ante',
                bet_amount: OpenStruct.new(credit: 100)
              ),
            ]
          )
        )
        subject.send(:ante, entity_ante)
        expect(entity_ante.find(Transmission::Component::BetAmount).cash).to eq(100)
        expect(entity_ante.find(Transmission::Component::BetAmount).credit).to eq(100)
      end
    end

    context '#baccarat_bet' do
      it 'should set bet amount' do
        subject.instance_variable_set(
          :@input,
          OpenStruct.new(
            bet_options: [
              OpenStruct.new(
                name: 'test',
                bet_amount: OpenStruct.new(credit: 100)
              ),
            ]
          )
        )
        subject.send(:baccarat_bet, entity_test)
        expect(entity_test.find(Transmission::Component::BetAmount).cash).to eq(100)
        expect(entity_test.find(Transmission::Component::BetAmount).credit).to eq(100)
      end
    end
  end
end
