# frozen_string_literal: true

describe Transmission::System::CalculateAntePayout do
  subject { described_class.new(nil, nil) }

  let(:ante_paytable) do
    {
      "triple_8": {
        "odds": 168,
      },
      "straight_flush_with_8": {
        "odds": 99,
      },
      "straight_flush": {
        "odds": 33,
      },
      "three_of_a_kind": {
        "odds": 28,
      },
      "straight_with_8": {
        "odds": 15,
      },
      "flush_with_8": {
        "odds": 9,
      },
      "double_8": {
        "odds": 8,
      },
      "straight": {
        "odds": 5,
      },
      "flush": {
        "odds": 3,
      },
      "single_8": {
        "odds": 1,
      },
    }.deep_symbolize_keys
  end

  let(:best_hand) do
    Transmission::Component::BestHand.new
  end

  let(:bet_amount) do
    bet_amount = Transmission::Component::BetAmount.new
    bet_amount.credit = 1
    bet_amount.cash   = 10

    bet_amount
  end

  let(:entity) do
    Clutch::Entity.new(
      bet_amount,
      Transmission::Component::Payout.new
    )
  end

  it 'should be kind of ReactiveSystem' do
    expect(subject).to be_kind_of(Clutch::System::ReactiveSystem)
  end

  it 'should config watch component(s)' do
    expect_classes = []
    expect_classes << Transmission::Component::BetAmount
    expect_classes << Transmission::Component::Payout

    expect(described_class::WATCHS).to eq(expect_classes)
  end

  it 'should config reference component(s)' do
    expect_classes = [Transmission::Component::BestHand]
    expect(described_class::REFERENCE).to eq(expect_classes)
  end

  context '#before_all' do
    before 'each' do
    end

    it 'should set instance variable @ante_paytable' do
    end
  end

  context '#compose_multiplier' do
    before 'each' do
      subject.instance_variable_set(:@best_hand, best_hand)
      subject.instance_variable_set(:@ante_paytable, ante_paytable)
    end

    context 'when best hand is triple_8' do
      before 'each' do
        best_hand.rank = 'triple_8'
      end

      it 'should return the answer 168' do
        expect(subject.send(:compose_multiplier)).to eq(168)
      end
    end

    context 'when best hand is straight_flush_with_8' do
      before 'each' do
        best_hand.rank = 'straight_flush_with_8'
      end

      it 'should return the answer 99' do
        expect(subject.send(:compose_multiplier)).to eq(99)
      end
    end

    context 'when best hand is nothing' do
      before 'each' do
        best_hand.rank = 'nothing'
      end

      it 'should return the answer nil' do
        expect(subject.send(:compose_multiplier)).to eq(nil)
      end
    end
  end

  context '#process' do
    context 'when compose_multiplier return a integer (10) and bet credit (1)' do
      before 'all' do
        allow(subject)
          .to receive(:compose_multiplier)
          .and_return(10)
      end

      it 'should calculate the correct payout credit (10)' do
        subject.send(:process, entity)
        expect(entity.find(Transmission::Component::Payout).credit).to eq(10)
        expect(entity.find(Transmission::Component::Payout).cash).to eq(100)
      end
    end

    context 'when compose_multiplier return nil' do
      before 'all' do
        allow(subject)
          .to receive(:compose_multiplier)
          .and_return(nil)
      end

      it 'should calculate the correct payout credit (0)' do
        subject.send(:process, entity)
        expect(entity.find(Transmission::Component::Payout).credit).to eq(0)
        expect(entity.find(Transmission::Component::Payout).cash).to eq(0)
      end
    end
  end
end
