# frozen_string_literal: true

describe Transmission::System::DealtPlayerCard do
  subject { described_class.new(nil, nil) }

  let(:deck) { Transmission::Component::CardDeck.new }
  let(:tray) { Transmission::Component::DiscardTray.new }
  let(:ref_list) do
    {
      card_deck: [deck],
    }
  end

  it 'should be kind of CommandSystem' do
    expect(subject).to be_kind_of(Clutch::System::CommandSystem)
  end

  it 'should config watch component(s)' do
    expect_classes = [Transmission::Component::HandCard]
    expect(described_class::WATCHS).to match_array(expect_classes)
  end

  it 'should config reference component(s)' do
    expect_classes = [
      Transmission::Component::CardDeck,
    ]
    expect(described_class::REFERENCE).to match_array(expect_classes)
  end

  context 'command' do
    it 'should config method to deal command' do
      expect(described_class.tasks['deal']).to eq(:process_dealt)
    end
  end

  context '#before_all' do
    before 'each' do
      allow(subject)
        .to receive(:references)
        .and_return(ref_list)
    end

    it 'should set instance variable @deck' do
      subject.send(:before_all)
      expect(subject.instance_variable_get(:@deck)).to eq(deck)
    end
  end

  context '#process_dealt' do
    it 'should dealt card to all hand_card' do
    end
  end
end
