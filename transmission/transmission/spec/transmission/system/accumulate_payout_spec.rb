# frozen_string_literal: true

describe Transmission::System::AccumulatePayout do
  subject { described_class.new(nil, nil) }
  let(:payout) { Transmission::Component::Payout.new.tap(&:changes_applied) }
  let(:action_win) { Transmission::Component::ActionWin.new }
  let(:entity) { Clutch::Entity.new(payout) }

  subject do
    instance = described_class.new(nil, nil)
    instance.instance_variable_set(:@action_win, action_win)

    instance
  end

  it 'should be kind of ReactiveSystem' do
    expect(subject).to be_kind_of(Clutch::System::ReactiveSystem)
  end

  it 'should config watch component(s)' do
    expect_classes = [Transmission::Component::Payout]

    expect(described_class::WATCHS).to eq(expect_classes)
  end

  it 'should config reference component(s)' do
    expect_classes = [Transmission::Component::ActionWin]

    expect(described_class::REFERENCE).to eq(expect_classes)
  end

  context '#before_all' do
    before 'each' do
      subject.instance_variable_set(:@entities, [entity])
      subject.entities_handle = [entity]
    end

    it 'should set instance variable @action_win' do
      action_win.cash   = 10
      action_win.credit = 10
      subject.instance_variable_set(:@action_win, action_win)
      subject.send(:before_all)

      expect(subject.instance_variable_get(:@action_win).cash).to eq(10)
      expect(subject.instance_variable_get(:@action_win).credit).to eq(10)
    end
  end

  context '#process' do
    context 'when have 3 new payout (10, 20 ,30)' do
      before 'all' do
        payout.cash   = 10
        payout.credit = 10
        subject.send(:process, entity)
        payout.cash   = 20
        payout.credit = 20
        subject.send(:process, entity)
        payout.cash   = 30
        payout.credit = 30
        subject.send(:process, entity)
      end

      it 'should get the correct action win' do
        expect(subject.instance_variable_get(:@action_win).cash).to eq(60)
        expect(subject.instance_variable_get(:@action_win).credit).to eq(60)
      end
    end
  end
end
