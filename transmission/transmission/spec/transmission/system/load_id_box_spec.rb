# frozen_string_literal: true

# describe Transmission::System::LoadIdBox do
#   subject { described_class.new(nil, nil) }
#   let(:game_engine) do
#     wrapper = Transmission::GameEngineWrapper.new(950404013)
#     wrapper.create(nil, nil)

#     wrapper
#   end
#   let(:hit_counter) { 3 }
#   let(:hit_id) { [10, 20, 30] }
#   let(:result_id) { [10, 20, 30, 40, 50, 60, 70, 80] }
#   let(:hit_free_game) { true }
#   let(:id_box) { Transmission::Component::IdBox.new }
#   let(:entity) { Clutch::Entity.new(id_box) }

#   let(:save) do
#     {
#       table: {
#         id_box: {
#           hit_counter: hit_counter,
#           hit_id: hit_id,
#           result_id: result_id,
#           hit_free_game: hit_free_game,
#         },
#       },
#     }
#   end

#   it 'should be kind of CommandSystem' do
#     expect(subject).to be_kind_of(Clutch::System::CommandSystem)
#   end

#   it 'should config watch component(s)' do
#     expect_classes = [Transmission::Component::IdBox]
#     expect(described_class::WATCHS).to eq(expect_classes)
#   end

#   context 'command' do
#     it 'should config method to query_game command' do
#       expect(described_class.tasks['query_game']).to eq(:on_load)
#     end
#   end

#   context '#executable?' do
#     before 'all' do
#       subject.instance_variable_set(:@entities, [entity])
#       subject.instance_variable_set(:@command, 'query_game')
#     end

#     it 'should pass for resume round' do
#       allow(game_engine)
#         .to receive(:resume_round?)
#         .and_return(true)
#       subject.instance_variable_set(:@game_engine, game_engine)
#       expect(subject.send(:executable?)).to be(true)
#     end

#     it 'should pass for load last round' do
#       allow(game_engine)
#         .to receive(:new_round?)
#         .and_return(true)
#       subject.instance_variable_set(:@game_engine, game_engine)
#       expect(subject.send(:executable?)).to be(true)
#     end

#     it 'should not pass for new game' do
#       allow(game_engine)
#         .to receive(:new_round?)
#         .and_return(false)
#       allow(game_engine)
#         .to receive(:resume_round?)
#         .and_return(false)
#       subject.instance_variable_set(:@game_engine, game_engine)
#       expect(subject.send(:executable?)).to be(false)
#     end
#   end

#   context '#before_all' do
#     before 'all' do
#       subject.instance_variable_set(:@entities, [entity])
#       allow(game_engine)
#         .to receive(:new_round?)
#         .and_return(true)
#       allow(subject)
#         .to receive(:last_round_save)
#         .and_return(save)
#       subject.instance_variable_set(:@game_engine, game_engine)
#       subject.send(:before_all)
#     end

#     it 'should load save from game engine' do
#       expect(subject.instance_variable_get(:@save)).to eq(save)
#     end

#     it 'should load and cache stage data from game engine' do
#       expect(subject.instance_variable_get(:@hit_counter)).to eq(hit_counter)
#       expect(subject.instance_variable_get(:@hit_id)).to eq(hit_id)
#       expect(subject.instance_variable_get(:@result_id)).to eq(result_id)
#       expect(subject.instance_variable_get(:@hit_free_game)).to eq(hit_free_game)
#     end
#   end

#   context '#on_load' do
#     before 'all' do
#       expect(entity.find(Transmission::Component::IdBox).hit_counter)
#         .to eq(nil)
#       expect(entity.find(Transmission::Component::IdBox).hit_id)
#         .to eq([])
#       expect(entity.find(Transmission::Component::IdBox).result_id)
#         .to eq([])
#       expect(entity.find(Transmission::Component::IdBox).hit_free_game)
#         .to eq(nil)
#       subject.instance_variable_set(:@hit_counter, hit_counter)
#       subject.instance_variable_set(:@hit_id, hit_id)
#       subject.instance_variable_set(:@result_id, result_id)
#       subject.instance_variable_set(:@hit_free_game, hit_free_game)
#       subject.send(:on_load, entity)
#     end

#     it 'should load score_card' do
#       expect(entity.find(Transmission::Component::IdBox).hit_counter)
#         .to eq(hit_counter)
#       expect(entity.find(Transmission::Component::IdBox).hit_id)
#         .to eq(hit_id)
#       expect(entity.find(Transmission::Component::IdBox).result_id)
#         .to eq(result_id)
#       expect(entity.find(Transmission::Component::IdBox).hit_free_game)
#         .to eq(hit_free_game)
#     end
#   end
# end
