# frozen_string_literal: true

describe Transmission::System::SetupTableLimit do
  let(:game_engine) do
    wrapper = Transmission::GameEngineWrapper.new(622742907)
    wrapper.create(nil, nil)

    wrapper
  end
  let(:denoms) { game_engine.game_config.denoms.value }
  let(:denom_info) do
    denom_info = Transmission::Component::DenomInfo.new
    denom_info.denoms = denoms
    denom_info.denom_index = 0
    denom_info.denom = denoms[0]

    denom_info
  end

  subject do
    instance = described_class.new(nil, nil)
    instance.instance_variable_set(:@game_engine, game_engine)
    instance.instance_variable_set(:@denom_info, denom_info)

    instance
  end

  it 'should be kind of CommandSystem' do
    expect(subject).to be_kind_of(Clutch::System::CommandSystem)
  end

  it 'should config watch component(s)' do
    expect_classes = [
      Transmission::Component::TableLabel,
      Transmission::Component::MaxBet,
      Transmission::Component::MinBet,
    ]
    expect(described_class::WATCHS).to eq(expect_classes)
  end

  it 'should config reference component(s)' do
    expect_classes = [
      Transmission::Component::DenomInfo,
    ]

    expect(described_class::REFERENCE).to eq(expect_classes)
  end

  context 'command' do
    it 'should config method to query_game command' do
      expect(described_class.tasks['query_game']).to eq(:setup)
    end
  end

  context '#before_all' do
    it 'should set instance variable @denom_info' do
      subject.send(:before_all)
      expect(subject.instance_variable_get(:@denom_info)).to eq(denom_info)
    end
  end

  context '#setup' do
    it 'should setup the table max/min bet credit amount' do
      max_bet_comp   = Transmission::Component::MaxBet.new
      min_bet_comp   = Transmission::Component::MinBet.new

      entity = Clutch::Entity.new(max_bet_comp, min_bet_comp)
      subject.send(:setup, entity)
      expect(max_bet_comp.cash).to eq(denoms.max * max_bet_comp.credit)
      expect(min_bet_comp.cash).to eq(denoms.min * min_bet_comp.credit)
    end
  end
end
