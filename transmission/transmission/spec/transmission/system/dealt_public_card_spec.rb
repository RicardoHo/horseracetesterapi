# frozen_string_literal: true

describe Transmission::System::DealtPublicCard do
  let(:game_engine) do
    wrapper = Transmission::GameEngineWrapper.new(970328033)
    wrapper.create(nil, nil)

    wrapper
  end

  let(:subject) do
    instance = described_class.new(nil, nil)
    instance.instance_variable_set(:@game_engine, game_engine)
    instance
  end

  let(:deck) { Transmission::Component::CardDeck.new }
  let(:ref_list) do
    {
      card_deck: [deck],
    }
  end

  it 'should be kind of CommandSystem' do
    expect(subject).to be_kind_of(Clutch::System::CommandSystem)
  end

  it 'should config watch component(s)' do
    expect_classes = [Transmission::Component::PublicCard]
    expect(described_class::WATCHS).to match_array(expect_classes)
  end

  it 'should config reference component(s)' do
    expect_classes = [
      Transmission::Component::CardDeck,
    ]
    expect(described_class::REFERENCE).to match_array(expect_classes)
  end

  context 'command' do
    it 'should config method to deal command' do
      expect(described_class.tasks['deal']).to eq(:process_dealt)
    end

    it 'should config method to raise command' do
      expect(described_class.tasks['deal']).to eq(:process_dealt)
    end

    it 'should config method to fold command' do
      expect(described_class.tasks['deal']).to eq(:process_dealt)
    end
  end

  context '#before_all' do
    before 'all' do
      allow(subject)
        .to receive(:references)
        .and_return(ref_list)
    end

    it 'should set instance variable @deck' do
      allow(subject)
        .to receive(:num_of_card_by_command)
        .and_return('stub')
      allow(subject)
        .to receive(:dealt_public_cards)
        .and_return('stub')
      subject.send(:before_all)
      expect(subject.instance_variable_get(:@deck)).to eq(deck)
    end
  end

  context '#num_of_card_by_command' do
    it 'should return 2 for command deal' do
      subject.instance_variable_set(:@command, :deal)
      expect(subject.send(:num_of_card_by_command)).to eq(3)
    end

    it 'should return 3 for command raise' do
      subject.instance_variable_set(:@command, :raise)
      expect(subject.send(:num_of_card_by_command)).to eq(2)
    end

    it 'should return 3 for command fold' do
      subject.instance_variable_set(:@command, :fold)
      expect(subject.send(:num_of_card_by_command)).to eq(2)
    end
  end

  context '#dealt_public_cards' do
    before 'all' do
      subject.instance_variable_set(:@deck, deck)
      allow(Transmission::Util::RNGUtil)
        .to receive(:draw_multiple_from_weight_table)
        .and_return([['Ad', 'Td'], { fake_card_pool: 1 }])
    end

    it 'dealts 2 cards - Ad and Td' do
      subject.send(:dealt_public_cards, 2)
      dealt_cards = subject.instance_variable_get(:@new_public_cards)
      expect(dealt_cards.size).to eq(2)
      expect(dealt_cards).to eq(['Ad', 'Td'])
    end

    it 'update card pool after dealt' do
      begin_card_pool = deck.card_pool
      subject.send(:dealt_public_cards, 2)
      expect(deck.card_pool).not_to eq(begin_card_pool)
    end
  end

  context '#process_dealt' do
    it 'should collect dealt cards' do
      subject.instance_variable_set(:@new_public_cards, ['Ad', 'Th'])
      component = Transmission::Component::PublicCard.new(5)
      entity = Clutch::Entity.new(component)

      subject.send(:process_dealt, entity)
      expect(component.cards).to eq(
        subject.instance_variable_get(:@new_public_cards)
      )
    end
  end
end
