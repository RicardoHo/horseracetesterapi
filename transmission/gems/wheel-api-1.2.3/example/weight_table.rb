require_relative '../lib/wheel-api.rb'

WheelApi.base_url = 'http://hq-int-gateway-vapp01.laxino.local/wheel_app/'

table = {
  a: 1,
  1 => 2,
  'ws' => 4
}
weight_table = WheelApi.get_weight_table(
  property_id: 1,
  game_id: 1,
  login_name: 'Test'
)
p '==Draw Example(With Replacement)=='
symbols, new_table = weight_table.draw(table: table, num: 10, replacement: true)
p symbols
p new_table

p '==Draw Example(Without Replacement)=='
symbols, new_table = weight_table.draw(table: table, num: 2, replacement: false)
p symbols
p new_table
