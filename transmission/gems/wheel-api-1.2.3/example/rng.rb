require_relative '../lib/wheel-api.rb'

WheelApi.base_url = 'http://hq-int-gateway-vapp01.laxino.local/wheel_app/'

rng = WheelApi.get_rng(
  property_id: 1,
  game_id: 1,
  login_name: 'Test'
)

p '==Draw Example(With Hash)=='
info = {
  num_of_rng: 2,
  limit: 200,
  repeat: true
}
result = rng.draw(
  info
)

p result

p '==Draw Example(With WheelApi::RNG::Info)=='
info = WheelApi::RNG::Info.new(
  num_of_rng: 2,
  limit: 200,
  repeat: true
)

result = rng.draw(info)

p result

p '==Multi Draw Example(Hash)=='

infos = [
  {
    num_of_rng: 2,
    limit: 200,
    repeat: true
  },
  {
    num_of_rng: 3,
    limit: 300,
    repeat: false
  }
]
result = rng.multi_draw(infos)
p result

p '==Multi Draw Example(With WheelApi::RNG::Info)=='
infos = [
  WheelApi::RNG::Info.new(
    num_of_rng: 2,
    limit: 200,
    repeat: true
  ),
  WheelApi::RNG::Info.new(
    num_of_rng: 3,
    limit: 300,
    repeat: false
  )
]
result = rng.multi_draw(infos)
p result
