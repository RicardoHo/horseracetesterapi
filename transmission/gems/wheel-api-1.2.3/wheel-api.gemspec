require File.expand_path('lib/wheel-api/version', __dir__)
# $LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name          = 'wheel-api'
  spec.version       = WheelApi::VERSION
  spec.authors       = ['Cheok Meng Chan']
  spec.email         = ['cheokmeng.chan@laxino.com']

  spec.summary       = 'API Client For Wheel'
  spec.description   = 'A simple client for Wheel Remote API'
  spec.homepage      = 'http://home.rnd.laxino.com'

  spec.require_paths = ['lib']
  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.add_dependency  'bundler',      '>= 1.16'
  spec.add_dependency  'http',         '>= 3.0'
  spec.add_dependency  'multi_json',   '1.14.1'
  # spec.add_dependency  'oj',           '>= 3.6'
  spec.add_dependency  'hashie',       '>= 3.5'
end
