# require 'oj'
require 'multi_json'
require 'hashie'

require_relative 'wheel-api/version'
require_relative 'wheel-api/error'
require_relative 'wheel-api/client'
require_relative 'wheel-api/base'
require_relative 'wheel-api/rng'
require_relative 'wheel-api/weight_table'

module WheelApi
  module ClassMethod
    attr_writer :base_url
    attr_writer :developer_token

    DEFAULT_BASE_URL = 'http://localhost:3000'.freeze
    DEFAULT_DEVELOPER_TOKEN = 'token'.freeze

    def base_url
      @base_url || DEFAULT_BASE_URL
    end

    def developer_token
      @developer_token || DEFAULT_DEVELOPER_TOKEN
    end

    def reset
      @base_url = nil
      @developer_token = nil
    end

    # @return [WheelApi::RNG]
    def get_rng(opts = {})
      RNG.new(opts)
    end

    # @return [WheelApi::WeightTable]
    def get_weight_table(opts = {})
      WeightTable.new(opts)
    end
  end

  extend ClassMethod
end
