module WheelApi
  class WeightTable < Base
    # @param table       [Hash]
    # ex: { a: 2, b: 3 }, key is symbol, value is weight, value must be int
    # @param num         [Integer]
    # @param replacement [Boolean]
    # @return [ symbols, weight_table ] [Array<String>, Hash]
    # replacement mean per draw with use original weight_table or not
    # if replacement is false then return new weight table after draw
    def draw(table:, num: 1, replacement: false, ref_id: nil)
      params = request_base_info
      params[:num_of_symbols] = num
      params[:weight_table] = table
      params[:with_replacement] = replacement
      params.merge!(ref_id: ref_id) if ref_id

      res = perform(params)

      [res[:symbols], res[:latest_weight_table]]
    end

    private

    def request_base_info
      {
        action: 'weight_table',
        developer_token: WheelApi.developer_token
      }
    end
  end
end