module WheelApi
  class RNG < Base
    # @param info [WheelApi::RNG::INFO]
    # @return [Array<Integer>]
    def draw(info, ref_id: nil)
      res = multi_draw([info], ref_id: ref_id)
      res.first
    end

    # @param infos [Array<WheelApi::RNG::Info>]
    # @return [Array<Array<Integer>>]
    def multi_draw(infos = [], ref_id: nil)
      return [] unless infos.is_a?(Array) && !infos.empty?
      infos.map! { |v| v.is_a?(Info) ? v : change_type(v) }
      params = request_base_info
      params[:request_rng_info] = infos
      params.merge!(ref_id: ref_id) if ref_id
      res = perform(params)

      res
    end

    # @param num_of_rng [Integer]
    # @param limit      [Integer]
    # @param repeat     [Boolean]
    # @return [WheelApi::RNG::Info]
    class Info < Hashie::Trash
      include Hashie::Extensions::Dash::Coercion
      include Hashie::Extensions::IgnoreUndeclared

      property :num_of_rng,        required: true,            coerce: Integer
      property :limit,             required: true,            coerce: Integer
      property :repeat,            required: true,            message: 'must be ture or false.'
      coerce_key :repeat, ->(v) do
        v if v.is_a?(TrueClass) || v.is_a?(FalseClass)
      end
    end

    private

    def request_base_info
      {
        action: 'rng',
        developer_token: WheelApi.developer_token
      }
    end

    def change_type(info)
      raise ArgumentError, "info (#{info}) must be Hash." unless info.is_a?(Hash)
      Info.new(info)
    end
  end
end
