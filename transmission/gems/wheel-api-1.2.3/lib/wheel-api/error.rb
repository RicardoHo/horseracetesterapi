module WheelApi
  # Custom error class for rescuing from all WheelApi-API errors
  class Error < StandardError

    class << self
      def from_response( body )
        err_hash = parse_error( body )
        new( err_hash[:description], err_hash[:error_code], err_hash[:code] )
      end

      private
      def parse_error( body )
        MultiJson.load( body, symbolize_keys: true )
      end
    end

    # @return [String]
    attr_reader :code

    # @return [String]
    attr_reader :error_code

    # @return [String]
    attr_reader :description

    def initialize( description = "", error_code = nil, code = nil )
      super( description )
      @code = code
      @error_code = error_code
      @description = description
    end

  end

  # Raised when Remote Call WheelApi Timeout
  class TimeoutError < Error; end

  # Raised when Connect WheelApi Fail
  class ConnectionError < Error; end

  # Raised when WheelApi returns a 4xx HTTP status code
  class ClientError < Error; end

  # Raised when WheelApi returns the HTTP status code 400
  class BadRequestError < ClientError; end

  # Raised when WheelApi returns the HTTP status code 448
  class UnrecoverableError < ClientError; end

  # Raised when WheelApi returns the HTTP status code 449
  class RecoverableError < ClientError; end

  # Raised when WheelApi returns the HTTP status code 450
  class RetryableError < ClientError; end

  # Raised when WheelApi returns a 5xx HTTP status code
  class ServerError < Error; end

  # Raised when WheelApi returns the HTTP status code 500
  class InternalServerError < ServerError; end

  # Raised when WheelApi returns the HTTP status code 502
  class BadGateway < ServerError; end

  # Raised when WheelApi returns the HTTP status code 503
  class ServiceUnavailable < ServerError; end

  # Raised when WheelApi returns the HTTP status code 504
  class GatewayTimeout < ServerError; end

  # Raised when WheelApi returns unexpected HTTP status code
  class UnknowError < ServerError; end

end
