module WheelApi
  class Client
    class HttpExceptionInspector

      def call( response )

        case response.code
        when 200
          return true
        when 400
          raise WheelApi::BadRequestError.from_response( response.body.to_s )
        when 448
          raise WheelApi::UnrecoverableError.from_response( response.body.to_s )
        when 449
          raise WheelApi::RecoverableError.from_response( response.body.to_s )
        when 450
          raise WheelApi::RetryableError.from_response( response.body.to_s )
        when 500
          raise WheelApi::InternalServerError.new "Something is technically wrong."
        when 502
          raise WheelApi::BadGateway.new "The server returned an invalid or incomplete response."
        when 503
          raise WheelApi::ServiceUnavailable.new "Server is not ready to handle the request."
        when 504
          raise WheelApi::GatewayTimeout.new "Gateway Time-out."
        else
          raise WheelApi::UnknowError.new "Unknow Error."
        end
      end

    end
  end
end
