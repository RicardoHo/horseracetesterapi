require 'http'
module WheelApi
  class Client

    DEFAULT_TIMEOUT = 5

    module Base
      # Post to a response
      # @params url String
      # @option params [Hash]
      # @return [HTTP::Response]
      def post(url:, params: nil, type: :json, headers: {})
        perform(:post, url, type => params, headers: headers)
      end

      # Get to a response
      # @params url String
      # @option params [Hash]
      # @return [HTTP::Response]
      def get(url:, params: nil, headers: {})
        perform(:get, url, params: params, headers: headers)
      end

      private

      # @return [HTTP::Response]
      def perform(verb, url, opts = {})
        response = HTTP.timeout(
          DEFAULT_TIMEOUT ## default connect timeout
        ).request verb, url, opts
        inspector.call(response)
      rescue WheelApi::Error => e
        raise e
      rescue HTTP::TimeoutError => e
        raise WheelApi::TimeoutError, e
      rescue HTTP::ConnectionError => e
        raise WheelApi::ConnectionError, e
      else
        response
      end
    end
  end
end
