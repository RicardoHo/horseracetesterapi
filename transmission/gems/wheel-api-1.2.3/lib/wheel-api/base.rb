module WheelApi
  class Base
    attr_reader :property_id
    attr_reader :game_id
    attr_reader :login_name

    def initialize(property_id:, game_id:, login_name:)
      @url = WheelApi.base_url + '/messages/create.json'
      @property_id = property_id
      @game_id = game_id
      @login_name = login_name
    end

    private

    def perform(params)
      headers = {
        'gs-property-id' => property_id,
        'gs-game-id' => game_id,
        'gs-login-name' => login_name
      }
      res = WheelApi::Client.post(url: @url, params: params, headers: headers)
      MultiJson.load(res.body.to_s, symbolize_keys: true)
    rescue MultiJson::ParseError => e
      raise UnknowError, e
    end

    def request_base_info
      raise NotImplementedError
    end
  end
end
