require File.expand_path('../spec_helper', __dir__)
describe WheelApi::RNG do
  let(:url) { 'http://localhost:3000/messages/create.json' }

  subject { described_class.new(property_id: 1, game_id: 1, login_name: 'Test') }

  describe '.draw' do
    let(:params) do
      WheelApi::RNG::Info.new(
        num_of_rng: 3,
        limit: 200,
        repeat: true
      )
    end

    let(:res)  do
      [[5, 65, 49]]
    end

    it 'should pass correct params to perform method' do
      req_data = {
        action: 'rng',
        developer_token: 'token',
        request_rng_info: [params]
      }
      expect(subject).to receive(:perform).once.with(req_data).and_return(res)
      subject.draw(params)
    end

    it 'should return Array of random number' do
      req_data = {
        action: 'rng',
        developer_token: 'token',
        request_rng_info: [params]
      }
      expect(subject).to receive(:perform).once.with(req_data).and_return(res)
      result = subject.draw(params)
      expect(result).to eq(res.first)
    end
  end

  describe '.multi_draw' do
    let(:params) do
      [
        WheelApi::RNG::Info.new(
          num_of_rng: 3,
          limit: 200,
          repeat: true
        ),
        {
          num_of_rng: 2,
          limit: 100,
          repeat: false
        }
      ]
    end

    let(:res)  do
      [[5, 65, 49], [6, 89]]
    end

    it 'should pass correct params to perform method' do
      req_data = {
        action: 'rng',
        developer_token: 'token',
        request_rng_info: params
      }
      expect(subject).to receive(:perform).once.with(req_data).and_return(res)
      subject.multi_draw(params)
    end

    it 'should return Array of [random number]' do
      req_data = {
        action: 'rng',
        developer_token: 'token',
        request_rng_info: params
      }
      expect(subject).to receive(:perform).once.with(req_data).and_return(res)
      result = subject.multi_draw(params)
      expect(result).to eq(res)
    end
  end

  describe '.change_type' do
    let(:correct_params)  do
      {
        num_of_rng: 1,
        limit: 200,
        repeat: true
      }
    end

    it 'should raise ArgumentError when type is not Hash' do
      expect { subject.send(:change_type, 'A') }.to raise_error ArgumentError, 'info (A) must be Hash.'
    end

    it 'should change typ to RNG::Info when params is correct' do
      expect(subject.send(:change_type, correct_params)).to be_an_instance_of(WheelApi::RNG::Info)
    end
  end

  describe WheelApi::RNG::Info do
    let(:params) do
      {
        num_of_rng: 1,
        limit: 200,
        repeat: false
      }
    end

    context 'should raise error when param missing' do
      it 'when property num_of_rng missing' do
        params.delete(:num_of_rng)
        expect { described_class.new params }.to raise_error ArgumentError, "The property 'num_of_rng' is required for WheelApi::RNG::Info."
      end

      it 'when property limit missing' do
        params.delete(:limit)
        expect { described_class.new params }.to raise_error ArgumentError, "The property 'limit' is required for WheelApi::RNG::Info."
      end

      it 'when property repeat missing' do
        params.delete(:repeat)
        expect { described_class.new params }.to raise_error ArgumentError, "The property 'repeat' must be ture or false."
      end
    end

    context 'should raise error when param type eror' do
      it 'when property repeat type eror' do
        params[:repeat] = 'true'
        expect { described_class.new params }.to raise_error ArgumentError, "The property 'repeat' must be ture or false."
      end
    end
  end
end
