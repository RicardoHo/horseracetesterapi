# frozen_string_literal: true

require File.expand_path('../spec_helper', __dir__)
describe WheelApi::Base do
  subject { described_class.new(property_id: 1, game_id: 1, login_name: 'Test') }
  let(:url) { 'http://localhost:3000/messages/create.json' }

  describe '.perform' do
    let(:success_res) { { a: 1, b: { c: 3 } } }
    let(:http_200_res) { HTTP::Response.new(version: 1.1, status: 200, body: success_res.to_json) }
    let(:http_string_res) { HTTP::Response.new(version: 1.1, status: 200, body: 'HI') }

    it 'should call client post method' do
      params = { action: 'rng' }
      headers = { 'gs-game-id' => 1, 'gs-login-name' => 'Test', 'gs-property-id' => 1 }
      expect(WheelApi::Client).to receive(:post).once.with(url: url, params: params, headers: headers).and_return(http_200_res)
      subject.send(:perform, params)
    end

    it 'should return hash when response is json' do
      params = { action: 'rng' }
      headers = { 'gs-game-id' => 1, 'gs-login-name' => 'Test', 'gs-property-id' => 1 }
      expect(WheelApi::Client).to receive(:post).once.with(url: url, params: params, headers: headers).and_return(http_200_res)
      res = subject.send(:perform, params)
      expect(res).to eql(success_res)
    end

    it 'should raise MultiJson::ParseError when response is not josn format' do
      params = { action: 'rng' }
      headers = { 'gs-game-id' => 1, 'gs-login-name' => 'Test', 'gs-property-id' => 1 }
      expect(WheelApi::Client).to receive(:post).once.with(url: url, params: params, headers: headers).and_return(http_string_res)
      expect { subject.send(:perform, params) }.to raise_error WheelApi::UnknowError
    end

    it 'should raise error when client raise WheelApi error' do
      params = { action: 'rng' }
      headers = { 'gs-game-id' => 1, 'gs-login-name' => 'Test', 'gs-property-id' => 1 }
      expect(WheelApi::Client).to receive(:post).once.with(url: url, params: params, headers: headers).and_raise(WheelApi::Error.new('UnrecoverableError'))
      expect { subject.send(:perform, params) }.to raise_error WheelApi::Error
    end
  end
end
