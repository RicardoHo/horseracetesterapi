require File.expand_path('../spec_helper', __dir__)
describe WheelApi::WeightTable do
  let(:url) { 'http://localhost:3000/messages/create.json' }

  subject { described_class.new(property_id: 1, game_id: 1, login_name: 'Test') }

  describe '.draw' do
    let(:table) do
      {
        a: 1,
        b: 2,
        c: 3
      }
    end

    let(:num) { 2 }
    let(:with_replacement) { true }
    let(:without_replacement) { false }

    let(:res_without_replacement) do
      {
        symbols: %w[c b]
      }
    end

    let(:res_with_replacement) do
      {
        symbols: %w[c b],
        latest_weight_table: { a: 1, b: 1, c: 2 }
      }
    end

    context 'without_replacement' do
      it 'should pass correct params to perform method' do
        req_data = {
          action: 'weight_table',
          developer_token: 'token',
          num_of_symbols: num,
          weight_table: table,
          with_replacement: without_replacement
        }
        expect(subject).to receive(:perform).once.with(req_data).and_return(res_without_replacement)
        subject.draw(table: table, num: 2)
      end

      it 'return correct response' do
        req_data = {
          action: 'weight_table',
          developer_token: 'token',
          num_of_symbols: num,
          weight_table: table,
          with_replacement: without_replacement
        }
        expect(subject).to receive(:perform).once.with(req_data).and_return(res_without_replacement)
        symbols, new_table = subject.draw(table: table, num: 2)
        expect(symbols).to eq res_without_replacement[:symbols]
        expect(new_table).to be_nil
      end
    end

    context 'with_replacement' do
      it 'should pass correct params to perform method' do
        req_data = {
          action: 'weight_table',
          developer_token: 'token',
          num_of_symbols: num,
          weight_table: table,
          with_replacement: with_replacement
        }
        expect(subject).to receive(:perform).once.with(req_data).and_return(res_with_replacement)
        subject.draw(table: table, num: 2, replacement: with_replacement)
      end

      it 'return correct response' do
        req_data = {
          action: 'weight_table',
          developer_token: 'token',
          num_of_symbols: num,
          weight_table: table,
          with_replacement: with_replacement
        }
        expect(subject).to receive(:perform).once.with(req_data).and_return(res_with_replacement)
        symbols, new_table = subject.draw(table: table, num: 2, replacement: with_replacement)
        expect(symbols).to eq res_with_replacement[:symbols]
        expect(new_table).to eq res_with_replacement[:latest_weight_table]
      end
    end
  end
end
