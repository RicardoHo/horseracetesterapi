require File.expand_path('../../spec_helper', __dir__)

describe WheelApi::Client do
  context 'Base Module' do
    subject do
      described_class.new
    end
    let(:url) { 'http://localhost' }

    describe '.post' do
      context 'type :json' do
        let(:mock_headers) { { 'Content-Type' => 'application/json; charset=UTF-8', 'Connection' => 'close', 'Host' => 'localhost', 'User-Agent' => 'http.rb/4.3.0' } }
        let(:params) { { a: 1, b: 2 } }
        before(:each) do
          @stub_web = stub_request(:post, url)
                      .with(body: params, headers: mock_headers)
                      .to_return(status: 200, body: params.to_json)
        end

        it 'should return response' do
          response = subject.send(:post, url: url, params: params)
          expect(response.body.to_s).to eq(params.to_json)
        end

        it 'return 200 state' do
          response = subject.send(:post, url: url, params: params)
          expect(response.code).to eq 200
        end

        it 'should call stub web' do
          subject.send(:post, url: url, params: params)
          expect(@stub_web).to have_been_requested
        end
      end

      context 'type :body' do
        let(:params) { 'Testing' }
        let(:mock_headers) { { 'Connection' => 'close', 'Host' => 'localhost', 'User-Agent' => 'http.rb/4.3.0' } }
        before(:each) do
          @stub_web = stub_request(:post, url)
                      .with(body: params, headers: mock_headers)
                      .to_return(status: 200, body: params)
        end

        it 'should return response' do
          response = subject.send(:post, url: url, params: params, type: :body)
          expect(response.body.to_s).to eq(params)
        end

        it 'return 200 state' do
          response = subject.send(:post, url: url, params: params, type: :body)
          expect(response.code).to eq 200
        end

        it 'should call stub web' do
          subject.send(:post, url: url, params: params, type: :body)
          expect(@stub_web).to have_been_requested
        end
      end

      context 'type :form' do
        let(:params) { { a: 1, b: 2 } }
        let(:mock_headers) { { 'Content-Type' => 'application/x-www-form-urlencoded', 'Connection' => 'close', 'Host' => 'localhost', 'User-Agent' => 'http.rb/4.3.0' } }
        before(:each) do
          @stub_web = stub_request(:post, url)
                      .with(body: 'a=1&b=2', headers: mock_headers)
                      .to_return(status: 200, body: params.to_json)
        end

        it 'should return response' do
          response = subject.send(:post, url: url, params: params, type: :form)
          expect(response.body.to_s).to eq(params.to_json)
        end

        it 'return 200 state' do
          response = subject.send(:post, url: url, params: params, type: :form)
          expect(response.code).to eq 200
        end

        it 'should call stub web' do
          subject.send(:post, url: url, params: params, type: :form)
          expect(@stub_web).to have_been_requested
        end
      end

      context 'handle timeout' do
        let(:params) { { a: 1, b: 2 } }
        before(:each) do
          @stub_web = stub_request(:post, url)
                      .and_timeout
        end

        it 'should raise WheelApi::TimeoutError' do
          expect { subject.send(:post, url: url, params: params) }.to raise_error WheelApi::TimeoutError, 'connection error: Connection timed out'
        end
      end
    end

    describe '.get' do
      let(:params) { { a: 1, b: 2 } }
      let(:mock_headers) { { 'Connection' => 'close', 'Host' => 'localhost', 'User-Agent' => 'http.rb/4.3.0' } }

      context 'normal' do
        before(:each) do
          @stub_web = stub_request(:get, url)
                      .with(query: params, headers: mock_headers)
                      .to_return(status: 200, body: params.to_json)
        end

        it 'should return response' do
          response = subject.send(:get, url: url, params: params)
          expect(response.body.to_s).to eq(params.to_json)
        end

        it 'return 200 state' do
          response = subject.send(:get, url: url, params: params)
          expect(response.code).to eq 200
        end

        it 'should call stub web' do
          response = subject.send(:get, url: url, params: params)
          expect(@stub_web).to have_been_requested
        end
      end

      context 'handle timeout' do
        before(:each) do
          @stub_web = stub_request(:get, url)
                      .with(query: params, headers: mock_headers)
                      .and_timeout
        end

        it 'should raise WheelApi::TimeoutError' do
          expect { subject.send(:get, url: url, params: params) }.to raise_error WheelApi::TimeoutError, 'connection error: Connection timed out'
        end
      end
    end
  end
end
