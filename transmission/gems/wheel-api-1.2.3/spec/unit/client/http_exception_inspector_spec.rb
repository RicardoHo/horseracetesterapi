require File.expand_path('../../../spec_helper', __FILE__)

describe WheelApi::Client::HttpExceptionInspector do
  subject { described_class.new }
  let( :success_res ) { { a:1, b:{c:3} } }
  let( :error_res ){
    {
      code: "A10",
      error_code: "error_code",
      description: "Error description",
      resolution: "resolution"
    }.to_json
  }
  let( :http_200_res ){ HTTP::Response.new( { version: 1.1 ,status: 200, body: success_res } ) }
  let( :http_400_res ){ HTTP::Response.new( { version: 1.1 ,status: 400, body: error_res } ) }
  let( :http_448_res ){ HTTP::Response.new( { version: 1.1 ,status: 448, body: error_res } ) }
  let( :http_449_res ){ HTTP::Response.new( { version: 1.1 ,status: 449, body: error_res } ) }
  let( :http_450_res ){ HTTP::Response.new( { version: 1.1 ,status: 450, body: error_res } ) }
  let( :http_500_res ){ HTTP::Response.new( { version: 1.1 ,status: 500, body: error_res } ) }
  let( :http_502_res ){ HTTP::Response.new( { version: 1.1 ,status: 502, body: error_res } ) }
  let( :http_503_res ){ HTTP::Response.new( { version: 1.1 ,status: 503, body: error_res } ) }
  let( :http_504_res ){ HTTP::Response.new( { version: 1.1 ,status: 504, body: error_res } ) }
  let( :http_505_res ){ HTTP::Response.new( { version: 1.1 ,status: 505, body: error_res } ) }

  context ".call" do
    it "can pass when response status is 200" do
      expect{ subject.(http_200_res) }.to_not raise_error
    end

    it "should rais BadRequestError when response status is 400" do
      expect{ subject.(http_400_res) }.to raise_error WheelApi::BadRequestError, "Error description"
    end

    it "should rais UnrecoverableError when response status is 448" do
      expect{ subject.(http_448_res) }.to raise_error WheelApi::UnrecoverableError, "Error description"
    end

    it "should rais RecoverableError when response status is 449" do
      expect{ subject.(http_449_res) }.to raise_error WheelApi::RecoverableError, "Error description"
    end

    it "should rais RetryableError when response status is 450" do
      expect{ subject.(http_450_res) }.to raise_error WheelApi::RetryableError, "Error description"
    end

    it "should rais InternalServerError when response status is 500" do
      expect{ subject.(http_500_res) }.to raise_error WheelApi::InternalServerError, "Something is technically wrong."
    end

    it "should rais BadGateway when response status is 502" do
      expect{ subject.(http_502_res) }.to raise_error WheelApi::BadGateway, "The server returned an invalid or incomplete response."
    end

    it "should rais ServiceUnavailable when response status is 503" do
      expect{ subject.(http_503_res) }.to raise_error WheelApi::ServiceUnavailable, "Server is not ready to handle the request."
    end

    it "should rais GatewayTimeout when response status is 504" do
      expect{ subject.(http_504_res) }.to raise_error WheelApi::GatewayTimeout, "Gateway Time-out."
    end

    it "should rais UnknowError when response status without caught" do
      expect{ subject.(http_505_res) }.to raise_error WheelApi::UnknowError, "Unknow Error."
    end

  end

end
