require File.expand_path('../../spec_helper', __FILE__)

describe WheelApi do
  subject{ described_class }

  describe ".base_url" do

    it "should return default value when @base_url is nil" do
      expect( subject.base_url ).to eq "http://localhost:3000"
    end

    it "should return @base_url" do
      mock_url = "http://wheel-api.com"
      subject.instance_variable_set(:@base_url, mock_url)
      expect( subject.base_url ).to eq( mock_url )
    end

  end

  describe ".base_url=" do

    it "should assign new base url" do
      subject.base_url = "http://wheel-api.com"
      expect( subject.base_url ).to eq( "http://wheel-api.com" )
    end

    it "should update instance variable" do
      subject.base_url = "http://wheel-api.com"
      expect( subject.instance_variable_get(:@base_url) ).to eq( "http://wheel-api.com" )
    end

  end

  describe ".developer_token" do

    it "should return default value when @developer_token is nil" do
      expect( subject.developer_token ).to eq "token"
    end

    it "should return @developer_token" do
      mock_token = "HiToken"
      subject.instance_variable_set(:@developer_token, mock_token)
      expect( subject.developer_token ).to eq( mock_token )
    end

  end

  describe ".developer_token=" do

    it "should assign new developer_token" do
      subject.developer_token = "Hitoken"
      expect( subject.developer_token ).to eq( "Hitoken" )
    end

    it "should update instance variable" do
      subject.developer_token = "Hitoken"
      expect( subject.instance_variable_get(:@developer_token) ).to eq( "Hitoken" )
    end

  end

  describe ".reset" do
    it "should reset @base_url" do
      subject.instance_variable_set(:@base_url, "http://wheel-api.com")
      subject.reset
      expect( subject.base_url ).to eq( "http://localhost:3000" )
    end

    it "should reset @developer_token" do
      subject.instance_variable_set(:@developer_token, "HiToken")
      subject.reset
      expect( subject.developer_token ).to eq( "token" )
    end
  end

end
