[TOC]

# Changelog
All notable changes to this project will be documented in this file.

## Unreleased

## 0.5.4
### Added
- add bitbucket pipeline config file
### Changed
- Fixed cleanup action checking issue
### Deleted
- Command method checking in command hook
- rubycritic rake task

## 0.5.3
### Added
- add comment to Entity#component_hash and Entity#to_hash
### Changed
- Fixed CleanupSystem#remove pass incorrect param to Entity#batch_remove
- Fixed incorrect comment of Entity#remove

## 0.5.2
### Added
- SystemAdmin provide #find
### Changed
- update attribute hook options handling

## 0.5.1
### Changed
- update Dry-Struct lib version

## 0.5.0
### Added
- Provide getter for get all entities that contains reference component(s)

### Changed
- Optimize String#camel_case and String#snake_case

## 0.4.1
### Changed
- Fixed cleanup system not working when the condition was not set
- Update react system apply changes algorithm

## 0.4.0
### Changed
- Attribute module
  - init and reset by pass deep clone value
- Component
  - create @represented at first use

### Added
- GameDefinition module
  - fundamental GameDefinition struct
- EntitySystem
  - provide namespace_for_watch class API for bypass full namespace in watch
  - provide reference DSL keyword and attribute reader references
- Gamebox
  - provide @game_def and @game_engine
- Component
  - provide pack DSL keyword

## 0.3.1
### Changed
- Attribute module
  - use class_eval for better performance of created method
  - cancle setter and getter customization
- Component base class
  - fine tune .new
  - extend Attribute

## 0.3.0
### Added
- ECS Manager (Beta)
- Component base class
- Entity base class
- event execute API of System Admin
- Attribute module

## 0.2.0
### Added
- System Admin

### Changed
- rename Clutch::StateMachine::Gamebox to Clutch::StateMachine::GameboxStateMachine

## 0.1.0
### Added
- Gamebox
- Gamebox State Machine
