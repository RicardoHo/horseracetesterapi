# frozen_string_literal: true

namespace :spec do
  require 'rspec/core/rake_task'
  desc 'Run unit test specs'
  RSpec::Core::RakeTask.new(:unit) do |t|
    ENV['TEST_SUITE'] = 'unit'
    t.pattern = 'spec/lib/**/*_spec.rb'
    t.rspec_opts = '-c --format documentation --format html --out reports/spec/index.html'
  end
end
