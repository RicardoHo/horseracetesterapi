# frozen_string_literal: true

namespace :rdoc do
  require 'yard'
  desc 'Generate RDoc include private method'
  YARD::Rake::YardocTask.new(:include_private) do |task|
    task.options = ['--private', '--output-dir', 'reports/doc']
  end
end
