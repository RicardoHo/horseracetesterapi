# frozen_string_literal: true

lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH << lib unless $LOAD_PATH.include?(lib)
require 'clutch/version'

DESCRIPTION = <<~DESCPT
  A ECS framework ruby implementation designed for game development.
  Shipped with state control, sytem management, event management, etc..
  Define your own component, system and event message parser; and setup state machine config, event-state-system mapping. Then you're able to go.
DESCPT
Gem::Specification.new do |spec|
  spec.name          = 'clutch'
  spec.version       = Clutch::VERSION
  spec.authors       = ['Alpha Huang']
  spec.email         = ['alpha.huang@gamesourcecloud.com']
  spec.homepage      = 'https://bitbucket.org/gamesource/clutch'
  spec.summary       = 'A ECS framework ruby implementation'
  spec.description   = DESCRIPTION
  spec.files         = %x`git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features|development|tasks)/})
  end
  spec.require_paths = ['lib']

  spec.required_ruby_version = '~> 2.5'
  spec.add_dependency('rake', '~> 12.3')
  spec.add_dependency('multi_json', '1.14.1')
  spec.add_dependency('dry-struct', '~> 1.0')
  spec.add_dependency('aasm', '~> 5.0')
  spec.add_dependency('representable', '~> 3.0')
end
