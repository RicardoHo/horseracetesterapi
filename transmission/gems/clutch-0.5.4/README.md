- [Clutch](#markdown-header-clutch)
  - [API Document](#markdown-header-api-document)
  - [Usage](#markdown-header-usage)
    - [Install](#markdown-header-install)
    - [System](#markdown-header-system)
    - [Component](#markdown-header-component)

# Clutch

A ECS framework ruby implementation designed for game development.
Shipped with state control, sytem management, event management, etc..
Define your own component, system and event message parser; and setup state machine config, event-state-system mapping. Then you're able to go.

## API Document
[RDoc]()

## Usage

### Install
```
$ gem install clutch
```
install gem via command or add clutch to Gemfile
```Gemfile
  gem 'clutch'
```
and run
```
$ bundle install
```

### System
```ruby
class System < Clutch::System

end
```

### Component
```ruby
class Component < Clutch::Component

end
```
