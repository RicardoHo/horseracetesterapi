# frozen_string_literal: true

require 'spec_helper'

describe Clutch::Component do
  subject { described_class.new }
  subject(:test_component) do
    Class.new(described_class) do
      attr_reader :test
      property :test

      def initialize(test)
        @test = test
      end
    end.new('test')
  end

  it 'should be kind of represetable' do
    expect(subject).to be_kind_of(Representable::Decorator)
  end

  describe '.inherited' do
    it 'should call .inherited when inherited component' do
      expect(described_class).to receive(:inherited).once
      Class.new(described_class)
    end

    it 'should set constant SIGNATURE add prepared_sign by 1' do
      pending('code changed')
      init_sign = described_class.instance_variable_get(:@prepared_sign) || 0
      expect(Class.new(described_class)::SIGNATURE).to eq(init_sign)
      expect(described_class.instance_variable_get(:@prepared_sign))
        .to eq(init_sign + 1)
    end
  end

  describe '#dirty?' do
    it 'should return value of instance variable dirty' do
      expect_value = true
      subject.instance_variable_set(:@dirty, expect_value)
      expect(subject.dirty?).to be(subject.instance_variable_get(:@dirty))
    end
  end

  describe '#to_hash?' do
    it 'should return hash with symbolized key' do
      result = test_component.to_hash.keys.all? { |key| key.is_a?(Symbol) }
      expect(result).to be(true)
    end
  end
end
