# frozen_string_literal: true

require 'spec_helper'

class Foo
  include Clutch::StateMachine::GameboxStateMachine

  def on_running
    'trigger start event after step on_running'
  end
end

describe Clutch::StateMachine::GameboxStateMachine do
  subject(:object) { Foo.new }

  it 'current state is ready' do
    expect(object.ready?).to be(true)
  end

  context 'current state is ready' do
    it 'should execute start event successfully' do
      object.start
      expect(object.running?).to be(true)
    end

    it 'should throw error for execute enqueue_event event' do
      expect { object.enqueue_event }.to raise_error(AASM::InvalidTransition)
    end

    it 'should throw error for execute close event' do
      expect { object.close }.to raise_error(AASM::InvalidTransition)
    end

    it 'should throw error for execute terminate event' do
      expect { object.terminate }.to raise_error(AASM::InvalidTransition)
    end
  end

  context 'current state is running' do
    before 'all' do
      object.start
    end

    it 'should execute enqueue_event event successfully' do
      object.enqueue_event
      expect(object.running?).to be(true)
    end

    it 'should execute close event successfully' do
      object.close
      expect(object.closing?).to be(true)
    end

    it 'should throw error for execute start event' do
      expect { object.start }.to raise_error(AASM::InvalidTransition)
    end

    it 'should throw error for execute terminate event' do
      expect { object.terminate }.to raise_error(AASM::InvalidTransition)
    end
  end

  context 'current state is closing' do
    before 'all' do
      object.start
      object.close
    end

    it 'should execute terminate event successfully' do
      object.terminate
      expect(object.closed?).to be(true)
    end

    it 'should throw error for execute start event' do
      expect { object.start }.to raise_error(AASM::InvalidTransition)
    end

    it 'should throw error for execute enqueue_event event' do
      expect { object.enqueue_event }.to raise_error(AASM::InvalidTransition)
    end

    it 'should throw error for execute close event' do
      expect { object.close }.to raise_error(AASM::InvalidTransition)
    end
  end

  context 'current state is closing' do
    before 'all' do
      object.start
      object.close
      object.terminate
    end

    it 'should throw error for execute start event' do
      expect { object.start }.to raise_error(AASM::InvalidTransition)
    end

    it 'should throw error for execute enqueue_event event' do
      expect { object.enqueue_event }.to raise_error(AASM::InvalidTransition)
    end

    it 'should throw error for execute close event' do
      expect { object.close }.to raise_error(AASM::InvalidTransition)
    end

    it 'should throw error for execute terminate' do
      expect { object.terminate }.to raise_error(AASM::InvalidTransition)
    end
  end
end
