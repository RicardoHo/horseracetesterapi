# frozen_string_literal: true

require 'spec_helper'

class FakeClass < Representable::Decorator
  include Representable::JSON
  include Representable::Hash
  include Representable::Hash::AllowSymbols
  include Clutch::Attribute

  def self.new(*args)
    super.tap do |instance|
      instance.instance_variable_set(:@represented, instance)
    end
  end

  def initialize; end
end

describe Clutch::Attribute do
  subject(:fake_instance) { FakeClass.new }

  describe '.attribute' do
    before :each do
      fake_instance.class.class_eval do
        attribute :test
      end
    end

    it 'should provide attribute reader and writter' do
      expect(fake_instance).to respond_to(:test)
      expect(fake_instance).to respond_to(:test=)
    end

    context 'init value' do
      before :each do
        fake_instance.class.class_eval do
          attribute :test, 'init_value'
        end
      end

      it 'should set the init value' do
        expect(fake_instance.test).to eq('init_value')
      end

      describe '#reset' do
        it 'should reset to init value' do
          fake_instance.test = 'new_value'
          fake_instance.reset
          expect(fake_instance.test).to eq('init_value')
        end
      end
    end
  end
end
