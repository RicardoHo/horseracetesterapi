# frozen_string_literal: true

class FakeSystem
  def execute; end

  def handover; end
end

describe Clutch::SystemAdmin do
  let(:valid_type) { :initialize }
  let(:invalid_type) { :non_accept }
  let(:fake_system_class) { FakeSystem }
  let(:config_hash) do
    {
      "#{valid_type}": [
        fake_system_class,
      ],
      "#{invalid_type}": [
        fake_system_class,
      ],
    }
  end
  let(:system_config) do
    OpenStruct.new(system: config_hash)
  end
  let(:definition) do
    OpenStruct.new(system_config: system_config)
  end

  subject { described_class.new(definition, nil) }

  it 'should generate method for each system type' do
    pending('code changed')
    method_name = :"execute_#{valid_type}"
    expect(subject.respond_to?(method_name)).to be(true)
  end

  it 'should skip non accept system type' do
    pending('code changed')
    method_name = :"execute_#{invalid_type}"
    expect(subject.respond_to?(method_name)).to be(false)
  end

  describe '#execute_initialize(generated_method)' do
    it 'should trigger all system of the type call #execute' do
      pending('code changed')
      expect_any_instance_of(fake_system_class).to receive(:execute).once
      subject.public_send(:"execute_#{valid_type}") { |test| test }
    end
  end
end
