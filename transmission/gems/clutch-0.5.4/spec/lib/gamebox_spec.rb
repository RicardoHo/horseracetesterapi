# frozen_string_literal: true

require 'spec_helper'

class EventMessage
  def name
    'test'
  end
end

describe Clutch::Gamebox do
  subject { described_class.new }
  let(:event_message) { EventMessage.new }

  it 'should be running state' do
    pending('code changed')
    expect(subject.running?).to be(true)
  end

  describe '#push_event' do
    context 'running state' do
      it 'should call #enqueue_event once' do
        pending('code changed')
        expect(subject).to receive(:enqueue_event).once
        subject.push_event(event_message)
      end
    end

    context 'non-running state' do
      it 'should throw event rejected error' do
        pending('code changed')
        subject.close
        expect { subject.push_event(event_message) }
          .to raise_error(Clutch::EventRejected)
      end
    end
  end

  describe '#on_running' do
  end

  describe '#execute' do
  end
end
