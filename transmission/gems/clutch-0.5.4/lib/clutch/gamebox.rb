# frozen_string_literal: true

module Clutch
  class Gamebox
    include StateMachine::GameboxStateMachine

    attr_reader :run_mode
    attr_reader :entities
    attr_reader :system_admin
    attr_reader :ecs_manager
    attr_reader :group_manager

    def initialize(definition, game_engine = nil)
      @entities         = []
      @ecs_manager      = Manager::ECSManager.new
      @system_admin     = SystemAdmin.new(definition, game_engine)
      @execute_callback = proc do |system_result|
        next unless system_result

        apply_system_result(system_result)
      end
      register_ecs

      system_admin.execute_initialize(&@execute_callback)
      start
    end

    #
    # push new incoming event message into event manager
    # if gamebox still running and message is valid
    #
    # @param [Clutch::EventMessage] event_message
    #
    def execute_event(event_message)
      enqueue_event
    rescue AASM::InvalidTransition
      raise Clutch::EventRejected,
        format(Clutch::REJECT_EVENT_MSG, event_message.command, current_state),
        caller
    else
      system_admin.execute_event(event_message, &@execute_callback)
    end

    def inspect
      data = {}
      data[:entities] = {}
      entities.each_with_object(data[:entities]) do |entity, ent_data|
        pack_entity(entity, ent_data)
      end

      data
    end

    def create_entity(*components)
      entity = Clutch::Entity.new(*components)
      new_entity(entity)
    end

    def new_entity(entity)
      entities.push(entity)
      ecs_manager.new_entity(entity)
    end

    def delete_entity(entity)
      entities.delete(entity)
      ecs_manager.destroyed_entity(entity)
    end

    def dirty_entity(entity)
      ecs_manager.dirty_entity(entity)
    end

    private

    def pack_entity(entity, ent_data)
      entity.to_hash.each do |key, value|
        if ent_data.key?(key)
          if ent_data[key].is_a?(Array)
            ent_data[key].push(value)
          else
            ent_data[key] = [ent_data[key], value]
          end
        else
          ent_data[key] = value
        end
      end
    end

    def apply_system_result(system_result)
      apply_new_entities(system_result.adds)
      apply_destroy_entities(system_result.deletes)
      apply_dirty_entities(system_result.dirties)
    end

    def apply_new_entities(entities)
      entities.each { |entity| new_entity(entity) }
    end

    def apply_destroy_entities(entities)
      entities.each { |entity| delete_entity(*entity) }
    end

    def apply_dirty_entities(entities)
      entities.each { |entity| dirty_entity(entity) }
    end

    def register_ecs
      system_admin.systems.each do |system|
        system.entities_handle =
          ecs_manager.register(system.class::ECS_SIGNATURE) \
          if system.class.const_defined?(:ECS_SIGNATURE)
        system.reference_handle =
          ecs_manager.register(system.class::REF_ECS_SIGNATURE) \
          if system.class.const_defined?(:REF_ECS_SIGNATURE)
      end
    end

    #
    # Trigger when gamebox start running
    # which will call #execute every tick
    # where tick interval can be config
    #
    def on_running
      # TODO
      # create a non-blocking loop
      # for periodically trigger system process
      # and (optional) trigger IO handle by pass execute result
    end

    #
    # system execution entry, triggered every tick
    #
    def execute
      # TODO
      # execute all reactive system
      # execute all command system and update result to event
      # execute all recurrent system
      # execute all cleanup system
    end
  end
end
