# frozen_string_literal: true

module Clutch
  module Manager
    #
    # ECS is the short form of effective component slice
    # which means a entity holds effective components to a system
    #
    class ECSManager
      attr_reader :entities_by_ecs_signature
      attr_reader :ecs_signatures_by_entitiy_id

      def initialize
        @entities_by_ecs_signature    = {}
        @ecs_signatures_by_entitiy_id = {}
      end

      #
      # register ecs signature by components
      #
      # @param [Array<Integer>] ecs_signature
      #
      # @return [Array] the container of entities of register ecs signature
      #
      def register(ecs_signature)
        ecs_signature.sort!
        @entities_by_ecs_signature[ecs_signature] ||= []
      end

      #
      # setup the relation of new entity to each system
      #
      # @param [Clutch::Entity] entity
      #
      def new_entity(entity)
        entity_ecs_signatures = (@ecs_signatures_by_entitiy_id[entity.id] ||= [])
        update_entity_ecs_map(
          entity,
          entities_by_ecs_signature.keys,
          entity_ecs_signatures
        )
      end

      #
      # update the list of entities of releated ecs signature
      # to the destroyed entity
      #
      # @param [Clutch::Entity] entity
      #
      def destroyed_entity(entity)
        ecs_signatures = @ecs_signatures_by_entitiy_id.delete(entity.id)
        ecs_signatures.each do |ecs_signature|
          @entities_by_ecs_signature[ecs_signature].delete(entity)
        end
      end

      #
      # handle dirty entity by update related list,
      # the dirty entity can be either increased or reduced
      #
      # @param [Clutch::Entity] entity
      #
      def dirty_entity(entity)
        increased_entity(entity) if entity.increased?
        reduced_entity(entity)   if entity.reduced?
        entity.changes_applied
      end

      private

      def update_entity_ecs_map(entity, ecs_signatures, entity_ecs_signatures)
        ecs_signatures.each do |ecs_signature|
          if ecs_signature.subset?(entity.singature)
            @entities_by_ecs_signature[ecs_signature].push(entity)
            entity_ecs_signatures.push(ecs_signature)
          end
        end
      end

      def increased_entity(entity)
        entity_ecs_signatures = @ecs_signatures_by_entitiy_id[entity.id]
        ecs_signatures = entities_by_ecs_signature.reject do |ecs_signature, _|
          entity_ecs_signatures.include?(ecs_signature)
        end.keys
        update_entity_ecs_map(
          entity,
          ecs_signatures,
          entity_ecs_signatures
        )
      end

      def reduced_entity(entity)
        entity_signature      = entity.singature
        entity_ecs_signatures = @ecs_signatures_by_entitiy_id[entity.id]
        entity_ecs_signatures.each do |ecs_signature|
          unless ecs_signature.subset?(entity_signature)
            @entities_by_ecs_signature[ecs_signature].delete(entity)
            entity_ecs_signatures.delete(ecs_signature)
          end
        end
      end
    end
  end
end
