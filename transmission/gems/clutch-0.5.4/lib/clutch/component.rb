# frozen_string_literal: true
require 'representable/json'
require 'representable/hash'

module Clutch
  class Component < Representable::Decorator
    include Representable::JSON
    include Representable::Hash
    include Representable::Hash::AllowSymbols
    include Clutch::Attribute

    class << self
      def inherited(subclass)
        subclass.const_set(:SIGNATURE, ComponentSign.request)
      end

      def pack(type, name: nil)
        remove_const(:PACK_TYPE) if const_defined?(:PACK_TYPE)
        remove_const(:PACK_NAME) if const_defined?(:PACK_NAME)

        name ||= self.class.name.split('::').last.snake_case
        name_by_layer = name.to_s.split('.').reverse!.map!(&:to_sym)
        const_set(:PACK_TYPE, type)
        const_set(:PACK_NAME, name_by_layer)
      end
    end

    def initialize; end

    #
    # output all attributes of the component in hash format
    # all keys in symbol form
    #
    # @return [Hash]
    #
    def to_hash
      @represented ||= self

      hash = super.deep_symbolize_keys
      return hash unless self.class.const_defined?(:PACK_NAME)

      pack(hash)
    end

    private

    def pack(hash)
      return hash if hash.empty?

      container = case self.class::PACK_TYPE
      when :property
        hash
      when :collection
        [hash]
      end
      self.class::PACK_NAME.inject(container) do |packed, layer|
        orig_packed   = packed
        packed        = {}
        packed[layer] = orig_packed

        packed
      end
    end
  end
end
