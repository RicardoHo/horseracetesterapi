# frozen_string_literal: true

module Clutch
  class SystemAdmin
    attr_reader :systems

    VALID_TYPES = %i[
      initialize
      recurrent
      command
      reactive
      cleanup
    ]

    #
    # Dynamic create entry method for different type's system
    #
    # @param [Hash] system_config
    #
    def initialize(game_definition, game_engine)
      @systems = []
      systems_by_type = game_definition.system_config.system

      systems_by_type.delete_if { |type, _systems| !VALID_TYPES.include?(type) }
      setup_system_execute_entry(game_definition, game_engine)
      setup_last_react(*@reactive_systems) if @reactive_systems
    end

    def execute_initialize
      @initialize_systems&.each do |system|
        yield system.execute if block_given?
      end
      @initialize_systems = nil
    end

    def execute_event(event, &block)
      command = event.command
      input   = event.input
      execute_cleanup(command, input, &block)  if @cleanup_systems
      execute_command(command, input, &block)  if @command_systems
      execute_reactive(command, input, &block) if @reactive_systems
    end

    def find(system_class)
      systems.find { |system| system.is_a?(system_class) }
    end

    private

    def setup_system_execute_entry(game_definition, game_engine)
      game_definition.system_config.system.each do |type, system_classes|
        systems = system_classes.map do |system|
          system.new(game_definition, game_engine)
        end
        @systems.push(*systems)
        instance_variable_set(:"@#{type}_systems", systems)
        method_name = :"execute_#{type}"
        next if self.class.method_defined?(method_name)

        self.class.class_eval <<~EVAL
          def #{method_name}(command, input, &block)
            @#{type}_systems.each do |system|
              yield system.execute(command, input)
            end
          end
        EVAL
      end
    end

    def do_setup_last_react(last_react_systems)
      last_react_systems.dup.size.times do
        system             = last_react_systems.shift
        target_ecs         = system.class::WATCHS
        system.after_react = target_ecs if last_react_systems.empty?
        non_applyable      = []
        last_react_systems.each do |next_system|
          next_ecs = next_system.class::WATCHS
          if next_ecs.subset?(target_ecs) ||
             target_ecs.intersect?(next_ecs)
            non_applyable.push(*target_ecs.intersection(next_ecs))
          elsif target_ecs.subset?(next_ecs)
            non_applyable.push(*target_ecs)
          end

          break if non_applyable == target_ecs
        end

        system.after_react = target_ecs.difference(non_applyable)
      end
    end

    def setup_last_react(*reactive_systems)
      return if reactive_systems.empty?

      system_map = { signs: [], systems: [] }
      reactive_systems.reverse.each_with_object(system_map) do |system, hash|
        unless hash[:signs].include?(system.class::ECS_SIGNATURE)
          hash[:signs].push(system.class::ECS_SIGNATURE)
          hash[:systems].insert(0, system)
        end
      end
      do_setup_last_react(system_map[:systems])
    end
  end
end
