# frozen_string_literal: true

module Clutch
  module StateMachine
    module Adapter
      def included(base)
        base.class_eval(&state_definition)
      end
    end
  end
end
