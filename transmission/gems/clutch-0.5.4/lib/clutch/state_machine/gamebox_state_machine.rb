# frozen_string_literal: true

module Clutch
  module StateMachine
    module GameboxStateMachine
      extend Adapter

      def self.state_definition
        proc do
          include AASM
          extend Forwardable

          def_delegators :aasm, :current_state

          aasm no_direct_assignment: true do
            state :ready, initial: true
            state :running
            state :closing
            state :closed

            event :start, after: :on_running do
              transitions from: :ready, to: :running
            end

            event :close do
              transitions from: :running, to: :closing
            end

            event :enqueue_event do
              transitions from: :running, to: :running
            end

            event :terminate do
              transitions from: :closing, to: :closed
            end
          end
        end
      end
    end
  end
end
