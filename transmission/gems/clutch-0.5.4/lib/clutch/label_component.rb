# frozen_string_literal: true

module Clutch
  class LabelComponent < Component
    def self.title(title)
      const_set(:TITLE, title)
    end

    def title
      self.class::TITLE
    end
  end
end
