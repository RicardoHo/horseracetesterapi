# frozen_string_literal: true

module Clutch
  class Error < RuntimeError
    DESCP = 'Error description was not defined.'

    def self.code(code_name)
      remove_const(:CODE) if const_defined?(:CODE)
      const_set(:CODE, code_name)
    end

    def self.description(description)
      remove_const(:DESCP)
      const_set(:DESCP, description)
    end

    def initialize(description = DESP)
      @description = description
      super
    end

    def to_hash
      {
        code: self.class::CODE,
        description: @description,
      }
    end
  end

  class EventRejected < Error
    code :C01
  end

  class PlaySequenceError < Error
    code :C02
  end
end
