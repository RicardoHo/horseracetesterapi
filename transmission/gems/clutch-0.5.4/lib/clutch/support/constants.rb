# frozen_string_literal: true

module Clutch
  REJECT_EVENT_MSG = <<~MSG
    Event - %s - has been rejected due to gamebox is not running
    Current gamebox state is %s
  MSG

  ATTR_EVAL_STR = <<~EVAL
    %{represent_type} :%{name}, %{represent_opts}

    def %{name}
      @%{name} ||= self.class.attributes&.dig(:%{name}).dup
    end

    def %{name}=(val)
      @dirty   = true
      @%{name} = val
    end
  EVAL

  INCORRECT_CLEANUP_ACTION_MSG = <<~MSG
    The cleanup action %s was incorrect
  MSG
end
