# frozen_string_literal: true

module Clutch
  module Attribute
    def self.included(klass)
      klass.extend(ClassMethods)
    end

    #
    # changes of attribute has been applied by observer
    #
    def changes_applied
      @dirty = false
    end

    #
    # is any attribute of the component changed
    #
    # @return [Boolean]
    #
    def dirty?
      @dirty
    end

    #
    # reset all attribute to init value
    #
    def reset
      self.class.attributes&.each do |name, init_val|
        instance_variable_set(:"@#{name}", init_val.dup)
      end
    end

    module ClassMethods
      attr_reader :attributes

      #
      # to create getter and setter method for given insatance variable
      #
      # @param [Symbol] name instance variable name
      # @param [Symbol] represent_type
      #   either property or collection, default property
      # @param [Hash] represent_opts
      #
      def attribute(name, init_val = nil, type: :property, **represent_opts)
        @attributes ||= {}
        @attributes[name] = init_val

        represent_opts_str = represent_opts.to_a.map do |pair|
          value = pair[1].is_a?(Symbol) ? ":#{pair[1]}" : pair[1].to_s
          "#{pair[0]}: #{value}"
        end.join(', ')
        param_map = {
          name: name,
          represent_type: type,
          represent_opts: "{#{represent_opts_str}}",
        }
        class_eval(format(ATTR_EVAL_STR, param_map))
      end
    end
  end
end
