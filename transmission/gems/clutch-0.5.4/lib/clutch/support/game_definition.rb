# frozen_string_literal: true

module Clutch
  include Types

  module Struct
    class GameConfig < Dry::Struct
      class GameObject < Dry::Struct
        attribute :attribute, Types::Hash
      end

      attribute :game_object, GameObject
    end

    class SystemConfig < Dry::Struct
      class EventConfig < Dry::Struct
        attribute :init,      Types::Boolean.optional.default(false, shared: true)
        attribute :next,      Types.Array(Types::String).optional.default([], shared: true)
        attribute :transform, Types.Array(Types::String).optional.default([], shared: true)
      end

      attribute :event, Types::Hash.meta(
        of: Types::Hash.meta(of: EventConfig)
      )
      attribute :system, Types::Hash.meta(of: Types.Array(Types::String))
    end
  end

  class GameDefinition < Dry::Struct
    attribute :game_config,   Struct::GameConfig
    attribute :system_config, Struct::SystemConfig
  end
end
