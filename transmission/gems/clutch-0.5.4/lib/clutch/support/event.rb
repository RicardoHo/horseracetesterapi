# frozen_string_literal: true

module Clutch
  include Types

  class Event < Dry::Struct
    attribute :action,    Types::String
    attribute :data_type, Types::String
    attribute :data,      Types::Hash
    alias_method :command, :action
    alias_method :input,   :data
  end
end
