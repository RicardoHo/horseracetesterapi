# frozen_string_literal: true

module Clutch
  module Types
    include ::Dry.Types

    Int     = Strict::Integer
    Nil     = Strict::Nil
    Hash    = Strict::Hash
    String  = Strict::String
    Boolean = Strict::Bool
  end
end
