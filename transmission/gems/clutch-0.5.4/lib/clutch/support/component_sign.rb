# frozen_string_literal: true

module Clutch
  module ComponentSign
    extend self

    def request
      sign
    ensure
      prepare_new_sign
    end

    def current
      sign
    end

    def sign
      @sign ||= 0
    end

    def prepare_new_sign
      @sign += 1
    end
  end
end
