# frozen_string_literal: true

module Clutch
  class Entity
    attr_reader :components
    attr_reader :singature

    # @todo
    # currently the entity signature is composite
    # by all component's signature
    # use other way to calculate entity signature maybe better
    def initialize(*components)
      @components = []
      @singature  = []

      batch_add(*components)
    end

    #
    # id of the entity
    #
    # @return [Integer]
    #
    def id
      __id__
    end

    #
    # find specific component by component class, nil if not found
    #
    # @param [Class] component_class
    #
    # @return [Clutch::Component]
    # @return [NilClass] if not found
    #
    def find(component_class)
      @components.find do |component|
        component.is_a?(component_class)
      end
    end

    def select(*component_classes)
      component_classes.each_with_object([]) do |comp, list|
        list.push(find(comp))
      end
    end

    #
    # is the entity increased or reduced in terms of
    # the add or remove of component(s)
    #
    # @return [Boolean]
    #
    def dirty?
      @increased || @reduced
    end

    #
    # is the entity increased which means has new component add-in
    #
    # @return [Boolean]
    #
    def increased?
      @increased
    end

    #
    # is the entity reduced which means has component removed
    #
    # @return [Boolean]
    #
    def reduced?
      @reduced
    end

    #
    # reset the increased and reduced flag to false
    #
    def changes_applied
      @increased = false
      @reduced   = false
    end

    #
    # add component in
    #
    # @param [Clutch::Component] new_comp new component instance to be add
    #
    def add(new_comp)
      return if @singature.include?(new_comp.class::SIGNATURE)

      @increased = true
      @components.push(new_comp)
      @singature.push(new_comp.class::SIGNATURE)

      self
    end

    #
    # remove specific component
    #
    # @param [Clutch::Component] comp
    #
    def remove(comp)
      return unless @singature.include?(comp.class::SIGNATURE)

      @reduced = true
      @components.delete(comp)
      @singature.delete(comp.class::SIGNATURE)

      self
    end

    #
    # batch add multiple component into entity
    #
    # @param [Array] *comps each component is kind of Clutch::Component
    #
    # @return [Entity]
    #
    def batch_add(*comps)
      comps.each { |comp| add(comp) }
      self
    end

    #
    # batch remove multiple component from entity
    #
    # @param [Array<Clutch::Component>] *comps
    #
    # @return [Entity]
    #
    def batch_remove(*comps)
      comps.each { |comp| remove(comp) }
      self
    end

    def remove_all
      @components.each { |comp| remove(comp) }
      self
    end

    #
    # determine a given ecs has at least one component dirty
    #
    # @param [Array<Clutch::Component>] ecs
    #
    # @return [Boolean]
    #
    def ecs_dirty?(ecs)
      @components.any? do |comp|
        ecs.include?(comp.class) && comp.dirty?
      end
    end

    #
    # render all component data in hash format
    #
    # @return [Hash]
    #
    def component_hash
      @components.each_with_object({}) do |comp, comp_hash|
        comp_hash.deep_merge!(comp.to_hash)
      end
    end

    #
    # render all component data pack with entity label in hash format
    #
    # @return [Hash]
    #
    def to_hash
      comp_hash = component_hash
      return comp_hash if comp_hash.empty?

      label = @components.find { |comp| comp.is_a?(LabelComponent) }
      return comp_hash unless label

      {
        "#{label.title}": comp_hash,
      }
    end
  end
end
