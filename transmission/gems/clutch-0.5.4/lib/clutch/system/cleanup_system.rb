# frozen_string_literal: true

module Clutch
  module System
    class CleanupSystem < EntitySystem
      class <<self
        def cleanup(action, components: [], condition: nil)
          raise NoMethodError,
            format(INCORRECT_CLEANUP_ACTION_MSG, action) \
            unless private_method_defined?(action)

          class_eval <<~EVAL, __FILE__, __LINE__
            def process(entity)
              return unless #{condition.nil?} || #{condition}(entity)

              #{action}(entity, *#{components})
            end
          EVAL
        end
      end

      private

      def destroy(entity)
        @destroy_entities.push(entity)
      end

      def remove(entity, *components)
        components = entity.components.select do |comp|
          self.class::WATCHS.include?(comp.class)
        end if components.empty?
        entity.batch_remove(*components)
        @dirty_entities.push(entity)
      end

      def reset(entity, *components)
        components = entity.components if components == :all
        components = entity.components.select do |comp|
          self.class::WATCHS.include?(comp.class)
        end if components.empty?
        components.each(&:reset)
      end
    end
  end
end
