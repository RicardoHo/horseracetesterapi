# frozen_string_literal: true

module Clutch
  module System
    #
    # Initialize system only execute on gamebox start
    # for setup purpose, it is no need to lookup
    # effective component slice for the execution.
    #
    class InitializeSystem < BaseSystem
      def execute
        return unless executable?

        before_all
        process_all
        after_all

        @result
      ensure
        recover
      end

      protected

      def executable?
        !@initialized
      end

      def process_all
        process
        @initialized = true
      end

      def process
        raise NotImplementedError
      end
    end
  end
end
