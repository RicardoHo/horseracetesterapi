# frozen_string_literal: true

module Clutch
  module System
    class ReactiveSystem < EntitySystem
      # @return [Array<Clutch::Entities>]
      attr_reader :active_entities
      # @return [Array<Clutch::Component>]
      attr_accessor :after_react

      def initialize(game_definition, game_engine)
        super
        self.after_react = []
      end

      #
      # override
      # invoke before #process_all
      # use to build up active entities in reactive system
      # in terms of new entities to the system and/or
      # all ecs dirty non-new entities
      #
      def before_all
        @active_entities = @entities.difference(@entities_snapshot)
        @active_entities += ecs_dirty_entities
      end

      #
      # override
      # invoke after #process_all
      # use to build up entities snapshot in reactive system
      #
      def after_all
        @entities_snapshot = @entities.dup
      end

      def entities_handle=(handle)
        super
        @entities_snapshot = handle
      end

      #
      # overrid
      # invoke after #process
      # notify components change has been applied if any
      #
      def after_process
        after_react.each do |component_class|
          @entity.find(component_class).changes_applied
        end
      end

      private

      #
      # override
      # clean active entities in reactive system
      #
      def recover
        super
        @active_entities = nil
      end

      #
      # find all ecs dirty entities
      #
      def ecs_dirty_entities
        @entities.intersection(@entities_snapshot).select do |entity|
          entity.ecs_dirty?(self.class::WATCHS)
        end
      end
    end
  end
end
