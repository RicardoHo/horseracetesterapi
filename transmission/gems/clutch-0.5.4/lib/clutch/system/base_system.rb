# frozen_string_literal: true

module Clutch
  module System
    class BaseSystem
      def initialize(game_def, game_engine)
        @result      = SystemResult.new
        @game_def    = game_def
        @game_engine = game_engine
      end

      #
      # the system execute entry API
      # the execute invoke method in below sequence
      # #executable? -> #before_all -> #process_all -> #after_all
      # and invoke #recover after all in ensure scope
      #
      # @param [Symbol] command
      # @param [Object] input
      #
      # @return [Clutch::System::SystemResult]
      #
      def execute(command, input)
        @command = command
        @input   = input
        @result.reset
        return unless executable?

        before_all
        process_all
        after_all

        @result
      ensure
        recover
      end

      protected

      #
      # executable api, check before real processing take place
      # by default always true
      #
      # @return [Boolean]
      #
      def executable?
        true
      end

      #
      # invoke before #process_all
      #
      def before_all; end

      #
      # the system main logic entry
      #
      def process_all
        raise NotImplementedError
      end

      #
      # invoke after #process_all
      #
      def after_all; end

      #
      # to recover all variabels that holds
      # execute-related data to clean state
      #
      def recover
        @command = nil
        @input   = nil
      end

      #
      # use to create entity and record the newly create entity
      #
      # @param [Array<Clucth::Component>] *components
      #
      # @return [Clutch::Entity]
      #
      def create_entity(*components)
        entity = Clutch::Entity.new(*components)
        @result.new_entity(entity)

        entity
      end
    end
  end
end
