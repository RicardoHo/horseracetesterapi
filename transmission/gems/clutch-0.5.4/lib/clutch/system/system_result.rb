# frozen_string_literal: true

module Clutch
  module System
    class SystemResult
      attr_reader :adds
      attr_reader :deletes
      attr_reader :dirties

      def initialize
        reset
      end

      def reset
        @adds    = []
        @deletes = []
        @dirties = []
      end

      def new_entity(entity)
        @adds.push(entity)
      end

      def destroyed_entity(entity)
        @deletes.push(entity)
      end

      def dirty_entity(entity)
        @dirties.push(entity)
      end
    end
  end
end
