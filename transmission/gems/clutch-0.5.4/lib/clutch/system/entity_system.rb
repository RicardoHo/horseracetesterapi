# frozen_string_literal: true

module Clutch
  module System
    class EntitySystem < BaseSystem
      class <<self
        def namespace_for_watch=(namespace)
          const_set(
            :NAMESPACE,
            Object.const_get(
              namespace.to_s.constanize
            )
          )
        end

        def watch(*component_classes)
          return if const_defined?(:WATCHS)

          component_classes.map! do |klass|
            NAMESPACE.const_get(klass.to_s.camel_case)
          end if NAMESPACE
          signature = component_classes.map do |comp_class|
            comp_class::SIGNATURE
          end.sort!
          const_set(:WATCHS, component_classes)
          const_set(:ECS_SIGNATURE, signature)
        end

        def reference(*component_classes)
          return if const_defined?(:REFERENCE)

          component_classes.map! do |klass|
            NAMESPACE.const_get(klass.to_s.camel_case)
          end if NAMESPACE
          signature = component_classes.map do |comp_class|
            comp_class::SIGNATURE
          end.sort!
          const_set(:REFERENCE, component_classes)
          const_set(:REF_ECS_SIGNATURE, signature)
        end

        def create(trigger_point, source:)
          parameter = method(trigger_point.to_sym).parameters.to_h[:req]
          param_str = ''
          param_str = "(#{parameter})" if parameter
          class_eval <<~EVAL
            alias_method :_trigger_point, :trigger_point
            def #{trigger_point}#{param_str}
              _trigger_point#{param_str}
              create_entity(send(#{source}))
            end
          EVAL
        end

        # TODO
        # setup data source and parser for generate
        # check in data after each execute
        def check_in; end

        # TODO
        # check out data from game engine
        # and build up entities
        def check_out; end
      end

      def entities_handle=(handle)
        return if @entities

        @entities = handle
      end

      def reference_handle=(handle)
        return if @references

        @references = handle
      end

      protected

      def active_entities
        @entities
      end

      def references
        return unless @references

        @references
          .flat_map do |ref|
            ref.select(*self.class::REFERENCE)
          end
          .group_by do |comp|
            comp.class.name.split('::').last.snake_case.to_sym
          end
      end

      def reference_entities
        return unless @references

        @references
      end

      def process_all
        active_entities.each do |entity|
          @entity = entity
          before_process
          process(entity)
          update_process_result(entity)
          after_process
        end
      ensure
        @entity = nil
      end

      def before_process; end

      def process(_entity)
        raise NotImplementedError
      end

      def update_process_result(entity)
        @result.dirty_entity(entity) if entity.dirty?
      end

      def after_process; end
    end
  end
end
