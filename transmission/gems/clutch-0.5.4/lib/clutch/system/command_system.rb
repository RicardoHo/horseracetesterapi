# frozen_string_literal: true

module Clutch
  module System
    class CommandSystem < EntitySystem
      class CommandTasks < Hash
        def add(name, method)
          self[name] = method
        end
      end

      class <<self
        attr_reader :tasks

        def inherited(subclass)
          subclass.init_tasks
        end

        def init_tasks
          @tasks = CommandTasks.new
        end

        def command(name, command_method = nil)
          command_method = :"process_#{name}" unless command_method
          @tasks.add(name.to_s, command_method)
        end
      end

      private

      def executable?
        self.class.tasks.key?(@command.snake_case)
      end

      def before_all
        @command_method = self.class.tasks.fetch(@command.snake_case)
      end

      def recover
        super
        @command_method = nil
      end

      def process(entity)
        send(@command_method, entity)
      end
    end
  end
end
