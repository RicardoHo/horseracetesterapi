# frozen_string_literal: true

class Hash
  #
  # convert key to symbol form resursivly
  #
  # @return [Hash]
  #
  def deep_symbolize_keys
    deep_transform_keys(&:to_sym)
  end

  def deep_merge(other)
    merger = proc do |_key, v1, v2|
      if v1.is_a?(Hash) && v2.is_a?(Hash)
        v1.merge(v2, &merger)
      elsif v1.is_a?(Array) && v2.is_a?(Array)
        v1 + v2
      else
        v2
      end
    end
    merge(other, &merger)
  end

  def deep_merge!(other)
    merger = proc do |_key, v1, v2|
      if v1.is_a?(Hash) && v2.is_a?(Hash)
        v1.merge!(v2, &merger)
      elsif v1.is_a?(Array) && v2.is_a?(Array)
        v1 + v2
      else
        v2
      end
    end
    merge!(other, &merger)
  end

  private

  def deep_transform_keys(&block)
    _deep_transform_keys_in_object(self, &block)
  end

  def _deep_transform_keys_in_object(object, &block)
    case object
    when Hash
      object.each_with_object({}) do |(key, value), result|
        result[yield(key)] = _deep_transform_keys_in_object(value, &block)
      end
    when Array
      object.map { |e| _deep_transform_keys_in_object(e, &block) }
    else
      object
    end
  end
end
