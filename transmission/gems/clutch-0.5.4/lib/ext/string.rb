# frozen_string_literal: true

#
# Extend ruby class
#
class String
  #
  # @author Alpha Huang
  # @since 0.1.0
  #
  # represent string in camel case
  # can camelize sneak case,
  # lower camel case (camelCase) and
  # title case (Title Case/title case)
  # @example
  #   'foo_bar'.camel_case => 'FooBar'
  #
  # @param [Boolean] lower
  #
  # @return [String]
  #
  def camel_case(lower: false)
    return dup.tap do |e|
      e[0] = lower ? e[0].downcase : e[0].upcase
    end unless match?(/[_: -]+/)

    split(/[_: -]+/)
      .map(&:capitalize)
      .join
      .tap { |e| e[0] = e[0] && lower ? e[0].downcase : e[0] }
  end

  #
  # @author Alpha Huang
  # @since 0.1.0
  #
  # represent string in sneak case
  # support covert camel case, lower camel
  # and ruby class name (Foo::Boo)
  # @example
  #   'FooBar'.snake_case => 'foo_bar'
  #
  # @return [String]
  #
  def snake_case
    gsub(/::/, '')
      .gsub(/([A-Z]+)([A-Z][a-z])/, '\1_\2')
      .gsub(/([a-z\d])([A-Z])/, '\1_\2')
      .gsub(/(\w+) (\w+)/, '\1_\2')
      .tr(' -', '_')
      .downcase
  end

  #
  # transform to ruby class constant format,
  # the string should seperate each word by dot
  # @example
  #   'foo.boo'.constanize => 'Foo::Boo'
  #   'foo_boo'.constanize => 'Foo::Boo'
  #
  # @return [String]
  #
  def constanize
    split('.')
      .map(&:camel_case)
      .join('::')
  end
end
