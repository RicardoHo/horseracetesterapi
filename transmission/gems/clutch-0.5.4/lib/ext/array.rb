# frozen_string_literal: true

class Array
  #
  # determine is self a subset of other array
  #
  # @param [Array] other potential superset of self
  #
  # @return [Boolean]
  #
  def subset?(other)
    (self - other).empty?
  end

  #
  # determine is self intersect with other array
  #
  # @param [Array] other potential intersection array
  #
  # @return [Boolean]
  #
  def intersect?(other)
    !(self & other).empty?
  end

  #
  # return the intersection of self to the other array
  #
  # @param [Array] other
  #
  # @return [Array]
  #
  def intersection(other)
    self & other
  end

  #
  # return the union of self to the other array
  #
  # @param [Array] other
  #
  # @return [Array]
  #
  def union(other)
    self | other
  end

  #
  # return the difference of self to the other array
  #
  # @param [Array] other
  #
  # @return [Array]
  #
  def difference(other)
    self - other
  end
end
