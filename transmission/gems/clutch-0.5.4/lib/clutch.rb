# frozen_string_literal: true

require 'aasm'
require 'forwardable'
require 'representable'
require 'multi_json'
# require 'oj'
require 'dry-struct'

$LOAD_PATH.unshift(__dir__)
# extend ruby native class
require 'ext/hash'
require 'ext/array'
require 'ext/string'

# clutch core class
require 'clutch/version'
require 'clutch/support'
require 'clutch/errors'
require 'clutch/entity'
require 'clutch/component'
require 'clutch/label_component'
require 'clutch/system'
require 'clutch/manager'
require 'clutch/system_admin'
require 'clutch/state_machine'
require 'clutch/gamebox'

module Clutch
  extend self

  def create_gamebox
    Clutch::Gamebox.new
  end
end
