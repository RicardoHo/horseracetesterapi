require 'faker'
require 'jwt'
require_relative 'worker'
require_relative 'stats'
require_relative '../../lib/pinion-api.rb'

module Stress
  class Client < Worker
    attr_reader :stats, :instance, :player_token

    def random_string(length = 5)
      value = ''
      source = ('a'..'z').to_a + ('A'..'Z').to_a + (0..9).to_a + ['_', '-', '.']
      length.times { value += source[rand(source.size)].to_s }
    end

    def random_data(length = 5, keys=5)
      data = {}
      keys.times do
        key = Faker::App.name.to_sym
        data[key] = random_string(length)
      end
      data
    end

    def random_bet_opts
      opts = {}
      @values[:bet_opts].each do |opt|
        opts[opt] = rand(100) + 1
      end
      opts
    end

    def random_payout_opts
      opts = {}
      @values[:bet_opts].each do |opt|
        opts[opt] = rand(5)
      end
      opts
    end

    def set_default_values
      @values = {
        secret: 'secret',
        game_id: 1,
        property_id: 1,
        instance_id: nil,
        bet_opts: [:a, :b, :c],
        bet_count: 2,
        result_count: 1
      }.merge!(@options)
      init_player_token
    end

    def init_player_token
      player_id = rand(10000000000) + 1
      name = Faker::App.name
      data = {
        licensee_id: @values[:property_id],
        casino_id: @values[:property_id],
        property_id: @values[:property_id],
        player_id: player_id,
        login_name: name,
        acct_login: name,
        holdername: name,
        currency_id: 1,
        currency: "EUR",
        whitelisted: false,
        test_mode: false,
        created_at: "2019-02-26 10:32:47 +0000"
      }
      @player_token = JWT.encode data, @values[:secret], 'HS256'
    end

    def reload
      @instance = PinionApi.get_game_instance(
        player_token,
        nil,
        @values[:game_id],
        @values[:instance_id]
      )
      @instance.open
      @values[:instance_id] = @instance.instance_id unless @values[:instance_id]
    end

    def on_init
      # For init game instance
      reload
      instance.leave
    end

    def default_stats
      [:open, :init_bet, :multi_bet, :update, :result, :take_win, :leave].each_with_object({}) do |key, result|
        result[key] = Stats.new
      end
    end

    def run
      @stats = default_stats
      open
      update
      @values[:bet_count].times do |i|
        i > 0 ? multi_bet : init_bet
      end
      @values[:result_count].times { result }
      take_win
      leave
      stats
    end

    def open
      st = Time.now
      reload
      stats[:open].count += 1
      stats[:open].tt += Time.now - st
    end

    def update
      instance.instance_data = { data: random_data(128, 5) }
      st = Time.now
      res = instance.update
      stats[:update].count += 1
      stats[:update].tt += Time.now - st
    end

    def init_bet
      instance.current_round.denom = instance.config.denoms.value.first
      instance.current_round.current_action = { data: random_data(128, 5) }
      instance.instance_data = { data: random_data(128, 5) }
      st = Time.now
      res = instance.bet(
        bet_opts: random_bet_opts
      )
      stats[:init_bet].count += 1
      stats[:init_bet].tt += Time.now - st
    end

    def multi_bet
      instance.current_round.current_action = { data: random_data(128, 5) }
      instance.instance_data = { data: random_data(128, 5) }
      st = Time.now
      res = instance.bet(
        bet_opts: random_bet_opts
      )
      stats[:multi_bet].count += 1
      stats[:multi_bet].tt += Time.now - st
    end

    def result
      instance.current_round.current_action = { data: random_data(128, 5) }
      instance.instance_data = { data: random_data(128, 5) }
      st = Time.now
      res = instance.result
      stats[:result].count += 1
      stats[:result].tt += Time.now - st
    end

    def take_win
      instance.current_round.current_action = { data: random_data(128, 5) }
      instance.instance_data = { data: random_data(128, 5) }
      st = Time.now
      res = instance.take_win(
        payout_opts: random_payout_opts
      )
      stats[:take_win].count += 1
      stats[:take_win].tt += Time.now - st
    end

    def leave
      st = Time.now
      res = instance.leave
      stats[:leave].count += 1
      stats[:leave].tt += Time.now - st
    end



  end
end
