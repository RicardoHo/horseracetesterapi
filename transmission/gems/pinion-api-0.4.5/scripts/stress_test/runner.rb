require_relative 'worker'
require_relative 'client'
require_relative 'stats'
require 'celluloid/current'
module Stress

  class Runner < Worker
    include Celluloid::Internals::Logger

    def run
      @total_num_of_req = @options[:turns] * @options[:concurrency]
      @options[:turns].times do |i|
        start_time = Time.now
        @options[:concurrency].times.map do
          @pool.future.run
        end.inject(r = []) do |result, e|
          result << e.value
        end

        @total_elapsed += Time.now - start_time
        @total_req += @options[:concurrency]

        store_stats(r)
        print_client_stats if i % @options[:print_interval] == 0
      end
      print_client_stats
    end

    def store_stats(results)
      results.each do |stats|
        stats.each do |key, single_stats|
          @total_stats[key] ||= Stats.new
          @total_stats[key].count += single_stats.count
          @total_stats[key].tt += single_stats.tt
        end
      end
    end

    def print_client_stats
      avg_elapsed = @total_elapsed / @total_req
      complete_time = Time.now + ( @total_num_of_req - @total_req ) * avg_elapsed
      @total_stats.each do |key, stats|
        avg = stats.tt / stats.count
        info "#{key} api total time: #{stats.tt} sec, avg: #{avg} sec,  Throughput: #{1.0 / avg}"
      end

      info "concurrency: #{options[:concurrency]}"
      info "Requests: #{options[:requests]}"
      info "Total Requests:: #{@total_req}"
      info "Throughput: #{1.0 / avg_elapsed} rounds/sec"
      info "Avg elapsed: #{avg_elapsed}"
      info "Stress Validation Will Completed at #{ complete_time }"
    end

    def on_init
      setup_pool
      init_client_stats
    end

    def init_client_stats
      @total_elapsed = 0
      @total_req = 0
      @total_stats = {}
    end

    def setup_pool
      @pool = Client.pool(size: @options[:concurrency], args: [client_args])
    end

    def client_args
      {
        property_id: @options[:property_id] || 1,
        game_id: @options[:game_id] || 1,
        bet_count: 2,
        result_count: 1
      }
    end
  end
end


options = {
  url: ENV.fetch('PINION_URL', 'http://0.0.0.0:3000'),
  concurrency: ENV.fetch('CONCURRENCY', '4').to_i,       # number of clients
  turns: ENV.fetch('TURNS', '100').to_i,                   # number of turn
  retryable: ENV.fetch('RETRY', 'true') == 'true',
  property_id: ENV.fetch('PROPERTY_ID', '1').to_i,
  game_id: ENV.fetch('GAME_ID', '1').to_i,
  print_interval: ENV.fetch('PRINT_INTERVAL', '1000').to_i
}


begin
  PinionApi.base_url = options[:url]
  Stress::Runner.new(options).run
rescue Exception => e
  raise e unless options[:retryable]
  puts "rescue exception #{e.message}, sleep 10s, retry"
  sleep 10
  Celluloid.shutdown
  Celluloid.boot
  puts "retry now"
  retry
end
