require 'celluloid/current'

module Stress
  class Worker
    include Celluloid
    include Celluloid::Internals::Logger

    attr_reader :options

    def initialize(**opts)
      @options = opts
      yield @options if block_given?
      set_default_values
      validate
      on_init
    end

    def worker_id
      Thread.current.object_id
    end

    def worker_name
      self.class.name
    end

    protected

    def set_default_values; end
    def validate; end
    def on_init; end
  end
end
