require File.expand_path('lib/pinion-api/version', __dir__)
# $LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name          = 'pinion-api'
  spec.version       = PinionApi::VERSION
  spec.authors       = ['Cheok Meng Chan']
  spec.email         = ['cheokmeng.chan@laxino.com']

  spec.summary       = 'API Client For Pinion'
  spec.description   = 'A simple client for Pinion Remote API'
  spec.homepage      = 'http://home.rnd.laxino.com'

  spec.require_paths = ['lib']
  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.add_dependency  'bundler',      '>= 1.16'
  spec.add_dependency  'rake',         '>= 12.3.0'
  # spec.add_dependency  'oj',           '>= 3.7.9'
  spec.add_dependency  'http',         '>= 4.0.0'
  spec.add_dependency  'multi_json',   '1.14.1'
  spec.add_dependency  'hashie',       '>= 3.5.7'
  spec.add_dependency  'hashdiff',     '>= 1.0.0'
end
