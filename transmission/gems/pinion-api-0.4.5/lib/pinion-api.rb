require 'multi_json'
require 'hashie'
require 'hashdiff'

require_relative 'pinion-api/version'
require_relative 'pinion-api/error'
require_relative 'pinion-api/client'
require_relative 'pinion-api/game_round'
require_relative 'pinion-api/game_config'
require_relative 'pinion-api/game_instance'
require_relative 'pinion-api/game'

module PinionApi
  module ClassMethod
    attr_writer :base_url
    attr_writer :developer_token

    DEFAULT_BASE_URL = 'http://localhost:3000'.freeze
    DEFAULT_DEVELOPER_TOKEN = 'token'.freeze

    def base_url
      @base_url || DEFAULT_BASE_URL
    end

    def developer_token
      @developer_token || DEFAULT_DEVELOPER_TOKEN
    end

    def reset
      @base_url = nil
      @developer_token = nil
    end

    # Alias for PinionApi::GameInstance.new
    #
    # @return [PinionApi::GameInstance]
    def get_game_instance(player_token:, machine_token:, game_id:, instance_id: nil, system_completed_code: nil, login_name:, property_id:)
      PinionApi::GameInstance.new(
        player_token: player_token,
        machine_token: machine_token,
        game_id: game_id,
        instance_id: instance_id,
        system_completed_code: system_completed_code,
        login_name: login_name,
        property_id: property_id
      )
    end
  end

  extend ClassMethod
end
