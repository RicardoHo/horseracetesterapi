module PinionApi
  # Wrapper for the PinionApi API
  class Client
    Dir[File.expand_path('client/*.rb', __dir__)].each { |f| require f }
    include Base

    module ClassMethod
      def get(url:, params: nil, headers: {})
        new.get(url: url, params: params, headers: headers)
      end

      def post(url:, params: nil, type: :json, headers: {})
        new.post(url: url, params: params, type: type, headers: headers)
      end
    end

    extend ClassMethod

    private

    def inspector
      @inspector ||= HttpExceptionInspector.new
    end
  end
end
