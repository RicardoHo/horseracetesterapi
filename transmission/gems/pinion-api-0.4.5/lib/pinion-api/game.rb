# frozen_string_literal: true

module PinionApi
  class Game
    attr_reader :game_id,
                :property_id,
                :player_token,
                :login_name,
                :base_url
    def initialize(game_id:, property_id:, player_token: nil, login_name: nil)
      @player_token = player_token
      @game_id = game_id
      @property_id = property_id
      @login_name = login_name
      @base_url = PinionApi.base_url + "/action/games/#{@game_id}"
    end

    def configs
      params = {
        property_id: @property_id,
        developer_token: PinionApi.developer_token
      }
      url = @base_url + "/configs/retrieve.json"
      res = perform(url, params)
    end

    def perform(url, params)
      headers = {
        'gs-property-id' => property_id,
        'gs-game-id' => game_id,
        'gs-login-name' => login_name
      }
      res = PinionApi::Client.get(url: url, params: params, headers: headers)
      MultiJson.load(res.body.to_s, symbolize_keys: true)
    rescue MultiJson::ParseError => e
      raise PinionApi::UnknowError, e
    end
  end
end