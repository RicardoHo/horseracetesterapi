module PinionApi
  class GameRound
    attr_reader :actions, :state, :round_options, :current_action, :game_ctx, :prog_payout_info, :jps_info
    attr_accessor :denom, :ref_id

    COMPLETED_BY_SYSTEM = 'system'
    COMPLETED_BY_PLAYER = 'player'

    def initialize(opts ={})
      @ref_id = opts[:ref_id] || nil
      @actions = opts[:actions] || []
      @denom = opts[:denom] || nil
      @state = opts[:state] || 'init'
      @round_options = opts[:round_options] || {}
      @completed_by = opts[:completed_by] || nil
      @jps_info = opts[:jps_info] || nil
      @prog_payout_info = opts[:prog_payout_info] || nil
      @current_action = nil
      @game_ctx = {}
    end

    def game_ctx=(value)
      raise ArgumentError, 'value must be hash' unless value.is_a? Hash

      @game_ctx = value
    end

    def system_completed?
      @completed_by == 'system'
    end

    def last_action
      actions.last
    end

    def init?
      state == 'init'
    end

    def opened?
      state == 'opened'
    end

    def closed?
      state == 'closed'
    end

    def hit_jps?
      !jps_info.nil?
    end

    def att_paid_jps_locked?
      state == 'att_paid_jps_locked'
    end

    def hit_prog_payout?
      !prog_payout_info.nil?
    end

    def att_paid_prog_payout_locked?
      state == 'att_paid_prog_payout_locked'
    end

    def current_action=(value)
      raise ArgumentError, 'value must bet Hash' unless value.is_a? Hash

      @current_action = value
    end

    def bet_options
      round_options.map { |key, value| round_options[key] = value[:bet_amt] }
    end

    def payout_options
      round_options.map { |key, value| round_options[key] = value[:payout_amt] }
    end

    def denom=(denom)
      @denom = denom
    end

    def update(opts = {})
      @state = opts[:state]
      @ref_id = opts[:ref_id]
      @jps_info = opts[:jps_info] || nil
      @prog_payout_info = opts[:prog_payout_info] || nil
      shift if current_action
    end

    def update_options(bet_opts: {}, payout_opts: {})
      raise ArgumentError, 'bet_opts must bet Hash' unless bet_opts.is_a? Hash
      raise ArgumentError, 'payout_opts must bet Hash' unless payout_opts.is_a? Hash

      bet_opts.each do |opt, bet_amt|
        opt_sym = opt.to_sym
        round_options[opt_sym] ||= { bet_amt: 0, payout_amt: 0 }
        round_options[opt_sym][:bet_amt] += bet_amt
      end

      payout_opts.each do |opt, payout_amt|
        opt_sym = opt.to_sym
        round_options[opt_sym][:payout_amt] ||= 0
        round_options[opt_sym][:payout_amt] = payout_amt
      end
    end

    def shift
      @actions << @current_action
      @current_action = nil
    end

    def request_info
      {
        current_action: @current_action,
        game_ctx: @game_ctx
      }
    end
  end
end
