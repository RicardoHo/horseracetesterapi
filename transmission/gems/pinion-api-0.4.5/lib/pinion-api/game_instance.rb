# frozen_string_literal: true

module PinionApi
  class GameInstance
    extend Forwardable

    attr_reader :game_id,
                :player_token,
                :machine_token,
                :instance_id,
                :system_completed_code,
                :seq_id,
                :instance_data,
                :currency,
                :current_round,
                :last_round,
                :session_id,
                :game_config,
                :inspection_info,
                :property_id,
                :login_name

    def initialize(player_token:, machine_token:, game_id:, instance_id: nil, system_completed_code: nil, login_name:, property_id:)
      @player_token = player_token
      @machine_token = machine_token
      @game_id = game_id
      @property_id = property_id
      @login_name = login_name
      @instance_id = instance_id
      @seq_id = nil
      @base_url = PinionApi.base_url + "/action/games/#{game_id}/instances"
      @base_url += "/#{instance_id}" if instance_id
      @system_completed_code = system_completed_code

      @current_round = nil
      @last_round = nil
      @session_id = nil
      @game_config = nil
      @pre_instance_data = {}
      @instance_data = {}
      @inspection_info = nil
    end

    alias config game_config

    def instance_data=(data = {})
      raise ArgumentError, 'data must be Hash' unless data.is_a? Hash

      @instance_data = data
    end

    def test_mode_enabled?
      !!inspection_info[:test_mode]
    end

    alias test_mode test_mode_enabled?

    def open
      params = request_base_params
      url = @base_url + '/open'
      res = perform(url, params)
      unless instance_id
        @instance_id = res[:instance_id]
        @base_url += "/#{instance_id}"
      end
      @session_id = res[:session_id]
      @currency = res[:currency]
      @instance_data = res[:instance] || {}
      @inspection_info = res[:inspection] || {}
      update_data(res)
      init_round_data(res)
      init_game_config(res[:game_config])
    end

    def bet(bet_opts:)
      params = request_base_params
      params[:denom] = current_round.denom
      params[:bet_options] = bet_opts
      if current_round.current_action
        params[:round] = current_round.request_info
      end
      params[:instance] = { replace: instance_data } if instance_data_changed?
      url = @base_url + '/bet'
      res = perform(url, params)
      update_data(res)
      @current_round.update(res[:round])
      @current_round.update_options(
        bet_opts: bet_opts
      )
      {
        balance: res[:balance],
        lottery_ctx: res[:lottery_ctx]
      }
    end

    def result
      params = request_base_params
      if current_round.current_action
        params[:round] = current_round.request_info
      end
      params[:instance] = { replace: instance_data } if instance_data_changed?
      url = @base_url + '/result'
      res = perform(url, params)
      update_data(res)
      @current_round.update(res[:round])
      nil
    end

    def take_win(payout_opts: {})
      params = request_base_params
      unless payout_opts.empty?
        params[:payout_options] = payout_opts
        @current_round.update_options(
          payout_opts: payout_opts
        )
      end
      if current_round.current_action
        params[:round] = current_round.request_info
      end
      params[:instance] = { replace: instance_data } if instance_data_changed?
      url = @base_url + '/take_win'
      res = perform(url, params)
      code = res[:code]
      update_data(res)
      @current_round.update(res[:round])

      case code
      when 'OK'
        @last_round = @current_round
        @current_round = GameRound.new(res[:next_round])
        {
          balance: res[:balance],
          lottery_ctx: res[:lottery_ctx]
        }
      when 'hit_jps'
        {
          jps_info: res.dig(:round, :jps_info)
        }
      end
    end

    def update
      return unless instance_data_changed?

      params = request_base_params
      params[:instance] = { replace: instance_data }
      url = @base_url + '/update'
      res = perform(url, params)
      update_data(res)
      nil
    end

    def leave
      params = request_base_params
      url = @base_url + '/leave'
      res = perform(url, params)
      update_data(res)
      nil
    end

    private

    def instance_data_changed?
      !Hashdiff.diff(@pre_instance_data, instance_data).empty?
    end

    def update_data(res)
      @seq_id = res[:seq_id]
      @pre_instance_data = Marshal.load(Marshal.dump(instance_data))
    end

    def init_game_config(config)
      @game_config = GameConfig.new(config)
    end

    def init_round_data(res)
      @current_round = GameRound.new(res[:round])
      @last_round = GameRound.new(res[:last_round]) if res[:last_round]
    end

    def request_base_params
      params = {
        player_token: @player_token,
        machine_token: @machine_token,
        game_id: game_id,
        seq_id: @seq_id,
        developer_token: PinionApi.developer_token
      }
      if @system_completed_code
        params[:system_completed_code] = system_completed_code
      end
      params
    end

    def perform(url, params)
      headers = {
        'gs-property-id' => property_id,
        'gs-game-id' => game_id,
        'gs-login-name' => login_name
      }
      res = PinionApi::Client.post(url: url, params: params, headers: headers)
      MultiJson.load(res.body.to_s, symbolize_keys: true)
    rescue MultiJson::ParseError => e
      raise PinionApi::UnknowError, e
    end
  end
end
