# frozen_string_literal: true

module PinionApi
  class Denoms < Hashie::Trash
    include Hashie::Extensions::IgnoreUndeclared
    include Hashie::Extensions::Dash::Coercion
    include Hashie::Extensions::MergeInitializer

    property :value,              coerce: Array(Integer)
    property :default_index,      coerce: Integer
  end

  class GameConfig < Hashie::Trash
    include Hashie::Extensions::IgnoreUndeclared
    include Hashie::Extensions::Dash::Coercion
    include Hashie::Extensions::MergeInitializer

    property :math_name,           coerce: String
    property :denoms,              coerce: Denoms
    property :extra,               coerce: Hash
  end
end
