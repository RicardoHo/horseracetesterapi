require 'http'
module PinionApi
  class Client
    module Base
      DEFAULT_TIMEOUT = 8 # sec

      # Post to a response
      # @params url String
      # @option params [Hash]
      # @return [HTTP::Response]
      def post(url:, params: nil, type: :json, headers: {})
        perform(:post, url, type => params, headers: headers)
      end

      # Get to a response
      # @params url String
      # @option params [Hash]
      # @return [HTTP::Response]
      def get(url:, params: nil, headers: {})
        perform(:get, url, params: params, headers: headers)
      end

      private

      # @return [HTTP::Response]
      def perform(verb, url, opts = {})
        response = HTTP.timeout(DEFAULT_TIMEOUT).request verb, url, opts
        inspector.call(response)
      rescue PinionApi::Error => e
        raise e
      rescue HTTP::TimeoutError => e
        raise PinionApi::TimeoutError.new(e)
      rescue HTTP::ConnectionError => e
        raise PinionApi::ConnectionError.new(e)
      else
        response
      end
    end
  end
end
