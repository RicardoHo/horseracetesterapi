module PinionApi
  class Client
    class HttpExceptionInspector
      def call(response)
        case response.code
        when 200
          true
        when 400
          raise PinionApi::BadRequestError.from_response(response.body.to_s)
        when 448
          raise PinionApi::UnrecoverableError.from_response(response.body.to_s)
        when 449
          raise PinionApi::RecoverableError.from_response(response.body.to_s)
        when 450
          raise PinionApi::RetryableError.from_response(response.body.to_s)
        when 500
          raise PinionApi::InternalServerError, 'Something is technically wrong.'
        when 502
          raise PinionApi::BadGateway, 'The server returned an invalid or incomplete response.'
        when 503
          raise PinionApi::ServiceUnavailable, 'Server is not ready to handle the request.'
        when 504
          raise PinionApi::GatewayTimeout, 'Gateway Time-out.'
        else
          raise PinionApi::UnknowError, 'Unknow Error.'
        end
      end
    end
  end
end
