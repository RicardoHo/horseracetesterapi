require_relative '../lib/pinion-api.rb'

def get_random_string(length = 5)
  source = ('a'..'z').to_a + ('A'..'Z').to_a + (0..9).to_a + ['_', '-', '.']
  key = ''
  length.times { key += source[rand(source.size)].to_s }
  key
end

denom = 10
bet_amt = 10
bet_opts = {
  a: 5,
  b: 5
}
payout_amt = 20
payout_opts = {
  b: 20
}
machine_token = ''
player_token = 'eyJhbGciOiJIUzI1NiJ9.eyJsaWNlbnNlZV9pZCI6MSwiY2FzaW5vX2lkIjoxLCJwcm9wZXJ0eV9pZCI6MSwicGxheWVyX2lkIjo1NDcwNjA5ODUxNTMxNzE2MDAwLCJsb2dpbl9uYW1lIjoidGVzdDAwNCIsImFjY3RfbG9naW4iOiJ0ZXN0MDA0IiwiaG9sZGVybmFtZSI6InRlc3RfaG9sZGVybmFtZTMiLCJjdXJyZW5jeV9pZCI6OCwiY3VycmVuY3kiOiJFVVIiLCJ3aGl0ZWxpc3RlZCI6ZmFsc2UsInRlc3RfbW9kZSI6ZmFsc2UsImNyZWF0ZWRfYXQiOiIyMDE5LTAyLTI2IDEwOjMyOjQ3ICswMDAwIn0.Ftt2Nbpp7nF_hq39V0yCYlIDGEtki3zr-pFT6V28x9s'
developer_token = 'ias'
game_id = 1
instance_id = nil #'01DS4MPGCHYWTX7WSF7GM35GJF'
PinionApi.base_url = 'http://0.0.0.0:3000'

def print_round_info(round)
  return
  p round.denom
  p round.ref_id
  p round.state
  p round.game_ctx
  p round.actions
  p round.last_action
  p round.round_options
end

def print_instance_info(instance)
  return
  p instance.instance_id
  p instance.currency
  p instance.session_id
  p instance.current_round
  p instance.last_round
  p instance.instance_data
  p instance.game_config.extra
end

tts = {
  open: 0,
  leave: 0,
  bet: 0,
  bet_2: 0,
  result: 0,
  take_win: 0,
  update: 0
}

property_id = 1
login_name = 'Test'
tt = 1

bt = 1
tt.times do
  gi = PinionApi.get_game_instance(
    player_token: player_token,
    machine_token: machine_token,
    game_id: game_id,
    instance_id: instance_id,
    property_id: property_id,
    login_name: login_name
  )

  ts = Time.now
  res = gi.open
  tts[:open] += Time.now - ts

  print_instance_info(gi)
  print_round_info(gi.current_round)

  bt.times do
    # p "=" * 10 + "Game Instance Update" + "=" * 10
    gi.instance_data = { data: get_random_string(512) }
    ts = Time.now
    res = gi.update
    tts[:update] += Time.now - ts
    print_instance_info(gi)
    print_round_info(gi.current_round)

    # p "=" * 10 + "Bet" + "=" * 10
    gi.current_round.current_action = { data: get_random_string(512) }
    gi.current_round.game_ctx = { ctx: get_random_string(512) }
    gi.current_round.denom = denom
    gi.instance_data = { data: get_random_string(512) }
    ts = Time.now
    res = gi.bet(
      bet_opts: bet_opts
    )
    tts[:bet] += Time.now - ts
    print_instance_info(gi)
    print_round_info(gi.current_round)

    gi.current_round.current_action = { data: get_random_string(512) }
    gi.current_round.denom = denom
    gi.instance_data = { data: get_random_string(512) }
    ts = Time.now
    res = gi.bet(
      bet_opts: bet_opts
    )
    tts[:bet_2] += Time.now - ts
    # p "Bet 2 #{res}"
    print_instance_info(gi)
    print_round_info(gi.current_round)

    # p "=" * 10 + "Result" + "=" * 10
    gi.current_round.current_action = { data: get_random_string(512) }
    gi.instance_data = { data: get_random_string(512) }
    ts = Time.now
    res = gi.result
    tts[:result] += Time.now - ts
    print_instance_info(gi)
    print_round_info(gi.current_round)

    # p "=" * 10 + "Take Win" + "=" * 10
    gi.current_round.current_action = { data: get_random_string(512) }
    ts = Time.now
    res = gi.take_win(
      payout_opts: payout_opts
    )
    # p "Take Win #{res}"
    tts[:take_win] += Time.now - ts
    print_instance_info(gi)
    print_round_info(gi.current_round)
  end
  ts = Time.now
  res = gi.leave
  tts[:leave] += Time.now - ts
end
tts.each do |key, value|
  p "#{key} total time : #{value}, avg. time: #{value / tt}"
end
