# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'config_kit/version'

Gem::Specification.new do |spec|
  spec.name          = "config_kit"
  spec.version       = ConfigKit::VERSION
  spec.authors       = ["Ben Wu"]
  spec.email         = ["ben.wu@laxino.com"]

  spec.summary       = %q{Write a short summary, because Rubygems requires one.}
  spec.description   = %q{Write a longer description or delete this line.}
  spec.homepage      = "http://github.com/cheokman"
  spec.licenses      = ['MIT', 'Ruby']

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "https ://rubygems.org"
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "bin"
  spec.executables   = ['ck']
  spec.require_paths = ["lib"]

  spec.add_development_dependency "rspec", "~> 3.0"

  spec.add_dependency "bundler", "~> 1.12"
  spec.add_dependency "diplomat", "2.0.2"
  spec.add_dependency "git", "1.3.0"
end