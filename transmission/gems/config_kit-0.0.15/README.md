# Configuration Kit(config_kit)

## Introduction

config_kit is a tool to manage the application configuration using consul as back service as configuration store, and support configuration versioning, rollout or rollback control.

## How to Use

To understand config_kit, first we need to understand basic configuration management approaches using in config_kit. Next we will provide quick starting for you to experience config_kit functionalities.

### Seven Types of Configuration Management

config_kit provides command line(ck) to init, bootstrap, deploy, rollout, rollback for appication configuration

* init: init is to initialize metadata for specific data center, which will including the informaction of data center name and entitled environment for the data center

```bash
ck init -n 'moidc' -e 'Laxino.MO.Staging' -c 'stg0'
```

* describe: retrieve inforamation about all configuration or app configuration

>describe all information of configuration

```bash
ck describe
```

>describe one application information of configuration

```bash
ck describe --app axle
```

>describe specified version of an application of configuration

```bash
ck describe --app axle --version 1.0.0
```


* get: get is to retrive the full contents of the configuration

>get current version full configuration information

```bash
ck get
```

>get current version full configuration information and output to yaml format

```bash
ck get -o yaml
```

>get current version full configuration of an application

```bash
ck get --app axle -o yaml
```

>get specify version configuration of an application

```bash
ck get --app axle --version 1.0.0 -o yaml
```

* bootstrap: bootstrap is to create a application configuration tied specified version, and will not take active after bootstrap.

>bootstrap from file

```shell
ck bootstrap --from-file file:///config/int0 --app all --version 1.0.0
```

>bootstrap from git

```bash
ck bootstrap --from-git ssh://git_repository/config.git --app all --version=1.0.0
```

* deploy: deploy is not only to creat a application configuration, but also set as default configuration for active after deploy

* rollout: rollout is to activate specified version.

* rollback: TBD

### Setup Consul Storage

To setup consul, we using docker to run a testing consul server, if you need a consul server in production, pls using stack file in cluster folder.

```
docker container run -d --name=consul -p 8400:8400 -p 8500:8500 -p 8600:53/udp consul
```

You can open urs http://localhost:8500 to access UI of consul

### Prepare the configuration data

To create configuration data, pls refere to README in config folder, and two configuration data for axle and infra is placed in same folder as well.

### Default Setting of config_kit

In order to simplified process for new user, we have default settings in config_kit to shorten start step for new user.

In config_kit

* default consul server: http://localhost:8500
* default configuration data: ./config (where example configuration is ready for you)

### Let's Go

Assume consul container is up and running, so we can use deploy command to deploy the first configuration in config folder

```
ck bootstrap
```

Then open http://localhost:8500 to check the configuration data.

### How to manage configuration data

### Todo

#### Cli
- [ ] provide deployment summary for deploy command;
- [ ] provide bootstrp summary for bootstrap command;
- [ ] provide rollback summary for rollback command;
- [ ] provide rollout summary fro rollout command;
- [ ] support --pretty options in get command for output formatting;
- [ ] enhance error output for all comamnds;

#### Configuration
- [X] support change configuration throught environment;

#### Data Loader
- [ ] support load configuration data from git repository;
- [ ] support checksum for loaded data;
- [ ] join all array elements with comma;

#### Doc
- [ ] provide writing configuration data file guideline;
- [ ] complete manage configuration data section;

#### Manager
- [ ] provide API to integrate with d13n, which can get configuration data from application;

## Thank you!
Pls let me know if you have any problems.

