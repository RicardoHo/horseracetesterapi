require 'diplomat'
require 'config_kit/error'
module ConfigKit
  class Client
    class ConfigKitCreateError < ConfigKit::Error; end
    class ConfigKitUpdateError < ConfigKit::Error; end
    class ConfigKitTxnError < ConfigKit::Error; end
    class ConfigKitReadError < ConfigKit::Error; end
    attr_reader :url, :acl_token, :opts

    class ConsulConnection
      attr_reader :config, :connection

      extend Forwardable
      def_delegators :@connection, :put, :get, :delete, :txn
      def initialize(url, acl_token)
        setup_consul(url, acl_token)
      end

      def setup_consul(url, acl_token)
        @config = Diplomat.configure do |config|
          config.url = url
          config.acl_token = acl_token unless acl_token.nil?
        end
        @connection = Diplomat
      end

      def put!(key, value, options = nil)
        response = put(key, value, options = nil)
        raise ConfigKitUpdateError, "Config Kit Update key:#{key} error" unless response
        response
      end

      def create_txn(data)
        prepare_txn_data(data,type='create')
        self
      end

      def read_txn(data,recurse=false)
        prepare_txn_data(data,type='read',recurse)
        self
      end

      def update_txn(data)
        prepare_txn_data(data,type='read')
        self
      end

      def delete_txn(data,recurse=false)
        prepare_txn_data(data,type='delete',recurse)
        self
      end

      def init_txn
        reset_txn_data
      end

      def perform_txn
        begin
          txn(@txn_data)
        rescue => e
          raise ConfigKitTxnError.new "perform txn error:#{e.message}"
        ensure
          reset_txn_data
        end
      end

      def reset_txn_data
        @txn_data = []
      end

      def prepare_txn_data(data, type='read', recurse=false)
        init_txn if @txn_data.nil?
        verb = determine_verb(type, recurse)
        data.each_pair do |k,v|
          kv = {
             "Verb" => verb,
              "Key" => k.to_s,
            "Value" => v.to_s
          }

          @txn_data << {"KV" => kv}
        end
        @txn_data
      end
      
      def determine_verb(_type, recurse)
        verb = nil
        type = _type.to_s
        case type
        when 'read'
          if recurse
            verb = 'get-tree'
          else
            verb = 'get'
          end
        when 'update'
          verb = 'set'
        when 'create'
          verb = 'set'
        when 'delete'
          if recurse
            verb = 'delete-tree'
          else
           verb = 'delete'
          end
        else
          verb = nil
        end
        verb
      end
    end

    def initialize(url, acl_token)
      @url = url
      @acl_token = acl_token
      @connection = ConsulConnection.new(@url, @acl_token)
    end

    def create(key,value)
      response = @connection.put(key, value, cas: 0)
      raise ConfigKitCreateError, "Config Kit create #{key} error" unless response
      response
    end

    def update(key, value)
      response = @connection.put(key, value)
      raise ConfigKitUpdateError, "Config Kit update #{key} error" unless response
    end

    def atom_update(key,value)
      begin
        retries ||= 0
        modify_idx = @connection.get(key, modify_index: true)
        response = @connection.put!(key, value, cas: modify_idx)
      rescue ConfigKitUpdateError => e
        if (retries += 1) < 3
          #
          # TODO: 
          # 1. Need to log
          # 2. Need to delay and retry refactor
          #
          sleep(0.5)
          retry
        end
      end
    end

    #
    # TODO: enhancement
    #
    def read(key, convert_to_hash=true, recurse=true)
      begin
        ConfigKit.logger.debug "getting key: #{key}"
        response = @connection.get(key, convert_to_hash: convert_to_hash, recurse: recurse)
        response
      rescue Diplomat::KeyNotFound => e
        return nil
      rescue Faraday::ConnectionFailed => e
        raise ConfigKitReadError, "config server #{@url} is not avaliable. #{e.message}"
      rescue Diplomat::UnknownStatus => e
        raise ConfigKitReadError, "Unknown error #{e.message}."
      rescue Exception => e
        ConfigKit.logger.debug e.backtrace.join("\n ")
        return nil
      end

    end

    #
    # TODO: enhancement
    #

    def delete(key)
      response = @connection.delete(url, recurse: true)
      response
    end

    def create_txn(data)
      @connection.create_txn(data)
    end

    def read_txn(data)
      @connection.read_txn(data)
    end

    def update_txn(data)
      @connection.update_txn(data)
    end

    def delete_txn(data)
      @connection.delete_txn(data)
    end

    def init_txn
      @connection.init_txn
    end

    def perform_txn
      @connection.perform_txn
    end
  end
end