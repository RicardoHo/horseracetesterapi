require 'config_kit/ext/hash'
module ConfigKit
  class DeployData < SlashedHash
    class DeployDataOptsError < ConfigKit::Error; end
    def initialize(name, version, opts)
      @name = name 
      @version = version[0] == 'v' ? version : "v#{version}"
      @api_version = opts.fetch(:api_version, '1.0')
      @kind = opts.fetch(:kind, 'config_kit')
      @extra = opts.fetch(:extra, :no_default)
      @cs = opts.fetch(:cs, "no_cs") if @extra != :default_only
      
      ConfigKit.logger.debug "Deploy in #{@extra} options"
      check_extra(@extra)
      @binded_data = binding_data
      super(@binded_data)
    end

    def binding_deploy
      {'deploy' => data}
    end

    def binding_kind
      { @kind => binding_deploy}
    end

    def binding_default
      { 
        @kind => {
          'deploy' => deploy_default_data
        }
      }
    end

    def binding_data
      if @extra == :no_default
        binding_kind
      elsif @extra == :set_default
        binding_default.deep_merge(binding_kind)
      elsif @extra == :default_only
        binding_default
      end
    end

    def data
      { 
        @name => { 
          @version => 
                {
                  "ts" => ts,
                  "cs" => @cs
                }
                }
      }
    end

    def deploy_default_data
      {
        @name => {
          'default' => @version
        }
      }
    end

private
    def extra_options
      [:no_default, :set_default, :default_only]
    end

    def check_extra(extra)
      raise DeployDataOptsError.new "extra options(#{extra_options.join('|')}) error: #{extra}." unless extra_options.include?(extra)
    end

    def ts
      (Time.now.to_f * 1000).to_i
    end
  end
end