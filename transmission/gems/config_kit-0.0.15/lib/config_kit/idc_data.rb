require 'config_kit/ext/slashed_hash'

module ConfigKit
  class IDCData < SlashedHash
    def initialize(name, env, codename, opts)
      @name, @env, @codename = name, env, codename
      @api_version = opts.fetch(:api_version, '1.0')
      @kind = opts.fetch(:kind, 'config_kit')
      @bind_data = binding_data
      super(@bind_data)
    end

    def data
      { 
        'name' => @name,
        'env' => @env,
        'codename' => @codename
      }
    end

    def binding_idc
      { 'idc' => data}
    end

    def binding_data
      { @kind => binding_idc }
    end
  end
end
