module ConfigKit
  class SlashedHash < ::Hash
    class SlashedHashTypeError < Exception; end
    def initialize(hash, keep_nesting=false)
      self.merge!(hash) if keep_nesting

      self.merge!(dot_flattened(hash))
      SlashedHash.symbolize(self)
    end

    def inspect
      "#<#{self.class.name}:#{object_id} #{super}>"
    end

    def to_hash
      {}.replace(self)
    end

    def self.symbolize(hash)
      hash.keys.each do |key|
        hash[key.to_sym] = hash.delete(key)
      end
    end

    protected
    # turns {'a' => {'b' => 'c'}} into {'a.b' => 'c'}
    def dot_flattened(nested_hash, names=[], result={})
      nested_hash.each do |key, val|
        next if val == nil
        if val.respond_to?(:has_key?)
          dot_flattened(val, names + [key], result)
        elsif val.is_a?(Array)
          result[(names + [key]).join('/')] = (val.map do |e|
            raise SlashedHashTypeError.new "Not Support #{e.class.name} in array" unless e.respond_to?(:to_s)
            e.to_s
          end).join(',')
        else
          result[(names + [key]).join('/')] = val
        end
      end
      result
    end

  end
end
