require 'logger'
module ConfigKit
  class Configuration
    attr_accessor :url, :acl_token, :options, :api_version, :kind, :debug, :logger
    def initialize(url, opts={})
      @url = url
      @opts = opts
      @acl_token = opts[:acl_token]
      @api_version = 'v1.0'
      @kind = 'config_kit'
      @logger = @opts.fetch(:logger, ::Logger.new(STDOUT))
      @debug = @opts[:debug] || false
      @logger.level = ::Logger::DEBUG if @debug 
    end
  end
end