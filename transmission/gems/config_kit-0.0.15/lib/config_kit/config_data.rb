require 'config_kit/ext/slashed_hash'

module ConfigKit
  class ConfigData < SlashedHash
    class MetadataError < ConfigKit::Error; end
    class MetadataMissingRequireFieldError < ConfigKit::Error; end

    class DataError < ConfigKit::Error; end
    class DataMissingRequireFieldError < ConfigKit::Error; end

    def initialize(config_data, name, api_version='v1.0', kind='config_kit')
      
      @defined_api_version=api_version
      @defined_kind = kind

      @metadata_required_fields = ['api_version', 'kind']
      @data_required_fields = ['version', 'namespace']
      @config_data = config_data
      @name = name
      check_metadata
      check_data
      @binded_data = binding_data
      super(@binded_data)
    end

    def check_data
      raise DataError, 'Config Kit Support Hash Data Formate ONLY.' unless data.is_a?(Hash)
      @data_required_fields.each do |f|
        raise DataMissingRequireFieldError,"Config Kit Raw data missing require field #{f} " unless data.has_key?(f)
      end
    end

    def check_metadata
      raise MetadataError, 'Config Kit Support Hash Data Formate ONLY.' unless @config_data.is_a?(Hash)
      @metadata_required_fields.each do |f|
        raise MetadataMissingRequireFieldError,"Config Kit Meta data missing require field #{f} " unless @config_data.has_key?(f)
      end
      raise MetadataError, "Config Kit API Version metadata mis-match #{@config_data['api_version']}." unless support_api?
      raise MetadataError, "Config Kit Kind metadate mis-match #{@config_data['kind']}." unless same_kind?
    end

    def binding_version
      { data_version => data }
    end

    def binding_name
      { @name => binding_version }
    end

    def binding_kind
      { kind => binding_name }
    end

    #
    # /v1/config_kit/axle/v1.0/database => mysql
    #

    def binding_data
      binding_kind
    end

    def same_kind?
      @defined_kind == kind  
    end

    def support_api?
      @defined_api_version == api_version
    end

    def data_version
      _version = @config_data && @config_data[@name] && @config_data[@name]['version']
      version = _version[0] == 'v' ? _version : "v#{_version}"
      version
    end

    def data_cs
      nil
    end

    def data
      @config_data && @config_data[@name]
    end

    def api_version
      @config_data['api_version']
    end

    def kind
      @config_data['kind']
    end
  end
end