require 'uri'
module ConfigKit::Cli
  class Bootstrap < Command
    def self.uri_parser(uri)
      URI.parse(uri)
    end

    def self.command; "bootstrap"; end

    def initialize(args)
      @uri_string = 'file:///./config'
      @app = 'all'
      super(args)
      @uri_kls = self.class.uri_parser(@uri_string)
      check_args
    end

    def check_args
      raise ConfigKit::Cli::Command::CommandFailure.new "Wrong bootstrap source from #{@from}, pls use file or git" unless ['git', 'file'].include?(@from)
      raise ConfigKit::Cli::Command::CommandFailure.new "Missmatch source from #{@from} and uri #{@uri_string}" if @from == 'file' && @uri_kls.scheme != 'file'
      raise ConfigKit::Cli::Command::CommandFailure.new "Missmatch source from #{@from} and uri #{@uri_string}" if @from == 'git' && !['git', 'http', 'ssh'].include?(@uri_kls.scheme)
      raise ConfigKit::Cli::Command::CommandFailure.new "Missmatch tag from #{@from} and uri #{@uri_string}" if @from == 'git' && @tag.nil?
    end

    def run
      begin
        @output = ConfigKit::Manager.bootstrap(@app, @from, @uri_kls, @tag, {})
        pp @output.to_h.to_json
      rescue => e
        ConfigKit.logger.error "Unexpected error attempting to get config data #{@uri_string} in for #{@app.nil? ? 'all' : @app}"
        ConfigKit.logger.debug "#{e}: #{e.backtrace.join("\n   ")}"
        raise ConfigKit::Cli::Command::CommandFailure.new(e.to_s)
      end
    end

    private

    
    def options
      OptionParser.new %Q{Usage: #{$0} #{self.class.command} [OPTIONS] ["description"] }, 40 do |opts|
        opts.separator ''
        opts.separator 'Specific options:'

        opts.on('-g URI', '--from-git', 'Specify read config from git') do |uri_string|
          @from = 'git'
          @uri_string = uri_string
        end

        opts.on('-f URI', '--from-file', 'Specify read config from file') do |uri_string|
          @from = 'file'
          @uri_string = uri_string
        end

        opts.on('-a APP', '--app', 'Specify an app of config to create') do |app|
          @app = app
        end

        opts.on('-t TAG', '--tag', 'Specify version tag to boostrap(if using git repository for version control)') do |tag|
          @tag = tag
        end
      end
    end
  end

end