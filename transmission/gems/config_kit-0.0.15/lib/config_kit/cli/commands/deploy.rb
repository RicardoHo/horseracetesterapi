module ConfigKit::Cli
  class Deploy < Command
    def self.command; "deploy"; end
    
    def initialize(args)
      @app = nil
      @version = nil
      super(args)
      check_args
    end

    def check_args
      raise ConfigKit::Cli::Command::CommandFailure.new "Missing app parameters" if @app.nil?
      raise ConfigKit::Cli::Command::CommandFailure.new "Missing version parameters" if @version.nil?
    end

    def run
      begin
        ConfigKit.logger.debug "Run command #{self.class.command} with options #{@app}:#{@version}"
        @output = ConfigKit::Manager.deploy(@app, @version)
        pp @output.to_h.to_json
      rescue ConfigKit::Cli::Command::CommandFailure
        raise
      rescue => e
        ConfigKit.logger.error "Unexpected error attempting to get config data #{@uri} in env #{@env} for #{@app.nil? ? 'all' : @app}"
        ConfigKit.logger.debug "#{e}: #{e.backtrace.join("\n   ")}"
        raise ConfigKit::Cli::Command::CommandFailure.new(e.to_s)
      end
    end

    private
    def options
      OptionParser.new %Q{Usage: #{$0} #{self.class.command} [OPTIONS] ["description"] }, 40 do |opts|
        opts.separator ''
        opts.separator 'Specific options:'
        
        opts.on('-a APP', '--app', 'Specify an app of config to create(default: nil to deploy all apps)') do |app|
          @app = app
        end

        opts.on('-v VERSION', '--version', 'Specify a version of application to describe') do |version|
          @version = version[0] == 'v' ? version : "v#{version}"
        end
      end
    end
  end

end