module ConfigKit::Cli
  class Describe < Command
    def self.command; "describe"; end

    def initialize(args)
      @app = 'idc'
      @output = nil
      args << ['-a', @app] unless args.include?('-a')
      super(args)
    end

    def run
      begin
        @output = ConfigKit::Manager.describe(@app, @version)
        pp @output
      rescue ConfigKit::Cli::Command::CommandFailure
        raise
      rescue => e
        ConfigKit.logger.error "Unexpected error attempting to get config data #{@uri} in env #{@env} for #{@app.nil? ? 'all' : @app}"
        ConfigKit.logger.debug "#{e}: #{e.backtrace.join("\n   ")}"
        raise ConfigKit::Cli::Command::CommandFailure.new(e.to_s)
      end
    end

    private
    def options
      OptionParser.new %Q{Usage: #{$0} #{self.class.command} [OPTIONS] ["description"] }, 40 do |opts|
        opts.separator ''
        opts.separator 'Specific options:'

        opts.on('-a APP', '--app', 'Specify an application to describe') do |app|
          @app = app
        end

        opts.on('-v VERSION', '--version', 'Specify an verion of application to describe') do |version|
          @version = version
        end
      end
    end
  end
end