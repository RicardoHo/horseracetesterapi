module ConfigKit::Cli
  class Rollback < Command
    def self.command; "rollback"; end

    def initialize(args)
      @app = nil
      super(args)
    end

    def run
      begin
        @output = ConfigKit::Manager.rollback(@app, @version)
        pp @output.to_h.to_json
      rescue ConfigKit::Cli::Command::CommandFailure
        raise
      rescue => e
        ConfigKit.logger.error "Unexpected error attempting to get config data #{@uri} in env #{@env} for #{@app.nil? ? 'all' : @app}"
        ConfigKit.logger.debug "#{e}: #{e.backtrace.join("\n   ")}"
        raise ConfigKit::Cli::Command::CommandFailure.new(e.to_s)
      end
    end

    private
    def options
      OptionParser.new %Q{Usage: #{$0} #{self.class.command} [OPTIONS] ["description"] }, 40 do |opts|
        opts.separator ''
        opts.separator 'Specific options:'

        opts.on('-v VERSION', '--version', 'Specify a existing version to rollout') do |version|
          @version = version[0] == 'v' ? version : "v#{version}"
        end

        opts.on('-a APP', '--app', 'Specify an app of config to create(default: all to deploy all apps)') do |app|
          @app = app
        end
      end
    end
  end

end