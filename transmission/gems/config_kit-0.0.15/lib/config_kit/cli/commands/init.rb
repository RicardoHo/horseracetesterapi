module ConfigKit::Cli
  class Init < Command
    def self.command; "init"; end

    def initialize(args)
      @output
      super(args)
    end

    def run
      begin
        raise ConfigKit::Cli::Command::CommandFailure.new 'Missing name options' if @name.nil?
        raise ConfigKit::Cli::Command::CommandFailure.new 'Missing environment options' if @env.nil?
        @output = ConfigKit::Manager.init(@name, @env, @codename)
        pp @output.to_h.to_json
      rescue ConfigKit::Cli::Command::CommandFailure
        raise
      rescue => e
        ConfigKit.logger.error "Unexpected error attempting to get config data #{@uri} in env #{@env} for #{@app.nil? ? 'all' : @app}"
        ConfigKit.logger.debug "#{e}: #{e.backtrace.join("\n   ")}"
        raise ConfigKit::Cli::Command::CommandFailure.new(e.to_s)
      end
    end

    private
    def options
      OptionParser.new %Q{Usage: #{$0} #{self.class.command} [OPTIONS] ["description"] }, 40 do |opts|
        opts.separator ''
        opts.separator 'Specific options:'

        opts.on('-n NAME', '--name', 'Specify a name of IDC to config') do |name|
          @name = name
        end

        opts.on('-e ENV', '--env', 'Specify a environment of IDC to config') do |env|
          @env = env
        end

        opts.on('-c CODENAME', '--codename', 'Specify a codename of IDC to config') do |codename|
          @codename = codename
        end

      end
    end
  end

end