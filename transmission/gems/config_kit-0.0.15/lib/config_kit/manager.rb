require 'optparse'
require 'config_kit/tool'
module ConfigKit
  class Manager
    class IDCEnvMissing < ConfigKit::Error; end
    def self.bootstrap(app, from, uri_kls, version, opts={})
      opts['uri_kls'] = uri_kls
      opts['version'] = version
      opts['from'] = from
      new(app, opts).bootstrap
    end

    def self.describe(app, version, opts={})
      opts['version'] = version
      new(app, opts).describe
    end

    def self.rollback(app, version, opts={})
      opts['version'] = version
      new(app, opts).rollback
    end

    def self.deploy(app, version, opts={})
      opts['version'] = version
      new(app, opts).deploy
    end

    def self.init(name, env='int0', codename='int0', opts={})
      app = 'idc'
      opts['name'] = name
      opts['env'] = env
      opts['codename'] = codename
      opts['skip_codename_check'] = true
      new(app,opts).init
    end

    def self.get(app,opts={})
      new(app, opts).get
    end

    def initialize(app, opts)
      @app = app
      @opts = opts
      @tool = ConfigKit::Tool.new
      unless opts['skip_codename_check'] == true
        codename = @tool.get_idc_codename
        env = @tool.get_idc_env
        raise ConfigKit::Manager::IDCEnvMissing.new 'IDC environment/codename missing, pls init it first!' if codename.nil? && env.nil?
        @opts['codename'] = codename
        @opts['env'] = env
      end
    end

    def init
      name = @opts.delete('name')
      env = @opts.delete('env')
      codename = @opts.delete('codename')
      @tool.init_txn
      @tool.idc_init_txn(name, env, codename, @opts)
      @tool.perform_txn
    end

    def describe
      version = @opts['version']
      @tool.describe(@app, version)
    end

    def bootstrap
      create
      {app: @app, version: @opts['version']}
    end

    def create(extra=:no_default)
      ConfigKit::Data::Loader.load(@app, @opts['from'], @opts['uri_kls'], @opts['env'], @opts['codename'], @opts['version']) do |data|
        @tool.init_txn
        data.each_pair do |k,v|
          @tool.bootstrap_txn(v, k)
          version, cs = get_deploy_info(@tool.config_data)
          @tool.deploy_txn(k, version, extra,cs)
        end
        @tool.perform_txn
      end
    end

    def get_deploy_info(config_data)
      return [] unless config_data.kind_of?(ConfigKit::ConfigData)
      [config_data.data_version, config_data.data_cs]
    end

    def get
      data = @tool.get(@app)[@app]
      data || {}
    end

    def deploy
      change_default
    end

    def rollback
      change_default
    end

    def change_default
      @tool.deploy_txn(@app, @opts['version'], :default_only)
    end
  end
end