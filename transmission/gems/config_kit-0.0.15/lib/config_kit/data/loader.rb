require 'uri'
module ConfigKit
  module Data
    class Loader

      class LoaderFailure < StandardError
        attr_reader :options
        def initialize message, opts=nil
          super message
          @options = opts
        end
      end

      def self.info(message)
        STDOUT.puts message
      end

      def self.err(message)
        STDERR.puts message
      end


      @loaders = []

      def self.inherited(subclass)
        @loaders << subclass
      end

      ldrs = ldrs = File.expand_path(File.join(File.dirname(__FILE__), 'loaders', '*.rb'))
      Dir[ldrs].each {|loader| require loader}

      def self.load(app, from, uri_kls, env, codename, version, &block)
        @loader_names = @loaders.map{ |l| l.loader}

        @uri_kls, @env, @app, @version, @codename = uri_kls, env, app, version, codename

        loader = from


        if loader.nil?
          ConfigKit.logger.error "from empty source with #{@uri_kls.to_s}"
        elsif !@loader_names.include?(loader)
          ConfigKit.logger.error "Unrecognize loader: #{loader} for #{@uri}(#{@codename})"
        else
          loader_class = @loaders.find{ |c| c.loader == loader}
          ConfigKit.logger.debug "#{loader_class.loader} is loading env(#{@env}) app(#{app}) data(#{@version}) codename(#{codename}) from #{uri_kls.to_s}"
          loader_class.new(@uri_kls, @env, @codename, @app, @version).run(&block)
          ConfigKit.logger.debug "#{loader_class.loader} is loaded env(#{@env}) app(#{app}) data(#{@version}) codename(#{codename}) from #{uri_kls.to_s}"
        end
      rescue => e
        raise ConfigKit::Data::Loader::LoaderFailure.new e.message
      end
      
      attr_reader :cursor,:batch_size, :files
      def initialize(batch_size=10)
        @files = []
        @batch_size = batch_size
        @current_files = []
        @cursor = 0
        @files = retrieve_files
      end

      def file_count
        @files.count
      end

      def next_batch
        if finish?
          @current_files = []
          @cursor = file_count
          return @cursor
        end
        _next_cursor = next_cursor
        @current_files = @files[@cursor.._next_cursor - 1]
        @cursor = _next_cursor
      end

      def next_cursor
        @cursor + @batch_size > file_count ? file_count : (@cursor + @batch_size)
      end

      def finish?
        @cursor >= file_count
      end

      def retrieve_files
        raise NotImplementedError
      end
    end
  end
end