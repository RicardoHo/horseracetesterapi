require 'yaml'
module ConfigKit::Data
  class FileLoader < Loader
    def self.loader; "file"; end

    attr_reader :files
    def initialize(uri_kls, env, codename, app, branch)
      @uri_kls, @env, @codename, @app, @branch = uri_kls, env, codename, app, branch
      @path = File.expand_path('.',File.join(retrieve_path(@uri_kls.path),@codename))
      super()
    end

    def run(&block)
      return run_all unless block
      run_batch(&block)
    end

    def run_all
      files_data = {}
      @files.each do |f|
        files_data.merge!(load_one(f))
      end
      files_data
    end

    def run_batch(&block)
      while !finish?
        next_batch
        files_data = {}
        @current_files.each do |f|
          files_data.merge!(load_one(f))
        end
        block.call(files_data)
      end
    end

    def retrieve_files
      files = if @app == 'all' 
        Dir["#{@path}/**/*.yml"].select { |f| match_for?(f, @codename) }
      else
        Dir["#{@path}/**/*.yml"].select {|f| match_for?(f, @app) && match_for?(f, @codename)}
      end
      raise ConfigKit::Data::Loader::LoaderFailure.new('No data file found.') if files.empty?
      files
    end
private

    def retrieve_path(path)
      split_path = path[1..-1].split('/')
      return path[1..-1] if split_path[0] == '.'
      path
    end

    def codename_app_for(f)
      File.basename(f).split('.')[0..1]
    end

    def load_one(f)
      codename, app = codename_app_for(f)
      raise ConfigKit::Data::Loader::LoaderFailure.new("Wrong data file codename(#{codename}) for loaded #{@codename}") unless codename == @codename
      {app => YAML.load_file(f)}
    end

    def match_for?(f,info)
      File.basename(f).split('.').find {|a| a == info}
    end
  end
end