require 'git'
module ConfigKit::Data
  class GitLoader < Loader
    class GitLoaderError < ConfigKit::Error; end
    def self.loader; "git"; end
    attr_reader :clone_path
    def initialize(uri_kls, env, codename, app, version)
      @uri_kls, @env, @codename, @app, @version = uri_kls, env, codename, app, version
      @clone_path = Dir.mktmpdir
      @file_path = "file://#{@clone_path}"
      @file_kls = URI.parse(@file_path)
      clone
    end

    def run(&block)
      ConfigKit.logger.debug "Git is loading env(#{@env}) #{@app} from #{@uri_kls.to_s}"
      begin
        FileLoader.new(@file_kls, @env, @codename, @app, @version).run(&block)
      ensure
        FileUtils.rm_rf @clone_path
      end
    end

private
    def clone
      ConfigKit.logger.debug "Git is cloning env(#{@env}) #{@app} codename(#{@codename}) from #{@uri_kls.to_s}"
      begin
        g = Git.clone(@uri_kls.to_s, @clone_path)
        tags =  g.tags.map { |t| t.name }
        raise GitLoaderError.new "Version(#{@version}) not found" unless tags.include?(@version)
        g.checkout(@version)
      rescue GitLoaderError => e
        raise GitLoaderError.new e.message
      rescue Exception => e
        raise GitLoaderError.new "Unknown error to load (#{@version}) in #{@uri_kls.to_s}"
      end
    end
  end
end