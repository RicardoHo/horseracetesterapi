require 'config_kit/config_data'
require 'config_kit/deploy_data'
require 'config_kit/idc_data'
require 'config_kit/ext/hash'
module ConfigKit
  class Tool
    class ConfigMetadataError < ConfigKit::Error; end
    class MissingDefaultVersionError < ConfigKit::Error;end
    attr_reader :kind, :namespace, :api_version, :config_data, :deploy_data, :idc_data
    def initialize(opts={}, api_version=ConfigKit.config.api_version, kind=ConfigKit.config.kind)
      @kind = kind
      @api_version = api_version
      @url = opts.fetch(:url, ConfigKit.config.url)
      @acl_token = opts.fetch(:acl_token, ConfigKit.config.acl_token)
      @client = ConfigKit::Client.new(@url, @acl_token)
    end

    def bootstrap_config(data, name)
      ConfigKit::ConfigData.new(data, name, api_version, kind)
    end

    def bootstrap_txn(data,name)
      @config_data = bootstrap_config(data, name)
      @client.create_txn(@config_data)  
    end

    def deploy_config(name, version,extra,cs)
      ConfigKit::DeployData.new(name, version, extra: extra, cs: cs)
    end

    def deploy_txn(app,version,extra,cs=nil)
      if extra != :no_default && check_version_for?(app,version)
        ConfigKit.logger.debug "Missing version(#{version}) for app(#{app}) to set default"
        raise MissingDefaultVersionError.new "Missing default version for app(#{app}), pls set default version first.\n"
      end
      ConfigKit.logger.debug "compose deploy data for app(#{app}:#{version})"
      @deploy_data = deploy_config(app,version,extra, cs)
      @client.create_txn(@deploy_data)
      @client.perform_txn
      @deploy_data
    end

    def check_version_for?(app, version)
      path = path_for(app, version)
      @client.read(path).nil?
    end

    def idc_config(name, env, codename, opts)
      ConfigKit::IDCData.new(name, env, codename, opts)
    end

    def idc_init_txn(name, env, codename, opts={})
      @idc_data = idc_config(name, env, codename, opts)
      @client.create_txn(@idc_data)
    end

    def get_idc
      data =  @client.read(idc_path)
      return {'idc' => 'N/A'} if data.nil?
      data['config_kit']['idc']
    end

    def get_idc_env
      data =  @client.read(idc_path)
      return nil if data.nil?
      data['config_kit']['idc']['env'] 
    end

    def get_idc_codename
      data =  @client.read(idc_path)
      return nil if data.nil?
      data['config_kit']['idc']['codename'] 
    end

    def describe(app, version)
      @content = {}
      data = @client.read(idc_path)
      return @content if data.nil?
      @content['idc'] = data['config_kit']['idc']
      data = @client.read(deploy_path)
      return @content if data.nil?
      @content['deploy'] = data['config_kit']['deploy']
      unless app == 'idc'
        if version.nil?
          data = @client.read(path_for(app))
          return @content if data.nil?
          @content[app] = data['config_kit'][app]
        else
          data = @client.read(path_for(app))
          return @content if data.nil?
          @content[app] = data['config_kit'][app][version]
        end
      end
      @content
    end

    def delete_txn(data, name, version=nil)
      config = bootstrap_config(data)
    end

    def get(app)
      return get_all if app == 'all'
      get_one(app)
    end

    def get_all()
      config_data = {}
      all_apps.each do |app|
        config_data = config_data.deep_merge(get_one(app))
      end
      config_data
    end

    def get_one(app)
      context = {}
      app_data = get_app(app)
      context[app] = app_data
      context[app]['idc'] = get_idc
      unless app == 'infra'
        infra_data = get_app('infra')['service'] 
        context[app]['service'] = infra_data
      end
      context
    end

    def get_app(app)
      version = version_for(app)
      if version.nil?
        ConfigKit.logger.debug "Missing default version for app(#{app})"
        raise MissingDefaultVersionError.new "Missing default version for app(#{app}), pls set default version first.\n"
      end
      app_path = path_for(app, version)
      app_version = @client.read(app_path)[kind]
      app_data = {}
      app_data[app] = app_version[app][version]
    end

    def all_apps
      apps = @client.read(deploy_path)[kind]['deploy'].keys
      apps
    end

    def version_for(app)
      @client.read(deploy_app_path(app),false)
    end

    def init_txn
      @client.init_txn
    end

    def perform_txn
      @client.perform_txn
    end

    def deploy_app_path(app)
      "#{deploy_path}/#{app}/default"
    end

    def deploy_path
      "#{kind}/deploy"
    end

    def path_for(app, version=nil)
      return "/#{kind}/#{app}" if version.nil?
      "#{kind}/#{app}/#{version}" 
    end

    def idc_path
      "#{kind}/idc"
    end
  end
end