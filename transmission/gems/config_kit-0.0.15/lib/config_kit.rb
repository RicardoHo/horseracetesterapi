require 'config_kit/error'
require 'config_kit/configuration'
require 'config_kit/client'
require 'config_kit/config_data'
require 'config_kit/deploy_data'
require 'config_kit/manager'

module ConfigKit
  def self.config
    @config ||= Configuration.new(ENV.fetch('CK_URL','http://localhost:8500'), debug: ENV.fetch('DEBUG', false))
  end

  def self.logger
    self.config.logger
  end

  def self.logger=(log)
    self.config.logger = log
  end
end