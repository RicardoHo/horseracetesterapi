# Configuration Kit(config_kit)

Configuration Kit is tool kit to handle application configuration which need to specified with deployment environment, so that all the configuration for the deployment environment can be isolated from application code phrase(which is including three phrase only, they are test, development and production). Finally, we can archive build once and run everywhere.

## Design Detail

We are use consul as configration data store and reply on its high avaibility and reliabilty to maintain and provide robust configuration management service for all running docker container.

### Configruation Data Source Structure

Config kit will support two type of data source, which are local file directory and git source repository structure

#### Local File Directory

Assum local file data source is located in current_path/config, under this config folder we will separate all config data per deployment environment and application, which will share one same infra configuration data within this deployment environment.

Here is an example

* file:///./config/int0/int0.infra.yml
* file:///./config/int0/int0.axle.yml
* file:///./config/int0/int0.belt.yml

Above is for integration0 environment. Similarily, for staging0 environment, we can have below configuration data file structure:

* file:///./config/stg0/stg0.infra.yml
* file:///./config/stg0/stg0.axle.yml
* file:///./config/stg0/stg0.belt.yml

#### Git source repository

Similary with local file, we will define the following structure in git repository

* git://git_repos/config_kit.git/int0/int0.infra.yml
* git://git_repos/config_kit.git/int0/int0.axle.yml
* git://git_repos/config_kit.git/int0/int0.belt.yml
* git://git_repos/config_kit.git/stg0/stg0.infra.yml
* git://git_repos/config_kit.git/stg0/stg0.axle.yml
* git://git_repos/config_kit.git/stg0/stg0.belt.yml

### K/V Structure

Since consul is KV storage and we use this property to store all the configuration.

Application Configuration

* /v1/config_kit/axle/v1.0/database => 'mysql'

Deployment Configuration:

* /v1/config_kit/deploy/axle/default => v1.0
* /v1/config_kit/deploy/axle/v1.0/ts => 201705061235 # Timestamp
* /v1/config_kit/deploy/axle/v1.0/cm => b193bc91     # Checksum or git commit hash

## Configuration List

### Description

 * key: reference key in Axle.config, which is symble. If key is dotted, you need to single quote to reference it as symble such as Axle.config[:
 'service.schedule.base_path'].
 * default: it is default value of this configuration.
 * type: Ruby data type of this configuration.
 * allowed_from_server: It can be overwrited by config-kit from server side.
 * description: describe the information of configuration.
 * deprecated: indicate configuration is deprecated or not.

### Axle

#### Configuration Catagory

* default: No prefix and including axle service host, port etc..
* service: containe the configuration of service host, port and endpoint path etc...
* database: containe the configuration of database host name, port, username and password etc...
* jackpot: containe the configuration about Jackpot such alert configuration...
* game: containe the configuration for game;
* alert: container the configuration for alert;

Pls follow the catagory mentioned above with proper prefix to named the configuration list below.

#### Config Data
| key     | default | type | allowed_from_server | description         | deprecated |
|---------|---------|------|---------------------|---------------------|-------------|
| port    | 3000    | Integer | false | Define service port. | false |
| host    | 'localhost' | String | false | Define Service host. | false |
| log_file_name | 'axle.log' | String | false | log file name | false |
| log_level | 'info' | String | false | Sets the level of logger.  | false |
| log_file_path | 'stdout' | String | false | Define a path to the log file, excluding the filename. | false |
| config_path | DefaultSource.config_path | String | false | Path to <b>axle.yml</b>. If undefined, the agent checks the following directories (in order): <b>config/axle.yml</b>, <b>axle.yml</b>, <b>$HOME/.axle/axle.yml</b> and <b>$HOME/axle.yml</b>. | false |
| config_search_path | DefaultSource.config_search_paths | String | false | An array of candidate locations for the service\'s configuration file. | false |
| service.action.base_path | '/action' | String | false | Defines base path for action service endpoint. | false |
| service.schedule.base_path | '/schedule' | String | false | Defines base path for schedule service endpoint. | false |
| service.loopback.base_path | '/loopback' | String | false | Defines base path for loopback service endpoint. | false |
| service.operation.base_path | '/operation' | String | false | Defines base path for operation service endpoint. | false |
| database.adapter  | 'mysql2'    | String | true | database adapter for sequel | false |
| database.host     | 'localhost' | String | true | database server hostname | false |
| database.port     | 3306        | Integer | true | database server port | false |
| database.username | 'laxino'    | String | true | database server username | false |
| database.passward | 'passord'   | String | true | database password | false |
| database.database | 'g2_axle_production' | String | true | database name | false |
| jackpot.submit_status.#{property_id}.path | 'http://games.83suncity.local' | String | true | Jackpot Status Submission Path | false |
| jackpot.submit_status.#{property_id}.secret_access_key | 'akjlasdfasdfalas;sdkfajs' | String | true | Jackpot Status Submission Secret | false |
| jackpot.submit_status.#{property_id}.stats_postfix | 'jackpotstats' | String | true | Jackpot Status Submission Stats Postfix | false |
| jackpot.submit_status.#{property_id}.post_with_content_md5 | true | Boolean | true | Jackpot Status ON/OFF Context md5 Checksum | false |
| jackpot.allow_debug_mode | false | Boolean | false | 'Jackpot debug mode ON/OFF' | false |
| service.fams.host | 'fams' | String | true | 'FAMS host name' | false |
| service.fams.port | 80 | Integer | true | 'FAMS host name' | false |
| service.fams.bet_path | '/fund_adapter/bet' | String | false | 'FAMS result path.' | false |
| service.fams.cancel_bet_path | '/fund_adapter/cancel_bet' | String | false | 'FAMS cancel bet path.' | false |
| service.fams.query_balance_path | '/fund_adapter/query_player_balance' | String | false | 'FAMS query player balance path.' | false |
| service.fams.get_balance_path | '/get_player_balance_info' | String | false | 'FAMS get player balance info path.' | false |
| service.rap.host | 'rap' | String | true | 'RAP host name' | false |
| service.rap.port | 80 | Integer | true | 'RAP service port' | false |
| service.rap.get_token_path | '/get_third_party_token' | String | false | 'RAP service port' | false |
| service.sm.host | 'sm' | String | true | 'Station Management Service host name' | false |
| service.sm.port | 80 | Integer | true | 'Station Management Service port' | false |
| service.sm.message_path | '/messages/create.json' | String | false | 'Station Management Service message path' | false |
| service.sm.validate_machine_token_path | '/validate_machine_token.json' | String | false | 'Station Management Service validate machine token path' | false |
| service.anms.host | 'snms' | String | true | 'ANMS host name' | false |
| service.anms.port | 80 | Integer | true | 'ANMS port' | false |
| service.anms.request_send_alert_path | '/messages/request_send_alert.json' | String | false | 'ANMS message path' | false |
| service.js.host | 'job_scheduler.com' | String | true | 'Job Scheduler host name' | false |
| service.js.port | 80 | Integer | true | 'Job Scheduler Service Port' | false |
| service.js.message_path | '/messages/create.json' | String | false | 'Job Scheduler Service message path' | false |
| service.self_schedule_host | 'axle.com' | String | true | 'axle schedule url' | false |
| player.token_secret | 'secret' | String | false | 'Player token (JWT) secret key.' | false |
| alert.mail_server | 'moexc01.mo.laxino.com' | String | true | 'alert mail server.' | false |
| game.allow_test_mode | false | Boolean | false | 'Allow test mode player ON/OFF' | false |
| alert.big_win.property_ids | [20000] | Array | false | 'Properties that need to send big win alert mail' | false |
| alert.big_win.mail_context.serverity | 2 | Integer | false | 'Big win alert mail serverity' | false |
| alert.jackpot.win.mail_recipient | 'lm.cs@laxino.com' | String | true | Jackpot Win Alert Mail Recipient | false |
| alert.jackpot.win.mail_context.serverity | 2 | Integer | true | serverity of Jackpot Win Alert | false |
| alert.jackpot.win.anms_property_ids | [20000] | Array | false | 'Properties that need to send jackpot win alert mail by ANMS' | false |
| alert.jackpot.submit_status.exception.mail_recipient | 'lm.cs@laxino.com' | String | true | Jackpot Status Submision Exception Mail Recipient | false |
| alert.jackpot.submit_status.exception.mail_context.serverity | 2 | Integer | true | Serverity of Jackpot Status Submision Exception Mail | false |
| alert.jackpot.snapshot_report.mail_recipient | 'lm.cs@laxino.com' | String | true | Jackpot Snapshot Report Recipient | false |