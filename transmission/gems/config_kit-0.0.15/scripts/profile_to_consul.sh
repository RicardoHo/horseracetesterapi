#!/bin/bash

CONSUL_SERVER='http://pwd10_0_38_4-8500.host1.labs.play-with-docker.com/'

KEY="config/axle/data"
VALUE="config/default.yml"
curl -X PUT --data-binary @${VALUE} \
  -H "Content-type: text/x-yaml" \
  ${CONSUL_SERVER}v1/kv/${KEY}