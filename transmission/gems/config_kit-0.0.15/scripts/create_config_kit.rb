require 'yaml'
require 'diplomat'

class SlashedHash < ::Hash
  def initialize(hash, keep_nesting=false)
    self.merge!(hash) if keep_nesting

    self.merge!(dot_flattened(hash))
    SlashedHash.symbolize(self)
  end

  def inspect
    "#<#{self.class.name}:#{object_id} #{super}>"
  end

  def to_hash
    {}.replace(self)
  end

  def self.symbolize(hash)
    hash.keys.each do |key|
      hash[key.to_sym] = hash.delete(key)
    end
  end

  protected
  # turns {'a' => {'b' => 'c'}} into {'a.b' => 'c'}
  def dot_flattened(nested_hash, names=[], result={})
    nested_hash.each do |key, val|
      next if val == nil
      if val.respond_to?(:has_key?)
        dot_flattened(val, names + [key], result)
      else
        result[(names + [key]).join('/')] = val
      end
    end
    result
  end

end

files = Dir["/opt/workspace/project/open_platform/config_map/config/**/*.yml"]

Diplomat.configure do |config|
  config.url = 'http://localhost:8500'
  #config.url = 'http://pwd10_0_29_3-8500.host2.labs.play-with-docker.com/'
end
files.each do |f|
  k_v =  SlashedHash.new(YAML.load_file(f))
  
  # k_v.each_pair do |k, v|
  #   puts "key: #{k}"
  #   puts "values: #{v}"
  #   Diplomat::Kv.put(k,v.to_s)
  # end
end