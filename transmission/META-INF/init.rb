WARBLER_CONFIG = {}
ENV['GEM_HOME'] = File.expand_path(File.join('..', '..', '/'), __FILE__)
ENV['GEM_PATH'] = nil # RGs sets Gem.paths.path = Gem.default_path + [ GEM_HOME ]
ENV['BUNDLE_GEMFILE'] = File.expand_path(File.join('..', '..', 'transmission/Gemfile'), __FILE__)
require 'rubygems' unless defined?(Gem)
ENV['BUNDLE_WITHOUT'] = 'development:test:assets'

module Bundler
  module Patch
    def clean_load_path
      # nothing to be done for embedded JRuby
    end
  end
  module SharedHelpers
    def included(bundler)
      bundler.send :include, Patch
    end
  end
end

#require 'bundler/shared_helpers'
$LOAD_PATH.unshift File.expand_path(File.join('..', '..', 'transmission/lib'), __FILE__)
