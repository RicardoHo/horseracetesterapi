# -*- encoding: utf-8 -*-
# stub: d13n 0.4.2 ruby lib

Gem::Specification.new do |s|
  s.name = "d13n".freeze
  s.version = "0.4.2"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.metadata = { "allowed_push_host" => "Set to 'http://mygemserver.com'" } if s.respond_to? :metadata=
  s.require_paths = ["lib".freeze]
  s.authors = ["Ben Wu".freeze]
  s.date = "2018-09-13"
  s.description = "Write a longer description or delete this line.".freeze
  s.email = ["ben.wu@laxino.com".freeze]
  s.executables = ["d13n".freeze]
  s.files = ["bin/d13n".freeze]
  s.homepage = "http://github.com/cheokman".freeze
  s.rubygems_version = "2.7.10".freeze
  s.summary = "Write a short summary, because Rubygems requires one.".freeze

  s.installed_by_version = "2.7.10" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<rspec>.freeze, ["~> 3.0"])
      s.add_runtime_dependency(%q<bundler>.freeze, ["~> 1.14"])
      s.add_runtime_dependency(%q<statsd-instrument>.freeze, [">= 2.2.0", "~> 2.2"])
      s.add_runtime_dependency(%q<config_kit>.freeze, ["= 0.0.15"])
    else
      s.add_dependency(%q<rspec>.freeze, ["~> 3.0"])
      s.add_dependency(%q<bundler>.freeze, ["~> 1.14"])
      s.add_dependency(%q<statsd-instrument>.freeze, [">= 2.2.0", "~> 2.2"])
      s.add_dependency(%q<config_kit>.freeze, ["= 0.0.15"])
    end
  else
    s.add_dependency(%q<rspec>.freeze, ["~> 3.0"])
    s.add_dependency(%q<bundler>.freeze, ["~> 1.14"])
    s.add_dependency(%q<statsd-instrument>.freeze, [">= 2.2.0", "~> 2.2"])
    s.add_dependency(%q<config_kit>.freeze, ["= 0.0.15"])
  end
end
