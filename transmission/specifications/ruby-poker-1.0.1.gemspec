# -*- encoding: utf-8 -*-
# stub: ruby-poker 1.0.1 ruby lib lib

Gem::Specification.new do |s|
  s.name = "ruby-poker".freeze
  s.version = "1.0.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze, "lib".freeze]
  s.authors = ["Rob Olson".freeze]
  s.date = "2014-09-02"
  s.description = "Ruby library for comparing poker hands and determining the winner.".freeze
  s.email = "rob@thinkingdigitally.com".freeze
  s.extra_rdoc_files = ["README.rdoc".freeze, "CHANGELOG".freeze, "LICENSE".freeze]
  s.files = ["CHANGELOG".freeze, "LICENSE".freeze, "README.rdoc".freeze]
  s.homepage = "https://github.com/robolson/ruby-poker".freeze
  s.licenses = ["BSD".freeze]
  s.rdoc_options = ["--title".freeze, "Ruby Poker Documentation".freeze, "--main".freeze, "README.rdoc".freeze, "--inline-source".freeze, "-q".freeze]
  s.rubygems_version = "2.7.10".freeze
  s.summary = "Poker library in Ruby".freeze

  s.installed_by_version = "2.7.10" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<shoulda-context>.freeze, ["~> 1.1"])
    else
      s.add_dependency(%q<shoulda-context>.freeze, ["~> 1.1"])
    end
  else
    s.add_dependency(%q<shoulda-context>.freeze, ["~> 1.1"])
  end
end
