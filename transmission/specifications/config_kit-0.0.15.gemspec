# -*- encoding: utf-8 -*-
# stub: config_kit 0.0.15 ruby lib

Gem::Specification.new do |s|
  s.name = "config_kit".freeze
  s.version = "0.0.15"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.metadata = { "allowed_push_host" => "https ://rubygems.org" } if s.respond_to? :metadata=
  s.require_paths = ["lib".freeze]
  s.authors = ["Ben Wu".freeze]
  s.date = "2018-07-09"
  s.description = "Write a longer description or delete this line.".freeze
  s.email = ["ben.wu@laxino.com".freeze]
  s.executables = ["ck".freeze]
  s.files = ["bin/ck".freeze]
  s.homepage = "http://github.com/cheokman".freeze
  s.licenses = ["MIT".freeze, "Ruby".freeze]
  s.rubygems_version = "2.7.10".freeze
  s.summary = "Write a short summary, because Rubygems requires one.".freeze

  s.installed_by_version = "2.7.10" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<rspec>.freeze, ["~> 3.0"])
      s.add_runtime_dependency(%q<bundler>.freeze, ["~> 1.12"])
      s.add_runtime_dependency(%q<diplomat>.freeze, ["= 2.0.2"])
      s.add_runtime_dependency(%q<git>.freeze, ["= 1.3.0"])
    else
      s.add_dependency(%q<rspec>.freeze, ["~> 3.0"])
      s.add_dependency(%q<bundler>.freeze, ["~> 1.12"])
      s.add_dependency(%q<diplomat>.freeze, ["= 2.0.2"])
      s.add_dependency(%q<git>.freeze, ["= 1.3.0"])
    end
  else
    s.add_dependency(%q<rspec>.freeze, ["~> 3.0"])
    s.add_dependency(%q<bundler>.freeze, ["~> 1.12"])
    s.add_dependency(%q<diplomat>.freeze, ["= 2.0.2"])
    s.add_dependency(%q<git>.freeze, ["= 1.3.0"])
  end
end
