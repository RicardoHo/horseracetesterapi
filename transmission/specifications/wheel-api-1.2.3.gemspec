# -*- encoding: utf-8 -*-
# stub: wheel-api 1.2.3 ruby lib

Gem::Specification.new do |s|
  s.name = "wheel-api".freeze
  s.version = "1.2.3"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Cheok Meng Chan".freeze]
  s.date = "2021-06-15"
  s.description = "A simple client for Wheel Remote API".freeze
  s.email = ["cheokmeng.chan@laxino.com".freeze]
  s.files = [".dockerignore".freeze, ".gitignore".freeze, ".rspec".freeze, ".ruby-version".freeze, "Gemfile".freeze, "Gemfile.lock".freeze, "Guardfile".freeze, "Jenkinsfile".freeze, "Makefile".freeze, "Rakefile".freeze, "docker/development/Dockerfile".freeze, "docker/development/Dockerfile.cache".freeze, "docker/development/docker-compose.yml".freeze, "example/rng.rb".freeze, "example/weight_table.rb".freeze, "lib/wheel-api.rb".freeze, "lib/wheel-api/base.rb".freeze, "lib/wheel-api/client.rb".freeze, "lib/wheel-api/client/base.rb".freeze, "lib/wheel-api/client/http_exception_inspector.rb".freeze, "lib/wheel-api/core_ext/string.rb".freeze, "lib/wheel-api/error.rb".freeze, "lib/wheel-api/rng.rb".freeze, "lib/wheel-api/version.rb".freeze, "lib/wheel-api/weight_table.rb".freeze, "scripts/test.sh".freeze, "tasks/spec.rake".freeze, "wheel-api.gemspec".freeze]
  s.homepage = "http://home.rnd.laxino.com".freeze
  s.rubygems_version = "2.7.10".freeze
  s.summary = "API Client For Wheel".freeze

  s.installed_by_version = "2.7.10" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<bundler>.freeze, [">= 1.16"])
      s.add_runtime_dependency(%q<http>.freeze, [">= 3.0"])
      s.add_runtime_dependency(%q<multi_json>.freeze, ["= 1.14.1"])
      s.add_runtime_dependency(%q<hashie>.freeze, [">= 3.5"])
    else
      s.add_dependency(%q<bundler>.freeze, [">= 1.16"])
      s.add_dependency(%q<http>.freeze, [">= 3.0"])
      s.add_dependency(%q<multi_json>.freeze, ["= 1.14.1"])
      s.add_dependency(%q<hashie>.freeze, [">= 3.5"])
    end
  else
    s.add_dependency(%q<bundler>.freeze, [">= 1.16"])
    s.add_dependency(%q<http>.freeze, [">= 3.0"])
    s.add_dependency(%q<multi_json>.freeze, ["= 1.14.1"])
    s.add_dependency(%q<hashie>.freeze, [">= 3.5"])
  end
end
