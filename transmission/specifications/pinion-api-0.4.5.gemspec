# -*- encoding: utf-8 -*-
# stub: pinion-api 0.4.5 ruby lib

Gem::Specification.new do |s|
  s.name = "pinion-api".freeze
  s.version = "0.4.5"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Cheok Meng Chan".freeze]
  s.date = "2021-06-15"
  s.description = "A simple client for Pinion Remote API".freeze
  s.email = ["cheokmeng.chan@laxino.com".freeze]
  s.files = ["Gemfile".freeze, "Gemfile.lock".freeze, "Guardfile".freeze, "Jenkinsfile".freeze, "Makefile".freeze, "Rakefile".freeze, "docker/development/Dockerfile".freeze, "docker/development/Dockerfile.cache".freeze, "docker/development/docker-compose.yml".freeze, "example/game_configs.rb".freeze, "example/game_instance_example.rb".freeze, "lib/pinion-api.rb".freeze, "lib/pinion-api/client.rb".freeze, "lib/pinion-api/client/base.rb".freeze, "lib/pinion-api/client/http_exception_inspector.rb".freeze, "lib/pinion-api/core_ext/string.rb".freeze, "lib/pinion-api/error.rb".freeze, "lib/pinion-api/game.rb".freeze, "lib/pinion-api/game_config.rb".freeze, "lib/pinion-api/game_instance.rb".freeze, "lib/pinion-api/game_round.rb".freeze, "lib/pinion-api/version.rb".freeze, "pinion-api.gemspec".freeze, "scripts/stress_test/client.rb".freeze, "scripts/stress_test/runner.rb".freeze, "scripts/stress_test/stats.rb".freeze, "scripts/stress_test/worker.rb".freeze, "tasks/spec.rake".freeze]
  s.homepage = "http://home.rnd.laxino.com".freeze
  s.rubygems_version = "2.7.10".freeze
  s.summary = "API Client For Pinion".freeze

  s.installed_by_version = "2.7.10" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<bundler>.freeze, [">= 1.16"])
      s.add_runtime_dependency(%q<rake>.freeze, [">= 12.3.0"])
      s.add_runtime_dependency(%q<http>.freeze, [">= 4.0.0"])
      s.add_runtime_dependency(%q<multi_json>.freeze, ["= 1.14.1"])
      s.add_runtime_dependency(%q<hashie>.freeze, [">= 3.5.7"])
      s.add_runtime_dependency(%q<hashdiff>.freeze, [">= 1.0.0"])
    else
      s.add_dependency(%q<bundler>.freeze, [">= 1.16"])
      s.add_dependency(%q<rake>.freeze, [">= 12.3.0"])
      s.add_dependency(%q<http>.freeze, [">= 4.0.0"])
      s.add_dependency(%q<multi_json>.freeze, ["= 1.14.1"])
      s.add_dependency(%q<hashie>.freeze, [">= 3.5.7"])
      s.add_dependency(%q<hashdiff>.freeze, [">= 1.0.0"])
    end
  else
    s.add_dependency(%q<bundler>.freeze, [">= 1.16"])
    s.add_dependency(%q<rake>.freeze, [">= 12.3.0"])
    s.add_dependency(%q<http>.freeze, [">= 4.0.0"])
    s.add_dependency(%q<multi_json>.freeze, ["= 1.14.1"])
    s.add_dependency(%q<hashie>.freeze, [">= 3.5.7"])
    s.add_dependency(%q<hashdiff>.freeze, [">= 1.0.0"])
  end
end
