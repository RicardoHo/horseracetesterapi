# -*- encoding: utf-8 -*-
# stub: clutch 0.5.4 ruby lib

Gem::Specification.new do |s|
  s.name = "clutch".freeze
  s.version = "0.5.4"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Alpha Huang".freeze]
  s.date = "2021-06-15"
  s.description = "A ECS framework ruby implementation designed for game development.\nShipped with state control, sytem management, event management, etc..\nDefine your own component, system and event message parser; and setup state machine config, event-state-system mapping. Then you're able to go.\n".freeze
  s.email = ["alpha.huang@gamesourcecloud.com".freeze]
  s.files = [".gitattributes".freeze, ".gitignore".freeze, ".rspec".freeze, ".rubocop.yml".freeze, ".ruby-version".freeze, "CHANGELOG.md".freeze, "Gemfile".freeze, "Gemfile.lock".freeze, "Guardfile".freeze, "README.md".freeze, "Rakefile".freeze, "bitbucket-pipelines.yml".freeze, "clutch.gemspec".freeze, "config.reek".freeze, "lib/clutch.rb".freeze, "lib/clutch/component.rb".freeze, "lib/clutch/entity.rb".freeze, "lib/clutch/errors.rb".freeze, "lib/clutch/gamebox.rb".freeze, "lib/clutch/label_component.rb".freeze, "lib/clutch/manager.rb".freeze, "lib/clutch/manager/component_manager.rb".freeze, "lib/clutch/manager/ecs_manager.rb".freeze, "lib/clutch/manager/entity_manager.rb".freeze, "lib/clutch/manager/group_manager.rb".freeze, "lib/clutch/state_machine.rb".freeze, "lib/clutch/state_machine/adapter.rb".freeze, "lib/clutch/state_machine/gamebox_state_machine.rb".freeze, "lib/clutch/support.rb".freeze, "lib/clutch/support/attribute.rb".freeze, "lib/clutch/support/component_sign.rb".freeze, "lib/clutch/support/constants.rb".freeze, "lib/clutch/support/event.rb".freeze, "lib/clutch/support/game_definition.rb".freeze, "lib/clutch/support/types.rb".freeze, "lib/clutch/system.rb".freeze, "lib/clutch/system/base_system.rb".freeze, "lib/clutch/system/cleanup_system.rb".freeze, "lib/clutch/system/command_system.rb".freeze, "lib/clutch/system/entity_system.rb".freeze, "lib/clutch/system/initialize_system.rb".freeze, "lib/clutch/system/reactive_system.rb".freeze, "lib/clutch/system/recurrent_system.rb".freeze, "lib/clutch/system/system_result.rb".freeze, "lib/clutch/system_admin.rb".freeze, "lib/clutch/version.rb".freeze, "lib/ext/array.rb".freeze, "lib/ext/hash.rb".freeze, "lib/ext/string.rb".freeze]
  s.homepage = "https://bitbucket.org/gamesource/clutch".freeze
  s.required_ruby_version = Gem::Requirement.new("~> 2.5".freeze)
  s.rubygems_version = "2.7.10".freeze
  s.summary = "A ECS framework ruby implementation".freeze

  s.installed_by_version = "2.7.10" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<rake>.freeze, ["~> 12.3"])
      s.add_runtime_dependency(%q<multi_json>.freeze, ["= 1.14.1"])
      s.add_runtime_dependency(%q<dry-struct>.freeze, ["~> 1.0"])
      s.add_runtime_dependency(%q<aasm>.freeze, ["~> 5.0"])
      s.add_runtime_dependency(%q<representable>.freeze, ["~> 3.0"])
    else
      s.add_dependency(%q<rake>.freeze, ["~> 12.3"])
      s.add_dependency(%q<multi_json>.freeze, ["= 1.14.1"])
      s.add_dependency(%q<dry-struct>.freeze, ["~> 1.0"])
      s.add_dependency(%q<aasm>.freeze, ["~> 5.0"])
      s.add_dependency(%q<representable>.freeze, ["~> 3.0"])
    end
  else
    s.add_dependency(%q<rake>.freeze, ["~> 12.3"])
    s.add_dependency(%q<multi_json>.freeze, ["= 1.14.1"])
    s.add_dependency(%q<dry-struct>.freeze, ["~> 1.0"])
    s.add_dependency(%q<aasm>.freeze, ["~> 5.0"])
    s.add_dependency(%q<representable>.freeze, ["~> 3.0"])
  end
end
