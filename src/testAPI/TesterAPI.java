package testAPI;

import java.io.IOException;
import java.net.URISyntaxException;

public interface TesterAPI {

    //玩游戏的每一关
    //入参xml: 是包含赌注，玩家选择的一个xml字符串，具体格式后面进行详细说明
    //入参seed: 玩一步游戏时需要的随机数种子，是一个16进制数字符串，具体格式后面进行详细说明
    //返回continuePlay: 是否中奖，即是否可以进入下一关，若为true，则中奖，可以进入下一关，否则未中奖，游戏结束
    public boolean play(String xml, String seed) throws IOException, URISyntaxException;
    //当玩了一关游戏，即执行完一次play方法，可获得该关的赌注，单位为分
    public int getStepStake();
    //当玩了一关游戏，即执行完一次play方法，可获得该关的中奖金额，单位为分
    public int getStepWon();
    //当玩了一关游戏，即执行完一次play方法，可获得该关的玩家选择，具体格式后面进行详细说明
    public String getStepSelection();
    //当玩了一关游戏，即执行完一次play方法，可获得该关的游戏结果，具体格式后面进行详细说明
    public String getStepResult();
    //当玩了一关游戏，即执行完一次play方法，可获得下一关游戏，只有“海南珍宝”游戏会用到
//	    public VLTNextGame getNextGame();
}
