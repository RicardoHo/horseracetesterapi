package testAPI;

import java.io.*;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.CodeSource;
import java.util.ArrayList;
import java.util.stream.Collectors;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.jruby.Ruby;
import org.jruby.RubyModule;
import org.jruby.javasupport.JavaUtil;
import org.jruby.runtime.Helpers;
import org.jruby.runtime.builtin.IRubyObject;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;



public class testerMethods implements TesterAPI{

    private static String returnJson ="";
    private static ArrayList<Integer> stake = new ArrayList<Integer>();
    private static ArrayList<String> selection = new ArrayList<String>();
    private static String totalPayOut = "";
    private static String totalBet = "";
    private static String stepSelection = "";
    private static String stepResult = "";
    private static Ruby __ruby__ ;
    private static RubyModule rclass;
    private static IRubyObject robject;



    public boolean play(String xml, String seed) throws IOException, URISyntaxException {
        //call two helper methods

        create();
        analysisNprocess(xml,seed);
        exDataFromJson(returnJson);
        stake.clear();
        selection.clear();
        return false;
    }

    private static void exDataFromJson(String JsonData) {
        JSONObject jsonObject = new JSONObject(JsonData);
        System.out.println("check"+jsonObject);

        //retrieve totalPayOut data
        totalPayOut = jsonObject.getJSONObject("data").getJSONObject("table").get("totalPayoutAmt").toString();
        System.out.println(totalPayOut);

        //retrieve totalBet data
        totalBet = jsonObject.getJSONObject("data").getJSONObject("table").get("totalBetAmt").toString();
        System.out.println(totalBet);

        //retrieve stepSelection data
        JSONArray selectionArr = jsonObject.getJSONObject("data").getJSONObject("table").getJSONObject("lastBet").getJSONArray("dualForecast");
        stepSelection = selectionHelper(selectionArr);

        //retrieve stepResult data
        JSONArray resultArr = jsonObject.getJSONObject("data").getJSONObject("table").getJSONArray("winReference");
        stepResult = resultHelper(resultArr);

    }

    private static String resultHelper(JSONArray resultData) {
        String result = "";
        int n = resultData.length();
        StringBuilder sb = new StringBuilder();

        for(int i=0; i<n;i++) {
            sb.append(resultData.getString(i)+", ");
        }

        sb.deleteCharAt(sb.length()-2);
        result = sb.toString();
        System.out.println(result);

        return result;

    }

    private static String selectionHelper(JSONArray selectionData) {
        String result = "";
        int n = selectionData.length();
        StringBuilder sb = new StringBuilder();
        for(int i=0; i<n;i++) {
            JSONObject arrData = selectionData.getJSONObject(i);
            sb.append(arrData.getString("name")+", ");
        }

        sb.deleteCharAt(sb.length()-2);
        result = sb.toString();
        System.out.println(result);

        return result;

    }

    private static void analysisNprocess(String xml, String seed) {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = null;
        String jsonString = "";
        try {
            db = dbf.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));
            try {
                Document doc = db.parse(is);

                NodeList nl = doc.getDocumentElement().getElementsByTagName("bet");

                for (int itr = 0; itr < nl.getLength(); itr++){
                    Node node = nl.item(itr);

                    if (node.getNodeType() == Node.ELEMENT_NODE){
                        Element eElement = (Element) node;
                        String mySelection = eElement.getElementsByTagName("selection").item(0).getTextContent();
                        selection.add(itr, mySelection);
                        int myStake = Integer.parseInt(eElement.getElementsByTagName("stake").item(0).getTextContent());
                        stake.add(itr, myStake);
                    }
                }
                System.out.println(selection);
                System.out.println(stake);
                System.out.println(xmlBuilder(stake,selection));

                //create new XML
                String temp = xmlBuilder(stake,selection);

                //conver2Json
                JSONObject json1 = XML.toJSONObject(temp);
                jsonString = json1.toString(4);
                System.out.println(jsonString);
                returnJson = helper(jsonString, seed);


            } catch (SAXException e) {
                // handle SAXException
            } catch (IOException e) {
                // handle IOException
            } catch (Exception e)   {
                e.printStackTrace();
            }
        } catch (ParserConfigurationException e1) {
            // handle ParserConfigurationException
        }

    }

    private void create() throws IOException, URISyntaxException {

        String findPath = testAPI.TesterAPI.class
                .getProtectionDomain()
                .getCodeSource()
                .getLocation()
                .toURI()
                .getPath();
                
        StringBuffer sb = new StringBuffer(findPath);
        if(sb.charAt(sb.length()-1) == '/'){sb.deleteCharAt(sb.length()-1);findPath = sb.toString();}        
        System.out.println("JAR Path : " + findPath);

 //       "require './transmission/transmission/bin/tester_runner.rb'"
 //       String jarPath = "C:/Users/ricardo.ho/eclipse-workspace/horseRacing/libs/testerAPIv1Idea.jar";
        URL url3 = new URL("jar:file:" + findPath + "!/transmission/bin/tester_runner.rb");
        System.out.println("8.check = " + url3.getPath());
        InputStream is3 = url3.openStream();
        byte[] bytes3 = new byte[4096];
        while (is3.read(bytes3) != -1) {
            System.out.println(new String(bytes3, "UTF-8"));
            System.out.println("-------");
        }
        __ruby__ = Ruby.getGlobalRuntime();
        String bootstrap = "dir=" + "'jar:file:/C:/Users/ricardo.ho/eclipse-workspace/horseRacing/libs/testerAPIv1Idea.jar!/transmission/bin'" + new String(bytes3, "UTF-8");
        System.out.println(bootstrap);
        __ruby__.evalScriptlet(bootstrap);
        // retrieve namespaced class using getClassFromPath, instantiate object, call method
        rclass = __ruby__.getClassFromPath("Transmission::Runner");
        robject = Helpers.invoke(__ruby__.getCurrentContext(), rclass, "new");
//        }
//        return new testerMethods();
    }

    private static String helper(String input1, String input2) {
        String resultData = "";
        IRubyObject param = JavaUtil.convertJavaToRuby(__ruby__,input1);
        IRubyObject param2 = JavaUtil.convertJavaToRuby(__ruby__,input2);
        System.out.println(param);
        IRubyObject robject = Helpers.invoke(__ruby__.getCurrentContext(), rclass, "new");
        Helpers.invoke(__ruby__.getCurrentContext(), robject, "play", param,param2);
        System.out.println(Helpers.invoke(__ruby__.getCurrentContext(), robject, "str"));
        resultData = Helpers.invoke(__ruby__.getCurrentContext(), robject, "str").asJavaString();

        try {
//	        JSONObject jsonObject = new JSONObject(resultData);
//          xml = XML.toString(jsonObject);
            System.out.println(resultData);

        }catch (JSONException err){
            System.out.println(err.toString());
        }
        return resultData;

    }

    private static String xmlBuilder(ArrayList<Integer> stake, ArrayList<String> selection) {
        //<roundInfo><id>VT0N070ZXC</id></roundInfo>
        String header = "<data>";
        String tail = "<denom>1</denom><playMode>dual_forecast</playMode></data><dataType>multipleBet</dataType><action>multipleBet</action>";
        StringBuilder sb = new StringBuilder();
        sb.append(header);
        for(int i = 0; i < stake.size(); i++) {
            sb.append("<betOptions><betAmount><credit>");
            sb.append(stake.get(i));
            sb.append("</credit></betAmount><name>");
            sb.append(decideMode(selection.get(i)));
            sb.append("</name></betOptions>");
        }
        sb.append(tail);
        return sb.toString();
    }

    private static String decideMode(String selection) {

        String result = "";
        if(selection.length() == 1) {
            result = "win_" +  selection;
        } else if (selection.contains("c")) {
            result = "place_" +  selection.substring(1);
        } else {

            result = "dual_forecast_" + selection;
        }
        return result;
    }

    @Override
    public int getStepStake() {
        int myGetStepStake = Integer.parseInt(totalBet);
        return myGetStepStake;
    }

    @Override
    public int getStepWon() {
        int myGetStepWon = Integer.parseInt(totalPayOut);
        return myGetStepWon;
    }

    public String getStepSelection() {
        return stepSelection;
    }

    @Override
    public String getStepResult() {
        return stepResult;
    }

}


