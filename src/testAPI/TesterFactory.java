package testAPI;

public class TesterFactory {
    public static TesterAPI create(int gameId) throws GameNotFoundException {

        if(gameId != 913142505) {
            throw new GameNotFoundException("Game Not Found.");
        }else {
            return  new testerMethods();
        }
    }
}
