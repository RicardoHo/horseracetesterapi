package testAPI;

import java.io.IOException;
import java.net.URISyntaxException;

class testMain {

    public static void main(final String[] args) throws GameNotFoundException, IOException, URISyntaxException {


        String mySeed = "hello";
        String xml = "<step>\r\n"
                + "    <index>1</index>\r\n"
                + "    <bet>\r\n"
                + "        <index>0</index>\r\n"
                + "        <selection>31</selection>\r\n"
                + "        <stake>50</stake>\r\n"
                + "    </bet>\r\n"
                + "    <bet>\r\n"
                + "        <index>0</index>\r\n"
                + "        <selection>c6</selection>\r\n"
                + "        <stake>50</stake>\r\n"
                + "    </bet>\r\n"
                + "    <bet>\r\n"
                + "        <index>0</index>\r\n"
                + "        <selection>1</selection>\r\n"
                + "        <stake>50</stake>\r\n"
                + "    </bet>\r\n"
                + "    <bet>\r\n"
                + "        <index>0</index>\r\n"
                + "        <selection>12</selection>\r\n"
                + "        <stake>50</stake>\r\n"
                + "    </bet>\r\n"
                + "    <bet>\r\n"
                + "        <index>0</index>\r\n"
                + "        <selection>4</selection>\r\n"
                + "        <stake>50</stake>\r\n"
                + "    </bet>\r\n"
                + "    <bet>\r\n"
                + "        <index>0</index>\r\n"
                + "        <selection>7</selection>\r\n"
                + "        <stake>50</stake>\r\n"
                + "    </bet>\r\n"
                + "    <bet>\r\n"
                + "        <index>0</index>\r\n"
                + "        <selection>23</selection>\r\n"
                + "        <stake>50</stake>\r\n"
                + "    </bet>\r\n"
                + "    <bet>\r\n"
                + "        <index>0</index>\r\n"
                + "        <selection>25</selection>\r\n"
                + "        <stake>50</stake>\r\n"
                + "    </bet>\r\n"
                + "</step>\r\n"
                + "";
        TesterAPI tm = TesterFactory.create(913142505);


        System.out.println(tm.play(xml, mySeed));
        System.out.println(tm.getStepResult());
        System.out.println(tm.getStepSelection());
        System.out.println(tm.getStepStake());
        System.out.println(tm.getStepWon());

    }

}